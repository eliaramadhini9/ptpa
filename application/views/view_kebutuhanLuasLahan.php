<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">DATA KEBUTUHAN LUAS LAHAN TPA SAMPAH</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <div class="mailbox-controls">
          <button type="button" class="btn btn-default btn-sm btn-refresh-card" onclick="window.location.reload();" data-toggle="tooltip" data-container="body" title="Reload Halaman"><i class="fa fa-sync-alt"></i></button>
          <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-data">Masukan Data</button>
        </div>
        <table id="abc" class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tahun</th>
              <th>Sampah Masuk (m<sup>3</sup>/hari)</th>
              <th>Densitas Sampah Awal (kg/m<sup>3</sup>)</th>
              <th>Sampah di TPA (kg/hari)</th>
              <th>Densitas Sampah Kompaksi di TPA (kg/m<sup>3</sup>)</th>
              <th>Sampah TPA setelah Kompaksi (m<sup>3</sup>/hari)</th>
              <th>Sampah TPA setelah Kompaksi (m<sup>3</sup>/tahun)</th>
              <th>Akumulasi Sampah TPA setelah Kompaksi (m<sup>3</sup>/tahun)</th>
              <th>Ketebalan Lift (2,44 - 3,66)m</th>
              <th>Tebal Lapisan Penutup/hr (m)</th>
              <th>Final Cover (m)</th>
              <th>Total Lapisan (Lift + Lapisan Penutup)m</th>
              <th>Jumlah Lift Direncanakan</th>
              <th>Luas Tiap Sel (Ditentukan)(m<sup>2</sup>)</th>
              <th>Masa Pakai Landfill (tahun)</th>
              <th>Jumlah Sel yang Dibutuhkan</th>
              <th>Luas Sub Sel (Penggurugan/hari)(m<sup>2</sup>)</th>
              <th>Luas Area Penimbunan Sampah (m<sup>2</sup>)</th>
              <th>Luas Area/tahun (ha)</th>
              <th>Pembagian Zona (ha)</th>
              <th>Pertambahan Luas Lahan (m<sup>2</sup>)</th>
              <th>Luas Landfill (ha)</th>
              <th>Daily Cover/sel (m<sup>3</sup>)</th>
              <th>Total Daily Cover (m<sup>3</sup>)</th>
              <th>Daily Cover (m<sup>3</sup>)/Tahun</th>
              <th>Akumulasi Daily Cover (m<sup>3</sup>)</th>
              <th>Luas Lahan TPA (ha) </th>
              </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            if ($dataKebutuhanLuasLahan->num_rows()>0) {
              foreach ($dataKebutuhanLuasLahan->result_array() as $item) :
            ?>
            <tr>
              <td><?php echo $no;?></td>
              <td><?php echo $item['tahun'];?></td>
              <td><?php echo $item['sampah_masuk'];?></td>
              <td><?php echo $item['dens_sampah_awal'];?></td>
              <td><?php echo $item['sampah_tpa'];?></td>
              <td><?php echo $item['dens_sampah_kompaksi'];?></td>
              <td><?php echo $item['setelah_kompaksi_hari'];?></td>
              <td><?php echo $item['setelah_kompaksi_tahun'];?></td>
              <td><?php echo $item['setelah_kompaksi_tahun'];?></td>
              <td><?php echo $item['ketebalan_lift'];?></td>
              <td><?php echo $item['tebal_lapisan'];?></td>
              <td><?php echo $item['final_cover'];?></td>
              <td><?php echo $item['tot_lapisan'];?></td>
              <td><?php echo $item['jumlah_lift'];?></td>
              <td><?php echo $item['luas_tiapsel'];?></td>
              <td><?php echo $item['masa_landfill'];?></td>
              <td><?php echo $item['sel_dibutuhkan'];?></td>
              <td><?php echo $item['luas_subSel'];?></td>
              <td><?php echo $item['luas_area_penimbunan'];?></td>
              <td><?php echo $item['luas_area_tahun'];?></td>
              <td><?php echo $item['luas_area_tahun'];?></td>
              <td><?php echo $item['luas_area_penimbunan'];?></td>
              <td></td>
              <td><?php echo $item['dailyCover_sel'];?></td>
              <td><?php echo $item['dailyCover_total'];?></td>
              <td><?php echo $item['dailyCover_tahun'];?></td>
              <td><?php echo $item['dailyCover_akumulasi'];?></td>
              <td ><?php echo $item['luas_lahan_tpa'];?></td>
            </tr>
              <?php
                $no++;
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="28" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php foreach ($kebutuhanLuasLahan->result_array() as $item) : ?>
<div class="modal fade" id="modal-data">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
        Data Perhitungan Kebutuhan Luas Lahan
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun_domestik" value="<?php echo $item['tahun'];?>" readonly autofocus>
            <div class="form-group">
              <label class="control-label">Jumlah Densitas Sampah Awal</label>
              <div>
                <input type="text" class="form-control" name="dens_sampah_awal" required autofocus value="<?php echo $item['dens_sampah_awal']; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Densitas Sampah Kompaksi di TPA</label>
              <div>
                <input type="text" class="form-control" name="dens_sampah_kompaksi" value="<?php echo $item['dens_sampah_kompaksi']; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Ketebalan Lift</label>
              <div>
                <input type="text" class="form-control" name="ketebalan_lift" value="<?php echo $item['ketebalan_lift']; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Tebal Lapisan Penutup / hari</label>
              <div>
                <input type="text" class="form-control" name="tebal_lapisan" value="<?php echo $item['tebal_lapisan']; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Lift Direncanakan</label>
              <div>
                <input type="text" class="form-control" name="jumlah_lift" value="<?php echo $item['jumlah_lift']; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Luas Tiap Sel</label>
              <div>
                <input type="text" class="form-control" name="luas_tiapsel" value="<?php echo $item['luas_tiapsel']; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Masa Pakai Landfill</label>
              <div>
                <input type="text" class="form-control" name="masa_landfill" value="<?php echo $item['masa_landfill']; ?>" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="tambahKebutuhanLuasLahan" value="tambahKebutuhanLuasLahan">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach; ?>

