<div class="col-md-12">
  <h3 class="text-heading title_color">SELAMAT DATANG..</h3>
  <div class="card">
    <!-- /.card-header -->
    <div class="card-body">
      <div class="row">
          <p style="text-align: justify;">
            <strong>Sistem Perencanaan Luas Lahan TPA (Tempat Akhir Pemrosesan) Sampah</strong> adalah sistem yang digunakan untuk membantu dalam menentukan luas lahan TPA untuk 15, 30 atau 50 tahun kedepan. Selain itu sistem ini juga dapat membantu dalam memproyeksikan data penduduk, data PDRB (Produk Domestik R Bruto), data timbulan sampah yang dihasilkan baik domestik maupun non-domestik pada suatu kabupaten selama 15 atau 30 tahun. 
          </p>

          <p style="text-align: justify;">
            Luas Lahan TPA yang dibutuhkan oleh suatu kabupaten tergantung dari data-data kependudukan setempat, seperti halnya data pertumbuhan penduduk, data pertumbuhan PDRB, data timbulan sampah yang dihasilkan tiap tahunnya. 
          </p>
        </div>
    </div>
  </div>
</div>
</div>