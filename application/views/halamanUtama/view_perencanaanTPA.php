<div class="col-md-12">
  <div class="card">
    <!-- /.card-header -->
    <div class="card-body">
      <div class="row">
          <p><strong>PERENCANAAN TEKNIS TPA</strong></p><br>
          <div class="single-defination">
            <p class="mb-20">I. Penentuan Lokasi TPA</p>
            <p style="text-align: justify;">
              Penentuan lokasi TPA suatu kabupaten didasarkan pada kriteria tertentu. Proses pemilihan calon lokasi TPA tersebut dilakukan melalui beberapa tahapan. Tahapan-tahapan tersebut dibagi menjadi 3 tahap yaitu:
            </p>
            <ol>
              <li>Tahapan Pertama</li>
              <p style="text-align: justify;">Tahapan pertama adalah kriteria regional yang didasarkan pada tata guna lahan. Lokasi yang terpilih adalah daerah dengan peruntukan sebagai daerah dengan lahan yang tak terpakai (tegalan) atau juga bisa lahan keras/kering non produktif.</p>
              <li>Tahapan Kedua</li>
              <p style="text-align: justify;">Tahapan kedua adalah kriteria penyisih, yaitu penilaian menurut SNI 19-3984-1995 digunakan dalam pemilihan calon lokasi TPA karena penilaian ini bersifat umum dan digunakan untuk memilih TPA sampah di kota-kota di Indonesia. Penilaian ini memiliki banyak kriteria mulai dari kondisi umum lokasi, kondisi fisik, biologis, sosial masyarakat, dan lainlain. Penilaian kelayakan ini menilai tinggi parameter yang mempunyai kelebihan sebagai potensi yang dapat dimanfaatkan lebih lanjut dan menilai lebih rendah kondisi parameter sebagai sebuah kendala yang memerlukan rencana kelola agar dampak dari kendala calon lokasi tersebut tidak mengurangi kualitas lingkungan nantinya. Berikut gambar dari aspek penilaian kriteria penyisih :</p>
              <div class="row gallery-item">
                <div class="col-md-4">
                  <a href="assets/img/kriteriaPenyisih.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/kriteriaPenyisih.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Kriteria Penyisih a</p>
                </div>
                <div class="col-md-4">
                 <a href="assets/img/kriteriaPenyisih2.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/kriteriaPenyisih2.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Kriteria Penyisih b</p>
                </div>
                <div class="col-md-4">
                  <a href="assets/img/kriteriaPenyisih3.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/kriteriaPenyisih3.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Kriteria Penyisih c</p>
                </div>
              </div>
              <li>Tahapan Ketiga</li>
              <p style="text-align: justify;">Tahapan ketiga adalah kriteria penetapan yaitu dalam hal ini instansi yang berwenang telah menyetujui dan menetapkan lokasi terpilih berdasarkan kebijakan dan ketentuan yang berlaku.</p>
            </ol>
            <p class="mb-20">II. Perencanaan Dasar</p>
            <ol>
              <li>Proyeksi Penduduk</li>
              <ol>
                <li>Jumlah penduduk memberikan pengaruh terhadap jumlah sampah yang dihasilkan suatu daerah. Oleh karena itu, jumlah penduduk perlu di prediksi sehingga kita mengetahui jumlah sampah yang akan diolah selama tahun perencanaan.  Adapun tiga metode yang digunakan untuk menentukan proyeksi penduduk yaitu :</li>
                <ul class="list-unstyled">
                  <ul>
                    <li>Metode Aritmatik</li>
                      <div class="col-md-6">
                        <a href="assets/img/aritmatik.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/aritmatik.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Rumus Metode Aritmatik a</p>
                      </div>
                      <p>Apabila rumus di atas diubah dalam bentuk regresi, menjadi :</p>
                      <div class="col-md-6">
                       <a href="assets/img/aritmatik1.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/aritmatik1.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Rumus Metode Aritmatik b</p>
                      </div>
                    <li>Metode Geometrik</li>
                    <div class="col-md-6">
                        <a href="assets/img/geometri.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/geometri.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Rumus Metode Geometri a</p>
                      </div>
                      <p>Apabila rumus di atas diubah dalam bentuk regresi, menjadi :</p>
                      <div class="col-md-6">
                       <a href="assets/img/geometri1.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/geometri1.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Rumus Metode Geometri b</p>
                      </div>
                    <li>Metode Eksponensial</li>
                    <div class="col-md-6">
                        <a href="assets/img/ls.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/ls.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Rumus Metode Eksponsial a</p>
                      </div>
                      <p>Apabila rumus di atas diubah dalam bentuk regresi, menjadi :</p>
                      <div class="col-md-6">
                       <a href="assets/img/ls1.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(assets/img/ls1.jpg);"></div></a><p style="text-align: center; font-size: small;"><b>Gambar</b> Rumus Metode Eksponsial b</p>
                      </div>
                  </ul>
                </ul>
                <p>Penentuan metode proyeksi dapat dilakukan dengan pengujian angka korelasi, dimana metode yang dipilih adalah metode yang memiliki standar deviasi paling kecil.</p>
              </ol>
              <li>Proyeksi Produk Domestik Regional Bruto (PDRB) </li>
              <ol>
                <li></li>
                <li style="text-align: justify;">1 PDRB memberikan pengaruh terhadap jumlah sampah yang dihasilkan suatu daerah. Oleh karena itu, PDRB perlu di prediksi sehingga kita mengetahui jumlah sampah yang akan diolah selama tahun perencanaan. Proyeksi PDRB dapat menggunakan metode yang sama dengan proyeksi penduduk yakni metode aritmatik, geometri dan least square. Metode terpilih adalah metode yang memiliki nilai standar deviasi yang paling kecil.</li>
              </ol>
              <li>Timbulan Sampah Wilayah Perencanaan</li>
              <p style="text-align: justify;">Untuk menentukan kapasitas lahan TPA yang diperlukan perlu melakukan perhitungan timbulan sampah yang dihasilkan wilayah perencanaan selama tahun perencanaan. Timbulan sampah dibagi menjadi 2, timbulan sampah domestik dan timbulan sampah non domestik</p>
              <ol>
                <li style="text-align: justify;">Timbulan Sampah Domestik</li>
                <p style="text-align: justify;">Untuk memproyeksikan timbulan sampah, perlu diketahui timbulan sampah tahun-tahun sebelumnya. Selain itu tingkat pertumbuhan PDRB juga ikut berpengaruh terhadap pertumbuhan timbulan sampah. Karena salah satu faktor yang mempengaruhi besarnya timbulan sampah adalah faktor ekonomi. Faktor ekonomi suatu daerah dapat dilihat dengan nilai PDRB daerah tersebut. Timbulan/Kapita untuk Kota Kecil berdasarkan SNI 19-3983-1995 adalah 2,52.75 L/orang/hari, sehingga diambil timbulan/kapita sebesar 2.5 L/orang/hari.</p>
                <p style="text-align: justify;">Dengan Perhitungan yang sama, dapat di cari timbulan sampah berdasarkan PDRB. Perbandingan timbulan unit, timbulan sampah berdasarkan jumlah penduduk dan berdasarkan PDRB yang diproyeksikan dengan persen pelayanan, lalu dipilih metode yang menghasilkan unit timbulan sampah paling besar.</p>
                <li style="text-align: justify;">Timbulan Sampah Non-Domestik</li>
                <p style="text-align: justify;">Selain timbulan sampah domestik, timbulan sampah non domestik juga perlu di proyeksikan. Proyeksi timbulan sampah non domestik di dapat dari proyeksi fasilitas yang dimiliki wilayah perencanaan.</p>
              </ol>
              <li>Analisis Komposisi Sampah</li>

          </div>
    </div>
  </div>
</div>
</div>