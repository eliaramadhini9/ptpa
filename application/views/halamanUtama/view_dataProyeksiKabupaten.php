<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">DATA JUMLAH PENDUDUK DAN NILAI PDRB</h3>
      <div class="card-tools">
        <ul class="nav nav-pills ml-auto">
          <li class="nav-item">
            <a class="nav-link active" href="#jumlahPenduduk" data-toggle="tab">Jumlah Penduduk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#jumlahPdrb" data-toggle="tab">Jumlah PDRB</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="card-body">
      <div class="tab-content p-0">
        <div class="chart tab-pane active" id="jumlahPenduduk">
          <div class="card-body">
            <p style="text-align: center;">Data Jumlah Penduduk</p>
            <table id="abcd" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th rowspan="2" width="10%" style="text-align: center;">Tahun</th>
                  <th rowspan="2" width="30%" style="text-align: center;">Jumlah Penduduk</th>
                  <th colspan="2" width="40%" style="text-align: center;">Pertumbuhan Penduduk</th>
                  </tr>
                <tr>
                  <th width="50px" style="text-align: center;">Jiwa</th>
                  <th width="50px" style="text-align: center;">Persen (%)</th>
                </tr>
              </thead>
              <tbody>
              <?php if ($dataProyeksiPenduduk->num_rows()>0) {
                foreach ($dataProyeksiPenduduk->result_array() as $item) { 
                $jiwa = $item['jiwa'];
                $persen = $item['persen'];

                if ($jiwa == null || $persen == null) {
                  $jiwa = '-';
                  $persen = '-';
                }else{
                  $jiwa = number_format($jiwa,2);
                }
                ?>
                <tr>
                  <td align="center" data-toggle="tooltip" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
                  <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['jumlah_penduduk'],2);?> jiwa"><?php echo number_format($item['jumlah_penduduk'],2);?></td>
                  <td align="center" data-toggle="tooltip" data-container="body" title="<?php echo $jiwa;?> jiwa"><?php echo $jiwa;?></td>
                  <td align="center" data-toggle="tooltip" data-container="body" title="<?php echo round($persen,3);?> %"><?php echo round($persen,3);?></td>
                </tr>
              <?php }} else { ?>
                <tr>
                  <td colspan="7" style="text-align: center;">No Result Data</td>
                </tr>
              <?php } ?>
              </tbody>
              <?php foreach ($dataProyeksiPendudukTotal->result_array() as $item) { ?>
              <tfoot>
                <tr>
                  <th style="text-align: center">JUMLAH</th>
                  <th align="center" data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['jumlah_penduduk_total'],2);?> jiwa"><?php echo number_format($item['jumlah_penduduk_total'],2);?></th>
                  <th style="text-align: center" data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['jiwa_total'],2);?> jiwa"><?php echo number_format($item['jiwa_total'],2);?></th>
                  <th style="text-align: center" data-toggle="tooltip" data-container="body" title="<?php echo $item['persen_total'];?> %"><?php echo $item['persen_total'];?></th>
                </tr>
              </tfoot>
              <?php } ?>
            </table>
          </div>
        </div>
        <div class="chart tab-pane" id="jumlahPdrb">
          <div class="card-body">
            <p style="text-align: center;">Data Nilai PDRB</p>
            <table id="abcd" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th rowspan="2" width="10%" style="text-align: center;">Tahun</th>
                  <th rowspan="2" width="30%" style="text-align: center;">Besaran (dalam Rp)</th>
                  <th colspan="2" width="40%" style="text-align: center;">Pertumbuhan PDRB</th>
                  </tr>
                <tr>
                  <th width="50px" style="text-align: center;">Besaran (dalam Rp)</th>
                  <th width="50px" style="text-align: center;">Persen (%)</th>
                </tr>
              </thead>
              <tbody>
              <?php if ($dataProyeksiPdrb->num_rows()>0) {
                foreach ($dataProyeksiPdrb->result_array() as $item) { 
                $besaran = $item['besaran'];
                $persen = $item['persen'];

                if ($besaran == null || $persen == null) {
                  $besaran = '-';
                  $persen = '-';
                }else{
                  $besaran = number_format($besaran,2);
                }
                ?>
                <tr>
                  <td align="center" data-toggle="tooltip" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
                  <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['besaran'],2);?> besaran"><?php echo number_format($item['besaran'],2);?></td>
                  <td align="center" data-toggle="tooltip" data-container="body" title="<?php echo $besaran;?> besaran"><?php echo $besaran;?></td>
                  <td align="center" data-toggle="tooltip" data-container="body" title="<?php echo round($persen,3);?> %"><?php echo round($persen,3);?></td>
                </tr>
              <?php }} else { ?>
                <tr>
                  <td colspan="7" style="text-align: center;">No Result Data</td>
                </tr>
              <?php } ?>
              </tbody>
              <?php foreach ($dataProyeksiPendudukTotal->result_array() as $item) { ?>
              <tfoot>
                <tr>
                  <th style="text-align: center">JUMLAH</th>
                  <th align="center" data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['jumlah_penduduk_total'],2);?> jiwa"><?php echo number_format($item['jumlah_penduduk_total'],2);?></th>
                  <th style="text-align: center" data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['jiwa_total'],2);?> jiwa"><?php echo number_format($item['jiwa_total'],2);?></th>
                  <th style="text-align: center" data-toggle="tooltip" data-container="body" title="<?php echo $item['persen_total'];?> %"><?php echo $item['persen_total'];?></th>
                </tr>
              </tfoot>
              <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h3 class="card-title"><i class="far fa-chart-bar  mr-1"></i> Grafik Pertumbuhan Penduduk dan PDRB</h3>
      <div class="card-tools">
        <ul class="nav nav-pills ml-auto">
          <li class="nav-item">
            <a class="nav-link active" href="#pendudukGeometri" data-toggle="tab">Pertumbuhan Penduduk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#pdrbGeometri" data-toggle="tab">Pertumbuhan PDRB</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="card-body">
      <div class="tab-content p-0">
        <div class="chart tab-pane active" id="pendudukGeometri">
          <div class="card-body">
            <?php foreach ($jumlahdatapp->result_array() as $a) {
              $b = $a['jumlah_data']+1; 
              foreach ($jumlahdataproyeksi_pp->result_array() as $item) {
                if ($item['jumlah_data_proyeksi'] > $b) { ?>
            <p style="text-align: center;">Proyeksi Penduduk Selama  <?php echo $item['jumlah_data_proyeksi']; ?> Tahun</p>
            <div class="chart">
              <canvas id="chartProyeksiPendudukGeometri" style="height:250px; min-height:250px"></canvas>
            </div>
            <?php } else{?>
            <div class="chart">
              <p style="text-align: center;">Data belum diproyeksikan</p>
            </div>
            <?php }}} ?>
          </div>
        </div>
        <div class="chart tab-pane" id="pdrbGeometri">
          <div class="card-body">
          <?php foreach ($jumlahdatapdrb->result_array() as $a) {
            $b = $a['jumlah_data']+1;
            foreach ($jumlahdataproyeksi_pdrb->result_array() as $item) { 
              if ($item['jumlah_data_proyeksi'] > $b) { ?>
            <p style="text-align: center;">Proyeksi PDRB Selama  <?php echo $item['jumlah_data_proyeksi']; ?> Tahun</p>
            <div class="chart">
              <canvas id="chartProyeksiPdrbGeometri" style="height:250px; min-height:250px"></canvas>
            </div>
            <?php } else{?>
            <div class="chart">
              <p style="text-align: center;">Data belum diproyeksikan</p>
            </div>
            <?php }}} ?>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-sm-6 col-6">
          <div class="description-block border-right">
          <?php foreach ($pertumbuhanproyeksiPendudukGeometri->result_array() as $pp) { ?>
            <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> <?php echo round($pp['pertumbuhan'],2);  ?></span>
            <h5 class="description-header">RATA-RATA PERTUMBUHAN PENDUDUK TIAP TAHUN</h5>
          <?php } ?>
          </div>
        </div>
        <div class="col-sm-6 col-6">
          <div class="description-block border-right">
          <?php foreach ($pertumbuhanproyeksiPdrbGeometri->result_array() as $pdrb) { ?>
            <span class="description-percentage text-warning"><i class="fas fa-caret-up"></i> <?php echo round($pdrb['pertumbuhan'],2);  ?></span>
            <h5 class="description-header">RATA-RATA PERTUMBUHAN PDRB TIAP TAHUN</h5>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h3 class="card-title"><i class="far fa-chart-bar  mr-1"></i> Grafik Timbulan Sampah Domestik dan Non-Domestik</h3>
    </div>
    <div class="card-body">
      <div class="chart">
        <canvas id="chartTimbulan" style="height:250px; min-height:250px"></canvas>
      </div>
    </div>
  </div>

  <br><br>
  <div class="dropdown-divider"></div>
  

</div>
