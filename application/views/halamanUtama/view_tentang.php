<div class="col-md-12">
  <h3 class="text-heading title_color">TENTANG</h3>
  <div class="card">
    <!-- /.card-header -->
    <div class="card-body">
      <div class="row">
          <p style="text-align: justify;">
            <strong>Sistem Perencanaan Luas Lahan TPA (Tempat Akhir Pemrosesan) Sampah</strong> dibuat sebagai tugas akhir mahasiswa Teknik Komputer. Sistem ini dibuat untuk membantu dalam menentukan luas lahan TPA untuk 15 atau 30 tahun kedepan. Selain itu sistem ini juga dapat membantu dalam memproyeksikan data penduduk, data PDRB (Produk Domestik R Bruto), data timbulan sampah yang dihasilkan baik domestik maupun non-domestik pada suatu kabupaten selama 15 atau 30 tahun. 
          </p>
      </div>
    </div>
  </div>
</div>