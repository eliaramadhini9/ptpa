<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $nama;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/adminlte3/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/adminlte3/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav">

  <div class="wrapper wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <form class="form-inline ml-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
               <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
               </button>
             </div>
          </div>
        </form>
      </li>
      <li class="nav-item dropdown user-menu">
        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-login" data-toggle="tooltip" data-container="body" title="Tambah Data"><i class="fas fa-user"></i>  Login</button>
      </li>
    </ul>
    </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="https://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="https://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="https://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">
          </div>
        </div>
      </div>
    <!-- Content Header (Page header) -->

      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>ChartJS</h1>
            </div>
          </div>
        </div>
      </section>

    <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-body">

                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Area Chart</h3>
                  </div>
                  <div class="card-body">
                    <div class="chart">
                      <canvas id="barChart" style="height:250px; min-height:250px"></canvas>
                    </div>
                  </div>
                </div>

                <div class="card card-white">
                  <div class="card-header">
                    <h3 class="card-title">Line Chart</h3>
                  </div>
                  <div class="card-body">
                    <div class="chart">
                      <canvas id="lineChart" style="height:250px; min-height:250px"></canvas>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

<div class="modal fade" id="modal-login">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <div class="input-group mb-4">
          <input type="text" class="form-control" autocomplete="off" placeholder="Username" name="username" id="username" required autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-4">
          <input type="password" class="form-control" placeholder="Password" autocomplete="off" name="password" id="password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <?php foreach ($dataterakhir->result_array() as $item) :
          ?>
          <input type="hidden" class="form-control" name="id_kab" value="<?php echo $item['id_kab'];?>">

        <?php endforeach; ?>
          </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary float-right" value="loginValidation" name="loginValidation">Masuk</button>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.0-beta.2
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Add Content Here -->
    </aside>
  <!-- /.control-sidebar -->
  </div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="assets/adminlte3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="assets/adminlte3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="assets/adminlte3/plugins/chart.js/Chart.min.js"></script>
<!-- FastClick -->
<script src="assets/adminlte3/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="assets/adminlte3/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/adminlte3/js/demo.js"></script>
<!-- page script -->


<?php
    /* Mengambil query report*/
    foreach($dataProyeksiPenduduk as $result){
        $tahunPenduduk[] = $result->tahun; //ambil bulan
        $pnPenduduk[] = (float) $result->pn; //ambil nilai
    }     
?>
<script>
  $(function () {
    var barChartCanvas = $('#barChart').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunPenduduk);?>,
      datasets: [
        {
          label               : 'Jumlah Penduduk',
          borderWidth         : 2,
          backgroundColor     : 'rgba(60,141,188,0.6)',
          borderColor         : 'rgba(60,141,188,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnPenduduk);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }


    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'line',
      data: barChartData, 
      options: barChartOptions
    })

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
    var lineChartOptions = jQuery.extend(true, {}, barChartOptions)
    var lineChartData = jQuery.extend(true, {}, barChartData)
    lineChartData.datasets[0].fill = false;
    lineChartData.datasets[1].fill = false;
    lineChartOptions.datasetFill = false

    var lineChart = new Chart(lineChartCanvas, { 
      type: 'line',
      data: lineChartData, 
      options: lineChartOptions
    })
  })

</script>
</body>
</html>
