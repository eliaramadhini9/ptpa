<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          Jumlah Penduduk Kabupaten Jepara Menurut Kecamatan Tahun 2018
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
      <div class="mailbox-controls">
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah" data-toggle="tooltip" data-container="body" title="Tambah Data"><i class="fas fa-plus"></i> Tambah Kecamatan</button>
        </div>
        <table id="abcdf" class="table table-bordered">
          <thead>
              <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Kecamatan</th>
                <th rowspan="2">Luas Daerah (Km<sup>2</sup>)</th>
                <th rowspan="2">Jumlah Penduduk (jiwa)</th>
                <th rowspan="2">Kepadatan Penduduk per Km<sup>2</sup></th>
                <th colspan="2">Aksi</th>
              </tr>
              <tr>
                <th>Edit</th>
                <th>Hapus</th>
              </tr>
            </thead>
            <tbody>
            <?php
                  $no=1;
                  if ($dataKecamatan->num_rows()>0) {
                    foreach ($dataKecamatan->result_array() as $item) { ?>
             <tr>
                <td style="text-align: center;"><?php echo $no;?></td>
                <td><?php echo ucwords($item['kecamatan']);?></td>
                <td><?php echo ucwords($item['luas']);?></td>
                <td><?php echo ucwords($item['jumlah_penduduk']);?></td>
                <td><?php echo ucwords($item['kepadatan']);?></td>
                <td align="center">
                  <a class="btn red" href="#edit-kecamatan<?php echo $item['id_kecamatan'];?>">
                    <i class="fas fa-edit" data-toggle="modal" data-target="#edit-kecamatan<?php echo $item['id_kecamatan'];?>"></i>
                  </a>
                </td>
                <td align="center">
                  <a class="btn red" href="#hapus-kecamatan<?php echo $item['id_kecamatan'];?>">
                    <i class="fas fa-trash" data-toggle="modal" data-target="#hapus-kecamatan<?php echo $item['id_kecamatan'];?>"></i>
                  </a>
                </td>
              </tr>
          <?php $no++;
                    }
                    }
                    
                    
                    else { ?>
                    <tr>
                      <td colspan="8">No Result Data</td>
                    </tr>
                    <?php

                    }
                    ?>

        </tbody>
      </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id='modal-tambah' tabindex="-1">
  <div class="modal-dialog modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          Edit Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body center-content">
            <div class="form-group">
              <label class="control-label">Kecamatan</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="kecamatan" required="on" autofocus />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Luas Daerah (Km<sup>2</sup>)</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="luas" required="on"  autofocus />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Penduduk (jiwa)</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="jumlah_penduduk" required="on" autofocus />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Kepadatan Penduduk per Km<sup>2</sup></label>
              <div>
                <input type="text" class="form-control form-control-sm" name="kepadatan" required="on" autofocus />
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
          <button type="submit" name="buatKecamatan" value="buatKecamatan" id="buatKecamatan" class="btn btn-success btn-sm float-right">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

 <?php
                    foreach ($dataKecamatanID->result_array() as $item) { ?>

<div class="modal fade" id="edit-kecamatan<?php echo $item['id_kecamatan'];?>" tabindex="-1">
  <div class="modal-dialog modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          Edit Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/editKecamatan/<?php echo $item['id_kecamatan'];?>">
        <div class="modal-body">
          <div class="card-body center-content">
            <div class="form-group">
              <label class="control-label">Kecamatan</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="kecamatan" required="on" value="<?php echo $item['kecamatan']; ?>" autofocus />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Luas Daerah (Km<sup>2</sup>)</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="luas" required="on" value="<?php echo $item['luas']; ?>" autofocus />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Penduduk (jiwa)</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="jumlah_penduduk" required="on" value="<?php echo $item['jumlah_penduduk']; ?>" autofocus />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Kepadatan Penduduk per Km<sup>2</sup></label>
              <div>
                <input type="text" class="form-control form-control-sm" name="kepadatan" required="on" value="<?php echo $item['kepadatan']; ?>" autofocus />
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success btn-sm float-right">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="hapus-kecamatan<?php echo $item['id_kecamatan'];?>" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center><p>Yakin ingin menghapus data Kecamatan <?php echo ucwords($item['kecamatan']);?> ?</p></center>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
         <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/hapusKecamatan/<?php echo $item['id_kecamatan'];?>"><button type="button" class="btn btn-primary">Hapus</button></a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<?php } ?>








