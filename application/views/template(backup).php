<?php if(!$this->session->userdata['login']){redirect(base_url('halamanUtama'));} ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="<?php base_url('') ?>../assets/maxi/img/favicon.png" type="image/png">
  <title><?php echo $nama; ?></title>

<!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->
  <link rel="stylesheet" href="<?php base_url('') ?>../assets/adminlte3/plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php base_url('') ?>../assets/adminlte3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Theme style -->
  <link rel="stylesheet" href="<?php base_url('') ?>../assets/adminlte3/css/adminlte.css">
<!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="<?php base_url('') ?>../assets/bower_components/Ionicons/css/ionicons.min.css">
<!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php base_url('') ?>../assets/adminlte3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- DataTables -->
  <link rel="stylesheet" href="<?php base_url('') ?>../assets/adminlte3/plugins/datatables/dataTables.bootstrap4.css">
  <style type="text/css">
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #28a745;
}

#power:hover {
    color: #e7122b;
}
#edit:hover {
    color: #28a745;
}
#hapus:hover {
    color: #ffc107;
}

.tools:hover {
    color: #16a838;
    display: none;
    float: right;
    display: inline-block;
}
.tools {
    color: #bfe1c8;
    display: none;
    float: right;
    display: inline-block;
   }
   .page-item.active .page-link {
    z-index: 1;
    color: #ffffff;
    background-color: #28a745;
    border-color: #28a745;
}
  </style>
</head>

<body class="hold-transition skin-teal layout-fixed layout-navbar-fixed text-sm pace-primary pace-done">
  <div class="pace pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
      <div class="pace-progress-inner">
      </div>
    </div>
    <div class="pace-activity">
    </div>
  </div>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-green navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <!-- <li class="nav-item dropdown">
        <?php foreach ($namaKabupaten->result_array() as $value) : ?>
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
          Kabupaten <?php echo ucwords($value['kabupaten']); ?>
        </a>
      <?php endforeach; ?>
        <div class="dropdown-menu">
          <?php
            if ($dataKabupaten->num_rows()>0) {
              foreach ($dataKabupaten->result_array() as $value) { ?>
              
          <a class="dropdown-item" tabindex="-1" href='<?php echo base_url();?><?php echo $value['id_kab'];?>/<?php echo $this->uri->segment(2) ?>'><?php echo ucwords($value['kabupaten']);?></a>
          <?php
            }
          }
          else { ?>
                    <a class="dropdown-item disabled">No Data</a>
                    <?php

                    }
                    ?>
        </div>
      </li> -->
      <li class="nav-item dropdown">
        <?php foreach ($namaKabupaten->result_array() as $value) : 
          
          ?>
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"  di>
          Kabupaten <?php echo ucwords($value['kabupaten']); ?>
        </a>
          <?php endforeach; ?>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-center">
          <?php
            if ($dataKabupaten->num_rows()>0) {
              foreach ($dataKabupaten->result_array() as $value) { ?>
          <a class="dropdown-item" tabindex="-1" href='<?php echo base_url();?><?php echo $value['id_kab'];?>/<?php echo $this->uri->segment(2) ?>'><?php echo ucwords($value['kabupaten']);?></a>
          <?php
            }
          }
          else { ?>
                    <a class="dropdown-item disabled">No Data</a>
                    <?php

                    }
                    ?>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/kabupaten" class="dropdown-item dropdown-footer"><b>semua kabupaten</b></a>
        </div>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link">
          <i class="far fa-calendar-alt"></i>&nbsp;
          <?php echo date('D, d-m-Y') ?>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i> Notifikasi
          <?php foreach ($notif->result_array() as $item) { 
            $belumBaca = $item['belumBaca'];

            if ($belumBaca !='0') {
          ?>
          <span class="badge badge-warning navbar-badge"><?php echo $belumBaca; ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/notifikasi" class="dropdown-item">
            <i class="fas fa-comment mr-2"></i> <?php echo $item['belumBaca']; ?> komentar baru
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
        </div>
          <?php } else { ?>
            </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a class="dropdown-item disabled" style="text-align: center;">tidak ada notifikasi</a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/notifikasi" class="dropdown-item" style="text-align: center;"><b>semua notifikasi</b></a>
        </div>
      <?php }} ?>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" id="power">
          <i class="fas fa-power-off"></i> Power
        </a>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
          <a href="<?php echo base_url('lockscreen');?>" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Lockscreen
                  <span class="float-right text-sm text-warning"><i class="fas fa-lock"></i></span>
                </h3>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url('logout');?>" class="dropdown-item">
            <div class="media">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Sign Out
                  <span class="float-right text-sm text-danger"><i class="fas fa-sign-out-alt"></i></span>
                </h3>
              </div>
            </div>
            <!-- Message End -->
          </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-green elevation-4 text-sm">
    <!-- Brand Logo -->
    <a href="" class="brand-link navbar-green">
      <img src="../assets/adminlte3/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light" style="color: white; font-family: 'Roboto'; font-size: 15px;"><b>SI</b>TUNG LUAS LAHAN TPA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header"><h6>MENU UTAMA</h6></li>
          <li class="nav-item">
            <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/beranda" class="nav-link <?php echo $this->uri->segment(2)=='beranda' ? 'active': '' ?>">
              <p>
                Beranda
              </p>
            </a>
          </li>
          <li class="nav-header"><h6>KRITERIA PENYISIH</h6></li>
          <li class="nav-item">
            <a class="nav-link <?php echo $this->uri->segment(2)=='kriteriaPenyisih' ? 'active': '' ?>" href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/kriteriaPenyisih">
              <p>
                Calon Lokasi TPA
              </p>
            </a>
          </li>
          <li class="nav-header"><h6>DATA PROYEKSI</h6></li>
          <li class="nav-item has-treeview <?php echo $this->uri->segment(2)=='proyeksiPenduduk' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='proyeksiPendudukAritmatik' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='proyeksiPendudukGeometri' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='proyeksiPendudukLQ' ? 'menu-open': '' ?>">
            <a href="#" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPenduduk' ? 'active': '' ?> or nav-link <?php echo $this->uri->segment(2)=='proyeksiPendudukAritmatik' ? 'active': '' ?> or nav-link <?php echo $this->uri->segment(2)=='proyeksiPendudukGeometri' ? 'active': '' ?> or nav-link <?php echo $this->uri->segment(2)=='proyeksiPendudukLQ' ? 'active': '' ?>">
              <p>
                Proyeksi Penduduk
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPenduduk" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPenduduk' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jumlah Penduduk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPendudukAritmatik" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPendudukAritmatik' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Metode Aritmatika</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPendudukGeometri" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPendudukGeometri' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Metode Geometri</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPendudukLQ" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPendudukLQ' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Metode Least Square</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview <?php echo $this->uri->segment(2)=='proyeksiPDRB' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='proyeksiPDRBAritmatik' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='proyeksiPDRBGeometri' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='proyeksiPDRBLQ' ? 'menu-open': '' ?>">
            <a href="#" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRB' ? 'active': '' ?> or nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRBAritmatik' ? 'active': '' ?> or nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRBGeometri' ? 'active': '' ?> or nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRBLQ' ? 'active': '' ?>">
              <p>
                Proyeksi PDRB
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRB' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jumlah PDRB</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRBAritmatik" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRBAritmatik' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Metode Aritmatika</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRBGeometri" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRBGeometri' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Metode Geometri</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRBLQ" class="nav-link <?php echo $this->uri->segment(2)=='proyeksiPDRBLQ' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Metode Least Square</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview <?php echo $this->uri->segment(2)=='timbulanDomestik' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='timbulanNonDomestik' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='timbulanTotal' ? 'menu-open': '' ?>">
            <a href="#" class="nav-link <?php echo $this->uri->segment(2)=='timbulanDomestik' ? 'active': '' ?> or nav-link <?php echo $this->uri->segment(2)=='timbulanTotal' ? 'active': '' ?>">
              <p>
                Proyeksi Timbulan Sampah
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/timbulanDomestik" class="nav-link <?php echo $this->uri->segment(2)=='timbulanDomestik' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Timbulan Domestik</p>
                </a>
              </li>
              <li class="nav-item">
                 <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/timbulanNonDomestik" class="nav-link <?php echo $this->uri->segment(2)=='timbulanNonDomestik' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Timbulan Non-Domestik</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/timbulanTotal" class="nav-link <?php echo $this->uri->segment(2)=='timbulanTotal' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Timbulan Total</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview <?php echo $this->uri->segment(2)=='materialAllBalance' ? 'menu-open': '' ?> or <?php echo $this->uri->segment(2)=='materialBalanceTahun' ? 'menu-open': '' ?>">
            <a href="#" class="nav-link <?php echo $this->uri->segment(2)=='materialAllBalance' ? 'active': '' ?> or <?php echo $this->uri->segment(2)=='materialBalanceTahun' ? 'active': '' ?>">
              <p>
                Proyeksi Material Balance 
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/materialAllBalance" class="nav-link <?php echo $this->uri->segment(2)=='materialAllBalance' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Material All Balance</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/materialBalanceTahun" class="nav-link <?php echo $this->uri->segment(2)=='materialBalanceTahun' ? 'active': '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Material Balance Proyeksi</p>
                </a>
              </li>
            </ul>
          </li>          
          <li class="nav-item">
            <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/kebutuhanLuasLahan" class="nav-link <?php echo $this->uri->segment(2)=='kebutuhanLuasLahan' ? 'active': '' ?>">
              <p>
                Kebutuhan Luas Lahan TPA
              </p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/drainase" class="nav-link <?php echo $this->uri->segment(2)=='drainase' ? 'active': '' ?>">
              <p>
                Drainase
              </p>
            </a>
          </li> -->
          <!-- <li class="nav-header">MISCELLANEOUS</li>
          <li class="nav-item">
            <a href="https://adminlte.io/docs/3.0" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Documentation</p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"><br>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <?php foreach ($namaKabupaten->result_array() as $item) : ?> -->
            <h1 class="m-0 text-dark"><?php echo $nama; ?><!-- <?php echo ucwords($item['kabupaten']); ?> --></h1>
          <!-- <?php endforeach; ?> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/beranda">Beranda</a></li>
              <li class="breadcrumb-item active"><?php echo $nama; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">

       <?php $this->load->view($view_name);?>

    </section>
    <a id="back-to-top" href="#" class="btn btn-dark back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block"><!-- 
      <b>Version</b> 3.0.0-beta.2 -->
    </div>
    <strong>Copyright &copy; 2019. <a href="<?php echo('beranda') ?>" style="color: green">sistem informasi perhitungan luas lahan tpa sampah</a>.</strong> by elia ramadhini
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- MODAL -->

<div class="modal fade" id="tambah-kabupaten">
  <div class="modal-dialog modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <i class="fas fa-plus"></i>
        Tambah Kabupaten
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body justify-content">
            <div class="form-group row">
              <label class="col-sm-3 control-label">Kabupaten</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="kabupaten" required="on" autofocus />
              </div>
              <div class="col-sm-6">
                <input type="hidden" class="form-control" name="id_kab" required="on" autofocus />
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="buatKabupaten" value="buatKabupaten">Buat</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- jQuery -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- FastClick -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php base_url('') ?>../assets/adminlte3/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php base_url('') ?>../assets/adminlte3/js/demo.js"></script>
<!-- DataTables -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/datatables/dataTables.bootstrap4.js"></script>
<!-- SweetAlert2 -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/pace-progress/pace.min.js"></script>

<!-- ChartJS -->
<script src="<?php base_url('') ?>../assets/adminlte3/plugins/chart.js/Chart.min.js"></script>

<script>
  <?php
    /* Mengambil query report*/
    foreach($proyeksiPendudukAritmatik as $aritmatik){
        $tahunAritmatikpp[] = $aritmatik->tahun; //ambil bulan
        $pnAritmatikpp[] = (float) $aritmatik->pn; //ambil nilai
    }

    foreach($proyeksiPendudukGeometri as $geometri){
        $tahunGeometripp[] = $geometri->tahun; //ambil bulan
        $pnGeometripp[] = (float) $geometri->pn; //ambil nilai
    }

    foreach($proyeksiPendudukLS as $ls){
        $tahunLSpp[] = $ls->tahun; //ambil bulan
        $pnLSpp[] = (float) $ls->pn; //ambil nilai
    }           
  ?>
  $(function () {
    var barChartCanvas = $('#chartProyeksiPendudukAritmatik').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunAritmatikpp);?>,
      datasets: [
        {
          label               : 'Jumlah Penduduk',
          borderWidth         : 2,
          backgroundColor     : 'rgba(50,205,50,0.6)',
          borderColor         : 'rgba(50,205,50,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnAritmatikpp);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }


    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })

    //geometri

    var barChartCanvas = $('#chartProyeksiPendudukGeometri').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunGeometripp);?>,
      datasets: [
        {
          label               : 'Jumlah Penduduk',
          borderWidth         : 2,
          backgroundColor     : 'rgba(50,205,50,0.6)',
          borderColor         : 'rgba(50,205,50,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnGeometripp);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }


    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })

    //least square

    var barChartCanvas = $('#chartProyeksiPendudukLS').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunLSpp);?>,
      datasets: [
        {
          label               : 'Jumlah PDRB',
          borderWidth         : 2,
          backgroundColor     : 'rgba(50,205,50,0.6)',
          borderColor         : 'rgba(50,205,50,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnLSpp);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })
  })
</script>


<script>
  <?php
    /* Mengambil query report*/
    foreach($proyeksiPdrbAritmatik as $aritmatik){
        $tahunAritmatikpdrb[] = $aritmatik->tahun; //ambil bulan
        $pnAritmatikpdrb[] = (float) $aritmatik->pn; //ambil nilai
    }

    foreach($proyeksiPdrbGeometri as $geometri){
        $tahunGeometripdrb[] = $geometri->tahun; //ambil bulan
        $pnGeometripdrb[] = (float) $geometri->pn; //ambil nilai
    }

    foreach($proyeksiPdrbLS as $ls){
        $tahunLSpdrb[] = $ls->tahun; //ambil bulan
        $pnLSpdrb[] = (float) $ls->pn; //ambil nilai
    }           
  ?>
  $(function () {
    var barChartCanvas = $('#chartProyeksiPdrbAritmatik').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunAritmatikpdrb);?>,
      datasets: [
        {
          label               : 'Nilai PDRB',
          borderWidth         : 2,
          backgroundColor     : 'rgba(255,215,0,0.6)',
          borderColor         : 'rgba(255,215,0,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnAritmatikpdrb);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }


    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })

    //geometri

    var barChartCanvas = $('#chartProyeksiPdrbGeometri').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunGeometripdrb);?>,
      datasets: [
        {
          label               : 'Nilai PDRB',
          borderWidth         : 2,
          backgroundColor     : 'rgba(255,215,0,0.6)',
          borderColor         : 'rgba(255,215,0,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnGeometripdrb);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }


    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })

    //least square

    var barChartCanvas = $('#chartProyeksiPdrbLS').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunLSpdrb);?>,
      datasets: [
        {
          label               : 'Nilai PDRB',
          borderWidth         : 2,
          backgroundColor     : 'rgba(255,215,0,0.6)',
          borderColor         : 'rgba(255,215,0,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnLSpdrb);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })
  })
</script>

<script>
  $(function () {
    $('#kriteriaPenyisih').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
    });
    $('#tabel2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
    });
    $('#abc').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
    });

    $('#abcd').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
    });

  });
</script>


<script>
  function sum() {
    var value1 = document.getElementById('bts_admin_jumlah').value;
    var value2 = document.getElementById('hak_pemilik_jumlah').value;
    var value3 = document.getElementById('kapasitas_jumlah').value;
    var value4 = document.getElementById('jml_pemilik_jumlah').value;
    var value5 = document.getElementById('part_mas_jumlah').value;
    var value6 = document.getElementById('tanah_jumlah').value;
    var value7 = document.getElementById('air_tanah_jumlah').value;
    var value8 = document.getElementById('aliran_air_jumlah').value;
    var value9 = document.getElementById('kaitan_jumlah').value;
    var value10 = document.getElementById('bhy_banjir_jumlah').value;
    var value11 = document.getElementById('penutup_jumlah').value;
    var value12 = document.getElementById('intens_hujan_jumlah').value;
    var value13 = document.getElementById('jln_lokasi_jumlah').value;
    var value14 = document.getElementById('transport_jumlah').value;
    var value15 = document.getElementById('jln_masuk_jumlah').value;
    var value16 = document.getElementById('lalin_jumlah').value;
    var value17 = document.getElementById('tata_lahan_jumlah').value;
    var value18 = document.getElementById('pertanian_jumlah').value;
    var value19 = document.getElementById('cgr_alam_jumlah').value;
    var value20 = document.getElementById('biologis_jumlah').value;
    var value21 = document.getElementById('bau_jumlah').value;
    var value22 = document.getElementById('estetika_jumlah').value;    
    var result = parseInt(value1) + parseInt(value2) + parseInt(value3) + parseInt(value4) + parseInt(value5) + parseInt(value6) + parseInt(value7) + parseInt(value8) + parseInt(value9) + parseInt(value10) + parseInt(value11) + parseInt(value12) + parseInt(value13) + parseInt(value14) + parseInt(value15) + parseInt(value16) + parseInt(value17) + parseInt(value18) + parseInt(value19) + parseInt(value20) + parseInt(value21) + parseInt(value22);
    if (!isNaN(result)) {
      document.getElementById('jumlah').value = result
    }
  }
</script>

<script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    $("#check-all").click(function(){ // Ketika user men-cek checkbox all
      if($(this).is(":checked")) // Jika checkbox all diceklis
        $(".check-item").prop("checked", true); // ceklis semua checkbox siswa dengan class "check-item"
      else // Jika checkbox all tidak diceklis
        $(".check-item").prop("checked", false); // un-ceklis semua checkbox siswa dengan class "check-item"
    });
    
    $("#btn-delete").click(function(){ // Ketika user mengklik tombol delete
      var confirm = window.confirm("Apakah Anda yakin ingin menghapus data ini?"); // Buat sebuah alert konfirmasi
      
      if(confirm) // Jika user mengklik tombol "Ok"
        $("#form-delete").submit(); // Submit form
    });
  });
</script>
</body>
</html>
