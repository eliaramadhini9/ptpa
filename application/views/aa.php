<div class="row">
  <div class="col-12">
    <div class="card card-outline collapsed-card card-teal">
      <div class="card-header">
        <h3 class="card-title">Tambah Calon Lokasi TPA</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example2" class="table table-bordered">
          <form class="form-horizontal" method="post">
          <thead>
            <tr>
              <th rowspan="2" class="center">No</th>
              <th rowspan="2" class="center">Parameter</th>
              <th colspan="3" class="center">Calon Lokasi TPA
                <span>
                  <div class="form-group">
                    <input type="text" min="0" max="200" class="form-control" autosave="off" placeholder="Masukkan Nama Calon TPA" name="calon_tpa" required="required">
                  </div>
                </span>
              </th>
            </tr>
            <tr>
              <th>Bobot</th>
              <th>Nilai</th>
              <th>Jumlah</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="5">I. UMUM</td>
            </tr>
            <tr>
              <td rowspan="6" align="center">1</td>
            </tr>
            <tr>
              <td>Batas Administrasi</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="bts_admin_jumlah" onkeyup="sum();" name="bts_admin_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Dalam batas administrasi</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Diluar batas administrasi tetapi dalam satu sistem pengelolaan TPA sampah terpadu</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Diluar batas administrasi dan diluar sistem pengelolaan TPA sampah terpadu</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Diluar batas administrasi</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin4">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="7" align="center">2</td>
            </tr>
            <tr>
              <td>Pemilik hak atas tanah</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="hak_pemilik_jumlah" onkeyup="sum();" name="hak_pemilik_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Pemerintah daerah/pusat</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Pribadi (satu orang)</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Swasta/perusahaan (satu)</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Lebih dari satu pemilik hak dan status kepemilikan</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik4">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Organisasi sosial/agama</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik5">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">3</td>
            </tr>
            <tr>
              <td>Kapasitas Lahan</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="kapasitas_jumlah" onkeyup="sum();" name="kapasitas_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>> 10 tahun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>5 tahun - 10 tahun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>3 tahun - 5 tahun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>kurang dari 3 tahun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas4">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="7" align="center">4</td>
            </tr>
            <tr>
              <td>Jumlah pemilik tanah</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="jml_pemilik_jumlah" onkeyup="sum();" name="jml_pemilik_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>1 kk</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>2 - 3 kk</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>4 - 5 kk</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>6 - 10 kk</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik4">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>lebih dari 10 kk</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik5">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">5</td>
            </tr>
            <tr>
              <td>Partisipasi masyarakat</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="part_mas_jumlah" onkeyup="sum();" name="part_mas_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Spontan</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Digerakkan</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Negosiasi</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td colspan="5">I. LINGKUNGAN FISIK</td>
            </tr>
            <tr>
              <td rowspan="5" align="center">1</td>
            </tr>
            <tr>
              <td>Tanah (diatas muka air tanah)</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="tanah_jumlah" onkeyup="sum();" name="tanah_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Harga kelulusan < 10^-9 cm/det</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Harga kelulusan 10^-9 - 10^-6 cm/det</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Harga kelulusan >10^-6 cm/det, TOLAK kecuali ada masukan teknologi</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">2</td>
            </tr>
            <tr>
              <td>Air Tanah</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="air_tanah_jumlah" onkeyup="sum();" name="air_tanah_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>≥ 10 m dengan kelulusan < 10^-6 cm/det</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>< 10 m dengan kelulusan < 10^-6 cm/det</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>≥ 10 m dengan kelulusan < 10^-6 - 10^-4 cm/det</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>< 10 m dengan kelulusan < 10^-6 - 10^-4 cm/det</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah4">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">3</td>
            </tr>
            <tr>
              <td>Sistem aliran air tanah</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air_bobot">
                </div>
              </td>
              <td></td>
             <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="aliran_air_jumlah" onkeyup="sum();" name="aliran_air_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Discharge area/lokal</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Recharge area dan discharge area lokal</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Recharge area regional dan lokal</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">4</td>
            </tr>
            <tr>
              <td>Kaitan dengan pemanfaatan air tanah</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="kaitan_jumlah" onkeyup="sum();" name="kaitan_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Kemungkinan pemanfaatan rendah dengan batas hidrolis</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Diproyeksikan untuk dimanfaatkan dengan batas hidrolis</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Diproyeksikan untuk dimanfaatkan tanpa batas hidrolis</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">5</td>
            </tr>
            <tr>
              <td>Bahaya Banjir</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="bhy_banjir_jumlah" onkeyup="sum();" name="bhy_banjir_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Tidak ada bahaya banjir</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Kemungkinan banjir > 25 tahunan</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Kemungkinan bajir < 25 tahunan, TOLAK</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">6</td>
            </tr>
            <tr>
              <td>Tanah penutup</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="penutup_jumlah" onkeyup="sum();" name="penutup_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Tanah penutup cukup</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Tanah penutup cukup sampai 1/2 umur pakai</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Tanah penutup tidak ada</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">7</td>
            </tr>
            <tr>
              <td>Intensitas hujan</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name=" intens_hujan_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="intens_hujan_jumlah" onkeyup="sum();" name=" intens_hujan_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Dibawah 500 mm pertahun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="intens_hujan1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Antara 500 mm sampai 1000 mm pertahun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="intens_hujan2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Diatas 1000 mm pertahun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="intens_hujan3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">8</td>
            </tr>
            <tr>
              <td>Jalan menuju lokasi</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="jln_lokasi_jumlah" onkeyup="sum();" name="jln_lokasi_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Datar dengan kondisi baik</td>
              <td>
              </td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Datar dengan kondisi buruk</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Naik/turun</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">9</td>
            </tr>
            <tr>
              <td>Transport sampah (satu jalan)</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="transport_jumlah" onkeyup="sum();" name="transport_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Kurang dari 15 menit dari centroid sampah</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Antara 16 menit - 30 menit dari centroid sampah</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Antara 31 menit - 60 menit dari centroid sampah</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Lebih dari 60 menit dari centroid sampah</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport4">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">10</td>
            </tr>
            <tr>
              <td>Jalan Masuk</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="jln_masuk_jumlah" onkeyup="sum();" name="jln_masuk_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Truk sampah tidak melalui daerah pemukiman</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Truk sampah melalui daerah pemukiman berkepadatan sedang (≤ 300 jiwa/ha)</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Truk sampah melalui daerah pemukiman berkepadatan sedang (≥300 jiwa/ha)</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">11</td>
            </tr>
            <tr>
              <td>Lalu Lintas</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="lalin_jumlah" onkeyup="sum();" name="lalin_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Terletak 500 dari jalan umum</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Terletak < 500 m pada lalu lintas rendah</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Terletak < 500 m pada lalu lintas sedang</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Terletak < 500 m pada lalu lintas tinggi</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin4">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">12</td>
            </tr>
            <tr>
              <td>Tata guna lahan</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="tata_lahan_jumlah" onkeyup="sum();" name="tata_lahan_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Mempunyai dampak sedikit terhadap tata guna tanah sekitar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Mempunyai dampak sedang terhadap tata guna tanah sekitar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Mempunyai dampak besar terhadap tata guna tanah sekitar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">13</td>
            </tr>
            <tr>
              <td>Pertanian</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="pertanian_jumlah" onkeyup="sum();" name="pertanian_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Berlokasi di lahan tidak produktif</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Tidak ada dampak terhadap pertanian sekitar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Ada pengaruh negatif terhadap pertanian sekitar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Berlokasi di tanah pertanian produktif</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian4">
                </div>
              </td>
              <td></td>
            </tr> 
            <tr>
              <td rowspan="5" align="center">14</td>
            </tr>
            <tr>
              <td>Daerah lindung / cagar alam</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="cgr_alam_jumlah" onkeyup="sum();" name="cgr_alam_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Tidak ada daerah lindung / cagar alam di sekitarnya</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Terdapat daerah lindung/ cagar alam di sekitarnya yang tidak terkena dampak negatif</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Terdapat daerah lindung/ cagar alam di sekitarnya yang terkena dampak negatif</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">15</td>
            </tr>
            <tr>
              <td>Biologis</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="biologis_jumlah" onkeyup="sum();" name="biologis_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Nilai habitat yang rendah</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Nilai habitat yang tinggi</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Habitat kritis</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">16</td>
            </tr>
            <tr>
              <td>Kebisingan dan bau</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="bau_jumlah" onkeyup="sum();" name="bau_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Terdapat zona penyangga</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Terdapat zona penyangga yang terbatas</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Tidak terdapat penyangga</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">17</td>
            </tr>
            <tr>
              <td>Estetika</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika_bobot">
                </div>
              </td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="estetika_jumlah" onkeyup="sum();" name="estetika_jumlah">
                </div>
              </td>
            </tr>
            <tr>
              <td>Operasi penimbunan tidak terlihat dari luar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika1">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Operasi penimbunan sedikit terlihat dari luar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika2">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td>Operasi penimbunan terlihat dari luar</td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika3">
                </div>
              </td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2" align="center">TOTAL</td>
              <td></td>
              <td></td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input type="number" disabled="on" class="form-control" autosave="off" id="jumlah" name="jumlah">
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="card-body">
      <button type="submit" class="btn btn-success float-right swalDefaultSuccess" name="buatCalonTPA" value="buatCalonTPA">Simpan</button>
      <button type="reset" class="btn btn-default" id="back-to-top" value="reset">Reset</button>
      </div>
    </form>
    </div>
  </div>
</div>