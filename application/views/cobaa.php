<div class="row">
  <div class="col-md-5">
    <div class="card card-green">
      <div class="card-header">
        <h5 class="card-title">
          ARITMATIK
        </h5>
      </div>
      <div class="card-body">
        <table id="example3" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="30px">Tahun</th>
              <th width="50px">Penduduk</th>
              <th width="50px">Pn = Po - Ka (Ta-To)</th>
            </tr>
          </thead>
          <tbody>
            <?php
                  foreach ($dataProyeksiPendudukAritmatik->result_array() as $item) { ?>
            <tr>
              <td><?php echo $item['tahun'];?></td>
              <td><?php echo $item['penduduk'];?></td>
              <td><?php echo $item['pn'];?></td>
            </tr>
                <?php

                }
                ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card card-green">
      <div class="card-header">
        <h5 class="card-title">
          STANDAR DEVIASI ARITMATIK
        </h5>
      </div>
      <div class="card-body">
        <table id="example4" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Penduduk</th>
              <th>Hasil Hitung</th>
              <th>Yi - Ymean</th>
              <th>(Yi - Ymean)^2</th>
            </tr>
          </thead>
          <tbody>
            <?php
              foreach ($dataProyeksiPendudukAritmatikStandar->result_array() as $item) { ?>
            <tr>
              <td><?php echo $item['penduduk'];?></td>
              <td><?php echo $item['hasil_hitung'];?></td>
              <td><?php echo $item['yi_ymean'];?></td>
              <td><?php echo $item['yi_ymean2'];?></td>
            </tr>
            <?php
              }
            ?>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="3" align="center">JUMLAH</th>
              <th><?php echo $item['sum_yi_ymean2'];?></th>
            </tr>
            <tr>
              <th colspan="3" align="center">STANDAR DEVIASI</th>
              <th><?php echo $item['standar_deviasi'];?></th>
            </tr>

          </tfoot>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-outline card-white">
      <div class="card-header">
        <h3 class="card-title">RUMUS ARITMATIKA</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <blockquote class="quote-green">
          <h1>Pn = Po + Ka(Ta - To)</h1>
          <p>Keterangan : <br>
            Pn = Jumlah penduduk pada tahun proyeksi<br>
            Po = Jumlah penduduk pada awal proyeksi<br>
            Ka = Rata - rata Pertumbuhan Penduduk Per-Tahun (Po-Pn)/(Ta-To)<br>
            Ta = Tahun Akhir<br>
            To = Tahun Awal
          </p>
        </blockquote>
      </div>
      </div>
    </div>
  </div>
</div>