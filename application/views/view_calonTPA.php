<!-- konten kriteria penyisih -->

<div class="row">
  <div class="col-md-12">
    <?php
            foreach ($jumlahDataCalonTPA->result_array() as $item) : 
              if ($item['jumlahData'] < 0) {
              ?>
    <div class="card card-outline card-green">
    <?php } else{ ?>
    <div class="card card-outline collapsed-card card-green">
    <?php } endforeach; ?>
      <div class="card-header">
        <h3 class="card-title">
          <i class="fas fa-plus"></i>
        Tambah Calon Lokasi TPA
        </h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered">
          <form class="form-horizontal" method="post">
          <thead>
            <tr>
              <th  class="center">No</th>
              <th class="center">Parameter</th>
              <th class="center">Calon Lokasi TPA
                <span>
                  <div class="form-group">
                    <select name="calon_tpa" class="form-control" required="">
                      <option value="">pilih kecamatan</option>
                      <?php foreach ($dataKecamatan->result_array() as $value) { ?>
                      <option>Kecamatan <?php echo ucfirst($value['kecamatan']); ?></option>
                    <?php } ?>
                    </select>
                  </div>
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="3">I. UMUM</td>
            </tr>
            <tr>
              <td align="center">1</td>
              <td>Batas Administrasi</td>
              <td>
                <div class="form-group">
                  <select name="bts_admin_jumlah" id="bts_admin_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="50">dalam batas administrasi</option>
                      <option value="25">diluar batas administrasi tetapi dalam satu sistem pengelolaan TPA sampah terpadu</option>
                      <option value="5">diluar batas administrasi dan diluar sistem pengelolaan TPA sampah terpadu</option>
                      <option value="5">diluar batas administrasi</option>
                  </select>
                </div>
              </td>
            </tr>
            
            <tr>
              <td align="center">2</td>
              <td>Pemilik hak atas tanah</td>
              <td>
                <div class="form-group">
                  <select name="hak_pemilik_jumlah" id="hak_pemilik_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">pemerintah daerah atau pusat</option>
                      <option value="21">pribadi (satu orang)</option>
                      <option value="15">swasta atau perusahaan (satu)</option>
                      <option value="9">lebih dari satu pemilik hak dan status kepemilikan</option>
                      <option value="3">organisasi sosial atau agama</option>
                  </select>
                </div>
              </td>
            <tr>
              <td align="center">3</td>
              <td>Kapasitas Lahan</td>
              <td>
                <div class="form-group">
                  <select name="kapasitas_jumlah" id="kapasitas_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="50">lebih dari 10 tahun</option>
                      <option value="40">5 tahun sampai 10 tahun</option>
                      <option value="25">3 tahun sampai 5 tahun</option>
                      <option value="5">kurang dari 3 tahun</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">4</td>
              <td>Jumlah pemilik tanah</td>
              <td>
                <div class="form-group">
                  <select name="jml_pemilik_jumlah" id="jml_pemilik_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">1 KK</option>
                      <option value="21">2 sampai 3 KK</option>
                      <option value="15">4 sampai 5 KK</option>
                      <option value="9">6 sampai 10 KK</option>
                      <option value="3">lebih dari 10 KK</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">5</td>
              <td>Partisipasi masyarakat</td>
              <td>
                <div class="form-group">
                  <select name="part_mas_jumlah" id="part_mas_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">spontan</option>
                      <option value="15">digerakkan</option>
                      <option value="3">negosiasi</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3">II. LINGKUNGAN FISIK</td>
            </tr>
            <tr>
              <td align="center">1</td>
              <td>Tanah (diatas muka air tanah)</td>
              <td>
                <div class="form-group">
                  <select name="tanah_jumlah" id="tanah_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="50">harga kelulusan kurang dari 10^-9 cm/det</option>
                      <option value="35">harga kelulusan 10^-9 sampai 10^-6 cm/det</option>
                      <option value="25">harga kelulusan lebih dari 10^-6 cm/det, TOLAK (kecuali ada masukan teknologi)</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">2</td>
              <td>Air Tanah</td>
              <td>
                <div class="form-group">
                  <select name="air_tanah_jumlah" id="air_tanah_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="50">lebih dari atau sama dengan 10 meter, dengan kelulusan lebih dari 10^-6 cm/det</option>
                      <option value="40">kurang 10 meter, dengan kelulusan kurang dari 10^-6 cm/det</option>
                      <option value="15">lebih dari atau sama, dengan 10 meter dengan kelulusan kurang dari 10^-6 sampai 10^-4 cm/det</option>
                      <option value="5">kurang dari 10 meter, dengan kelulusan kurang dari 10^-6 sampai 10^-4 cm/det</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">3</td>
              <td>Sistem aliran air tanah</td>
              <td>
                <div class="form-group">
                  <select name="aliran_air_jumlah" id="aliran_air_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">discharge area atau lokal</option>
                      <option value="15">recharge area dan discharge area lokal</option>
                      <option value="3">recharge area regional dan lokal</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">4</td>
              <td>Kaitan dengan pemanfaatan air tanah</td>
              <td>
                <div class="form-group">
                  <select name="kaitan_jumlah" id="kaitan_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">kemungkinan pemanfaatan rendah dengan batas hidrolis</option>
                      <option value="15">diproyeksikan untuk dimanfaatkan dengan batas hidrolis</option>
                      <option value="3">diproyeksikan untuk dimanfaatkan tanpa batas hidrolis</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">5</td>
              <td>Bahaya Banjir</td>
              <td>
                <div class="form-group">
                  <select name="bhy_banjir_jumlah" id="bhy_banjir_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="20">tidak ada bahaya banjir</option>
                      <option value="10">kemungkinan banjir lebih dari 25 tahunan</option>
                      <option value="2">kemungkinan bajir kurang dari 25 tahunan, TOLAK (kecuali ada masukan teknologi)</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">6</td>
              <td>Tanah penutup</td>
              <td>
                <div class="form-group">
                  <select name="penutup_jumlah" id="penutup_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="40">tanah penutup cukup</option>
                      <option value="20">tanah penutup cukup sampai setengah umur pakai</option>
                      <option value="4">tanah penutup tidak ada</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">7</td>
              <td>Intensitas hujan</td>
              <td>
                <div class="form-group">
                  <select name="intens_hujan_jumlah" id="intens_hujan_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">dibawah 500 mm pertahun</option>
                      <option value="15">antara 500 mm sampai 1000 mm pertahun</option>
                      <option value="3">diatas 1000 mm pertahun</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">8</td>
              <td>Jalan menuju lokasi</td>
              <td>
                <div class="form-group">
                  <select name="jln_lokasi_jumlah" id="jln_lokasi_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="50">datar dengan kondisi baik</option>
                      <option value="25">datar dengan kondisi buruk</option>
                      <option value="5">naik atau turun</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">9</td>
              <td>Transport sampah (satu jalan)</td>
              <td>
                <div class="form-group">
                  <select name="transport_jumlah" id="transport_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="50">kurang dari 15 menit dari centroid sampah</option>
                      <option value="40">antara 16 menit - 30 menit dari centroid sampah</option>
                      <option value="15">antara 31 menit - 60 menit dari centroid sampah</option>
                      <option value="5">lebih dari 60 menit dari centroid sampah</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">10</td>
              <td>Jalan Masuk</td>
              <td>
                <div class="form-group">
                  <select name="jln_masuk_jumlah" id="jln_masuk_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="40">truk sampah tidak melalui daerah pemukiman</option>
                      <option value="20">truk sampah melalui daerah pemukiman berkepadatan sedang (≤ 300 jiwa/ha)</option>
                      <option value="4">truk sampah melalui daerah pemukiman berkepadatan tinggi (≥ 300 jiwa/ha)</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">11</td>
              <td>Lalu Lintas</td>
              <td>
                <div class="form-group">
                  <select name="lalin_jumlah" id="lalin_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">terletak 500 m dari jalan umum</option>
                      <option value="24">terletak < 500 m pada lalu lintas rendah</option>
                      <option value="9">terletak < 500 m pada lalu lintas sedang</option>
                      <option value="3">terletak pada lalu lintas tinggi</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">12</td>
              <td>Tata guna lahan</td>
              <td>
                <div class="form-group">
                  <select name="tata_lahan_jumlah" id="tata_lahan_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="50">mempunyai dampak sedikit terhadap tata guna tanah sekitar</option>
                      <option value="25">mempunyai dampak sedang terhadap tata guna tanah sekitar</option>
                      <option value="5">mempunyai dampak besar terhadap tata guna tanah sekitar</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">13</td>
              <td>Pertanian</td>
              <td>
                <div class="form-group">
                  <select name="pertanian_jumlah" id="pertanian_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">berlokasi di lahan tidak produktif</option>
                      <option value="15">tidak ada dampak terhadap pertanian sekitar</option>
                      <option value="3">ada pengaruh negatif terhadap pertanian sekitar</option>
                      <option value="3">berlokasi di tanah pertanian produktif</option>
                  </select>
                </div>
              </td>
            </tr> 
            <tr>
              <td align="center">14</td>
              <td>Daerah lindung atau cagar alam</td>
              <td>
                <div class="form-group">
                  <select name="cgr_alam_jumlah" id="cgr_alam_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="20">tidak ada daerah lindung atau cagar alam di sekitarnya</option>
                      <option value="2">terdapat daerah lindung atau cagar alam di sekitarnya yang tidak terkena dampak negatif</option>
                      <option value="2">terdapat daerah lindung atau cagar alam di sekitarnya yang terkena dampak negatif</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">15</td>
              <td>Biologis</td>
              <td>
                <div class="form-group">
                  <select name="biologis_jumlah" id="biologis_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">nilai habitat yang rendah</option>
                      <option value="15">nilai habitat yang tinggi</option>
                      <option value="3">habitat kritis</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">16</td>
              <td>Kebisingan dan bau</td>
              <td>
                <div class="form-group">
                  <select name="bau_jumlah" id="bau_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="20">terdapat zona penyangga</option>
                      <option value="10">terdapat zona penyangga yang terbatas</option>
                      <option value="2">tidak terdapat penyangga</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td align="center">17</td>
              <td>Estetika</td>
              <td>
                <div class="form-group">
                  <select name="estetika_jumlah" id="estetika_jumlah" onkeyup="sum();" class="form-control" required="">
                      <option value="">-</option>
                      <option value="30">operasi penimbunan tidak terlihat dari luar</option>
                      <option value="15">operasi penimbunan sedikit terlihat dari luar</option>
                      <option value="3">operasi penimbunan terlihat dari luar</option>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="2" align="center">TOTAL</td>
              <td>
                <div class="form-group" style="width: 70px">
                  <input style="border:none; background-color: transparent;" class="form-control" autosave="off" id="jumlah" name="jumlah">
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="card-body">
      <button type="submit" class="btn btn-success float-right" name="buatCalonTPA" value="buatCalonTPA">Simpan</button>
      <button type="reset" class="btn btn-default" id="back-to-top" value="reset">Reset</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- konten data kriteria penyisih -->

<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">Data Calon Lokasi TPA</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="kriteriaPenyisih" class="table table-bordered">
          <thead>
            <tr>
              <th rowspan="2" width="50px">No</th>
              <th rowspan="2" width="50%">Calon TPA</th>
              <th rowspan="2" width="20%">Total Kriteria</th>
              <th colspan="3">Aksi</th>
            </tr>
            <tr>
              <th width="50px">Detail</th>
              <th width="50px">Hapus</th>
            </tr>
            </thead>
            <tbody>
            <?php
                    $no=1;
                    if ($dataKriteria->num_rows()>0) {
                      foreach ($dataKriteria->result_array() as $item) { 
                        ?>
             <tr>
              <td align="center"><?php echo $no;?></td>
              <td>Kecamatan <?php echo ucwords($item['calon_tpa']);?></td>
              <td align="center"><?php echo $item['jumlah'];?></td>
              <td align="center">
                <a class="text-muted" href="#modal-detail<?php echo $item['id_kriteria'];?>" data-toggle="tooltip" data-container="body" title="Detail Data">
                  <i class="fas fa-search" data-toggle="modal" data-target="#modal-detail<?php echo $item['id_kriteria'];?>" id="edit"></i>
                </a>
              </td>
              </td>
              <td align="center">
                <a class="btn red" href="#modal-hapus<?php echo $item['id_kriteria'];?>" data-toggle="tooltip" data-container="body" title="Hapus Data">
                  <i class="fas fa-trash" data-toggle="modal" data-target="#modal-hapus<?php echo $item['id_kriteria'];?>" id="edit"></i>
                </a>
              </td>
            </tr>
          <?php
                    $no++;
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="8" style="text-align: center;">No Result Data</td>
                    </tr>
                    <?php

                    }
                    ?>
        </tbody>
          
        </table>
         <?php foreach ($dataCalonTPA->result_array() as $item) :
                        ?>
        <p style="background-color: yellow">Calon TPA yang disarankan sesuai dengan kriteria, yaitu Kecamatan <u><i><?php echo ucwords($item['calon_tpa']);?></i></u></p>

      <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>

<!-- modal -->
<?php
        foreach($dataKriteria->result_array() as $item):
        ?>
<div class="modal fade" id="modal-detail<?php echo $item['id_kriteria'];?>" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <i class="fas fa-search"></i>
          Detail Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="2" class="center">No</th>
                <th rowspan="2" class="center">Parameter</th>
                <th class="center">Calon Lokasi TPA Kecamatan <?php echo ucwords($item['calon_tpa']);?></th>
              </tr>
              <tr>
                <th>Nilai</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="5">I. UMUM</td>
              </tr>
              <tr>
                <td align="center">1</td>
                <td>Batas Administrasi</td>
                <td style="text-align: center;"><?php echo $item['bts_admin_jumlah'];?></td>
              </tr>
              <tr>
              </tr>
              
              <tr>
                <td align="center">2</td>
                <td>Pemilik hak atas tanah</td>
                <td style="text-align: center;"><?php echo $item['hak_pemilik_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">3</td>
                <td>Kapasitas Lahan</td>
                <td style="text-align: center;"><?php echo $item['kapasitas_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">4</td>
                <td>Jumlah pemilik tanah</td>
                <td style="text-align: center;"><?php echo $item['jml_pemilik_jumlah'];?></td>
              </tr>
              <tr>
                <td  align="center">5</td>
                <td>Partisipasi masyarakat</td>
                <td style="text-align: center;"><?php echo $item['part_mas_jumlah'];?></td>
              </tr>
              <tr>
                <td colspan="5">I. LINGKUNGAN FISIK</td>
              </tr>
              <tr>
                <td align="center">1</td>
                <td>Tanah (diatas muka air tanah)</td>
                <td style="text-align: center;"><?php echo $item['tanah_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">2</td>
                <td>Air Tanah</td>
                <td style="text-align: center;"><?php echo $item['air_tanah_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">3</td>
                <td>Sistem aliran air tanah</td>
                <td style="text-align: center;"><?php echo $item['aliran_air_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">4</td>
                <td>Kaitan dengan pemanfaatan air tanah</td>
                <td style="text-align: center;"><?php echo $item['kaitan_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">5</td>
                <td>Bahaya Banjir</td>
                <td style="text-align: center;"><?php echo $item['bhy_banjir_jumlah'];?></td>
              </tr>
              <tr>
                <td  align="center">6</td>
                <td>Tanah penutup</td>
                <td style="text-align: center;"><?php echo $item['penutup_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">7</td>
                <td>Intensitas hujan</td>
                <td style="text-align: center;"><?php echo $item['intens_hujan_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">8</td>
                <td>Jalan menuju lokasi</td>
                <td style="text-align: center;"><?php echo $item['jln_lokasi_jumlah'];?></td>
              </tr>
              <tr>
                <td  align="center">9</td>
                <td>Transport sampah (satu jalan)</td>
                <td style="text-align: center;"><?php echo $item['transport_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">10</td>
                <td>Jalan Masuk</td>
                <td style="text-align: center;"><?php echo $item['jln_masuk_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">11</td>
                <td>Lalu Lintas</td>
                <td style="text-align: center;"><?php echo $item['lalin_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">12</td>
                <td>Tata guna lahan</td>
                <td style="text-align: center;"><?php echo $item['tata_lahan_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">13</td>
                <td>Pertanian</td>
                <td style="text-align: center;"><?php echo $item['pertanian_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">14</td>
                <td>Daerah lindung / cagar alam</td>
                <td style="text-align: center;"><?php echo $item['cgr_alam_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">15</td>
                <td>Biologis</td>
                <td style="text-align: center;"><?php echo $item['biologis_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">16</td>
                <td>Kebisingan dan bau</td>
                <td style="text-align: center;"><?php echo $item['bau_jumlah'];?></td>
              </tr>
              <tr>
                <td align="center">17</td>
                <td>Estetika</td>
                <td style="text-align: center;"><?php echo $item['estetika_jumlah'];?></td>
              </tr>
              <tr>
                <td colspan="2" align="center"><b>TOTAL</b></td>
                <td style="text-align: center;"><b><?php echo $item['jumlah'];?></b></td>
              </tr>
            </tbody>          
          </table>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-edit<?php echo $item['id_kriteria'];?>" tabindex="-1">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        <i class="fas fa-edit"></i>
        Edit Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/kriteriaPenyisih_edit/<?php echo $item['id_kriteria'];?>">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th rowspan="2" class="center">No</th>
                  <th rowspan="2" class="center">Parameter</th>
                  <th colspan="3" class="center">Calon Lokasi TPA <div class="form-group">
                    <input type="text" class="form-control" name="calon_tpa" value="<?php echo $item['calon_tpa'];?>" required="required">
                  </div>
                  </th>
                </tr>
                <tr>
                  <th>Bobot</th>
                  <th>Nilai</th>
                  <th>Jumlah</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="5">I. UMUM</td>
                </tr>
                <tr>
                  <td rowspan="6" align="center">1</td>
                </tr>
                <tr>
                  <td>Batas Administrasi</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin_bobot" value="<?php echo $item['bts_admin_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="bts_admin_jumlah" onkeyup="sum();" name="bts_admin_jumlah" value="<?php echo $item['bts_admin_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Dalam batas administrasi</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin1" value="<?php echo $item['bts_admin1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Diluar batas administrasi tetapi dalam satu sistem pengelolaan TPA sampah terpadu</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin2" value="<?php echo $item['bts_admin2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Diluar batas administrasi dan diluar sistem pengelolaan TPA sampah terpadu</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin3" value="<?php echo $item['bts_admin3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Diluar batas administrasi</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin4" value="<?php echo $item['bts_admin4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="7" align="center">2</td>
                </tr>
                <tr>
                  <td>Pemilik hak atas tanah</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik_bobot" value="<?php echo $item['hak_pemilik_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="hak_pemilik_jumlah" onkeyup="sum();" name="hak_pemilik_jumlah" value="<?php echo $item['hak_pemilik_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Pemerintah daerah/pusat</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik1" value="<?php echo $item['hak_pemilik1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Pribadi (satu orang)</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik2" value="<?php echo $item['hak_pemilik2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Swasta/perusahaan (satu)</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik3" value="<?php echo $item['hak_pemilik3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Lebih dari satu pemilik hak dan status kepemilikan</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik4" value="<?php echo $item['hak_pemilik4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Organisasi sosial/agama</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="hak_pemilik5" value="<?php echo $item['hak_pemilik5'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="6" align="center">3</td>
                </tr>
                <tr>
                  <td>Kapasitas Lahan</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas_bobot" value="<?php echo $item['kapasitas_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="kapasitas_jumlah" onkeyup="sum();" name="kapasitas_jumlah" value="<?php echo $item['kapasitas_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>> 10 tahun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas1" value="<?php echo $item['kapasitas1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>5 tahun - 10 tahun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas2" value="<?php echo $item['kapasitas2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>3 tahun - 5 tahun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas3" value="<?php echo $item['kapasitas3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>kurang dari 3 tahun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kapasitas4" value="<?php echo $item['kapasitas4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="7" align="center">4</td>
                </tr>
                <tr>
                  <td>Jumlah pemilik tanah</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik_bobot" value="<?php echo $item['jml_pemilik_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="jml_pemilik_jumlah" onkeyup="sum();" name="jml_pemilik_jumlah" value="<?php echo $item['jml_pemilik_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1 kk</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik1" value="<?php echo $item['jml_pemilik1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>2 - 3 kk</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik2" value="<?php echo $item['jml_pemilik2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>4 - 5 kk</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik3" value="<?php echo $item['jml_pemilik3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>6 - 10 kk</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bts_admin_bobot" value="<?php echo $item['jml_pemilik4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>lebih dari 10 kk</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jml_pemilik5" value="<?php echo $item['jml_pemilik5'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">5</td>
                </tr>
                <tr>
                  <td>Partisipasi masyarakat</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas_bobot" value="<?php echo $item['part_mas_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="part_mas_jumlah" onkeyup="sum();" name="part_mas_jumlah" value="<?php echo $item['part_mas_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Spontan</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas1" value="<?php echo $item['part_mas1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Digerakkan</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas2" value="<?php echo $item['part_mas2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Negosiasi</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="part_mas3" value="<?php echo $item['part_mas3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td colspan="5">I. LINGKUNGAN FISIK</td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">1</td>
                </tr>
                <tr>
                  <td>Tanah (diatas muka air tanah)</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah_bobot" value="<?php echo $item['tanah_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="tanah_jumlah" onkeyup="sum();" name="tanah_jumlah" value="<?php echo $item['tanah_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Harga kelulusan < 10^-9 cm/det</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah1" value="<?php echo $item['tanah1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Harga kelulusan 10^-9 - 10^-6 cm/det</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah2" value="<?php echo $item['tanah2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Harga kelulusan >10^-6 cm/det, TOLAK kecuali ada masukan teknologi</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tanah3" value="<?php echo $item['tanah3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="6" align="center">2</td>
                </tr>
                <tr>
                  <td>Air Tanah</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah_bobot" value="<?php echo $item['air_tanah_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="air_tanah_jumlah" onkeyup="sum();" name="air_tanah_jumlah" value="<?php echo $item['air_tanah_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>≥ 10 m dengan kelulusan < 10^-6 cm/det</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah1" value="<?php echo $item['air_tanah1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>< 10 m dengan kelulusan < 10^-6 cm/det</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah2" value="<?php echo $item['air_tanah2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>≥ 10 m dengan kelulusan < 10^-6 - 10^-4 cm/det</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah3" value="<?php echo $item['air_tanah3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>< 10 m dengan kelulusan < 10^-6 - 10^-4 cm/det</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="air_tanah4" value="<?php echo $item['air_tanah4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">3</td>
                </tr>
                <tr>
                  <td>Sistem aliran air tanah</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air_bobot" value="<?php echo $item['aliran_air_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="aliran_air_jumlah" onkeyup="sum();" name="aliran_air_jumlah" value="<?php echo $item['aliran_air_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Discharge area/lokal</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air1" value="<?php echo $item['aliran_air1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Recharge area dan discharge area lokal</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air2" value="<?php echo $item['aliran_air2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Recharge area regional dan lokal</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="aliran_air3" value="<?php echo $item['aliran_air3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">4</td>
                </tr>
                <tr>
                  <td>Kaitan dengan pemanfaatan air tanah</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan_bobot" value="<?php echo $item['kaitan_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="kaitan_jumlah" onkeyup="sum();" name="kaitan_jumlah" value="<?php echo $item['kaitan_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Kemungkinan pemanfaatan rendah dengan batas hidrolis</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan1" value="<?php echo $item['kaitan1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Diproyeksikan untuk dimanfaatkan dengan batas hidrolis</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan2" value="<?php echo $item['kaitan2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Diproyeksikan untuk dimanfaatkan tanpa batas hidrolis</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="kaitan3" value="<?php echo $item['kaitan3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">5</td>
                </tr>
                <tr>
                  <td>Bahaya Banjir</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir_bobot" value="<?php echo $item['bhy_banjir_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="bhy_banjir_jumlah" onkeyup="sum();" name="bhy_banjir_jumlah" value="<?php echo $item['bhy_banjir_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Tidak ada bahaya banjir</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir1" value="<?php echo $item['bhy_banjir1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Kemungkinan banjir > 25 tahunan</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir2" value="<?php echo $item['bhy_banjir2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Kemungkinan bajir < 25 tahunan, TOLAK</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bhy_banjir3" value="<?php echo $item['bhy_banjir3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">6</td>
                </tr>
                <tr>
                  <td>Tanah penutup</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup_bobot" value="<?php echo $item['penutup_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="penutup_jumlah" onkeyup="sum();" name="penutup_jumlah" value="<?php echo $item['penutup_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Tanah penutup cukup</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup1" value="<?php echo $item['penutup1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Tanah penutup cukup sampai 1/2 umur pakai</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup2" value="<?php echo $item['penutup2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Tanah penutup tidak ada</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="penutup3" value="<?php echo $item['penutup3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">7</td>
                </tr>
                <tr>
                  <td>Intensitas hujan</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="intens_hujan_bobot" value="<?php echo $item['intens_hujan_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="intens_hujan_jumlah" onkeyup="sum();" name="intens_hujan_jumlah" value="<?php echo $item['intens_hujan_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Dibawah 500 mm pertahun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="intens_hujan1" value="<?php echo $item['intens_hujan1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Antara 500 mm sampai 1000 mm pertahun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="intens_hujan2" value="<?php echo $item['intens_hujan2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Diatas 1000 mm pertahun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="intens_hujan3" value="<?php echo $item['intens_hujan3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">8</td>
                </tr>
                <tr>
                  <td>Jalan menuju lokasi</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi_bobot" value="<?php echo $item['jln_lokasi_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="jln_lokasi_jumlah" onkeyup="sum();" name="jln_lokasi_jumlah" value="<?php echo $item['jln_lokasi_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Datar dengan kondisi baik</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi1" value="<?php echo $item['jln_lokasi1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Datar dengan kondisi buruk</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi2" value="<?php echo $item['jln_lokasi2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Naik/turun</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_lokasi3" value="<?php echo $item['jln_lokasi3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="6" align="center">9</td>
                </tr>
                <tr>
                  <td>Transport sampah (satu jalan)</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport_bobot" value="<?php echo $item['transport_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="transport_jumlah" onkeyup="sum();" name="transport_jumlah" value="<?php echo $item['transport_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Kurang dari 15 menit dari centroid sampah</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport1" value="<?php echo $item['transport1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Antara 16 menit - 30 menit dari centroid sampah</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport2" value="<?php echo $item['transport2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Antara 31 menit - 60 menit dari centroid sampah</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport3" value="<?php echo $item['transport3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Lebih dari 60 menit dari centroid sampah</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="transport4" value="<?php echo $item['transport4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">10</td>
                </tr>
                <tr>
                  <td>Jalan Masuk</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk_bobot" value="<?php echo $item['jln_masuk_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="jln_masuk_jumlah" onkeyup="sum();" name="jln_masuk_jumlah" value="<?php echo $item['jln_masuk_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Truk sampah tidak melalui daerah pemukiman</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk1" value="<?php echo $item['jln_masuk1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Truk sampah melalui daerah pemukiman berkepadatan sedang (≤ 300 jiwa/ha)</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk2" value="<?php echo $item['jln_masuk2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Truk sampah melalui daerah pemukiman berkepadatan sedang (≥300 jiwa/ha)</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="jln_masuk3" value="<?php echo $item['jln_masuk3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="6" align="center">11</td>
                </tr>
                <tr>
                  <td>Lalu Lintas</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin_bobot" value="<?php echo $item['lalin_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="lalin_jumlah" onkeyup="sum();" name="lalin_jumlah" value="<?php echo $item['lalin_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Terletak 500 dari jalan umum</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin1" value="<?php echo $item['lalin1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Terletak < 500 m pada lalu lintas rendah</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin2" value="<?php echo $item['lalin2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Terletak < 500 m pada lalu lintas sedang</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin3" value="<?php echo $item['lalin3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Terletak < 500 m pada lalu lintas tinggi</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="lalin4" value="<?php echo $item['lalin4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">12</td>
                </tr>
                <tr>
                  <td>Tata guna lahan</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan_bobot" value="<?php echo $item['tata_lahan_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="tata_lahan_jumlah" onkeyup="sum();" name="tata_lahan_jumlah" value="<?php echo $item['tata_lahan_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Mempunyai dampak sedikit terhadap tata guna tanah sekitar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan1" value="<?php echo $item['tata_lahan1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Mempunyai dampak sedang terhadap tata guna tanah sekitar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan2" value="<?php echo $item['tata_lahan2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Mempunyai dampak besar terhadap tata guna tanah sekitar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan3" value="<?php echo $item['tata_lahan3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="6" align="center">13</td>
                </tr>
                <tr>
                  <td>Pertanian</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="tata_lahan3" value="<?php echo $item['tata_lahan3'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="pertanian_jumlah" onkeyup="sum();" name="pertanian_jumlah" value="<?php echo $item['pertanian_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Berlokasi di lahan tidak produktif</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian1" value="<?php echo $item['pertanian1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Tidak ada dampak terhadap pertanian sekitar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian2" value="<?php echo $item['pertanian2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Ada pengaruh negatif terhadap pertanian sekitar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian3" value="<?php echo $item['pertanian3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Berlokasi di tanah pertanian produktif</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="pertanian4" value="<?php echo $item['pertanian4'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr> 
                <tr>
                  <td rowspan="5" align="center">14</td>
                </tr>
                <tr>
                  <td>Daerah lindung / cagar alam</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam_bobot" value="<?php echo $item['cgr_alam_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="cgr_alam_jumlah" onkeyup="sum();" name="cgr_alam_jumlah" value="<?php echo $item['cgr_alam_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Tidak ada daerah lindung / cagar alam di sekitarnya</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam1" value="<?php echo $item['cgr_alam1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Terdapat daerah lindung/ cagar alam di sekitarnya yang tidak terkena dampak negatif</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam2" value="<?php echo $item['cgr_alam2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Terdapat daerah lindung/ cagar alam di sekitarnya yang terkena dampak negatif</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="cgr_alam3" value="<?php echo $item['cgr_alam3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">15</td>
                </tr>
                <tr>
                  <td>Biologis</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis_bobot" value="<?php echo $item['biologis_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="biologis_jumlah" onkeyup="sum();" name="biologis_jumlah" value="<?php echo $item['biologis_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Nilai habitat yang rendah</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis1" value="<?php echo $item['biologis1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Nilai habitat yang tinggi</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis2" value="<?php echo $item['biologis2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Habitat kritis</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="biologis3" value="<?php echo $item['biologis3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">16</td>
                </tr>
                <tr>
                  <td>Kebisingan dan bau</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau_bobot" value="<?php echo $item['bau_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="bau_jumlah" onkeyup="sum();" name="bau_jumlah" value="<?php echo $item['bau_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Terdapat zona penyangga</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau1" value="<?php echo $item['bau1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Terdapat zona penyangga yang terbatas</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau2" value="<?php echo $item['bau2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Tidak terdapat penyangga</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="bau3" value="<?php echo $item['bau3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td rowspan="5" align="center">17</td>
                </tr>
                <tr>
                  <td>Estetika</td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika_bobot" value="<?php echo $item['estetika_bobot'];?>">
                    </div>
                  </td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="estetika_jumlah" onkeyup="sum();" name="estetika_jumlah" value="<?php echo $item['estetika_jumlah'];?>">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Operasi penimbunan tidak terlihat dari luar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika1" value="<?php echo $item['estetika1'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Operasi penimbunan sedikit terlihat dari luar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika2" value="<?php echo $item['estetika2'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Operasi penimbunan terlihat dari luar</td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="number" min="0" max="200" class="form-control" autosave="off" id="" name="estetika3" value="<?php echo $item['estetika3'];?>">
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td colspan="2" align="center">TOTAL</td>
                  <td></td>
                  <td></td>
                  <td style="text-align: center;">
                    <div class="form-group" style="width: 70px">
                      <input type="text" class="form-control" autosave="off" id="jumlah" name="jumlah" value="<?php echo $item['jumlah'];?>">
                    </div>
                  </td>
                </tr>
              </tbody>          
            </table>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success float-right" name="editKriteriaPenyisih" value="editKriteriaPenyisih" id="simpan">Simpan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-hapus<?php echo $item['id_kriteria'];?>" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center><p>Yakin Ingin Menghapus Data Calon Lokasi Kecamatan <?php echo ucwords($item['calon_tpa']);?> ?</p></center>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/kriteriaPenyisih_hapus/<?php echo $item['id_kriteria'];?>"><button type="button" class="btn btn-primary">Hapus</button></a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<?php endforeach;?>



