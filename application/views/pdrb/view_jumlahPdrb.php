<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">DATA PROYEKSI PDRB</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <form method="post" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB_hapus_checked" id="form-delete">
        <div class="mailbox-controls">
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah" data-toggle="tooltip" data-container="body" title="Tambah Data"><i class="fas fa-plus"></i> Tambah Data</button>
          <button type="button" class="btn btn-default btn-sm btn-refresh-card" onclick="window.location.reload();" data-toggle="tooltip" data-container="body" title="Reload Halaman"><i class="fa fa-sync-alt"></i></button>
          <button type="button" class="btn btn-default btn-sm" id="btn-delete" data-toggle="tooltip" data-container="body" title="Hapus data yang diceklis"><i class="far fa-trash-alt"></i></button>
          <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-proyeksi"data-toggle="tooltip" data-container="body" title="Lama Tahun Proyeksi">Proyeksi</button>
        </div>
        <table id="abcd" class="table table-bordered  table-hover">
          <thead>
            <tr>
              <th rowspan="2" width="2%"><input type="checkbox" id="check-all" value=""></th>
              <th rowspan="2" width="10%">Tahun</th>
              <th rowspan="2" width="10%">Tahun Ke</th>
              <th rowspan="2" width="20%">PDRB (dalam Rp)</th>
              <th colspan="2" width="40%">Pertumbuhan PDRB</th>
              <th colspan="2">Aksi</th>
              </tr>
            <tr>
              <th width="50px">Besaran (dalam Rp)</th>
              <th width="50px">Persen (%)</th>
              <th width="50px">Edit</th>
              <th width="50px">Hapus</th>
            </tr>
          </thead>
          <tbody>
            <?php
                    if ($dataProyeksipdrb->num_rows()>0) {
                      foreach ($dataProyeksipdrb->result_array() as $item) { 
                        $besaran = $item['besaran'];
                        $persen = $item['persen'];

                        if ($besaran == null || $persen == null) {
                          $besaran = '-';
                          $persen = '-';
                        }else{
                          $besaran = number_format($besaran,2);
                          $persen = round($persen,2);
                        }
                        ?>
             <tr>
              <td><input type='checkbox' class='check-item' name='id[]' value='<?php echo $item['id'];?>'/></td>
              <td data-toggle="tooltip" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td data-toggle="tooltip" data-container="body" title="Tahun Ke <?php echo $item['tahun_ke'];?>"><?php echo $item['tahun_ke'];?></td>
              <td data-toggle="tooltip" data-container="body" title="Rp <?php echo number_format($item['pdrb'],2);?>"><?php echo number_format($item['pdrb'],2);?></td>
              <td data-toggle="tooltip" data-container="body" title="Rp <?php echo $besaran;?>"><?php echo $besaran;?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $persen;?> %"><?php echo $persen;?></td>
              <td>
                <a class="btn red" href="#modal-edit<?php echo $item['id'];?>" data-toggle="tooltip" data-container="body" title="Edit Data Tahun <?php echo $item['tahun'];?>">
                  <i class="fas fa-edit" data-toggle="modal" data-target="#modal-edit<?php echo $item['id'];?>" id="edit"></i>
                </a>
              </td>
              <td align="center">
                <a class="btn red" href="#modal-hapus<?php echo $item['id'];?>" data-toggle="tooltip" data-container="body" title="Hapus Data Tahun <?php echo $item['tahun'];?>">
                  <i class="fas fa-trash" data-toggle="modal" data-target="#modal-hapus<?php echo $item['id'];?>" id="edit"></i>
                </a>
              </td>
            </tr>
        </tbody>
          <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="7" style="text-align: center;">No Result Data</td>
                    </tr>
                    <?php

                    }
                    ?>
        <tfoot>
          <?php foreach ($dataProyeksipdrbTotal->result_array() as $item) { ?>
          <tr>
            <td style="text-align: center;">#</td>
            <td align="center" colspan="2">JUMLAH</td>
            <td data-toggle="tooltip" data-container="body" title="Rp <?php echo number_format($item['pdrb_total'],2);?>"><?php echo number_format($item['pdrb_total'],2);?></td>
            <td data-toggle="tooltip" data-container="body" title="Rp <?php echo number_format($item['besaran_total'],2)?>"><?php echo number_format($item['besaran_total'],2);?></td>
            <td data-toggle="tooltip" data-container="body" title="<?php echo $item['persen_total'];?> %"><?php echo round($item['persen_total'],2)?></td>
            <td colspan="2" style="text-align: center;">#</td>
          </tr>
          <?php } ?>
        </tfoot>
      </table>
    </form>
      </div>
    </div>
  </div>
</div>


<!---modal---->

<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <i class="fas fa-plus"></i>
        Tambah Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <div class="form-group row">
              <label class="col-sm-5 control-label">Tahun</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="tahun" name="tahun" required="on" autofocus />
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Tahun Ke</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="tahun" name="tahun_ke" required="on" autofocus />
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Jumlah PDRB</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="pdrb" name="pdrb">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="tambahDataPDRB" value="tambahDataPDRB" id="simpan">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-proyeksi">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        Proyeksi
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php
        foreach($dataProyeksi_pdrb->result_array() as $item):
        ?>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <div class="form-group row">
              <label class="col-sm-4 control-label">Tahun Proyeksi</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="tahun" name="tahun_akhir" required="on" value="<?php echo $item['tahun_akhir'];?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4">Proyeksi</label>
              <div class="col-sm-7">
                <select name="proyeksi" class="form-control">
                  <option value="<?php echo $item['proyeksi15tahun'];?>">15 Tahun</option>
                  <option value="<?php echo $item['proyeksi30tahun'];?>">30 Tahun</option>
                  <option value="<?php echo $item['proyeksi50tahun'];?>">50 Tahun</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach;  ?>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="tambahDataProyeksi" value="tambahDataProyeksi" id="simpanTambah">Simpan</button>
        </div>
      </form>

    </div>
  </div>
</div>

<?php
        foreach($dataProyeksipdrb->result_array() as $item):
        ?>
<div class="modal fade" id="modal-edit<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <i class="fas fa-edit"></i>
        Edit Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB_edit/<?php echo $item['id'];?>">
        <div class="modal-body">
          <div class="card-body">
            <div class="form-group row">
              <label class="col-sm-5 control-label">Tahun</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="tahun" name="tahun" value="<?php echo $item['tahun'];?>" required="on">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Tahun Ke</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="tahun" name="tahun_ke" value="<?php echo $item['tahun_ke'];?>" required="on">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">PDRB</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="pdrb" name="pdrb" value="<?php echo $item['pdrb'];?>">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="editDataDrainase" value="editDataDrainase" id="simpan">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-hapus<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center><p>Yakin ingin menghapus data proyeksi PDRB tahun <?php echo $item['tahun'];?> ?</p></center>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB_hapus/<?php echo $item['id'];?>"><button type="button" class="btn btn-primary">Hapus</button></a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<?php endforeach;?>
