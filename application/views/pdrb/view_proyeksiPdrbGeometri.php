<div class="row">
  <div class="col-md-5">
    <div class="card card-green">
      <div class="card-header">
        <?php if ($jumlahdataproyeksi->num_rows()==14) {
            $a = "DATA PROYEKSI GEOMETRI SELAMA 15 TAHUN";
          } else if ($jumlahdataproyeksi->num_rows()==29){
            $a = "DATA PROYEKSI GEOMETRI SELAMA 30 TAHUN";
          }else if ($jumlahdataproyeksi->num_rows()==49) {
            $a = "DATA PROYEKSI GEOMETRI SELAMA 50 TAHUN";
          }else{
            $a = "DATA PROYEKSI GEOMETRI";
          }
              ?>
        <h5 class="card-title">
          <?php echo $a;?>
        </h5>
      <?php ?>
      </div>
      <div class="card-body">
        <table id="abc" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="30px">Tahun</th>
              <th width="50px">Besaran (dalam Rp)</th>
              <th width="50px">Pn</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($jumlahdataproyeksi->num_rows()>0) {
                  foreach ($dataProyeksipdrbGeometri->result_array() as $item) {
                    $besaran = $item['besaran'];
                    $pn = $item['pn'];

                    if ($besaran == null) {
                      $besaran = '-';
                      $pn = number_format($pn,2);
                    }else {
                      $besaran = number_format($besaran,2);
                      $pn = number_format($pn,2);
                    }
                    ?>
            <tr>
              <td style="text-align: center;" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td style="text-align: center;" data-container="body" title="Rp <?php echo $besaran;?>"><?php echo $besaran;?></td>
              <td data-container="body" title="Rp <?php echo $pn;?>"><?php echo $pn;?></td>
            </tr>
                <?php }} else { ?>
                    <tr>
                      <td colspan="3" style="text-align: center;">Tidak ada data proyeksi geometri, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB">Halaman Jumlah PDRB<a></td>
                    </tr>
                    <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card card-green">
      <div class="card-header">
        <h5 class="card-title">
          DATA STANDAR DEVIASI GEOMETRI
        </h5>
      </div>
      <div class="card-body">
        <table id="abcd" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tahun</th>
              <th>Besaran (dalam Rp)</th>
              <th>Hasil Hitung</th>
              <th>Yi - Ymean</th>
              <th>(Yi - Ymean)^2</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($jumlahdataproyeksi->num_rows()>0) {
              foreach ($dataProyeksipdrbGeometriStandar->result_array() as $item) { 
                ?>
            <tr>
              <td data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td data-container="body" title="Rp <?php echo number_format($item['besaran'],2);?>"><?php echo number_format($item['besaran'],2);?></td>
              <td data-container="body" title="Rp <?php echo number_format($item['hasil_hitung'],2);?>"><?php echo number_format($item['hasil_hitung'],2);?></td>
              <td data-container="body" title="Rp <?php echo number_format($item['yi_ymean'],2);?>"><?php echo number_format($item['yi_ymean'],2);?></td>
              <td data-container="body" title="Rp <?php echo number_format($item['yi_ymean2'],2);?>"><?php echo number_format($item['yi_ymean2'],2);?></td>
            </tr>
             <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="5" style="text-align: center;">Tidak ada data standar deviasi geometri, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB">Halaman Jumlah PDRB<a></td>
                    </tr>
                    <?php } ?>
          </tbody>
          <?php if ($jumlahdataproyeksi->num_rows()>0) {
                foreach ($dataProyeksipdrbGeometriStandarDeviasi->result_array() as $item) { ?>
          <tfoot>
            <tr>
              <td colspan="3">JUMLAH</td>
              <td data-container="body" title="Rp <?php echo number_format($item['sum_yi_ymean2'],2);?>"><?php echo number_format($item['sum_yi_ymean2'],2);?></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3">STANDAR DEVIASI</td>
              <td data-container="body" title="Rp <?php echo number_format($item['standar_deviasi'],2);?>"><?php echo number_format($item['standar_deviasi'],2);?></td>
              <td></td>
            </tr>
          </tfoot>
          <?php }} ?>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-outline card-white collapsed-card">
      <div class="card-header">
        <h3 class="card-title">RUMUS GEOMETRI</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-plus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <blockquote class="quote-green">
          <h1>Pn = Po + Ka(Ta - To)</h1>
          <p>Keterangan : <br>
            Pn = Jumlah penduduk pada tahun proyeksi<br>
            Po = Jumlah penduduk pada awal proyeksi<br>
            r  = Rata-rata pertumbuhan penduduk<br>
            n  = Kurun waktu proyeksi
          </p>
        </blockquote>
      </div>
      </div>
    </div>
  </div>
</div>