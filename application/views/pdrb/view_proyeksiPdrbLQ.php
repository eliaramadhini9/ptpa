<div class="row">
  <div class="col-md-12">
    <div class="card card-green">
      <div class="card-header">
        <?php if ($jumlahdataproyeksi->num_rows()==14) {
            $a = "DATA PROYEKSI LEAST SQUARE SELAMA 15 TAHUN";
          } else if ($jumlahdataproyeksi->num_rows()==29){
            $a = "DATA PROYEKSI LEAST SQUARE SELAMA 30 TAHUN";
          }else if ($jumlahdataproyeksi->num_rows()==49) {
            $a = "DATA PROYEKSI LEAST SQUARE SELAMA 50 TAHUN";
          }else{
            $a = "DATA PROYEKSI LEAST SQUARE";
          }
              ?>
        <h5 class="card-title">
          <?php echo $a;?>
        </h5>
      <?php ?>
      </div>
      <div class="card-body">
        <table id="abc" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="30px">Tahun</th>
              <th width="50px">Tahun Ke (X)</th>
              <th width="50px">Besaran (dalam Rp) (Y)</th>
              <th width="30px">XY</th>
              <th width="50px">X²</th>
              <th width="50px">Pn</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($jumlahdataproyeksi->num_rows()>0) {
              foreach ($dataProyeksipdrbLQ->result_array() as $item)  {  
              $besaran = $item['besaran'];
              $tahun_ke = $item['tahun_ke'];
              $xy = $item['xy'];
              $x2 = $item['x2'];
              $pn = $item['pn'];
              $sum_tahun_ke= $item['sum_tahun_ke'];
              $sum_besaran= $item['sum_besaran'];
              $sum_xy= $item['sum_xy'];
              $sum_x2= $item['sum_x2'];

              $avg_tahun_ke= $item['avg_tahun_ke'];
              $avg_besaran= $item['avg_besaran'];


              $sum_tahun_ke= number_format($sum_tahun_ke,0);
              $sum_besaran= number_format($sum_besaran,2);
              $sum_xy= number_format($sum_xy,2);
              $sum_x2= round($sum_x2,0);

                if ($besaran == null || $tahun_ke == null || $xy == null || $x2 == null) {
                  $besaran = '-';
                  $tahun_ke = '-';
                  $xy = '-';
                  $x2 = '-';
                  $pn = number_format($pn,2);
                }else{
                  $besaran = number_format($besaran,2);
                  $xy = number_format($xy,2);
                  $pn = number_format($pn,2);
                }
                ?>
            <tr>
              <td style="text-align: center;" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td style="text-align: center;" data-container="body" title="Tahun Ke-<?php echo $item['tahun_ke'];?>"><?php echo $tahun_ke;?></td>
              <td style="text-align: center;" data-container="body" title="Rp <?php echo $besaran;?>"><?php echo $besaran;?></td>
              <td style="text-align: center;" data-container="body" title="Rp <?php echo $xy;?>"><?php echo $xy;?></td>
              <td style="text-align: center;" data-container="body" title="<?php echo $x2;?>"><?php echo $x2;?></td>
              <td style="text-align: center;" data-container="body" title="Rp <?php echo $pn;?>"><?php echo $pn;?></td>

            </tr>
                <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="6" style="text-align: center;">Tidak ada data proyeksi least square, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB">Halaman Jumlah PDRB<a></td>
                    </tr>
                    <?php } ?>
          </tbody>
          <?php if ($jumlahdataproyeksi->num_rows()>0) {
                foreach ($dataProyeksipdrbLQTotal->result_array() as $item) { ?>
          <tfoot>
            <tr>
              <td style="text-align: center;">JUMLAH</td>
              <td style="text-align: center;" data-container="body" title="<?php echo $sum_tahun_ke;?>"><?php echo $sum_tahun_ke;?></td>
              <td style="text-align: center;" data-container="body" title="Rp <?php echo $sum_besaran;?>"><?php echo $sum_besaran;?></td>
              <td style="text-align: center;" data-container="body" title="Rp <?php echo $sum_xy;?>"><?php echo $sum_xy;?></td>
              <td style="text-align: center;" data-container="body" title="<?php echo $sum_x2;?>"><?php echo $sum_x2;?></td>
              <td></td>
            </tr>
            <tr>
              <td style="text-align: center;">RATA - RATA</td>
              <td style="text-align: center;" data-container="body" title="<?php echo round($avg_tahun_ke,0);?>"><?php echo round($avg_tahun_ke,0);?></td>
              <td style="text-align: center;" data-container="body" title="Rp <?php echo number_format($avg_besaran,2);?>"><?php echo number_format($avg_besaran,2);?></td>              
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tfoot>
          <?php }} ?>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-green">
      <div class="card-header">
        <h5 class="card-title">
          DATA STANDAR DEVIASI LEAST SQUARE
        </h5>
      </div>
      <div class="card-body">
        <table id="abcd" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tahun</th>
              <th>Besaran (dalam Rp)</th>
              <th>Hasil Hitung</th>
              <th>Yi - Ymean</th>
              <th>(Yi - Ymean)²</th>
            </tr>
          </thead>
          <tbody>
           <?php if ($jumlahdataproyeksi->num_rows()>0) {
              foreach ($dataProyeksipdrbLQStandar->result_array() as $item) { 
                $besaran    = $item['besaran'];
                $hasil_hitung    = $item['hasil_hitung'];
                $yi_ymean    = $item['yi_ymean'];
                $yi_ymean2    = $item['yi_ymean2'];

                $hasil10= number_format($besaran,2);
                $hasil11= number_format($hasil_hitung,2);
                $hasil12= number_format($yi_ymean,2);
                $hasil13= number_format($yi_ymean2,2)

                ?>
            <tr>
              <td data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td data-container="body" title="Rp <?php echo $hasil10;?>"><?php echo $hasil10;?></td>
              <td data-container="body" title="Rp <?php echo $hasil11;?>"><?php echo $hasil11;?></td>
              <td data-container="body" title="Rp <?php echo $hasil12;?>"><?php echo $hasil12;?></td>
              <td data-container="body" title="Rp <?php echo $hasil13;?>"><?php echo $hasil13;?></td>
            </tr>
            <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="5" style="text-align: center;">Tidak ada data standar deviasi least square, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPDRB">Halaman Jumlah PDRB<a></td>
                    </tr>
                    <?php } ?>
          </tbody>
           <?php if ($jumlahdataproyeksi->num_rows()>0) {
              foreach ($dataProyeksipdrbLQStandarDeviasi->result_array() as $item) { 
                ?>
          <tfoot>
            <tr>
              <td colspan="4" align="center">JUMLAH</td>
              <td data-container="body" title="Rp <?php echo number_format($item['sum_yi_ymean2'],2);?>"><?php echo number_format($item['sum_yi_ymean2'],2);?></td>
            </tr>
            <tr>
              <td colspan="4" align="center">STANDAR DEVIASI</td>
              <td data-container="body" title="Rp <?php echo number_format($item['standar_deviasi'],2);?>"><?php echo number_format($item['standar_deviasi'],2);?></td>
            </tr>
          </tfoot>
          <?php }} ?>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-outline card-white collapsed-card">
      <div class="card-header">
        <h3 class="card-title">RUMUS LEAST SQUARE</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-plus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <blockquote class="quote-green col-md-6">
          <h1>Pn = a + bx</h1>
          <p>Keterangan : <br>
            Pn = Jumlah penduduk pada tahun proyeksi<br>
            a = {(∑Y)(∑X²)-(∑X)(∑Y.X)}/{n(∑X²)-(∑X)²}<br>
            b = {n(∑Y.X)-(∑X)(∑Y)}/{n(∑X²)-(∑X)²}<br>
            x = nomer data tiap tahun
          </p>
        </blockquote>
      </div>
      </div>
    </div>
  </div>
</div>