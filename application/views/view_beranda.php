<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <?php
            foreach ($jumlahdataproyeksi_pp->result_array() as $item) : 
              if ($item['jumlah_data_proyeksi'] < 14) {
              ?>
        <h3 class="card-title">
          <i class="far fa-chart-bar mr-1"></i> Proyeksi Pertumbuhan Penduduk
        </h3>
        <?php } else {?>
        <h3 class="card-title">
          <i class="far fa-chart-bar  mr-1"></i> Proyeksi Pertumbuhan Penduduk Selama <?php echo $item['jumlah_data_proyeksi']; ?> Tahun
        </h3>
      <?php } endforeach; ?>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
         <div class="tab-content p-0">
          <!-- Morris chart - Sales -->
          <div class="chart tab-pane active" id="pendudukAritmatik" style="position: relative; height: 300px;">
            <div class="card-body">
            <p style="text-align: center;">Proyeksi Penduduk</p>
            <div class="chart">
              <canvas id="chartProyeksiPendudukAritmatik" style="height:250px; min-height:250px"></canvas>
            </div>
          </div>
                                   
           </div>
           <!-- <div class="chart tab-pane" id="pendudukGeometri" style="position: relative; height: 300px;">
            <div class="card-body">
            <p style="text-align: center;">Proyeksi Penduduk Menggunakan Metode Geometri</p>
            <div class="chart">
              <canvas id="chartProyeksiPendudukGeometri" style="height:250px; min-height:250px"></canvas>
            </div>
          </div>
                                   
           </div>
           <div class="chart tab-pane" id="pendudukLs" style="position: relative; height: 300px;">
            <div class="card-body">
            <p style="text-align: center;">Proyeksi Penduduk Menggunakan Metode Least Square</p>
            <div class="chart">
              <canvas id="chartProyeksiPendudukLS" style="height:250px; min-height:250px"></canvas>
            </div>
          </div>
                                   
           </div>
 -->
    </div>
  </div>
      <!-- ./card-body -->
      <!-- <div class="card-footer">
        <div class="row">
          <div class="col-sm-3 col-6">
            <div class="description-block border-right">
              <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 17%</span>
              <h5 class="description-header">$35,210.43</h5>
              <span class="description-text">TOTAL REVENUE</span>
            </div>
          </div>
          <div class="col-sm-3 col-6">
            <div class="description-block border-right">
              <span class="description-percentage text-warning"><i class="fas fa-caret-left"></i> 0%</span>
              <h5 class="description-header">$10,390.90</h5>
              <span class="description-text">TOTAL COST</span>
            </div>
          </div>
          <div class="col-sm-3 col-6">
            <div class="description-block border-right">
              <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 20%</span>
              <h5 class="description-header">$24,813.53</h5>
              <span class="description-text">TOTAL PROFIT</span>
            </div>
          </div>
          <div class="col-sm-3 col-6">
            <div class="description-block">
              <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 18%</span>
              <h5 class="description-header">1200</h5>
              <span class="description-text">GOAL COMPLETIONS</span>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>


<!-- CHART PDRB -->

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <?php
            foreach ($jumlahdataproyeksi_pdrb->result_array() as $item) : 
              if ($item['jumlah_data_proyeksi'] < 14) {
              ?>
        <h3 class="card-title">
          <i class="far fa-chart-bar  mr-1"></i> Proyeksi Pertumbuhan Nilai PDRB
        </h3>
        <?php } else {?>
        <h3 class="card-title">
          <i class="far fa-chart-bar  mr-1"></i> Proyeksi Pertumbuhan Nilai PDRB Selama <?php echo $item['jumlah_data_proyeksi']; ?> Tahun
        </h3>
      <?php } endforeach; ?>
        <!-- <div class="card-tools">
          <ul class="nav nav-pills ml-auto">
            <li class="nav-item">
              <a class="nav-link active" href="#pdrbAritmatik" data-toggle="tab">Metode Aritmatik</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#pdrbGeometri" data-toggle="tab">Metode Geometrik</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#pdrbLs" data-toggle="tab">Metode Least Square</a>
            </li>
          </ul>
        </div> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
         <div class="tab-content p-0">
          <!-- Morris chart - Sales -->
          <div class="chart tab-pane active" id="pdrbAritmatik" style="position: relative; height: 300px;">
            <div class="card-body">
            <p style="text-align: center;">Proyeksi PDRB (Produk Domestik Regional Bruto)</p>
            <div class="chart">
              <canvas id="chartProyeksiPdrbAritmatik" style="height:250px; min-height:250px"></canvas>
            </div>
          </div>
                                   
           </div>
           <!-- <div class="chart tab-pane" id="pdrbGeometri" style="position: relative; height: 300px;">
            <div class="card-body">
            <p style="text-align: center;">Proyeksi PDRB Menggunakan Metode Geometri</p>
            <div class="chart">
              <canvas id="chartProyeksiPdrbGeometri" style="height:250px; min-height:250px"></canvas>
            </div>
          </div>
                                   
           </div>
           <div class="chart tab-pane" id="pdrbLs" style="position: relative; height: 300px;">
            <div class="card-body">
            <p style="text-align: center;">Proyeksi PDRB Menggunakan Metode Least Square</p>
            <div class="chart">
              <canvas id="chartProyeksiPdrbLS" style="height:250px; min-height:250px"></canvas>
            </div>
          </div>
                                   
           </div> -->

    </div>
  </div>
      <!-- ./card-body -->
      <!-- <div class="card-footer">
        <div class="row">
          <div class="col-sm-3 col-6">
            <div class="description-block border-right">
              <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 17%</span>
              <h5 class="description-header">$35,210.43</h5>
              <span class="description-text">TOTAL REVENUE</span>
            </div>
          </div>
          <div class="col-sm-3 col-6">
            <div class="description-block border-right">
              <span class="description-percentage text-warning"><i class="fas fa-caret-left"></i> 0%</span>
              <h5 class="description-header">$10,390.90</h5>
              <span class="description-text">TOTAL COST</span>
            </div>
          </div>
          <div class="col-sm-3 col-6">
            <div class="description-block border-right">
              <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 20%</span>
              <h5 class="description-header">$24,813.53</h5>
              <span class="description-text">TOTAL PROFIT</span>
            </div>
          </div>
          <div class="col-sm-3 col-6">
            <div class="description-block">
              <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 18%</span>
              <h5 class="description-header">1200</h5>
              <span class="description-text">GOAL COMPLETIONS</span>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>









