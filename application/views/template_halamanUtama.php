<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="assets/maxi/img/favicon.png" type="image/png">
        <title>Sistem Informasi Perhitungan Luas Lahan TPA (Tempat Pemrosesan Akhir) Sampah</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/maxi/css/bootstrap.css">
        <link rel="stylesheet" href="assets/maxi/vendors/linericon/style.css">
        <link rel="stylesheet" href="assets/maxi/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/maxi/vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/maxi/vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="assets/maxi/vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="assets/maxi/vendors/animate-css/animate.css">
        <!-- main css -->
        <link rel="stylesheet" href="assets/maxi/css/style.css">
        <link rel="stylesheet" href="assets/maxi/css/responsive.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/adminlte3/css/adminlte.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="assets/adminlte3/plugins/fontawesome-free/css/all.min.css">
        <link rel="icon" href="../assets/maxi/img/favicon.png" type="image/png">
        <title>Maxitechture</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../assets/maxi/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/maxi/vendors/linericon/style.css">
        <link rel="stylesheet" href="../assets/maxi/css/font-awesome.min.css">
        <link rel="stylesheet" href="../assets/maxi/vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="../assets/maxi/vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="../assets/maxi/vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="../assets/maxi/vendors/animate-css/animate.css">
        <!-- main css -->
        <link rel="stylesheet" href="../assets/maxi/css/style.css">
        <link rel="stylesheet" href="../assets/maxi/css/responsive.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../assets/adminlte3/css/adminlte.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../assets/adminlte3/plugins/fontawesome-free/css/all.min.css">
        <style type="text/css">
            body {
                margin: 0;
                font-family: "Roboto",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                text-align: left;
                background-color: #fff;
            }
            #coba:hover{
                color: #f4e700;
            }
            #cobaAktif{
                color: #f4e700;
            }
            .carousel-control-prev-icon {
                background-color:#f4e700;
            }
            .carousel-control-next-icon {
                background-color:#f4e700;
            }
        </style>
    </head>

    <body>
        <header class="header_area">
            <div class="main_menu" id="mainNav">
              <nav class="navbar navbar-expand-md navbar-light">
                <div class="container box_1620">
                <!-- Brand and toggle get grouped for better mobile display -->
                <!-- <a class="navbar-brand logo_h" href="index.html"><img src="assets/maxi/img/logo.png" alt=""></a> -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item"><a class="nav-link" id="coba<?php echo $this->uri->segment(1)=='halamanUtama' ? 'Aktif': '' ?>" href="<?php echo base_url('halamanUtama');?>">Halaman Utama</a></li>
                        <li class="nav-item"><a class="nav-link" id="coba<?php echo $this->uri->segment(1)=='perencanaanTPA' ? 'Aktif': '' ?>" href="<?php echo base_url('perencanaanTPA');?>">Perencanaan TPA</a></li>
                        <li class="nav-item submenu dropdown">
                        <?php if ($this->uri->segment(1)=='dataKabupaten') { foreach ($namaKabupaten->result_array() as $value) { ?>
                            <a href="#" class="nav-link dropdown-toggle" id="coba<?php echo $this->uri->segment(1)=='dataKabupaten' ? 'Aktif': '' ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Proyeksi Kabupaten <u><?php echo ucwords($value['kabupaten']); ?></u></a>
                        <?php }} else{ ?>
                            <a href="#" class="nav-link dropdown-toggle" id="coba" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Proyeksi Kabupaten</a>
                        <?php } ?>
                                <ul class="dropdown-menu">
                                    <?php if ($dataKabupaten->num_rows()>0) {
                                        foreach ($dataKabupaten->result_array() as $value) { ?>
                                    <li class="nav-item"><a class="nav-link" tabindex="-1" href="<?php echo base_url();?>dataKabupaten/<?php echo $value['id_kab'];?>"><?php echo ucwords($value['kabupaten']);?></a></li>
                                    <?php }} else { ?>
                                    <li class="nav-item disabled"><a class="nav-link disabled">Tidak Ada Data</a></li>
                                    <?php } ?>
                                </ul>
                            </li> 
                            <li class="nav-item"><a class="nav-link" id="coba<?php echo $this->uri->segment(1)=='tentang' ? 'Aktif': '' ?>" href="<?php echo base_url('tentang');?>">Tentang</a></li>
                            <li class="nav-item"><a class="nav-link" id="coba" href="#modal-login" data-toggle="modal" data-target="#modal-login">Login</a></li>
                          </ul>
                        </div> 
                      </div>
                          </nav>
                        </div>
                    </header>
                    <!--================Header Menu Area =================-->
        
        <!--================Home Banner Area =================-->
        <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
              <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
        <div class="container">
          <div class="banner_content text-center">
            <h2 style="font-size: 35px;">Sistem Informasi Perhitungan</h2>
            <h2 style="font-size: 35px;">Luas Lahan TPA (Tempat Pemrosesan Akhir) Sampah</h2>
            <?php if($this->uri->segment(1)=='halamanUtama') { ?>
            <div class="page_link">
              <a>Halaman Utama</a>
            </div>            
        <?php } elseif($this->uri->segment(1)=='perencanaanTPA') { ?>
            <div class="page_link">
              <a>Perencanaan TPA</a>
            </div>
        <?php } elseif($this->uri->segment(1)=='tentang') { ?>
            <div class="page_link">
              <a>Tentang</a>
            </div>
        <?php } else foreach ($namaKabupaten->result_array() as $value) { ?>
            <div class="page_link">
              <a>Data Proyeksi</a>
              <a>Kabupaten <?php echo ucwords($value['kabupaten']); ?></a>
            </div>
        <?php } ?>
          </div>
        </div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Blog Area =================-->
        <br>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                <?php $this->load->view($view_name);?>
                
            </div>
        </div>
        </section>
        <!--================Blog Area =================-->
        
        <!--================Footer Area =================-->
        <footer class="footer_area p_120">
          <div class="container">
            <div class="row footer_inner">
              <div class="col-lg-5 col-sm-6">
                <aside class="f_widget news_widget">
                  <div class="f_title">
                    <h3>Pintasan</h3>
                  </div>
                  <p><a href="<?php echo base_url('halamanUtama');?>">Halaman Utama</a></p>
                  <p><a href="<?php echo base_url('perencanaanTPA');?>">Perencanaan TPA</a></p>
                  <p><a href="#">Data Kabupaten</a></p>
                  <ul>
                    <?php foreach ($dataKabupaten->result_array() as $value) { ?>
                    <li><a href="#"><?php echo ucwords($value['kabupaten']);?></a></li>
                  <?php } ?>
                  </ul>
                  <p><a href="<?php echo base_url('tentang');?>">Tentang</a></p>
                </aside>
              </div>
              <div class="col-lg-2">
                <aside class="f_widget social_widget">
                  <div class="f_title">
                    <h3>Ikuti Kami</h3>
                  </div>
                  <ul class="list">
                    <li><a href="https://www.facebook.com/kekeyferdiansyah"><i class="fab fa-facebook"></i></a></li>
                    <li><a href="https://www.twitter.com/RaRohali"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://www.instagram.com/eramadhini14"><i class="fab fa-instagram"></i></a></li>
                  </ul>
                </aside>
              </div>
            </div>
            <div class="copy_right_text">
              <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This system is made with love by <a href="https://www.facebook.com/kekeyferdiansyah" target="_blank">Elia Ramadhini</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
          </div>
        </footer>
        <!--================End Footer Area =================-->
        
<div class="modal fade" id="modal-login">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <div class="input-group mb-4">
          <input type="text" class="form-control" autocomplete="off" placeholder="Username" name="username" id="username" required autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-4">
          <input type="password" class="form-control" placeholder="Password" autocomplete="off" name="password" id="password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <?php foreach ($dataterakhir->result_array() as $item) :
          ?>
          <input type="hidden" class="form-control" name="id_kab" value="<?php echo $item['id_kab'];?>">

        <?php endforeach; ?>
          </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary float-right" value="loginValidation" name="loginValidation">Masuk</button>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>   
        
        
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/maxi/js/jquery-3.2.1.min.js"></script>
<script src="assets/maxi/js/popper.js"></script>
<script src="assets/maxi/js/bootstrap.min.js"></script>
<script src="assets/maxi/js/stellar.js"></script>
<script src="assets/maxi/vendors/lightbox/simpleLightbox.min.js"></script>
<script src="assets/maxi/vendors/nice-select/js/jquery.nice-select.min.js"></script>
<script src="assets/maxi/vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="assets/maxi/vendors/isotope/isotope-min.js"></script>
<script src="assets/maxi/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="assets/maxi/vendors/jquery-ui/jquery-ui.js"></script>
<script src="assets/maxi/js/jquery.ajaxchimp.min.js"></script>
<script src="assets/maxi/vendors/counter-up/jquery.waypoints.min.js"></script>
<script src="assets/maxi/vendors/counter-up/jquery.counterup.js"></script>
<script src="assets/maxi/vendors/popup/jquery.magnific-popup.min.js"></script>
<script src="assets/maxi/js/mail-script.js"></script>
<script src="assets/maxi/js/theme.js"></script>
<!-- jQuery -->
<script src="assets/adminlte3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="assets/adminlte3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="assets/adminlte3/plugins/chart.js/Chart.min.js"></script>
<!-- FastClick -->
<script src="assets/adminlte3/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="assets/adminlte3/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/adminlte3/js/demo.js"></script>
<!-- page script -->

<script src="../assets/maxi/js/jquery-3.2.1.min.js"></script>
<script src="../assets/maxi/js/popper.js"></script>
<script src="../assets/maxi/js/bootstrap.min.js"></script>
<script src="../assets/maxi/js/stellar.js"></script>
<script src="../assets/maxi/vendors/lightbox/simpleLightbox.min.js"></script>
<script src="../assets/maxi/vendors/nice-select/js/jquery.nice-select.min.js"></script>
<script src="../assets/maxi/vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="../assets/maxi/vendors/isotope/isotope-min.js"></script>
<script src="../assets/maxi/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="../assets/maxi/vendors/jquery-ui/jquery-ui.js"></script>
<script src="../assets/maxi/js/jquery.ajaxchimp.min.js"></script>
<script src="../assets/maxi/vendors/counter-up/jquery.waypoints.min.js"></script>
<script src="../assets/maxi/vendors/counter-up/jquery.counterup.js"></script>
<script src="../assets/maxi/vendors/popup/jquery.magnific-popup.min.js"></script>
<script src="../assets/maxi/js/mail-script.js"></script>
<script src="../assets/maxi/js/theme.js"></script>
<!-- jQuery -->
<script src="../assets/adminlte3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../assets/adminlte3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../assets/adminlte3/plugins/chart.js/Chart.min.js"></script>
<!-- FastClick -->
<script src="../assets/adminlte3/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/adminlte3/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/adminlte3/js/demo.js"></script>
<!-- page script -->

<script>
  <?php foreach($proyeksiPendudukGeometri as $geometri){
        $tahunGeometripp[] = $geometri->tahun; //ambil bulan
        $pnGeometripp[] = (float) $geometri->pn; //ambil nilai
    }          
  ?>
  $(function () {

    var barChartCanvas = $('#chartProyeksiPendudukGeometri').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunGeometripp);?>,
      datasets: [
        {
          label               : 'Jumlah Penduduk',
          borderWidth         : 2,
          backgroundColor     : 'rgba(50,205,50,0.6)',
          borderColor         : 'rgba(50,205,50,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnGeometripp);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }


    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })
  })
</script>

<script>
  <?php foreach($proyeksiPdrbGeometri as $geometri){
        $tahunGeometripdrb[] = $geometri->tahun; //ambil bulan
        $pnGeometripdrb[] = (float) $geometri->pn; //ambil nilai
    }          
  ?>
  $(function () {
    var barChartCanvas = $('#chartProyeksiPdrbGeometri').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($tahunGeometripdrb);?>,
      datasets: [
        {
          label               : 'Nilai PDRB',
          borderWidth         : 2,
          backgroundColor     : 'rgba(255,215,0,0.6)',
          borderColor         : 'rgba(255,215,0,0.9)',
          fill                : false,
          data                : <?php echo json_encode($pnGeometripdrb);?>
        },
      ]
    }

    var barChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }


    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(barChartCanvas, { 
      type: 'bar',
      data: barChartData, 
      options: barChartOptions
    })
  })
</script>

<script>
  <?php foreach($dataTimbulan as $timbulan){
        $tahunTimbulan[] = $timbulan->tahun;
        $timbulanDomestik[] = (float) $timbulan->timb_sampah_dom;
        $timbulanNonDomestik[] = (float) $timbulan->timb_sampah_nondom;
        $timbulanTotal[] = (float) $timbulan->total;
    }          
  ?>
  $(function () {
    var chartCanvas = $('#chartTimbulan').get(0).getContext('2d')

    var chartData = {
      labels  : <?php echo json_encode($tahunTimbulan);?>,
      datasets: [
        {
          label               : 'Timbulan Domestik',
          borderWidth         : 2,
          backgroundColor     : 'rgba(255,215,0,0.6)',
          borderColor         : 'rgba(255,215,0,0.9)',
          fill                : false,
          data                : <?php echo json_encode($timb_sampah_dom);?>
        },
      ]
    }

    var chartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: true
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : true,
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    var barChart       = new Chart(chartCanvas, { 
      type: 'bar',
      data: chartData, 
      options: chartOptions
    })
  })
</script>

</body>
</html>