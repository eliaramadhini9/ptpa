<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <?php if ($jumlahdataproyeksi->num_rows()==14) {
            $a = "DATA PROYEKSI TIMBULAN SAMPAH DOMESTIK SELAMA 15 TAHUN";
          } else if ($jumlahdataproyeksi->num_rows()==29){
            $a = "DATA PROYEKSI TIMBULAN SAMPAH DOMESTIK SELAMA 30 TAHUN";
          }else if ($jumlahdataproyeksi->num_rows()==49) {
            $a = "DATA PROYEKSI TIMBULAN SAMPAH DOMESTIK SELAMA 50 TAHUN";
          }else{
            $a = "DATA PROYEKSI TIMBULAN SAMPAH DOMESTIK";
          }
              ?>
        <h5 class="card-title">
          <?php echo $a;?>
        </h5>
      <?php ?>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <div class="mailbox-controls">
           
          <button type="button" class="btn btn-default btn-sm btn-refresh-card" onclick="window.location.reload();" data-toggle="tooltip" data-container="body" title="Reload Halaman"><i class="fa fa-sync-alt"></i></button>
          <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-data">Masukan Data</button>
        </div>
        <table id="abc" class="table table-bordered table-striped">

          <thead>
            <tr>
              <th>Tahun</th>
              <th>Jumlah Penduduk (jiwa)</th>
              <th>PDRB / kapita (Rp)</th>
              <th>Tingkat Pertumbuhan Penduduk (%)</th>
              <th>Tingkat Pertumbuhan PDRB (%)</th>
              <th>Tingkat Pertumbuhan Timbulan (%)</th>
              <th>Jumlah Timbulan Berdasarkan PDRB (L/orang/hari)</th>
              <th>Jumlah Timbulan Berdasarkan Penduduk (L/orang/hari)</th>
              <th>Timbulan per Kapita (L/org/hari)</th>
              <th>Total Timbulan Sampah (L/hr)</th>
              <th>Total Timbulan Sampah (m<sup>3</sup>/hr)</th>
              <th>Tingkat Pelayanan (%)</th>
              <th>Total Timbulan Sampah Terlayani (m<sup>3</sup>/hari)</th>
              <th>Tot Volume Sampah Domestik (m<sup>3</sup>/tahun)</th>
              </tr>
          </thead>
          <tbody>
            <?php

                $runningsum = 0;
            if ($dataTimbulanDomestik->num_rows()>0) {
              foreach ($dataTimbulanDomestik->result_array() as $item) :

                $timbulan_pdrb = $item['timbulan_pdrb'];
                $pert_timb = $item['pert_timb'];

                // $runningsum = $timbulan_pdrb;

                $runningsum += ($timbulan_pdrb + ($pert_timb/100));
            ?>
            <tr>
              <td align="center"><?php echo $item['tahun'];?></td>
              <td><?php echo round($item['penduduk'],2);?></td>
              <td><?php echo round($item['pdrb'],2);?></td>
              <td><?php echo $item['pert_pddk'];?></td>
              <td><?php echo $item['pert_pdrb'];?></td>
              <td><input style="border:none; background-color: transparent;" name="pert_timb" value="<?php echo $item['pert_timb'];?>"></td><td><input style="border:none; background-color: transparent;" name="runningsum" value="<?php echo $item['timbulan_pdrb'];?>"></td>
              <td><?php echo $item['timbulan_penduduk'];?></td>
              <td><?php echo $item['timb_kapita'];?></td>
              <td><?php echo $item['tot_timb_L'];?></td>
              <td><?php echo $item['tot_timb_m'];?></td>
              <td><?php echo $item['tingkat_pelayanan'];?></td>
              <td><?php echo $item['smph_terlayani'];?></td>
              <td><?php echo round($item['tot_vol'],2);?></td>
            </tr>
              <?php
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="14" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php foreach ($a_timbulan->result_array() as $a) :

            ?>
<?php foreach ($id_timbulan->result_array() as $item) :

            ?>
<div class="modal fade" id="modal-data">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
        Data Perhitungan Timbulan Sampah Domestik 
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun_domestik" value="<?php echo $item['tahun'];?>" readonly autofocus>
            <div class="form-group">
              <label class="control-label">Jumlah Timbulan berdasarkan PDRB</label>
              <div>
                <input type="text" class="form-control" name="timbulan_pdrb" required autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Timbulan berdasarkan Penduduk</label>
              <div>
                <input type="text" class="form-control" name="timbulan_pddk" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Tingkat Pelayanan</label>
              <div>
                <input type="text" class="form-control" name="tingkat_pelayanan" required>
              </div>
            </div>
            <input type="hidden" class="form-control" name="pert_pddk" value="<?php echo $a['pert_pddk'];?>">
            <input type="hidden" class="form-control" name="pert_timb" value="<?php echo $a['pert_timb'];?>">
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="jumlah_timbulan_pdrb" value="jumlah_timbulan_pdrb" id="simpanTambah">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="modal-penduduk">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        Jumlah Timbulan berdasarkan PDRB 
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun_timb_pddk" value="<?php echo $item['tahun'];?>" readonly autofocus>
            <div class="form-group row">
              <label class="col-sm-3 control-label">Jumlah</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="timbulan_pddk" required="on" autofocus>
              </div>
            </div>
            <input type="hidden" class="form-control" name="pert_pddk" value="<?php echo $a['pert_pddk'];?>" readonly autofocus>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="jumlah_timbulan_pddk" value="jumlah_timbulan_pddk" id="simpanTambah">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-layanan">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        Jumlah Timbulan berdasarkan PDRB 
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun_layanan" value="<?php echo $item['tahun'];?>" readonly autofocus>
            <div class="form-group row">
              <label class="col-sm-3 control-label">Jumlah</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="tingkat_pelayanan" required="on" autofocus>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="jumlah_tingkat_pelayanan" value="jumlah_tingkat_pelayanan" id="simpanTambah">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div> -->
<?php endforeach; ?>
<?php endforeach; ?>