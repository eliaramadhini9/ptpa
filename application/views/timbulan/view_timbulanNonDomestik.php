<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">DATA PROYEKSI TIMBULAN SAMPAH NON-DOMESTIK</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <div class="mailbox-controls">
          <button type="button" class="btn btn-default btn-sm btn-refresh-card" onclick="window.location.reload();" data-toggle="tooltip" data-container="body" title="Reload Halaman"><i class="fa fa-sync-alt"></i></button>
          <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-data">Masukan Data</button>
        </div>
        <table id="abc" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th rowspan="5">No</th>
              <th rowspan="5">Tahun</th>
              <th colspan="69">Uraian</th>
            </tr>
            <tr>
              <th colspan="2" rowspan="3">Penduduk</th>
              <th colspan="58">Timbulan Sampah Non-Domestik</th>
              <th colspan="5">Total Timbulan Sampah Non-Domestik</th>
              <th colspan="2">Total Timbulan Sampah Terlayani</th>
            </tr>
            <tr>
              <th colspan="16">Pendidikan</th>
              <th colspan="16">Peribadatan</th>
              <th colspan="10">Perdagangan dan Jasa</th>
              <th colspan="16">Kesehatan</th>
              <?php foreach ($dataTimbulanNonDomestik->result_array() as $item) {} ?>
              <th colspan="5"> Density <?php echo $item['density'];  ?> kg/m<sup>3</sup></th>
               <?php ?>
              <th colspan="2">Total</th>
            </tr>
            <tr>
              <th colspan="4">Taman Kanak - kanak</th>
              <th colspan="4">Sekolah Dasar</th>
              <th colspan="4">Sekolah Menengah Pertama</th>
              <th colspan="4">Sekolah Menengah Atas</th>
              <th colspan="4">Mushola</th>
              <th colspan="4">Masjid</th>
              <th colspan="4">Gereja</th>
              <th colspan="4">Pura/Klenteng/Vihara</th>
              <th colspan="3">Warung Makan/Restoran</th>
              <th colspan="4">Pasar dan Minimarket</th>
              <th colspan="3">Toko/Warung Klontong</th>
              <th colspan="4">Rumah Sakit</th>
              <th colspan="4">Rumah Bersalin</th>
              <th colspan="4">Puskesmas</th>
              <th colspan="4">Puskesmas Pembantu</th>
              <th colspan="5">Total</th>
              <th colspan="2">Total</th>
            </tr>
            <tr>
              <th>Luas Wilayah (km<sup>2</sup>)</th>
              <th>Jumlah Penduduk (jiwa)</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Orang</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Luas Pasar</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Jumlah Bed</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Jumlah Bed</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Jumlah Bed</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>Jumlah</th>
              <th>Jumlah Bed</th>
              <th>Unit timbulan sampah</th>
              <th>Jumlah timbulan sampah</th>
              <th>L / hr</th>
              <th>m<sup>3</sup> / hr</th>
              <th>m<sup>3</sup> / tahun</th>
              <th>kg / tahun</th>
              <th>ton / tahun</th>
              <th>Tingkat Pelayanan (%)</th>
              <th>Timbulan Sampah Terlayani</th>
            </tr>
          </thead>
          <tbody>
          <?php
            $no=1;
            if ($dataTimbulanNonDomestik->num_rows()>0) {
              foreach ($dataTimbulanNonDomestik->result_array() as $item) :
            ?>            
            <tr>
              <td align="center"><?php echo $no;?></td>
              <td align="center"><?php echo $item['tahun'];?></td>
              <td align="center"><?php echo number_format($item['luas_wilayah']);?></td>

              <td align="center"><?php echo $item['jml_penduduk'];?></td>
              <td align="center"><?php echo $item['tk_jumlah'];?></td>
              <td align="center"><?php echo $item['tk_orang'];?></td>
              <td align="center"><?php echo $item['tk_unit_sampah'];?></td>
              <td align="center"><?php echo $item['tk_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['sd_jumlah'];?></td>
              <td align="center"><?php echo $item['sd_orang'];?></td>
              <td align="center"><?php echo $item['sd_unit_sampah'];?></td>
              <td align="center"><?php echo $item['sd_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['smp_jumlah'];?></td>
              <td align="center"><?php echo $item['smp_orang'];?></td>
              <td align="center"><?php echo $item['smp_unit_sampah'];?></td>
              <td align="center"><?php echo $item['smp_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['sma_jumlah'];?></td>
              <td align="center"><?php echo $item['sma_orang'];?></td>
              <td align="center"><?php echo $item['sma_unit_sampah'];?></td>
              <td align="center"><?php echo $item['sma_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['mush_jumlah'];?></td>
              <td align="center"><?php echo $item['mush_orang'];?></td>
              <td align="center"><?php echo $item['mush_unit_sampah'];?></td>
              <td align="center"><?php echo $item['mush_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['mas_jumlah'];?></td>
              <td align="center"><?php echo $item['mas_orang'];?></td>
              <td align="center"><?php echo $item['mas_unit_sampah'];?></td>
              <td align="center"><?php echo $item['mas_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['grj_jumlah'];?></td>
              <td align="center"><?php echo $item['grj_orang'];?></td>
              <td align="center"><?php echo $item['grj_unit_sampah'];?></td>
              <td align="center"><?php echo $item['grj_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['pura_jumlah'];?></td>
              <td align="center"><?php echo $item['pura_orang'];?></td>
              <td align="center"><?php echo $item['pura_unit_sampah'];?></td>
              <td align="center"><?php echo $item['pura_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['wrg_jumlah'];?></td>
              <td align="center"><?php echo $item['wrg_unit_sampah'];?></td>
              <td align="center"><?php echo $item['wrg_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['mini_jumlah'];?></td>
              <td align="center"><?php echo $item['mini_luas'];?></td>
              <td align="center"><?php echo $item['mini_unit_sampah'];?></td>
              <td align="center"><?php echo $item['mini_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['toko_jumlah'];?></td>
              <td align="center"><?php echo $item['toko_unit_sampah'];?></td>
              <td align="center"><?php echo $item['toko_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['rs_jumlah'];?></td>
              <td align="center"><?php echo $item['rs_bed'];?></td>
              <td align="center"><?php echo $item['rs_unit_sampah'];?></td>
              <td align="center"><?php echo $item['rs_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['rsb_jumlah'];?></td>
              <td align="center"><?php echo $item['rsb_bed'];?></td>
              <td align="center"><?php echo $item['rsb_unit_sampah'];?></td>
              <td align="center"><?php echo $item['rsb_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['pusk_jumlah'];?></td>
              <td align="center"><?php echo $item['pusk_bed'];?></td>
              <td align="center"><?php echo $item['pusk_unit_sampah'];?></td>
              <td align="center"><?php echo $item['pusk_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['puskp_jumlah'];?></td>
              <td align="center"><?php echo $item['puskp_bed'];?></td>
              <td align="center"><?php echo $item['puskp_unit_sampah'];?></td>
              <td align="center"><?php echo $item['puskp_jml_timbulan'];?></td>

              <td align="center"><?php echo $item['tot_timb_lt'];?></td>
              <td align="center"><?php echo $item['tot_timb_mhr'];?></td>
              <td align="center"><?php echo $item['tot_timb_mth'];?></td>
              <td align="center"><?php echo $item['tot_timb_kg'];?></td>
              <td align="center"><?php echo $item['tot_timb_ton'];?></td>
              <td align="center"><?php echo $item['tingkat_pelayanan'];?></td>
              <td align="center"><?php echo $item['timb_smph_terlayani'];?></td>
            </tr>
              <?php
              $no++;
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="14" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php foreach ($tahunTimbulanNonDo->result_array() as $a) :

            ?>
            <?php foreach ($a_timbulannon->result_array() as $b) :

            ?>
<div class="modal fade" id="modal-data">
  <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
        Data Perhitungan Timbulan Sampah Non-Domestik 
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun" value="<?php echo $a['tahun'];?>" readonly>
            <input type="text" class="form-control" name="jml_pendudukb" value="<?php echo $b['jml_penduduk'];?>" readonly>
            <input type="hidden" class="form-control" name="jml_penduduka" value="<?php echo $a['jml_penduduk'];?>" readonly>
            <label class="control-label">A. PENDUDUK</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Luas Wilayah</label>
              <div>
                <input type="text" class="form-control" name="luas_wilayah" autofocus value="<?php echo $a['luas_wilayah'];?>">
              </div>
            </div>

            <label class="control-label">B. PENDIDIKAN</label>
            <div class="dropdown-divider"></div>
            <div class="row">
            <div class="col-md-3">
            <label class="control-label">1. Taman Kanak-kanak</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="tk_jumlah" autofocus value="<?php echo $a['tk_jumlah'];?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="tk_orang" value="<?php echo $a['tk_orang'];?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="tk_unit_sampah" value="<?php echo $a['tk_unit_sampah'];?>">
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">2. Sekolah Dasar</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="sd_jumlah" value="<?php echo $a['sd_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="sd_orang" value="<?php echo $a['sd_orang'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="sd_unit_sampah" value="<?php echo $a['sd_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">3. Sekolah Menengah Pertama</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="smp_jumlah" value="<?php echo $a['smp_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="smp_orang" value="<?php echo $a['smp_orang'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="smp_unit_sampah" value="<?php echo $a['smp_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">4. Sekolah Menengah Atas</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="sma_jumlah" value="<?php echo $a['sma_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="sma_orang" value="<?php echo $a['sma_orang'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="sma_unit_sampah" value="<?php echo $a['sma_unit_sampah'];?>" >
              </div>
            </div>
          </div>
          </div>

          <label class="control-label">C. PERIBADATAN</label>
            <div class="dropdown-divider"></div>
            <div class="row">
            <div class="col-md-3">
            <label class="control-label">1. Mushola</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="mush_jumlah" value="<?php echo $a['mush_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="mush_orang" value="<?php echo $a['mush_orang'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="mush_unit_sampah" value="<?php echo $a['mush_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">2. Masjid</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="mas_jumlah" value="<?php echo $a['mas_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="mas_orang" value="<?php echo $a['mas_orang'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="mas_unit_sampah" value="<?php echo $a['mas_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">3. Gereja</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="grj_jumlah" value="<?php echo $a['grj_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="grj_orang" value="<?php echo $a['grj_orang'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="grj_unit_sampah" value="<?php echo $a['grj_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">4. Pura/Klenteng/Vihara</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Gedung</label>
              <div>
                <input type="text" class="form-control" name="pura_jumlah" value="<?php echo $a['pura_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Jiwa</label>
              <div>
                <input type="text" class="form-control" name="pura_orang" value="<?php echo $a['pura_orang'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="pura_unit_sampah" value="<?php echo $a['pura_unit_sampah'];?>" >
              </div>
            </div>
          </div>
          </div>

          <label class="control-label">D. PERDAGANGAN DAN JASA</label>
            <div class="dropdown-divider"></div>
            <div class="row">
            <div class="col-md-4">
            <label class="control-label">1. Warung Makan / Restoran</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Warung / Restoran</label>
              <div>
                <input type="text" class="form-control" name="wrg_jumlah" value="<?php echo $a['wrg_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="wrg_unit_sampah" value="<?php echo $a['wrg_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-4">
            <label class="control-label">2. Pasar dan Minimarket</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Pasar dan Minimarket</label>
              <div>
                <input type="text" class="form-control" name="mini_jumlah" value="<?php echo $a['mini_jumlah'];?>"  autofocus>
                <input type="hidden" class="form-control" name="mini_jumlahb" value="<?php echo $b['mini_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Luas</label>
              <div>
                <input type="text" class="form-control" name="mini_luas" value="<?php echo $a['mini_luas'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="mini_unit_sampah" value="<?php echo $a['mini_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-4">
            <label class="control-label">3. Toko / Warung Kelontong</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Toko / Warung Kelontong</label>
              <div>
                <input type="text" class="form-control" name="toko_jumlah" value="<?php echo $a['toko_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="toko_unit_sampah" value="<?php echo $a['toko_unit_sampah'];?>" >
              </div>
            </div>
          </div>
          </div>

          <label class="control-label">E. KESEHATAN</label>
            <div class="dropdown-divider"></div>
            <div class="row">
            <div class="col-md-3">
            <label class="control-label">1. Rumah Sakit</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Rumah Sakit</label>
              <div>
                <input type="text" class="form-control" name="rs_jumlah" value="<?php echo $a['rs_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Bed</label>
              <div>
                <input type="text" class="form-control" name="rs_bed" value="<?php echo $a['rs_bed'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="rs_unit_sampah" value="<?php echo $a['rs_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">2. Rumah Bersalin</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Rumah Bersalin</label>
              <div>
                <input type="text" class="form-control" name="rsb_jumlah" value="<?php echo $a['rsb_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Bed</label>
              <div>
                <input type="text" class="form-control" name="rsb_bed" value="<?php echo $a['rsb_bed'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="rsb_unit_sampah" value="<?php echo $a['rsb_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">3. Puskesmas</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Puskesmas</label>
              <div>
                <input type="text" class="form-control" name="pusk_jumlah" value="<?php echo $a['pusk_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Bed</label>
              <div>
                <input type="text" class="form-control" name="pusk_bed" value="<?php echo $a['pusk_bed'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="pusk_unit_sampah" value="<?php echo $a['pusk_unit_sampah'];?>" >
              </div>
            </div>
          </div>
            <div class="col-md-3">
            <label class="control-label">4. Puskesmas Pembantu</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah Puskesmas Pembantu</label>
              <div>
                <input type="text" class="form-control" name="puskp_jumlah" value="<?php echo $a['puskp_jumlah'];?>"  autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Jumlah Bed</label>
              <div>
                <input type="text" class="form-control" name="puskp_bed" value="<?php echo $a['puskp_bed'];?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Unit Timbulan Sampah</label>
              <div>
                <input type="text" class="form-control" name="puskp_unit_sampah" value="<?php echo $a['puskp_unit_sampah'];?>" >
              </div>
            </div>
          </div>
          </div>

          <br>

          <label class="control-label">DENSITY</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah</label>
              <div>
                <input type="text" class="form-control" name="density" value="<?php echo $a['density'];?>" >
              </div>
            </div>
            <label class="control-label">TINGKAT PELAYANAN</label>
            <div class="dropdown-divider"></div>
            <div class="form-group">
              <label class="control-label">Jumlah</label>
              <div>
                <input type="text" class="form-control" name="tingkat_pelayanan" value="<?php echo $a['tingkat_pelayanan'];?>" >
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="tambahTimbulanNonDomestik" value="tambahTimbulanNonDomestik">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach; ?>
<?php endforeach; ?>