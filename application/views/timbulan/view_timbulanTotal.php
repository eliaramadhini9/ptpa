<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">DATA PROYEKSI TIMBULAN SAMPAH TOTAL</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="mailbox-controls">
          <button type="button" class="btn btn-default btn-sm btn-refresh-card" onclick="window.location.reload();" data-toggle="tooltip" data-container="body" title="Reload Halaman"><i class="fa fa-sync-alt"></i></button>
        </div>
        <table id="abc" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tahun</th>
              <th>Timbulan Sampah Domestik (m<sup>3</sup>/hari)</th>
              <th>Timbulan Sampah Non Domestik (m<sup>3</sup>/hari)</th>
              <th>Total Timbulan Sampah (m<sup>3</sup>/hari)</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ($dataTimbulanTotal->num_rows()>0) {
              foreach ($dataTimbulanTotal->result_array() as $item) :
            ?>
            <tr>
              <td align="center"><?php echo $item['tahun'];?></td>
              <td><?php echo round($item['timb_sampah_dom'],2);?></td>
              <td><?php echo round($item['timb_sampah_nondom'],2);?></td>
              <td><?php echo round($item['total'],2);?></td>
            </tr>
              <?php
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="4" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>