<div class="row">
  <!-- /.col -->
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Notifikasi Masuk</h3>
        <div class="card-tools">
          <div class="input-group input-group-sm">
            <input type="text" class="form-control" placeholder="Search Mail">
            <div class="input-group-append">
              <div class="btn btn-primary">
                <i class="fas fa-search"></i>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-tools -->
      </div>
      <!-- /.card-header -->
      <div class="card-body p-0">
        <div class="table-responsive mailbox-messages">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Kode</th>
                <th>Username</th>
                <th>Komentar</th>
                <th>Waktu</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($dataKomentar->num_rows()>0) { 
                $no=1;
                foreach ($dataKomentar->result_array() as $item) { 
                  if ($item['status']==0) {
                    ?>
            <tr>
              <td><a href="#modal-detail<?php echo $item['id'];?>" data-toggle="modal" data-target="#modal-detail<?php echo $item['id'];?>">N<?php echo $no; ?></a></td>
              <td><?php echo $item['username']; ?></td>
              <td><?php echo $item['komentar']; ?></td>
              <td><?php echo $item['tanggal']; ?></td>
              <td><?php echo $item['status']=='0' ? '<span class="badge badge-warning">belum dibaca</span>' : '' ?><?php echo $item['status']=='1' ? '<span class="badge badge-success">sudah dibaca</span>' : '' ?></td>
            </tr>
          <?php } else{ ?>
            <tr>
              <td>N<?php echo $no; ?></td>
              <td><?php echo $item['username']; ?></td>
              <td><?php echo $item['komentar']; ?></td>
              <td><?php echo $item['tanggal']; ?></td>
              <td><?php echo $item['status']=='0' ? '<span class="badge badge-warning">belum dibaca</span>' : '' ?><?php echo $item['status']=='1' ? '<span class="badge badge-success">sudah dibaca</span>' : '' ?></td>
            </tr>
          <?php } ?>
            <?php $no++; }} else{ ?>
              <td style="text-align: center;">Tidak ada komentar</td>
            <?php } ?>
            </tbody>
          </table>
          <!-- /.table -->
        </div>
        <!-- /.mail-box-messages -->
      </div>
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>

<?php $no=1;foreach ($dataKomentar->result_array() as $item) { ?>
<div class="modal" id="modal-detail<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          Komentar
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card card-primary">
            <div class="card-body">
              <div class="post">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="<?php base_url('') ?>../assets/adminlte3/img/user1-128x128.jpg" alt="user image">
                  <span class="username">
                    <a href="#"><?php echo $item['username'];?></a>
                    <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                  </span>
                  <span class="description"><?php echo $item['tanggal'];?></span>
                </div>
                <p><?php echo ucfirst($item['komentar']);?></p>
              </div>
            </div>
          </div>
          <input class="form-control form-control-sm" type="text" placeholder="balas komentar" onfocus="this.placeholder = ''" onblur="this.placeholder = 'balas komentar'">
          <input type="hidden" name="id" value="<?php echo $item['id']; ?>">
          <input type="hidden" name="kab_id" value="<?php echo $item['kab_id']; ?>">
          <div class="modal-footer justify-content-between">
            <a class="btn btn-default" href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/hapusKomentar/<?php echo $item['id'];?>">Hapus</a>
            <button type="submit" class="btn btn-primary float-right" name="status" value="status">OK</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>  