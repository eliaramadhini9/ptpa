<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          Data Kabupaten
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
      <form class="form-horizontal" method="POST">
        <div class="form-group row">
          <label class="col-sm-1 control-label">Kabupaten</label>
            <div class="col-sm-4">
              <div class="input-group input-group-sm">
                <input type="text" class="form-control" name="kabupaten" required="on">
                <input type="hidden" class="form-control" name="tahun" value="<?php echo date('Y') ?>">
                <span class="input-group-append">
                  <button type="submit" class="btn btn-default btn-flat" name="buatKabupaten" value="buatKabupaten">Tambah</button>
                </span>
              </div>
            </div>
          </div>
        </form>
        <table id="abcd" class="table table-bordered">
          <thead>
              <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Kabupaten</th>
                <th rowspan="2">Jumlah Kecamatan</th>
                <th colspan="2">Aksi</th>
              </tr>
              <tr>
                <th>Edit</th>
                <th>Hapus</th>
              </tr>
            </thead>
            <tbody>
            <?php
                  $no=1;
                  if ($dataKabupaten1->num_rows()>0) {
                    foreach ($dataKabupaten1->result_array() as $item) { ?>
             <tr>
                <td style="text-align: center;"><?php echo $no;?></td>
                <td><?php echo ucwords($item['kabupaten']);?></td>
                <td style="text-align: center;"><?php echo $item['jml_kec'];?> kecamatan <a href="<?php echo base_url();?><?php echo $item['id_kab'];?>/kecamatan">(lihat data)</a></td>
                <td align="center">
                  <a class="btn red" href="#edit-kabupaten<?php echo $item['id_kab'];?>">
                    <i class="fas fa-edit" data-toggle="modal" data-target="#edit-kabupaten<?php echo $item['id_kab'];?>"></i>
                  </a>
                </td>
                <td align="center">
                  <a class="btn red" href="#hapus-kabupaten<?php echo $item['id_kab'];?>">
                    <i class="fas fa-trash" data-toggle="modal" data-target="#hapus-kabupaten<?php echo $item['id_kab'];?>"></i>
                  </a>
                </td>
              </tr>
          <?php $no++;
                    }
                    }
                    
                    
                    else { ?>
                    <tr>
                      <td colspan="8">No Result Data</td>
                    </tr>
                    <?php

                    }
                    ?>

        </tbody>
      </table>
      </div>
    </div>
  </div>
</div>

 <?php
                    foreach ($dataKabupatenID->result_array() as $item) { ?>

<div class="modal fade" id="edit-kabupaten<?php echo $item['id_kab'];?>" tabindex="-1">
  <div class="modal-dialog modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          Edit Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/editKabupaten/<?php echo $item['id_kab'];?>">
        <div class="modal-body">
          <div class="card-body center-content">
            <div class="form-group">
              <label class="control-label">Kabupaten</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="kabupaten" required="on" value="<?php echo $item['kabupaten']; ?>" autofocus />
              </div>
            </div>
             <div class="form-group">
              <label class="control-label">Tahun dibuat</label>
              <div>
                <input type="text" class="form-control form-control-sm" name="tahun" required="on" value="<?php echo $item['tahun']; ?>">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success btn-sm float-right">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="hapus-kabupaten<?php echo $item['id_kab'];?>" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center><p>Yakin ingin menghapus data Kabupaten <?php echo ucwords($item['kabupaten']);?> ?</p></center>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
         <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/hapusKabupaten/<?php echo $item['id_kab'];?>"><button type="button" class="btn btn-primary">Hapus</button></a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<?php } ?>








