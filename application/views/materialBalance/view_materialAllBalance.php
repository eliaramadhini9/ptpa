<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">DATA MATERIAL BALANCE ALL</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <table id="abc" class="table table-bordered table-striped">

          <thead>
            <tr>
              <th>No</th>
              <th>Tahun</th>
              <th>Timbulan Sampah Terlayani</th>
              <th>Langsung dibuang ke TPA tanpa pengolahan</th>
              <!-- <th>Tk Pertumbuhan Penduduk (%)</th>
              <th>Tk Pertumbuhan PDRB (%)</th>
              <th>Tk Pertumbuhan Timbulan (%)</th>
              <th>Jumlah timbulan berdasarkan PDRB (L/orang/hari)</th>
              <th>Jumlah timbulan berdasarkan penduduk (L/orang/hari)</th>
              <th>Timbulan per kapita (L/org/hari)</th>
              <th>Tot Timblan Sampah (L/hr)</th>
              <th>Tot Timbulan Sampah (m3/hr)</th>
              <th>Tingkat Pelayanan (%)</th>
              <th>Total Timbulan Sampah Terlayani (m3/hari)</th>
              <th>Tot Volume Sampah Domestik (m3/tahun)</th> -->
              </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            if ($dataMaterialBalanceAll->num_rows()>0) {
              foreach ($dataMaterialBalanceAll->result_array() as $item) :
            ?>
            <tr>
              <td align="center"><?php echo $no;?></td>
              <td align="center"><?php echo $item['tahun'];?></td>
              <td><?php echo round($item['timbulan_smph_terlayani'],2);?></td>
              <td><?php echo round($item['langsung_ke_tpa'],2);?></td>
              <!-- <td><?php echo $item['pert_pddk'];?></td>
              <td><?php echo $item['pert_pdrb'];?></td>
              <td><input style="border:none; background-color: transparent;" name="pert_timb" value="<?php echo $item['pert_timb'];?>"></td><td><input style="border:none; background-color: transparent;" name="runningsum" value="<?php echo $item['timbulan_pdrb'];?>"></td>
              <td><?php echo $item['timbulan_penduduk'];?></td>
              <td><?php echo $item['timb_kapita'];?></td>
              <td><?php echo $item['tot_timb_L'];?></td>
              <td><?php echo $item['tot_timb_m'];?></td>
              <td><?php echo $item['tingkat_pelayanan'];?></td>
              <td><?php echo $item['smph_terlayani'];?></td>
              <td><?php echo round($item['tot_vol'],2);?></td> -->
            </tr>
              <?php
              $no++;
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="14" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">DATA PENGOLAHAN DI TPST</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <table id="tabel2" class="table table-bordered table-striped">

          <thead>
            <tr>
              <th>Tahun</th>
              <th>Sampah Masuk TPST</th>
              <th>Organik</th>
              <th>Anorganik</th>
              <th>Pengomposan</th>
              <th>Sisa Sampah Organik</th>
              <th>Kertas (0.83%)</th>
              <th>Kaca (0.18%)</th>
              <th>Plastik (13.30%)</th>
              <th>Logam (0.13%)</th>
              <th>Kayu (0.16%)</th>
              <th>Kain (0.15%)</th>
              <th>Karet (0.16%)</th>
              <th>Lain - Lain (5.09%)</th>
              <th>Total Sampah Anorganik</th>
              </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            if ($dataMaterialBalanceAll->num_rows()>0) {
              foreach ($dataMaterialBalanceAll->result_array() as $item) :
            ?>
            <tr>
              <td align="center"><?php echo $item['tahun'];?></td>
              <td align="center"><?php echo round($item['smph_masuk_tpst'],2);?></td>
              <td><?php echo round($item['organik'],2);?></td>
              <td><?php echo round($item['anorganik'],2);?></td>
              <td><?php echo round($item['pengomposan'],2);?></td>
              <td><?php echo round($item['sisa_organik'],2);?></td>
              <td><?php echo round($item['kertas'],2);?></td>
              <td><?php echo round($item['kaca'],2);?></td>
              <td><?php echo round($item['plastik'],2);?></td>
              <td><?php echo round($item['logam'],2);?></td>
              <td><?php echo round($item['kayu'],2);?></td>
              <td><?php echo round($item['kain'],2);?></td>
              <td><?php echo round($item['karet'],2);?></td>
              <td><?php echo round($item['lain_lain'],2);?></td>
              <td><?php echo round($item['tot_smph_anorganik'],2);?></td>
            </tr>
              <?php
              $no++;
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="15" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-pdrb">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        Jumlah Timbulan berdasarkan PDRB 
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun_domestik" value="<?php echo $item['tahun'];?>" readonly autofocus>
            <div class="form-group row">
              <label class="col-sm-3 control-label">Jumlah</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="timbulan_pdrb" required="on" autofocus>
              </div>
            </div>
            <input type="hidden" class="form-control" name="pert_timb" value="<?php echo $a['pert_timb'];?>" readonly autofocus>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="jumlah_timbulan_pdrb" value="jumlah_timbulan_pdrb" id="simpanTambah">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-penduduk">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        Jumlah Timbulan berdasarkan PDRB 
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun_timb_pddk" value="<?php echo $item['tahun'];?>" readonly autofocus>
            <div class="form-group row">
              <label class="col-sm-3 control-label">Jumlah</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="timbulan_pddk" required="on" autofocus>
              </div>
            </div>
            <input type="hidden" class="form-control" name="pert_pddk" value="<?php echo $a['pert_pddk'];?>" readonly autofocus>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="jumlah_timbulan_pddk" value="jumlah_timbulan_pddk" id="simpanTambah">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-layanan">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
        Jumlah Timbulan berdasarkan PDRB 
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <input type="hidden" class="form-control" name="tahun_layanan" value="<?php echo $item['tahun'];?>" readonly autofocus>
            <div class="form-group row">
              <label class="col-sm-3 control-label">Jumlah</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="tingkat_pelayanan" required="on" autofocus>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="jumlah_tingkat_pelayanan" value="jumlah_tingkat_pelayanan" id="simpanTambah">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>