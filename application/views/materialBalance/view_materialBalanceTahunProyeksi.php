<div class="row">
  <div class="col-6">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">Komposisi dan Karakteristik Sampah</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i></button>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <button type="button" class="btn btn-success btn-flat btn-sm" data-toggle="modal" data-target="#modal-tambah" data-toggle="tooltip" data-container="body" title="Tambah Data"><i class="fas fa-plus"></i> Tambah Komponen</button>
        <table id="abc" class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th rowspan="2">No</th>
              <th rowspan="2">Komponen Sampah</th>
              <th rowspan="2">%**</th>
              <th rowspan="2">Kadar* (%)</th>
              <th rowspan="2">N Kalor* (kkal/kg)</th>
              <th colspan="2">Aksi</th>
            </tr>
            <tr>
              <th>Edit</th>
              <th>Hapus</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            if ($dataMaterialBalanceTahun->num_rows()>0) {
              foreach ($dataMaterialBalanceTahun->result_array() as $item){
            ?>
            <tr>
              <td align="center"><?php echo $no;?></td>
              <td><?php echo ucwords($item['komponen']);?></td>
              <td style="text-align: center;"><?php echo $item['persen'];?></td>
              <td style="text-align: center;"><?php echo round($item['kadar_air'],2);?></td>
              <td style="text-align: center;"><?php echo round($item['kalor'],2);?></td>
              <td align="center">
                <a class="btn red" href="#modal-edit<?php echo $item['id'];?>" data-toggle="tooltip" data-container="body" title="Edit Data Tahun" id='edit'>
                  <i class="fas fa-edit" data-toggle="modal" data-target="#modal-edit<?php echo $item['id'];?>"></i>
                </a>
              </td>
              <td align="center">
                <a class="btn red" href="#modal-hapus<?php echo $item['id'];?>" data-toggle="tooltip" data-container="body" title="Hapus Data Tahun" id="hapus">
                  <i class="fas fa-trash" data-toggle="modal" data-target="#modal-hapus<?php echo $item['id'];?>"></i>
                </a>
              </td>
            </tr>
              <?php
              $no++; }}
              else { ?>
            <tr>
              <td colspan="8" style="text-align: center;">No Result Data</td>
            </tr>
          <?php } ?>
          </tbody>
            <?php foreach ($dataMaterialBalanceTahunTotal->result_array() as $item):?>
          <tfoot>
            <tr>
              <th style="text-align: center" colspan="2">T O T A L</th>
              <th style="text-align: center;"><?php echo $item['tot_persen'];?></th>
              <th style="text-align: center;"><?php echo $item['tot_kadar'];?></th>
              <th style="text-align: center;"><?php echo $item['tot_kalor'];?></th>
              <th style="text-align: center;" colspan="2">#</th>
            </tr>
          </tfoot>
        <?php endforeach; ?>
        </table>
        <p style="font-size: 5">* Sumber : BPS Provinsi Jawa Tengah Tahun 2016 
        <br>** Sumber : Dinas PU Kab. Jepara Tahun 2016</p>
      </div>
    </div>
  </div>

  <div class="col-6">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">Recovery Pengolahan Sampah Kabupaten</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i></button>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <table id="abcd" class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Jenis Sampah</th>
              <th>% Recovery Asumsi Rencana</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            if ($dataMaterialBalanceTahun1->num_rows()>0) {
              foreach ($dataMaterialBalanceTahun1->result_array() as $item) :

            ?>
            <tr>
              <td align="center"><?php echo $no;?></td>
              <td><?php echo ucwords($item['komponen']);?></td>
              <td style="text-align: center;"><?php echo $item['asumsi_rencana'];?><div class="tools">
                      <i class="fas fa-edit"data-toggle="modal" data-target="#modal-asumsi_rencana<?php echo $item['id'];?>"></i>
                    </div></td>
            </tr>
              <?php
              $no++;
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="3" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
          <?php foreach ($dataMaterialBalanceTahun1Total->result_array() as $item){?>
          <tfoot>
            <tr>
              <th style="text-align: center" colspan="2">T O T A L</th>
              <th style="text-align: center;"><?php echo $item['tot_asumsi'];?></th>
            </tr>
          </tfoot>
        <?php } ?>
        </table>
        <p style="font-size: 5">* Sumber : Trihadiningrum dkk, 2006</p>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">Total Komponen</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i></button>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <table id="abc" class="table table-bordered table-striped table-hover table-head-fixed">
          <thead>
            <tr>
              <th>No</th>
              <th>Komponen Sampah Anorganik</th>
              <th>%</th>
              <th>% Hitungan</th>
              <th>Total Komponen</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            if ($dataMaterialBalanceTahun2->num_rows()>0) {
              foreach ($dataMaterialBalanceTahun2->result_array() as $item) :
            ?>
            <tr>
              <td align="center"><?php echo $no;?></td>
              <td><?php echo ucwords($item['komponen']);?></td>
              <td style="text-align: center;"><?php echo round($item['persen'],2);?></td>
              <td style="text-align: center;"><?php echo round($item['hitungan'],2);?></td>
              <td style="text-align: center;"><?php echo round($item['tot_komponen'],2);?></td>
            </tr>
              <?php
              $no++;
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="14" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>
      <?php foreach ($dataMaterialBalanceTahunTotal->result_array() as $item) :
            ?>
          <tfoot>
            <tr>
              <th style="text-align: center" colspan="2">T O T A L</th>
              <th style="text-align: center;"><?php echo round($item['tot_persen1'],2);?></th>
              <th style="text-align: center;"><?php echo round($item['tot_hitungan'],2);?></th>
              <th style="text-align: center;"><?php echo round($item['total_komponen'],2);?></th>
            </tr>
          </tfoot>
        <?php endforeach; ?>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">Total Komponen</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i></button>
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">
        <table id="abc" class="table table-bordered table-striped table-hover table-head-fixed">
          <thead>
            <tr>
              <th>No</th>
              <th>Komponen</th>
              <th>Anorganik</th>
              <th>Total Sampah Recovery</th>
              <th>Sampah Anorganik Setelah Recovery</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            if ($dataMaterialBalanceTahun3->num_rows()>0) {
              foreach ($dataMaterialBalanceTahun3->result_array() as $a) :
            ?>
            <tr>
              <td align="center"><?php echo $no;?></td>
              <td><?php echo ucwords($a['komponen']);?></td>
              <td style="text-align: center;"><?php echo round($a['anorganik'],2);?></td>
              <td style="text-align: center;"><?php echo round($a['tot_smph_recovery'],2);?></td>
              <td style="text-align: center;"><?php echo round($a['setelah_recovery'],2);?></td>
            </tr>
              <?php
              $no++;
                endforeach;
                }
                
                else { ?>
            <tr>
              <td colspan="14" style="text-align: center;">No Result Data</td>
            </tr>
                <?php

                }
                ?>
          </tbody>

      <?php foreach ($dataMaterialBalanceTahunTotal->result_array() as $item) :
            ?>
          <tfoot>
            <tr>
              <th style="text-align: center" colspan="2">T O T A L</th>
              <th style="text-align: center;"><?php echo round($item['tot_anorganik'],2);?></th>
              <th style="text-align: center;"><?php echo round($item['tot_smph_recovery'],2);?></th>
              <th style="text-align: center;"><?php echo round($item['tot_setelah_recovery'],2);?></th>
            </tr>
          </tfoot>
        <?php endforeach; ?>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title">
          <i class="fas fa-plus"></i>
        Tambah Data Komponen dan Karakteristik Sampah
        </h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST">
        <div class="modal-body">
          <div class="card-body">
            <div class="form-group row">
              <label class="col-sm-5 control-label">Komponen Sampah</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="komponen" required autofocus>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Persen</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="persen">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Jumlah Kadar Air</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="kadar_air">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Jumlah Kalor</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="kalor">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right" name="tambahKomponenSampah" value="tambahKomponenSampah">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
        foreach($dataMaterialBalanceTahun->result_array() as $item):
        ?>
<div class="modal fade" id="modal-edit<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title">
          <i class="fas fa-edit"></i>
        Edit Data Komposisi dan Karakteristik Sampah 
        </h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/editKomponenSampah/<?php echo $item['id'];?>">
        <div class="modal-body">
          <div class="card-body">
            <div class="form-group row">
              <label class="col-sm-5 control-label">Komponen Sampah</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="komponen" value="<?php echo $item['komponen'];?>" required="on">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Persen</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="persen" value="<?php echo $item['persen'];?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Jumlah Kadar Air</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="kadar_air" value="<?php echo $item['kadar_air'];?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-5 control-label">Jumlah Kalor</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="kalor" value="<?php echo $item['kalor'];?>">
                <input type="hidden" class="form-control" name="asumsi_rencana" value="<?php echo $item['asumsi_rencana'];?>">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success float-right">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-asumsi_rencana<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
         Jenis Sampah <?php echo ucwords($item['komponen']);?>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="POST" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/editKomponenSampah/<?php echo $item['id'];?>">
        <div class="modal-body">
          <div class="card-body">
            <div class="form-group row">
              <label class="col-sm-6 control-label">Jumlah Recovery</label>
              <div class="col-sm-6">
                <input type="number" class="form-control" name="asumsi_rencana" value="<?php echo $item['asumsi_rencana'];?>" required="on">
                <input type="hidden" class="form-control" name="komponen" value="<?php echo $item['komponen'];?>">
                <input type="hidden" class="form-control" name="persen" value="<?php echo $item['persen'];?>">
                <input type="hidden" class="form-control" name="kadar_air" value="<?php echo $item['kadar_air'];?>">
                <input type="hidden" class="form-control" name="kalor" value="<?php echo $item['kalor'];?>">
              </div>
            </div>
          </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success btn-sm float-right">Simpan</button>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-hapus<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title">Hapus Data Komposisi dan Karakteristik Sampah</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center><p>Yakin ingin menghapus data komponen <?php echo $item['komponen'];?> ?</p></center>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
         <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/hapusKomponenSampah/<?php echo $item['id'];?>"><button type="button" class="btn btn-primary">Hapus</button></a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<?php endforeach;?>

