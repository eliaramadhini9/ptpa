<div class="modal fade" id="modal-xl<?php echo $item['id_kriteria'];?>">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Large Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th rowspan="2" class="center">No</th>
              <th rowspan="2" class="center">Parameter</th>
              <th colspan="3" class="center">Calon Lokasi TPA <?php echo $item['calon_tpa'];?></th>
            </tr>
            <tr>
              <th>Bobot</th>
              <th>Nilai</th>
              <th>Jumlah</th>
            </tr>
          </thead>
          <tbody>
          <?php
                    if ($dataKriteria->num_rows()>0) {
                      foreach ($dataKriteria->result_array() as $item) { 
                        ?>
            <tr>
              <td colspan="5">I. UMUM</td>
            </tr>
            <tr>
              <td rowspan="6" align="center">1</td>
            </tr>
            <tr>
              <td>Batas Administrasi</td>
              <td style="text-align: center;"><?php echo $item['bts_admin_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bts_admin_jumlah'];?></td>
            </tr>
            <tr>
              <td>Dalam batas administrasi</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bts_admin1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Diluar batas administrasi tetapi dalam satu sistem pengelolaan TPA sampah terpadu</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bts_admin2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Diluar batas administrasi dan diluar sistem pengelolaan TPA sampah terpadu</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bts_admin3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Diluar batas administrasi</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bts_admin4'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="7" align="center">2</td>
            </tr>
            <tr>
              <td>Pemilik hak atas tanah</td>
              <td style="text-align: center;"><?php echo $item['hak_pemilik_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['hak_pemilik_jumlah'];?></td>
            </tr>
            <tr>
              <td>Pemerintah daerah/pusat</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['hak_pemilik1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Pribadi (satu orang)</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['hak_pemilik2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Swasta/perusahaan (satu)</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['hak_pemilik3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Lebih dari satu pemilik hak dan status kepemilikan</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['hak_pemilik4'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Organisasi sosial/agama</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['hak_pemilik5'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">3</td>
            </tr>
            <tr>
              <td>Kapasitas Lahan</td>
              <td style="text-align: center;"><?php echo $item['kapasitas_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kapasitas_jumlah'];?></td>
            </tr>
            <tr>
              <td>> 10 tahun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kapasitas1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>5 tahun - 10 tahun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kapasitas2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>3 tahun - 5 tahun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kapasitas3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>kurang dari 3 tahun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kapasitas4'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="7" align="center">4</td>
            </tr>
            <tr>
              <td>Jumlah pemilik tanah</td>
              <td style="text-align: center;"><?php echo $item['jml_pemilik_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jml_pemilik_jumlah'];?></td>
            </tr>
            <tr>
              <td>1 kk</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jml_pemilik1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>2 - 3 kk</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jml_pemilik2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>4 - 5 kk</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jml_pemilik3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>6 - 10 kk</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jml_pemilik4'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>lebih dari 10 kk</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jml_pemilik5'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">5</td>
            </tr>
            <tr>
              <td>Partisipasi masyarakat</td>
              <td style="text-align: center;"><?php echo $item['part_mas_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['part_mas_jumlah'];?></td>
            </tr>
            <tr>
              <td>Spontan</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['part_mas1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Digerakkan</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['part_mas2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Negosiasi</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['part_mas3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="5">I. LINGKUNGAN FISIK</td>
            </tr>
            <tr>
              <td rowspan="5" align="center">1</td>
            </tr>
            <tr>
              <td>Tanah (diatas muka air tanah)</td>
              <td style="text-align: center;"><?php echo $item['tanah_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tanah_jumlah'];?></td>
            </tr>
            <tr>
              <td>Harga kelulusan < 10^-9 cm/det</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tanah1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Harga kelulusan 10^-9 - 10^-6 cm/det</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tanah2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Harga kelulusan >10^-6 cm/det, TOLAK kecuali ada masukan teknologi</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tanah3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">2</td>
            </tr>
            <tr>
              <td>Air Tanah</td>
              <td style="text-align: center;"><?php echo $item['air_tanah_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['air_tanah_jumlah'];?></td>
            </tr>
            <tr>
              <td>≥ 10 m dengan kelulusan < 10^-6 cm/det</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['air_tanah1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>< 10 m dengan kelulusan < 10^-6 cm/det</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['air_tanah2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>≥ 10 m dengan kelulusan < 10^-6 - 10^-4 cm/det</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['air_tanah3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>< 10 m dengan kelulusan < 10^-6 - 10^-4 cm/det</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['air_tanah4'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">3</td>
            </tr>
            <tr>
              <td>Sistem aliran air tanah</td>
              <td style="text-align: center;"><?php echo $item['aliran_air_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['aliran_air_jumlah'];?></td>
            </tr>
            <tr>
              <td>Discharge area/lokal</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['aliran_air1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Recharge area dan discharge area lokal</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['aliran_air2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Recharge area regional dan lokal</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['aliran_air3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">4</td>
            </tr>
            <tr>
              <td>Kaitan dengan pemanfaatan air tanah</td>
              <td style="text-align: center;"><?php echo $item['kaitan_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kaitan_jumlah'];?></td>
            </tr>
            <tr>
              <td>Kemungkinan pemanfaatan rendah dengan batas hidrolis</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kaitan1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Diproyeksikan untuk dimanfaatkan dengan batas hidrolis</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kaitan2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Diproyeksikan untuk dimanfaatkan tanpa batas hidrolis</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['kaitan3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">5</td>
            </tr>
            <tr>
              <td>Bahaya Banjir</td>
              <td style="text-align: center;"><?php echo $item['bhy_banjir_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bhy_banjir_jumlah'];?></td>
            </tr>
            <tr>
              <td>Tidak ada bahaya banjir</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bhy_banjir1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Kemungkinan banjir > 25 tahunan</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bhy_banjir2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Kemungkinan bajir < 25 tahunan, TOLAK</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bhy_banjir3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">6</td>
            </tr>
            <tr>
              <td>Tanah penutup</td>
              <td style="text-align: center;"><?php echo $item['penutup_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['penutup_jumlah'];?></td>
            </tr>
            <tr>
              <td>Tanah penutup cukup</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['penutup1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Tanah penutup cukup sampai 1/2 umur pakai</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['penutup2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Tanah penutup tidak ada</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['penutup3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">7</td>
            </tr>
            <tr>
              <td>Intensitas hujan</td>
              <td style="text-align: center;"><?php echo $item['intens_hujan_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['intens_hujan_jumlah'];?></td>
            </tr>
            <tr>
              <td>Dibawah 500 mm pertahun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['intens_hujan1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Antara 500 mm sampai 1000 mm pertahun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['intens_hujan2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Diatas 1000 mm pertahun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['intens_hujan3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">8</td>
            </tr>
            <tr>
              <td>Jalan menuju lokasi</td>
              <td style="text-align: center;"><?php echo $item['jln_lokasi_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jln_lokasi_jumlah'];?></td>
            </tr>
            <tr>
              <td>Datar dengan kondisi baik</td>
              <td>
              </td>
              <td style="text-align: center;"><?php echo $item['jln_lokasi1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Datar dengan kondisi buruk</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jln_lokasi2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Naik/turun</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jln_lokasi3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">9</td>
            </tr>
            <tr>
              <td>Transport sampah (satu jalan)</td>
              <td style="text-align: center;"><?php echo $item['transport_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['transport_jumlah'];?></td>
            </tr>
            <tr>
              <td>Kurang dari 15 menit dari centroid sampah</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['transport1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Antara 16 menit - 30 menit dari centroid sampah</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['transport2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Antara 31 menit - 60 menit dari centroid sampah</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['transport3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Lebih dari 60 menit dari centroid sampah</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['transport4'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">10</td>
            </tr>
            <tr>
              <td>Jalan Masuk</td>
              <td style="text-align: center;"><?php echo $item['jln_masuk_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jln_masuk_jumlah'];?></td>
            </tr>
            <tr>
              <td>Truk sampah tidak melalui daerah pemukiman</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jln_masuk1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Truk sampah melalui daerah pemukiman berkepadatan sedang (≤ 300 jiwa/ha)</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jln_masuk2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Truk sampah melalui daerah pemukiman berkepadatan sedang (≥300 jiwa/ha)</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jln_masuk3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">11</td>
            </tr>
            <tr>
              <td>Lalu Lintas</td>
              <td style="text-align: center;"><?php echo $item['lalin_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['lalin_jumlah'];?></td>
            </tr>
            <tr>
              <td>Terletak 500 dari jalan umum</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['lalin1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Terletak < 500 m pada lalu lintas rendah</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['lalin2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Terletak < 500 m pada lalu lintas sedang</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['lalin3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Terletak < 500 m pada lalu lintas tinggi</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['lalin4'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">12</td>
            </tr>
            <tr>
              <td>Tata guna lahan</td>
              <td style="text-align: center;"><?php echo $item['tata_lahan_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tata_lahan_jumlah'];?></td>
            </tr>
            <tr>
              <td>Mempunyai dampak sedikit terhadap tata guna tanah sekitar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tata_lahan1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Mempunyai dampak sedang terhadap tata guna tanah sekitar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tata_lahan2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Mempunyai dampak besar terhadap tata guna tanah sekitar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['tata_lahan3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="6" align="center">13</td>
            </tr>
            <tr>
              <td>Pertanian</td>
              <td style="text-align: center;"><?php echo $item['pertanian_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['pertanian_jumlah'];?></td>
            </tr>
            <tr>
              <td>Berlokasi di lahan tidak produktif</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['pertanian1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Tidak ada dampak terhadap pertanian sekitar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['pertanian2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Ada pengaruh negatif terhadap pertanian sekitar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['pertanian3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Berlokasi di tanah pertanian produktif</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['pertanian4'];?></td>
              <td></td>
            </tr> 
            <tr>
              <td rowspan="5" align="center">14</td>
            </tr>
            <tr>
              <td>Daerah lindung / cagar alam</td>
              <td style="text-align: center;"><?php echo $item['cgr_alam_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['cgr_alam_jumlah'];?></td>
            </tr>
            <tr>
              <td>Tidak ada daerah lindung / cagar alam di sekitarnya</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['cgr_alam1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Terdapat daerah lindung/ cagar alam di sekitarnya yang tidak terkena dampak negatif</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['cgr_alam2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Terdapat daerah lindung/ cagar alam di sekitarnya yang terkena dampak negatif</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['cgr_alam3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">15</td>
            </tr>
            <tr>
              <td>Biologis</td>
              <td style="text-align: center;"><?php echo $item['biologis_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['biologis_jumlah'];?></td>
            </tr>
            <tr>
              <td>Nilai habitat yang rendah</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['biologis1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Nilai habitat yang tinggi</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['biologis2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Habitat kritis</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['biologis3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">16</td>
            </tr>
            <tr>
              <td>Kebisingan dan bau</td>
              <td style="text-align: center;"><?php echo $item['bau_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bau_jumlah'];?></td>
            </tr>
            <tr>
              <td>Terdapat zona penyangga</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bau1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Terdapat zona penyangga yang terbatas</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bau2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Tidak terdapat penyangga</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['bau3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td rowspan="5" align="center">17</td>
            </tr>
            <tr>
              <td>Estetika</td>
              <td style="text-align: center;"><?php echo $item['estetika_bobot'];?></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['estetika_jumlah'];?></td>
            </tr>
            <tr>
              <td>Operasi penimbunan tidak terlihat dari luar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['estetika1'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Operasi penimbunan sedikit terlihat dari luar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['estetika2'];?></td>
              <td></td>
            </tr>
            <tr>
              <td>Operasi penimbunan terlihat dari luar</td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['estetika3'];?></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2" align="center">TOTAL</td>
              <td></td>
              <td></td>
              <td style="text-align: center;"><?php echo $item['jumlah'];?></td>
            </tr>
        <?php }} ?>
          </tbody>          
        </table>
      </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
