<div class="row">
  <div class="col-md-12">
    <div class="card card-green">
      <div class="card-header">
          <?php if ($jumlahdataproyeksi->num_rows()==14) {
            $a = "DATA PROYEKSI LEAST SQUARE SELAMA 15 TAHUN";
          } else if ($jumlahdataproyeksi->num_rows()==29){
            $a = "DATA PROYEKSI LEAST SQUARE SELAMA 30 TAHUN";
          }else if ($jumlahdataproyeksi->num_rows()==49) {
            $a = "DATA PROYEKSI LEAST SQUARE SELAMA 50 TAHUN";
          }else{
            $a = "DATA PROYEKSI LEAST SQUARE";
          }
              ?>
        <h5 class="card-title">
          <?php echo $a;?>
        </h5>
      <?php ?>
      </div>
      <div class="card-body">
        <table id="abcd" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="30px">Tahun</th>
              <th width="50px">Tahun Ke (X)</th>
              <th width="50px">Penduduk (Y)</th>
              <th width="30px">XY</th>
              <th width="50px">X²</th>
              <th width="50px">Pn</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($jumlahdataproyeksi->num_rows()>0) {
              foreach ($dataProyeksiPendudukLQ->result_array() as $item)  {
                  $penduduk = $item['penduduk'];
                  $xy = $item['xy'];

                if ($penduduk == null || $item['tahun_ke'] == null || $xy == null || $item['x2'] == null) {

                  $penduduk = '-';
                  $item['tahun_ke'] = '-';
                  $xy = '-';
                  $item['x2'] = '-';
                }else {
                    $penduduk = number_format($penduduk,2);
                    $xy = number_format($xy,2);
                  }
            ?>
            <tr>
              <td data-toggle="tooltip" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td data-toggle="tooltip" data-container="body" title="Tahun Ke-<?php echo $item['tahun_ke'];?>"><?php echo $item['tahun_ke'];?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $penduduk;?> jiwa"><?php echo $penduduk;?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $xy;?> jiwa"><?php echo $xy;?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $item['x2'];?>"><?php echo $item['x2'];?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['pn'],2);?> jiwa"><?php echo number_format($item['pn'],2);?></td>
            </tr>
                <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="6" style="text-align: center;">Tidak ada data proyeksi least square, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPenduduk">Halaman Jumlah Penduduk<a></td>
                    </tr>
                    <?php } ?>
          </tbody>
          <?php if ($jumlahdataproyeksi->num_rows()>0) {
                foreach ($dataProyeksiPendudukLQTotal->result_array() as $item) { ?>
          <tfoot>
            <tr>
              <td>JUMLAH</td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $item['sum_tahun_ke'];?>"><?php echo $item['sum_tahun_ke'];?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['sum_penduduk'],2);?> jiwa"><?php echo number_format($item['sum_penduduk'],2);?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['sum_xy'],2);?> jiwa"><?php echo number_format($item['sum_xy'],2);?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $item['sum_x2'];?>"><?php echo $item['sum_x2'];?></td>
              <td></td>

            </tr>
            <tr>
              <td>RATA - RATA</td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $item['avg_tahun_ke'];?>"><?php echo round($item['avg_tahun_ke'],0);?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['avg_penduduk'],2);?> jiwa"><?php echo number_format($item['avg_penduduk'],2);?></td>
              <td></td>
              <td></td>  
              <td></td>            
            </tr>
          </tfoot>
          <?php }} ?>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-green">
      <div class="card-header">
        <h5 class="card-title">
          DATA STANDAR DEVIASI LEAST SQUARE
        </h5>
      </div>
      <div class="card-body">
        <table id="abc" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tahun</th>
              <th>Penduduk</th>
              <th>Hasil Hitung</th>
              <th>Yi - Ymean</th>
              <th>(Yi - Ymean)²</th>
            </tr>
          </thead>
          <tbody>
           <?php if ($jumlahdataproyeksi->num_rows()>0) {
              foreach ($dataProyeksiPendudukLQStandar->result_array() as $item) { 
                ?>
            <tr>
              <td data-container="body" title="<?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td data-container="body" title="<?php echo $item['penduduk'];?>"><?php echo $item['penduduk'];?></td>
              <td data-container="body" title="<?php echo $item['hasil_hitung'];?>"><?php echo round($item['hasil_hitung'],2);?></td>
              <td data-container="body" title="<?php echo $item['yi_ymean'];?>"><?php echo round($item['yi_ymean'],2);?></td>
              <td data-container="body" title="<?php echo $item['yi_ymean2'];?>"><?php echo round($item['yi_ymean2'],2);?></td>
            </tr>
            <?php }} else {?>
                    <tr>
                      <td colspan="5" style="text-align: center;">Tidak ada data standar deviasi least square, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPenduduk">Halaman Jumlah Penduduk<a></td>
                    </tr>
                    <?php } ?>
          </tbody>
          <?php
            foreach ($jumlahdatapp->result_array() as $a) {
              $b = $a['jumlah_data'];
              if ($dataProyeksiPendudukLQ->num_rows()>$b) {
              foreach ($dataProyeksiPendudukLQStandarDeviasi->result_array() as $item) { 
                ?>
          <tfoot>
            <tr>
              <th colspan="4" align="center">JUMLAH</th>
              <th data-container="body" title="<?php echo $item['sum_yi_ymean2'];?>"><?php echo round($item['sum_yi_ymean2'],2);?></th>
            </tr>
            <tr>
              <th colspan="4" align="center">STANDAR DEVIASI</th>
              <th data-container="body" title="<?php echo $item['standar_deviasi'];?>"><?php echo round($item['standar_deviasi'],2);?></th>
            </tr>
          </tfoot>
          <?php
            }}}
            ?>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-outline card-white">
      <div class="card-header">
        <h3 class="card-title">RUMUS LEAST SQUARE</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <blockquote class="quote-green col-md-6">
          <h1>Pn = a + bx</h1>
          <p>Keterangan : <br>
            Pn = Jumlah penduduk pada tahun proyeksi<br>
            a = {(∑Y)(∑X²)-(∑X)(∑Y.X)}/{n(∑X²)-(∑X)²}<br>
            b = {n(∑Y.X)-(∑X)(∑Y)}/{n(∑X²)-(∑X)²}<br>
            x = nomer data tiap tahun
          </p>
        </blockquote>
      </div>
      </div>
    </div>
  </div>
</div>