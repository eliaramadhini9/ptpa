<div class="row">
  <div class="col-md-5">
    <div class="card card-green">
      <div class="card-header">
          <?php if ($jumlahdataproyeksi->num_rows()==14) {
            $a = "DATA PROYEKSI ARITMATIK SELAMA 15 TAHUN";
          } else if ($jumlahdataproyeksi->num_rows()==29){
            $a = "DATA PROYEKSI ARITMATIK SELAMA 30 TAHUN";
          }else if ($jumlahdataproyeksi->num_rows()==49) {
            $a = "DATA PROYEKSI ARITMATIK SELAMA 50 TAHUN";
          }else{
            $a = "DATA PROYEKSI ARITMATIK";
          }
              ?>
        <h5 class="card-title">
          <?php echo $a;?>
        </h5>
      <?php ?>
      </div>
      <div class="card-body">
        <table id="abcd" class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th width="30px">Tahun</th>
              <th width="50px">Penduduk</th>
              <th width="50px">Pn = Po - Ka (Ta-To)</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ($jumlahdataproyeksi->num_rows()>0) {
                  foreach ($dataProyeksiPendudukAritmatik->result_array() as $item) { 

                    $penduduk = $item['penduduk'];
                    $pn = $item['pn'];

                  if ($penduduk == null) {
                    $penduduk = '-';
                    $pn = number_format($pn,2);
                  }else {
                    $penduduk = number_format($penduduk,2);
                    $pn = number_format($pn,2);
                  }

                    ?>
            <tr>
              <td style="text-align: center;" data-toggle="tooltip" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td style="text-align: center;" data-toggle="tooltip" data-container="body" title="<?php echo $penduduk;?> jiwa"><?php echo $penduduk;?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo $pn;?> jiwa"><?php echo $pn;?></td>
            </tr>
                <?php
                    }
                    }
                else { ?>
            <tr>
              <td colspan="3" style="text-align: center;">Tidak ada data proyeksi aritmatik, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPenduduk">Halaman Jumlah Penduduk<a></td>
            </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card card-green">
      <div class="card-header">
        <h5 class="card-title">
          DATA STANDAR DEVIASI ARITMATIK
        </h5>
      </div>
      <div class="card-body">
        <table id="abc" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tahun</th>
              <th>Penduduk</th>
              <th>Hasil Hitung</th>
              <th>Yi - Ymean</th>
              <th>(Yi - Ymean)^2</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($jumlahdataproyeksi->num_rows()>0) {
                foreach ($dataProyeksiPendudukAritmatikStandar->result_array() as $item) { ?>
            <tr>
              <td data-toggle="tooltip" data-container="body" title="Tahun <?php echo $item['tahun'];?>"><?php echo $item['tahun'];?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['penduduk'],2);?> jiwa"><?php echo number_format($item['penduduk'],2);?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['hasil_hitung'],2);?> jiwa"><?php echo number_format($item['hasil_hitung'],2);?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['yi_ymean'],2);?> jiwa"><?php echo number_format($item['yi_ymean'],2);?></td>
              <td data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['yi_ymean2'],2);?> jiwa"><?php echo number_format($item['yi_ymean2'],2);?></td>
            </tr>
            <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="5" style="text-align: center;">Tidak ada data proyeksi aritmatik, tentukan proyeksi pada <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/proyeksiPenduduk">Halaman Jumlah Penduduk<a></td>
                    </tr>
                    <?php

                    }
                    ?>
          </tbody>
          <?php if ($jumlahdataproyeksi->num_rows()>0) {
                foreach ($dataProyeksiPendudukAritmatikStandarDeviasi->result_array() as $item) {?>
          <tfoot>
            <tr>
              <th colspan="3" align="center">JUMLAH</th>
              <th data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['sum_yi_ymean2'],2);?> jiwa"><?php echo number_format($item['sum_yi_ymean2'],2);?></th>
            </tr>
            <tr>
              <th colspan="3" align="center">STANDAR DEVIASI</th>
              <th data-toggle="tooltip" data-container="body" title="<?php echo number_format($item['standar_deviasi'],2);?> jiwa"><?php echo number_format($item['standar_deviasi'],2);?></th>
            </tr>
          </tfoot>
          <?php }} ?>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-outline card-white">
      <div class="card-header">
        <h3 class="card-title">RUMUS ARITMATIKA</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" data-container="body" title="Minimize"><i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <blockquote class="quote-green">
          <h1>Pn = Po + Ka(Ta - To)</h1>
          <p>Keterangan : <br>
            Pn = Jumlah penduduk pada tahun proyeksi<br>
            Po = Jumlah penduduk pada awal proyeksi<br>
            Ka = Rata - rata Pertumbuhan Penduduk Per-Tahun (Po-Pn)/(Ta-To)<br>
            Ta = Tahun Akhir<br>
            To = Tahun Awal
          </p>
        </blockquote>
      </div>
      </div>
    </div>
  </div>