<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">
          Data Drainase Kabupaten
          <button type="button" class="btn btn-success float-right btn-sm" data-toggle="modal" data-target="#tambah_data"><i class="fas fa-plus"></i> Tambah Data</button>
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <nav class="w-100">
          <div class="nav nav-tabs" id="product-tab" role="tablist">
            <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#satu" role="tab" aria-controls="product-desc" aria-selected="true">Data drainase menit maksimum</a>
          </div>
        </nav>
        <div class="tab-content p-3" id="nav-tabContent">
          <div class="tab-pane fade show active" id="satu" role="tabpanel" aria-labelledby="product-desc-tab">
            <div class="mailbox-controls">
              <button type="button" class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i></button>
              <button type="button" class="btn btn-default btn-sm btn-refresh-card" onclick="window.location.reload();"><i class="fa fa-sync-alt"></i></button>
              <!-- /.float-right -->
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th rowspan="2" class="center"><input type="checkbox" id="check-all"></th>
                  <th rowspan="2" class="center">Tahun</th>
                  <th colspan="10" class="center">Durasi Hujan (menit)</th>
                </tr>
                <tr>
                  <th>5</th>
                  <th>15</th>
                  <th>30</th>
                  <th>45</th>
                  <th>60</th>
                  <th>120</th>
                  <th>180</th>
                  <th>360</th>
                  <th>720</th>
                  <th>1440</th>
                </tr>
              </thead>
              <tbody>
              <?php
                      if ($dataDrainaseMenit->num_rows()>0) {
                        foreach ($dataDrainaseMenit->result_array() as $item) { 
                          ?>
                <tr>
                  <td align="center"><input type="checkbox" id="check-item" name="id_drainase[]" value='".$item->id_drainase."'></td>
                  <td align="center"><?php echo $item['tahun_drainase'];?></td>
                  <td align="center"><?php echo $item['menit5'];?></td>
                  <td align="center"><?php echo $item['menit15'];?></td>
                  <td align="center"><?php echo $item['menit30'];?></td>
                  <td align="center"><?php echo $item['menit45'];?></td>
                  <td align="center"><?php echo $item['menit60'];?></td>
                  <td align="center"><?php echo $item['menit120'];?></td>
                  <td align="center"><?php echo $item['menit180'];?></td>
                  <td align="center"><?php echo $item['menit360'];?></td>
                  <td align="center"><?php echo $item['menit720'];?></td>
                  <td align="center"><?php echo $item['menit1440'];?></td>
                </tr>
                 <?php
                    }
                    }
                  else { ?>
                <tr>
                  <td colspan="8">No Result Data</td>
                </tr>
                  <?php

                  }
                  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-outline collapsed-card card-green">
      <div class="card-header">
        <h3 class="card-title">Tambah Calon Lokasi TPA</h3> 
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></button>
        </div>
      </div>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_data">
  <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" method="post">
              <div class="row">
                <div class="col-md-2 pr-0">
                  <div class="form-group form-group-default">
                    <label>Tahun</label>
                    <input type="text" class="form-control" autosave="off" name="tahun_drainase">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>5</label>
                    <input type="number" class="form-control" autosave="off" name="menit5">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>15</label>
                    <input type="number" class="form-control" autosave="off" name="menit15">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>30</label>
                    <input type="number" class="form-control" autosave="off" name="menit30">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>45</label>
                    <input type="number" class="form-control" autosave="off" name="menit45">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>60</label>
                    <input type="number" class="form-control" autosave="off" name="menit60">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>120</label>
                    <input type="number" class="form-control" autosave="off" name="menit120">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>180</label>
                    <input type="number" class="form-control" autosave="off" name="menit180">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>360</label>
                    <input type="number" class="form-control" autosave="off" name="menit360">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>720</label>
                    <input type="number" class="form-control" autosave="off" name="menit720">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>1440</label>
                    <input type="number" class="form-control" autosave="off" name="menit1440">
                  </div>
                </div>
              </div>
            </form>
        </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-success float-right" name="tambahDataDrainase" value="tambahDataDrainase">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  $(document).ready(function() {
    //Enable check and uncheck all functionality
    $('#check-all').click(function () {
      if ($(this).is(":checked")) {
        $('.check-item').prop('checked', true) 
      } else {
        $('.check-item').prop('checked', false) 
      }
    })
  })
</script>

