<div class="row">
  <div class="col-12">
    <div class="card card-outline card-green">
      <div class="card-header">
        <h3 class="card-title">
          Data Drainase Kabupaten
        </h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="maximize" data-toggle="tooltip" data-container="body" title="Maximize"><i class="fas fa-expand"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <nav class="w-100">
          <div class="nav nav-tabs" id="product-tab" role="tablist">
            <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#satu" role="tab" aria-controls="product-desc" aria-selected="true">Data drainase menit maksimum</a>
            <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab" href="#dua" role="tab" aria-controls="product-comments" aria-selected="false">Data drainase yang di sejam-kan</a>
            <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab" href="#tiga" role="tab" aria-controls="product-comments" aria-selected="false">Hasil Analisa Frekuensi</a>
            <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab" href="#empat" role="tab" aria-controls="product-comments" aria-selected="false">Kala Ulang 25 Tahun</a>
            <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab" href="#lima" role="tab" aria-controls="product-comments" aria-selected="false">Kala Ulang 25 Tahun</a>
          </div>
        </nav>
        <div class="tab-content p-3 table-responsive" id="nav-tabContent">
          <div class="tab-pane fade show active" id="satu" role="tabpanel" aria-labelledby="product-desc-tab">
          <form method="post" action="<?php echo base_url('') ?>drainase_hapus_checked" id="form-delete">
            <div class="mailbox-controls">
              <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah_data"><i class="fas fa-plus" data-toggle="tooltip" data-container="body" title="Tambah Data"></i> Tambah Data</button>
              <button type="button" class="btn btn-default btn-sm btn-refresh-card" onclick="window.location.reload();" data-toggle="tooltip" data-container="body" title="Reload Halaman"><i class="fa fa-sync-alt"></i></button>
              <button type="button" class="btn btn-default btn-sm" id="btn-delete" data-toggle="tooltip" data-container="body" title="Hapus Data"><i class="far fa-trash-alt"></i></button>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th rowspan="2" width="5%"><input type="checkbox" id="check-all" value=""></th>
                  <th rowspan="2" width="10%">Tahun</th>
                  <th colspan="10" width="80%">Durasi Hujan (menit)</th>
                  <th colspan="2" width="5%">Aksi</th>
                </tr>
                <tr>
                  <th>5</th>
                  <th>15</th>
                  <th>30</th>
                  <th>45</th>
                  <th>60</th>
                  <th>120</th>
                  <th>180</th>
                  <th>360</th>
                  <th>720</th>
                  <th>1440</th>
                  <th>Edit</th>
                  <th>Hapus</th>
                </tr>
              </thead>
              <tbody>
              <?php
                      if ($dataDrainaseMenit->num_rows()>0) {
                        foreach ($dataDrainaseMenit->result_array() as $item) { 
                          ?>
                <tr>
                  <td align="center"><input type='checkbox' class='check-item' name='id[]' value='<?php echo $item['id'];?>'/></td>
                  <td align="center" class="mailbox-name"><?php echo $item['tahun'];?></td>
                  <td align="center"><?php echo $item['menit5'];?></td>
                  <td align="center"><?php echo $item['menit15'];?></td>
                  <td align="center"><?php echo $item['menit30'];?></td>
                  <td align="center"><?php echo $item['menit45'];?></td>
                  <td align="center"><?php echo $item['menit60'];?></td>
                  <td align="center"><?php echo $item['menit120'];?></td>
                  <td align="center"><?php echo $item['menit180'];?></td>
                  <td align="center"><?php echo $item['menit360'];?></td>
                  <td align="center"><?php echo $item['menit720'];?></td>
                  <td align="center"><?php echo $item['menit1440'];?></td>
                  <td align="center">
                    <a class="btn red" href="#modal-edit<?php echo $item['id'];?>" data-toggle="tooltip" data-container="body" title="Ubah Data">
                      <i class="fas fa-edit" data-toggle="modal" data-target="#modal-edit<?php echo $item['id'];?>"></i>
                    </a>
                  </td>
                  <td align="center">
                    <a class="btn red" href="#modal-edit<?php echo $item['id'];?>" data-toggle="tooltip" data-container="body" title="Hapus Data">
                      <i class="fas fa-trash" data-toggle="modal" data-target="#modal-hapus<?php echo $item['id'];?>"></i>
                    </a>
                  </td>
                </tr>
                 <?php
                    }
                    }
                  else { ?>
                <tr>
                  <td colspan="8">No Result Data</td>
                </tr>
                  <?php

                  }
                  ?>
              </tbody>
            </table>
          </form>
          </div>
          <div class="tab-pane fade" id="dua" role="tabpanel" aria-labelledby="product-comments-tab">
            <table id="example4" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th rowspan="2" width="20%">Tahun</th>
                  <th colspan="10" width="90%">Durasi Hujan (menit)</th>
                </tr>
                <tr>
                  <th>5</th>
                  <th>15</th>
                  <th>30</th>
                  <th>45</th>
                  <th>60</th>
                  <th>120</th>
                  <th>180</th>
                  <th>360</th>
                  <th>720</th>
                  <th>1440</th>
                </tr>
              </thead>

              <tbody>
                <?php
                        if ($dataDrainaseJam->num_rows()>0) {
                          foreach ($dataDrainaseJam->result_array() as $item) { 
                            ?>
                <tr>
                  <td align="center"><?php echo $item['tahun'];?></td>
                  <td align="center"><?php echo round($item['menit5jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit15jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit30jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit45jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit60jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit120jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit180jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit360jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit720jam'],2);?></td>
                  <td align="center"><?php echo round($item['menit1440jam'],2);?></td>
                </tr>
                  <?php
                            }
                            }
                            
                            else { ?>
                <tr>
                  <td colspan="8">No Result Data</td>
                </tr>
                  <?php

                  }
                  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th style="text-align: center">RATA - RATA</th>
                  <th style="text-align: center"><?php echo round($item['avg5'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg15'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg30'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg45'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg60'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg120'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg180'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg360'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg720'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['avg1440'],2);?></th>
                </tr>
                <tr>
                  <th style="text-align: center">STD DEV</th>
                  <th style="text-align: center"><?php echo round($item['stdev5'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev15'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev30'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev45'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev60'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev120'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev180'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev360'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev720'],2);?></th>
                  <th style="text-align: center"><?php echo round($item['stdev1440'],2);?></th>
                </tr>
              </tfoot>         
            </table>
          </div>
          <div class="tab-pane fade" id="tiga" role="tabpanel" aria-labelledby="product-comments-tab">
            <table id="example4" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="10%">Periode Ulang</th>
                  <th width="10%">Yt</th>
                  <th width="10%">5</th>
                  <th width="10%">15</th>
                  <th width="10%">30</th>
                  <th width="10%">45</th>
                  <th width="10%">60</th>
                  <th width="10%">120</th>
                  <th width="10%">180</th>
                  <th width="10%">360</th>
                  <th width="10%">720</th>
                  <th width="10%">1440</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                    if ($dataFrekuensiDrainase->num_rows()>0) {
                      foreach ($dataFrekuensiDrainase->result_array() as $item) { ?>
                <tr>
                  <td align="center"><?php echo $item['periode_ulang'];?></td>
                  <td align="center"><?php echo $item['yt'];?></td>
                  <td align="center"><?php echo $item['yt5'];?></td>
                  <td align="center"><?php echo $item['yt15'];?></td>
                  <td align="center"><?php echo $item['yt30'];?></td>
                  <td align="center"><?php echo $item['yt45'];?></td>
                  <td align="center"><?php echo $item['yt60'];?></td>
                  <td align="center"><?php echo $item['yt120'];?></td>
                  <td align="center"><?php echo $item['yt180'];?></td>
                  <td align="center"><?php echo $item['yt360'];?></td>
                  <td align="center"><?php echo $item['yt720'];?></td>
                  <td align="center"><?php echo $item['yt1440'];?></td>
                </tr>
                <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="13" style="text-align: center;">No Result Data</td>
                    </tr>
                    <?php

                    }
                    ?>
              </tbody>       
            </table>
          </div>
          <div class="tab-pane fade" id="empat" role="tabpanel" aria-labelledby="product-comments-tab">
            <table id="example4" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="10%">No</th>
                  <th width="10%">t</th>
                  <th width="10%">I</th>
                  <th width="10%">I x t</th>
                  <th width="10%">I2</th>
                  <th width="10%">I2 x t</th>
                  <th width="10%">Log t</th>
                  <th width="10%">Log I</th>
                  <th width="10%">Log t x Log I</th>
                  <th width="10%">(Log t)2</th>
                  <th width="10%">t1/2</th>
                  <th width="10%">I x t1/2</th>
                  <th width="10%">I2 x t1/2</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                    if ($dataKalaUlangDrainase->num_rows()>0) {
                      foreach ($dataKalaUlangDrainase->result_array() as $item) { ?>
                <tr>
                  <td align="center"><?php echo $item['id'];?></td>
                  <td align="center"><?php echo $item['t'];?></td>
                  <td align="center"><?php echo $item['i'];?></td>
                  <td align="center"><?php echo round($item['ixt'],2);?></td>
                  <td align="center"><?php echo round($item['i2'],2);?></td>
                  <td align="center"><?php echo round($item['i2xt'],2);?></td>
                  <td align="center"><?php echo round($item['logt'],2);?></td>
                  <td align="center"><?php echo round($item['logi'],2);?></td>
                  <td align="center"><?php echo round($item['logtxi'],2);?></td>
                  <td align="center"><?php echo round($item['logt2'],2);?></td>
                  <td align="center"><?php echo round($item['tset'],2);?></td>
                  <td align="center"><?php echo round($item['ixtset'],2);?></td>
                  <td align="center"><?php echo round($item['i2xtset'],2);?></td>
                </tr>
                <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="13" style="text-align: center;">No Result Data</td>
                    </tr>
                    <?php

                    }
                    ?>
                <tfoot>
                  <?php
                   foreach ($dataTotalKalaUlang->result_array() as $item): 
                            ?>
                  <tr>
                    <th></th>
                    <th style="text-align: center">S</th>
                    <th><?php echo $item['sum_i'];?></th>
                    <th><?php echo $item['sum_ixt'];?></th>
                    <th><?php echo round($item['sum_i2'],2);?></th>
                    <th><?php echo round($item['sum_i2xt'],2);?></th>
                    <th><?php echo round($item['sum_logt'],2);?></th>
                    <th><?php echo round($item['sum_logi'],2);?></th>
                    <th><?php echo round($item['sum_logtxi'],2);?></th>
                    <th><?php echo round($item['sum_logt2'],2);?></th>
                    <th></th>
                    <th><?php echo round($item['sum_ixtset'],2);?></th>
                    <th><?php echo round($item['sum_i2xtset'],2);?></th>
                  </tr>
                    <?php

                    endforeach;
                    ?>
                </tfoot>
              </tbody>       
            </table>
          </div>

          <div class="tab-pane fade" id="lima" role="tabpanel" aria-labelledby="product-comments-tab">
            <table id="example4" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th rowspan="2" width="10%">No</th>
                  <th rowspan="2" width="10%">t</th>
                  <th rowspan="2" width="10%">I</th>
                  <th colspan="3" width="30%">Talbot</th>
                  <th colspan="3" width="30%">Sherman</th>
                  <th colspan="3" width="30%">Ishiguro</th>
                </tr>
                <tr>
                  <th>I(1)</th>
                  <th>Delta(1)</th>
                  <th>Abs(1)</th>
                  <th>I(2)</th>
                  <th>Delta(2)</th>
                  <th>Abs(2)</th>
                  <th>I(3)</th>
                  <th>Delta(3)</th>
                  <th>Abs(3)</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                    if ($dataKalaUlangDrainase3->num_rows()>0) {
                      foreach ($dataKalaUlangDrainase3->result_array() as $item) { 

                        ?>
                <tr>
                  <td align="center"><?php echo $item['id'];?></td>
                  <td align="center"><?php echo $item['t'];?></td>
                  <td align="center"><?php echo $item['i'];?></td>
                  <td align="center"><?php echo round($item['talbot_i'],2);?></td>
                  <td align="center"><?php echo round($item['talbot_delta'],2);?></td>
                  <td align="center"><?php echo round($item['talbot_abs'],2);?></td>
                  <td align="center"><?php echo round($item['sherman_i'],2);?></td>
                  <td align="center"><?php echo round($item['sherman_delta'],2);?></td>
                  <td align="center"><?php echo round($item['sherman_abs'],2);?></td>
                  <td align="center"><?php echo round($item['ishiguro_i'],2);?></td>
                  <td align="center"><?php echo round($item['ishiguro_delta'],2);?></td>
                  <td align="center"><?php echo round($item['ishiguro_abs'],2);?></td>
                <?php
                    }
                    }
                    
                    else { ?>
                    <tr>
                      <td colspan="13" style="text-align: center;">No Result Data</td>
                    </tr>
                    <?php

                    }
                    ?>
                <tfoot>
                <?php foreach ($dataTotalKalaUlang25Tahun->result_array() as $item) : 

                        ?>
                  <tr>
                    <th style="text-align: center" colspan="3">Deviasi</th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['talbot_deviasi'],3);?></th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['sherman_deviasi'],3);?></th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['ishiguro_deviasi'],3);?></th>
                  </tr>
                  <tr>
                    <th style="text-align: center" colspan="3">Deviasi Rt</th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['talbot_deviasi_rt'],3);?></th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['sherman_deviasi_rt'],3);?></th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['ishiguro_deviasi_rt'],3);?></th>
                  </tr>
                  <tr>
                    <th style="text-align: center" colspan="3">StDev</th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['talbot_stddev'],3);?></th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['sherman_stddev'],3);?></th>
                    <th style="text-align: right;" colspan="3"><?php echo round($item['ishiguro_stddev'],3);?></th>                
                  </tr>
                <?php
                    endforeach;
                    ?>
                </tfoot>
              </tbody>       
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_data">
  <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <i class="fas fa-plus"></i>
        Tambah Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" method="POST">
              <div class="row">
                <div class="col-md-2 pr-0">
                  <div class="form-group form-group-default">
                    <label>Tahun</label>
                    <input type="text" class="form-control" autosave="off" name="tahun" autofocus>
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>5</label>
                    <input type="number" class="form-control" autosave="off" name="menit5">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>15</label>
                    <input type="number" class="form-control" autosave="off" name="menit15">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>30</label>
                    <input type="number" class="form-control" autosave="off" name="menit30">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>45</label>
                    <input type="number" class="form-control" autosave="off" name="menit45">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>60</label>
                    <input type="number" class="form-control" autosave="off" name="menit60">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>120</label>
                    <input type="number" class="form-control" autosave="off" name="menit120">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>180</label>
                    <input type="number" class="form-control" autosave="off" name="menit180">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>360</label>
                    <input type="number" class="form-control" autosave="off" name="menit360">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>720</label>
                    <input type="number" class="form-control" autosave="off" name="menit720">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>1440</label>
                    <input type="number" class="form-control" autosave="off" name="menit1440">
                  </div>
                </div>
              </div>
        </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success float-right" name="tambahDataDrainase" value="tambahDataDrainase" id="simpan">Simpan</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<?php
        foreach($dataDrainaseMenit->result_array() as $item):
        ?>

<div class="modal fade" id="modal-edit<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <i class="fas fa-edit"></i>
          Edit Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" method="POST" action="<?php echo base_url('') ?><?php echo $this->uri->segment(1) ?>/drainase_edit/<?php echo $item['id'];?>">
              <div class="row">
                <div class="col-md-2 pr-0">
                  <div class="form-group form-group-default">
                    <label>Tahun</label>
                    <input type="text" class="form-control" autosave="off" name="tahun" value="<?php echo $item['tahun'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>5</label>
                    <input type="text" class="form-control" autosave="off" name="menit5" value="<?php echo $item['menit5'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>15</label>
                    <input type="text" class="form-control" autosave="off" name="menit15" value="<?php echo $item['menit15'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>30</label>
                    <input type="text" class="form-control" autosave="off" name="menit30" value="<?php echo $item['menit30'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>45</label>
                    <input type="text" class="form-control" autosave="off" name="menit45" value="<?php echo $item['menit45'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>60</label>
                    <input type="text" class="form-control" autosave="off" name="menit60" value="<?php echo $item['menit60'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>120</label>
                    <input type="text" class="form-control" autosave="off" name="menit120" value="<?php echo $item['menit120'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>180</label>
                    <input type="text" class="form-control" autosave="off" name="menit180" value="<?php echo $item['menit180'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>360</label>
                    <input type="text" class="form-control" autosave="off" name="menit360" value="<?php echo $item['menit360'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>720</label>
                    <input type="text" class="form-control" autosave="off" name="menit720" value="<?php echo $item['menit720'];?>">
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group form-group-default">
                    <label>1440</label>
                    <input type="text" class="form-control" autosave="off" name="menit1440" value="<?php echo $item['menit1440'];?>">
                  </div>
                </div>
              </div>
        </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success float-right" name="editDataDrainase" value="editDataDrainase" id="simpan">Simpan</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-hapus<?php echo $item['id'];?>" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center><p>Yakin Ingin Menghapus Data Drainase Tahun <?php echo $item['tahun'];?> ?</p></center>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo base_url();?><?php echo $this->uri->segment(1) ?>/drainase_hapus/<?php echo $item['id'];?>"><button type="button" class="btn btn-primary">Hapus</button></a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<?php endforeach;?>


<script>
  $(function () {
    //Enable check and uncheck all functionality
    $('.checkbox-toggle').click(function () {
      var clicks = $(this).data('clicks')
      if (clicks) {
        //Uncheck all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').prop('checked', false)
        $('.checkbox-toggle .far.fa-check-square').removeClass('fa-check-square').addClass('fa-square')
      } else {
        //Check all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').prop('checked', true)
        $('.checkbox-toggle .far.fa-square').removeClass('fa-square').addClass('fa-check-square')
      }
      $(this).data('clicks', !clicks)
    })
  })
</script>

