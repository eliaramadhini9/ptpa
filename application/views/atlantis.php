
<script src="<?php base_url('') ?>assets/atlantis/js/core/jquery.3.2.1.min.js"></script>
  <script src="<?php base_url('') ?>assets/atlantis/js/core/popper.min.js"></script>
  <script src="<?php base_url('') ?>assets/atlantis/js/core/bootstrap.min.js"></script>
  <!-- jQuery UI -->
  <script src="<?php base_url('') ?>assets/atlantis/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
  <script src="<?php base_url('') ?>assets/atlantis/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
  <!-- Sweet Alert -->
  <script src="<?php base_url('') ?>assets/atlantis/js/plugin/sweetalert/sweetalert.min.js"></script>
  
  <!-- jQuery Scrollbar -->
  <script src="<?php base_url('') ?>assets/atlantis/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
  <!-- Atlantis JS -->
  <script src="<?php base_url('') ?>assets/atlantis/js/atlantis.min.js"></script>
  <!-- Atlantis DEMO methods, don't include it in your project! -->
  <script src="<?php base_url('') ?>assets/atlantis/js/setting-demo2.js"></script>

  <script type="text/javascript">
function delete_confirm(){
    if($('.checkbox:checked').length > 0){
        var result = confirm("Are you sure to delete selected users?");
        if(result){
            return true;
        }else{
            return false;
        }
    }else{
        alert('Select at least 1 record to delete.');
        return false;
    }
}
 
$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
  
    $('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
});
</script>


<script type="text/javascript">
  $(document).ready(function(){
    $(".btn-refresh-card").click(function(){
      var a=$(this).parents(".card");
      a.length&&(a.addClass("is-loading"),setTimeout(function(){
        a.removeClass("is-loading")
      },3e3))
  })
  });
</script>