<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_proyeksiPenduduk extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_proyeksiPenduduk');
		$this->load->model('model_kebutuhanLuasLahan');
		$this->load->model('model_proyeksiTimbulan');
		$this->load->model('model_notifikasi');
	}	

	public function proyeksiPenduduk()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahDataPenduduk')) {
			
			$this->model_proyeksiPenduduk->proyeksiPenduduk_tambah($kab_id);

			$this->model_proyeksiPenduduk->proyeksi_pp_delete($kab_id);
			$this->model_kebutuhanLuasLahan->proyeksikebutuhanLuasLahan_delete($kab_id);
			$this->model_proyeksiTimbulan->proyeksi_domestik_delete($kab_id);
			$this->model_proyeksiTimbulan->proyeksi_nondomestik_delete($kab_id);
	     	redirect($kab_id."/proyeksiPenduduk");
		}

 		if ($this->input->post('tambahDataProyeksi')) {
			$this->model_proyeksiPenduduk->proyeksi_pp_delete($kab_id);
			$this->model_proyeksiTimbulan->proyeksi_domestik_delete($kab_id);
			$this->model_proyeksiTimbulan->proyeksi_nondomestik_delete($kab_id);
			$this->model_kebutuhanLuasLahan->proyeksikebutuhanLuasLahan_delete($kab_id);

 			$a = $this->input->post('tahun_akhir');
 			$b = $this->input->post('proyeksi');

 			for ($i=$a+1; $i<=$b ; $i++) { 
			$this->model_proyeksiPenduduk->proyeksi_pp_tambah($i,$kab_id);
		}

 			for ($i=$a; $i<=$b ; $i++) { 
			$this->model_kebutuhanLuasLahan->proyeksiKebutuhanLuasLahan_tambah($i,$kab_id);
		}

		for ($o=$a-1; $o<=$b ; $o++) {
			$this->model_proyeksiTimbulan->proyeksi_domestik_tambah($o,$kab_id);
			$this->model_proyeksiTimbulan->proyeksi_nondomestik_tambah($o,$kab_id);
			$this->model_proyeksiTimbulan->proyeksi_nondomestik_tambah1($kab_id);
		}
	     	
	     	redirect($kab_id.'/proyeksiPendudukAritmatik');
 		}

    	$data['dataKabupaten'] = $this->model_proyeksiPenduduk->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPenduduk->Getnamakab($kab_id);
		$data['dataProyeksiPenduduk'] = $this->model_proyeksiPenduduk->GetDataProyeksiPenduduk($kab_id);
		$data['dataProyeksiPendudukTotal'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukTotal($kab_id);
		$data['dataProyeksi_pp'] = $this->model_proyeksiPenduduk->GetDataProyeksi_pp($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pp/view_jumlahPenduduk';
		$data['nama'] = 'Jumlah Penduduk';
		$this->load->view('template',$data);
	}

	public function proyeksiPenduduk_edit()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);

		$this->model_proyeksiPenduduk->proyeksiPenduduk_edit($id);
		redirect($kab_id."/proyeksiPenduduk");
	}

	public function proyeksiPenduduk_hapus()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		$this->model_proyeksiPenduduk->proyeksiPenduduk_hapus($id);
		$this->model_proyeksiPenduduk->proyeksi_pp_delete($kab_id);
		redirect($kab_id."/proyeksiPenduduk");
	}

	public function proyeksiPenduduk_hapus_checked() {
		$kab_id = $this->uri->segment(1);
		$id = $_POST['id']; // Ambil data NIS yang dikirim oleh view.php melalui form submit
        $this->model_proyeksiPenduduk->proyeksiPenduduk_hapus_checked($id);
		redirect($kab_id."/proyeksiPenduduk");
	}

	public function proyeksiPendudukAritmatik()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->model_proyeksiPenduduk->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPenduduk->Getnamakab($kab_id);
		$data['dataProyeksiPendudukAritmatik'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukAritmatik($kab_id);
		$data['dataProyeksiPendudukAritmatikStandar'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukAritmatikStandar($kab_id);
		$data['dataProyeksiPendudukAritmatikStandarDeviasi'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukAritmatikStandarDeviasi($kab_id);
		$data['jumlahdatapp'] = $this->model_proyeksiPenduduk->jumlah_data_pp($kab_id);
		$data['jumlahdataproyeksi'] = $this->model_proyeksiPenduduk->jumlah_data_proyeksi($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pp/view_proyeksiPendudukAritmatik';
		$data['nama'] = 'Proyeksi Penduduk Metode Aritmatik';
		$this->load->view('template',$data);
	}

	public function proyeksiPendudukGeometri()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->model_proyeksiPenduduk->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPenduduk->Getnamakab($kab_id);
		$data['dataProyeksiPendudukGeometri'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukGeometri($kab_id);
		$data['dataProyeksiPendudukGeometriStandar'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukGeometriStandar($kab_id);
		$data['dataProyeksiPendudukGeometriStandarDeviasi'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukGeometriStandarDeviasi($kab_id);
		$data['jumlahdatapp'] = $this->model_proyeksiPenduduk->jumlah_data_pp($kab_id);
		$data['jumlahdataproyeksi'] = $this->model_proyeksiPenduduk->jumlah_data_proyeksi($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pp/view_proyeksiPendudukGeometri';
		$data['nama'] = 'Proyeksi Penduduk Metode Geometri';
		$this->load->view('template',$data);
	}

	public function proyeksiPendudukLQ()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->model_proyeksiPenduduk->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPenduduk->Getnamakab($kab_id);
		$data['dataProyeksiPendudukLQ'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukLQ($kab_id);
		$data['dataProyeksiPendudukLQTotal'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukLQTotal($kab_id);
		$data['dataProyeksiPendudukLQStandar'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukLQStandar($kab_id);
		$data['dataProyeksiPendudukLQStandarDeviasi'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukLQStandarDeviasi($kab_id);
		$data['jumlahdatapp'] = $this->model_proyeksiPenduduk->jumlah_data_pp($kab_id);
		$data['jumlahdataproyeksi'] = $this->model_proyeksiPenduduk->jumlah_data_proyeksi($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pp/view_proyeksiPendudukLQ';
		$data['nama'] = 'Proyeksi Penduduk Metode Least Square';
		$this->load->view('template',$data);
	}
}
