<?php

class controller_halamanUtama extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('model_halamanUtama');
		$this->load->model('model_login');
		$this->load->model('model_proyeksiPenduduk');
		$this->load->model('model_proyeksiPDRB');
		$this->load->model('model_beranda');
	}

	public function index() {
		redirect(base_url('halamanUtama'));
	}

	public function halamanUtama() {
		$data['nama'] = 'Halaman Utama';
    	$data['dataKabupaten'] = $this->model_beranda->GetKab();
		$data['view_name'] = 'halamanUtama/view_halamanUtama';

		$data['dataterakhir'] = $this->model_login->Getdataterakhir();
		$id_kab = $this->input->post('id_kab');


		if ($this->input->post('loginValidation')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$login =$this->model_login->loginValidation($username,$password);
		
			if($login->num_rows() > 0){
				$dataSession = array(
					'login' => 'true',
					'username' => $username
				);
				$this->session->set_userdata($dataSession);			
				redirect(base_url($id_kab.'/beranda'));
			}else{
				$this->load->view('notification/loginFailed');
				$this->load->view('template_halamanUtama',$data);
			}
		}else{
			$this->load->view('template_halamanUtama',$data);
		}
	}

	public function perencanaanTPA() {
		$data['nama'] = 'Halaman Perencanaan TPA Sampah';
    	$data['dataKabupaten'] = $this->model_beranda->GetKab();
		$data['view_name'] = 'halamanUtama/view_perencanaanTPA';
		
		$data['dataterakhir'] = $this->model_login->Getdataterakhir();
		$id_kab = $this->input->post('id_kab');


		if ($this->input->post('loginValidation')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$login =$this->model_login->loginValidation($username,$password);
		
			if($login->num_rows() > 0){
				$dataSession = array(
					'login' => 'true',
					'username' => $username
				);
				$this->session->set_userdata($dataSession);			
				redirect(base_url($id_kab.'/beranda'));
			}else{
				$this->load->view('notification/loginFailed');
				$this->load->view('template_halamanUtama',$data);
			}
		}else{
			$this->load->view('template_halamanUtama',$data);
		}
	}


	public function dataProyeksiKabupaten() {
		$kab_id = $this->uri->segment(2);
		$data['dataterakhir'] = $this->model_login->Getdataterakhir();
		$id_kab = $this->input->post('id_kab');
		$data['nama'] = 'Halaman Utama';
		$data['view_name'] = 'halamanUtama/view_dataProyeksiKabupaten';
    	$data['namaKabupaten'] = $this->model_beranda->Getnamakab($kab_id);
    	$data['dataKabupaten'] = $this->model_beranda->GetKab();

		$data['dataKomentar'] = $this->model_halamanUtama->getDataKomentar($kab_id);

		$data['proyeksiPendudukGeometri'] = $this->model_halamanUtama->getProyeksiPendudukGeometri($kab_id);
		$data['pertumbuhanproyeksiPendudukGeometri'] = $this->model_halamanUtama->pertumbuhanproyeksiPendudukGeometri($kab_id);
		$data['proyeksiPdrbGeometri'] = $this->model_halamanUtama->getProyeksiPdrbGeometri($kab_id);
		$data['pertumbuhanproyeksiPdrbGeometri'] = $this->model_halamanUtama->pertumbuhanproyeksiPdrbGeometri($kab_id);
		$data['dataProyeksiPenduduk'] = $this->model_proyeksiPenduduk->GetDataProyeksiPenduduk($kab_id);
		$data['dataProyeksiPdrb'] = $this->model_proyeksiPDRB->GetDataProyeksipdrb($kab_id);
		$data['dataProyeksiPendudukTotal'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukTotal($kab_id);

		$data['dataTimbulan'] = $this->model_halamanUtama->dataTimbulan($kab_id);

		$data['jumlahdataproyeksi_pp'] = $this->model_beranda->jumlah_data_proyeksi_pp($kab_id);
		$data['jumlahdataproyeksi_pdrb'] = $this->model_beranda->jumlah_data_proyeksi_pdrb($kab_id);
		$data['jumlahdatapp'] = $this->model_proyeksiPenduduk->jumlah_data_pp($kab_id);
		$data['jumlahdatapdrb'] = $this->model_proyeksiPDRB->jumlah_data_pdrb($kab_id);
		$data['dataProyeksiPendudukAritmatik'] = $this->model_proyeksiPenduduk->GetDataProyeksiPendudukAritmatik($kab_id);

		$data['dataterakhir'] = $this->model_login->Getdataterakhir();
		$id_kab = $this->input->post('id_kab');

		if ($this->input->post('postKomentar')) {
			
			$this->model_halamanUtama->tambahKomentar($kab_id);
	     	redirect("dataKabupaten/".$kab_id);
		}


		if ($this->input->post('loginValidation')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$login =$this->model_login->loginValidation($username,$password);
		
			if($login->num_rows() > 0){
				$dataSession = array(
					'login' => 'true',
					'username' => $username
				);
				$this->session->set_userdata($dataSession);			
				redirect(base_url($id_kab.'/beranda'));
			}else{
				$this->load->view('notification/loginFailed');
				$this->load->view('template_halamanUtama',$data);
			}
		}else{
			$this->load->view('template_halamanUtama',$data);
		}

	}


	public function tentang() {
		$data['nama'] = 'Tentang';
    	$data['dataKabupaten'] = $this->model_beranda->GetKab();
		$data['view_name'] = 'halamanUtama/view_tentang';
		
		$data['dataterakhir'] = $this->model_login->Getdataterakhir();
		$id_kab = $this->input->post('id_kab');


		if ($this->input->post('loginValidation')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$login =$this->model_login->loginValidation($username,$password);
		
			if($login->num_rows() > 0){
				$dataSession = array(
					'login' => 'true',
					'username' => $username
				);
				$this->session->set_userdata($dataSession);			
				redirect(base_url($id_kab.'/beranda'));
			}else{
				$this->load->view('notification/loginFailed');
				$this->load->view('template_halamanUtama',$data);
			}
		}else{
			$this->load->view('template_halamanUtama',$data);
		}
	}
}

?>