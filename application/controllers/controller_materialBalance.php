<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_materialBalance extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_materialBalance');
		$this->load->model('model_notifikasi');
	}	

	public function materialAllBalance()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->model_materialBalance->GetKab();
    	$data['namaKabupaten'] = $this->model_materialBalance->Getnamakab($kab_id);
		$data['dataMaterialBalanceAll'] = $this->model_materialBalance->dataMaterialBalanceAll($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'materialBalance/view_materialAllBalance';
		$data['nama'] = 'Data Material All Balance';
		$this->load->view('template',$data);
	}

	public function materialBalanceTahun()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahKomponenSampah')) {
			$this->model_materialBalance->tambahKomponenSampah($kab_id);
	     	redirect($kab_id."/materialBalanceTahun");
		}

    	$data['dataKabupaten'] = $this->model_materialBalance->GetKab();
    	$data['namaKabupaten'] = $this->model_materialBalance->Getnamakab($kab_id);
		$data['dataMaterialBalanceTahun'] = $this->model_materialBalance->dataMaterialBalanceTahun($kab_id);
		$data['dataMaterialBalanceTahun1'] = $this->model_materialBalance->dataMaterialBalanceTahun1($kab_id);
		$data['dataMaterialBalanceTahun2'] = $this->model_materialBalance->dataMaterialBalanceTahun2($kab_id);
		$data['dataMaterialBalanceTahun3'] = $this->model_materialBalance->dataMaterialBalanceTahun3($kab_id);
		$data['dataMaterialBalanceTahunTotal'] = $this->model_materialBalance->dataMaterialBalanceTahunTotal($kab_id);
		$data['dataMaterialBalanceTahun1Total'] = $this->model_materialBalance->dataMaterialBalanceTahun1Total($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'materialBalance/view_materialBalanceTahunProyeksi';
		$data['nama'] = 'Data Material Balance Tahun Proyeksi';
		$this->load->view('template',$data);
	}

	public function editKomponenSampah()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		$this->model_materialBalance->editKomponenSampah($id);
		redirect($kab_id."/materialBalanceTahun");
	}

	public function hapusKomponenSampah()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		$this->model_materialBalance->hapusKomponenSampah($id);
		redirect($kab_id."/materialBalanceTahun");
	}

}
