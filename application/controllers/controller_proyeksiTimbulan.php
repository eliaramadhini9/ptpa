<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_proyeksiTimbulan extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_proyeksiTimbulan');
		$this->load->model('model_notifikasi');
		$this->load->model('model_proyeksiPenduduk');
	}	

	public function timbulanDomestik()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('jumlah_timbulan_pdrb')) {
			$a = $this->input->post('tahun_domestik');
			$b = $this->input->post('timbulan_pdrb');
 			$c = $this->input->post('pert_timb');
 			$d= $a+15;
 			$e=$b;
 			$f = $this->input->post('tahun_timb_pddk');
			$g = $this->input->post('timbulan_pddk');
 			$h = $this->input->post('pert_pddk');
 			$j= $a+15;
 			$k=$g;
 			$x = $this->input->post('tahun_layanan');
 			$tingkat_pelayanan = $this->input->post('tingkat_pelayanan');

 			$this->model_proyeksiTimbulan->coba_edit($a,$b,$kab_id);
 			$this->model_proyeksiTimbulan->timbulan_pddk($a,$g,$kab_id);

 			for ($i=$a+1; $i <= $d; $i++) {
 				$e = $e + ($c/100);
 				$this->model_proyeksiTimbulan->coba_edit_1($e,$i,$kab_id);
 			}
 			for ($o=$a+1; $o <= $j; $o++) {
 				$k = $k + ($h/100);
 				$this->model_proyeksiTimbulan->timbulan_pddk_1($k,$o,$kab_id);
 			}
 			for ($u=$a; $u <= ($a+15); $u++) {
 				$this->model_proyeksiTimbulan->tingkat_pelayanan($tingkat_pelayanan,$u,$kab_id);
 			}
 			redirect($kab_id."/timbulanDomestik");
 		}

 		
    	$data['dataKabupaten'] = $this->model_proyeksiTimbulan->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiTimbulan->Getnamakab($kab_id);
		$data['jumlahdataproyeksi'] = $this->model_proyeksiPenduduk->jumlah_data_proyeksi($kab_id);
		$data['dataTimbulanDomestik'] = $this->model_proyeksiTimbulan->dataTimbulanDomestik($kab_id);
    	$data['id_timbulan'] = $this->model_proyeksiTimbulan->id_timbulan($kab_id);
    	$data['a_timbulan'] = $this->model_proyeksiTimbulan->a_timbulan($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'timbulan/view_timbulanDomestik';
		$data['nama'] = 'Proyeksi Timbulan Sampah Domestik';
		$this->load->view('template',$data);
	}

	/*public function timbulanPDRB_edit() {
		$tahun = $this->uri->segment(2);
		$data = array(
	        'timbulan_pdrb' => $this->input->post('timbulan_pdrb'),       
		);
		$this->model_proyeksiTimbulan->timbulanPDRB_edit($tahun,$data);
		redirect("timbulanDomestik");
	}*/

	public function timbulanNonDomestik()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahTimbulanNonDomestik')) {
			$tahun = $this->input->post('tahun');
			$luas_wilayah = $this->input->post('luas_wilayah');
			$jml_penduduka = $this->input->post('jml_penduduka');
			$jml_pendudukb = $this->input->post('jml_pendudukb');
			$tk_jumlah = $this->input->post('tk_jumlah');
			$tk_orang = $this->input->post('tk_orang');
			$tk_unit_sampah = $this->input->post('tk_unit_sampah');
			$sd_jumlah = $this->input->post('sd_jumlah');
			$sd_orang = $this->input->post('sd_orang');
			$sd_unit_sampah = $this->input->post('sd_unit_sampah');
			$smp_jumlah = $this->input->post('smp_jumlah');
			$smp_orang = $this->input->post('smp_orang');
			$smp_unit_sampah = $this->input->post('smp_unit_sampah');
			$sma_jumlah = $this->input->post('sma_jumlah');
			$sma_orang = $this->input->post('sma_orang');
			$sma_unit_sampah = $this->input->post('sma_unit_sampah');
			$mush_jumlah = $this->input->post('mush_jumlah');
			$mush_orang = $this->input->post('mush_orang');
			$mush_unit_sampah = $this->input->post('mush_unit_sampah');
			$mas_jumlah = $this->input->post('mas_jumlah');
			$mas_orang = $this->input->post('mas_orang');
			$mas_unit_sampah = $this->input->post('mas_unit_sampah');
			$grj_jumlah = $this->input->post('grj_jumlah');
			$grj_orang = $this->input->post('grj_orang');
			$grj_unit_sampah = $this->input->post('grj_unit_sampah');
			$pura_jumlah = $this->input->post('pura_jumlah');
			$pura_orang = $this->input->post('pura_orang');
			$pura_unit_sampah = $this->input->post('pura_unit_sampah');
			$wrg_jumlah = $this->input->post('wrg_jumlah');
			$wrg_unit_sampah = $this->input->post('wrg_unit_sampah');
			$mini_jumlah = $this->input->post('mini_jumlah');
			$mini_jumlahb = $this->input->post('mini_jumlahb');
			$mini_luas = $this->input->post('mini_luas');
			$mini_unit_sampah = $this->input->post('mini_unit_sampah');
			$toko_jumlah = $this->input->post('toko_jumlah');
			$toko_unit_sampah = $this->input->post('toko_unit_sampah');
			$rs_jumlah = $this->input->post('rs_jumlah');
			$rs_bed = $this->input->post('rs_bed');
			$rs_unit_sampah = $this->input->post('rs_unit_sampah');
			$rsb_jumlah = $this->input->post('rsb_jumlah');
			$rsb_bed = $this->input->post('rsb_bed');
			$rsb_unit_sampah = $this->input->post('rsb_unit_sampah');
			$pusk_jumlah = $this->input->post('pusk_jumlah');
			$pusk_bed = $this->input->post('pusk_bed');
			$pusk_unit_sampah = $this->input->post('pusk_unit_sampah');
			$puskp_jumlah = $this->input->post('puskp_jumlah');
			$puskp_bed = $this->input->post('puskp_bed');
			$puskp_unit_sampah = $this->input->post('puskp_unit_sampah');
			$density = $this->input->post('density');
			$tingkat_pelayanan = $this->input->post('tingkat_pelayanan');


			$this->model_proyeksiTimbulan->inicoba3($kab_id,$luas_wilayah,$tingkat_pelayanan,$tk_unit_sampah,$sd_unit_sampah,$smp_unit_sampah,$sma_unit_sampah,$mush_unit_sampah,$mas_unit_sampah,$grj_unit_sampah,$pura_unit_sampah,$wrg_unit_sampah,$mini_unit_sampah,$toko_unit_sampah,$rs_unit_sampah,$rsb_unit_sampah,$pusk_unit_sampah,$puskp_unit_sampah, $density);
			$this->model_proyeksiTimbulan->inicoba($tahun,$kab_id,$tk_jumlah,$tk_orang,$sd_jumlah,$sd_orang,$smp_jumlah,$smp_orang,$sma_jumlah,$sma_orang,$mush_jumlah,$mush_orang,$mas_jumlah,$mas_orang,$grj_jumlah,$grj_orang,$pura_jumlah,$pura_orang,$wrg_jumlah,$mini_jumlah,$mini_luas,$toko_jumlah,$rs_jumlah,$rs_bed,$rsb_jumlah,$rsb_bed,$pusk_jumlah,$pusk_bed,$puskp_jumlah,$puskp_bed);


 			for ($b=$tahun+1; $b <= ($tahun+15); $b++) {
 				$tk_jumlah = $tk_jumlah*$jml_pendudukb/$jml_penduduka;
 				$tk_orang = $tk_orang*$jml_pendudukb/$jml_penduduka;
 				$sd_jumlah = $sd_jumlah*$jml_pendudukb/$jml_penduduka;
 				$sd_orang = $sd_orang*$jml_pendudukb/$jml_penduduka;
 				$smp_jumlah = $smp_jumlah*$jml_pendudukb/$jml_penduduka;
 				$smp_orang = $smp_orang*$jml_pendudukb/$jml_penduduka;
 				$sma_jumlah = $sma_jumlah*$jml_pendudukb/$jml_penduduka;
 				$sma_orang = $sma_orang*$jml_pendudukb/$jml_penduduka;
 				$mush_jumlah = $mush_jumlah*$jml_pendudukb/$jml_penduduka;
 				$mush_orang = $mush_orang*$jml_pendudukb/$jml_penduduka;
 				$mas_jumlah = $mas_jumlah*$jml_pendudukb/$jml_penduduka;
 				$mas_orang = $mas_orang*$jml_pendudukb/$jml_penduduka;
 				$grj_jumlah = $grj_jumlah*$jml_pendudukb/$jml_penduduka;
 				$grj_orang = $grj_orang*$jml_pendudukb/$jml_penduduka;
 				$pura_jumlah = $pura_jumlah*$jml_pendudukb/$jml_penduduka;
 				$pura_orang = $pura_orang*$jml_pendudukb/$jml_penduduka;
 				$wrg_jumlah = $wrg_jumlah*$jml_pendudukb/$jml_penduduka;
 				$mini_jumlah = $mini_jumlah*$jml_pendudukb/$jml_penduduka;
 				$mini_luas = ($mini_luas*$mini_jumlah)/$mini_jumlahb;
 				$toko_jumlah = $toko_jumlah*$jml_pendudukb/$jml_penduduka;
 				$rs_jumlah = $rs_jumlah*$jml_pendudukb/$jml_penduduka;
 				$rs_bed = $rs_bed*$jml_pendudukb/$jml_penduduka;
 				$rsb_jumlah = $rsb_jumlah*$jml_pendudukb/$jml_penduduka;
 				$rsb_bed = $rsb_bed*$jml_pendudukb/$jml_penduduka;
 				$pusk_jumlah = $pusk_jumlah*$jml_pendudukb/$jml_penduduka;
 				$pusk_bed = $pusk_bed*$jml_pendudukb/$jml_penduduka;
 				$puskp_jumlah = $puskp_jumlah*$jml_pendudukb/$jml_penduduka;
 				$puskp_bed = $puskp_bed*$jml_pendudukb/$jml_penduduka;

 				$this->model_proyeksiTimbulan->inicoba1($tk_jumlah,$b,$kab_id,$tk_orang,$sd_jumlah,$sd_orang,$smp_jumlah,$smp_orang,$sma_jumlah,$sma_orang,$mush_jumlah,$mush_orang,$mas_jumlah,$mas_orang,$grj_jumlah,$grj_orang,$pura_jumlah,$pura_orang,$wrg_jumlah,$mini_jumlah,$mini_luas,$toko_jumlah,$rs_jumlah,$rs_bed,$rsb_jumlah,$rsb_bed,$pusk_jumlah,$pusk_bed,$puskp_jumlah,$puskp_bed);
 			}

 			redirect($kab_id."/timbulanNonDomestik");
 		}

    	$data['dataKabupaten'] = $this->model_proyeksiTimbulan->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiTimbulan->Getnamakab($kab_id);
		$data['dataTimbulanNonDomestik'] = $this->model_proyeksiTimbulan->dataTimbulanNonDomestik($kab_id);
    	$data['tahunTimbulanNonDo'] = $this->model_proyeksiTimbulan->tahunTimbulanNonDo($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
    	$data['a_timbulannon'] = $this->model_proyeksiTimbulan->a_timbulannon($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'timbulan/view_timbulanNonDomestik';
		$data['nama'] = 'Proyeksi Timbulan Sampah Non-Domestik';
		$this->load->view('template',$data);
	}

	public function timbulanTotal()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->model_proyeksiTimbulan->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiTimbulan->Getnamakab($kab_id);
		$data['dataTimbulanTotal'] = $this->model_proyeksiTimbulan->dataTimbulanTotal($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'timbulan/view_timbulanTotal';
		$data['nama'] = 'Proyeksi Timbulan Sampah Total';
		$this->load->view('template',$data);
	}


}
