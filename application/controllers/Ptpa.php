<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ptpa extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('ptpa_model');
	}	


// <!-- fungsi Kriteria Penyisih -->

	public function kriteriaPenyisih()
	{
		$kab_id = $this->uri->segment(1);
		if ($this->input->post('buatCalonTPA')) {
			$this->ptpa_model->buatCalonTPA($kab_id);
      		redirect($kab_id.'/kriteriaPenyisih');
    	}
		$data['dataKabupaten'] = $this->ptpa_model->GetKab();
		$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataKriteria'] = $this->ptpa_model->GetDataKriteriaPenyisih($kab_id);
		$data['dataCalonTPA'] = $this->ptpa_model->GetCalonTPA($kab_id);
		$data['view_name'] = 'kriteriaPenyisih';
		$data['nama'] = 'Kriteria Penyisih';
		$this->load->view('template',$data);
	}

	public function kriteriaPenyisih_edit()
	{
		$kab_id = $this->uri->segment(1);
		$id_kriteria = $this->uri->segment(3);
		
		$this->ptpa_model->kriteriaPenyisih_edit($id_kriteria);
		redirect($kab_id.'/kriteriaPenyisih');
	}

	public function kriteriaPenyisih_hapus()
	{
		$kab_id = $this->uri->segment(1);
		$id_kriteria = $this->uri->segment(3);
		$this->ptpa_model->kriteriaPenyisih_hapus($id_kriteria);
		redirect($kab_id.'/kriteriaPenyisih');
	}


// <!-- fungsi Proyeksi Penduduk -->

	public function proyeksiPenduduk()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahDataPenduduk')) {
			
			$this->ptpa_model->proyeksiPenduduk_tambah($kab_id);

			$this->ptpa_model->proyeksi_pp_delete($kab_id);
			$this->ptpa_model->kebutuhanLuasLahan_delete($kab_id);
			$this->ptpa_model->domestik_delete($kab_id);
			$this->ptpa_model->nondomestik_delete($kab_id);
						
			$tahun = $this->input->post('tahun');
			$proyeksi = $tahun+14;

 			for ($i=$tahun; $i<=$proyeksi ; $i++) { 
			$this->ptpa_model->kebutuhanLuasLahan_tambah($i);
		}
	     	redirect($kab_id."/proyeksiPenduduk");
 		}

 		if ($this->input->post('tambahDataProyeksi')) {
			$this->ptpa_model->proyeksi_pp_delete($kab_id);

 			$a = $this->input->post('tahun_akhir');
 			$b = $this->input->post('proyeksi');

 			for ($i=$a+1; $i<=$b ; $i++) { 
			$this->ptpa_model->proyeksi_tambah($i,$kab_id);
		}

		/*for ($o=$a-1; $o<=$b ; $o++) { 
			$this->ptpa_model->domestik_tambah($o);
			$this->ptpa_model->nondomestik_tambah($o);
		}
			$this->ptpa_model->domestik_tambah_1($o);
	     	redirect("proyeksiPenduduk");*/
	     	
	     	redirect($kab_id.'/proyeksiPenduduk');
 		}

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksiPenduduk'] = $this->ptpa_model->GetDataProyeksiPenduduk($kab_id);
		$data['dataProyeksi_pp'] = $this->ptpa_model->GetDataProyeksi_pp($kab_id);
		$data['view_name'] = 'pp/proyeksiPenduduk';
		$data['nama'] = 'Proyeksi Penduduk';
		$this->load->view('template',$data);
	}

	public function proyeksiPenduduk_edit()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);

		$this->ptpa_model->proyeksiPenduduk_edit($id);
		redirect($kab_id."/proyeksiPenduduk");
	}

	public function proyeksiPenduduk_hapus()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		$this->ptpa_model->proyeksiPenduduk_hapus($id);
		redirect($kab_id."/proyeksiPenduduk");
	}

	public function proyeksiPenduduk_hapus_checked() {
		$kab_id = $this->uri->segment(1);
		$id = $_POST['id']; // Ambil data NIS yang dikirim oleh view.php melalui form submit
        $this->ptpa_model->proyeksiPenduduk_hapus_checked($id);
		redirect($kab_id."/proyeksiPenduduk");
	}

	public function proyeksiPendudukAritmatik()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksiPendudukAritmatik'] = $this->ptpa_model->GetDataProyeksiPendudukAritmatik($kab_id);
		$data['dataProyeksiPendudukAritmatikStandar'] = $this->ptpa_model->GetDataProyeksiPendudukAritmatikStandar($kab_id);
		$data['view_name'] = 'pp/proyeksiPendudukAritmatik';
		$data['nama'] = 'Proyeksi Penduduk Aritmatik';
		$this->load->view('template',$data);
	}

	public function proyeksiPendudukGeometri()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksiPendudukGeometri'] = $this->ptpa_model->GetDataProyeksiPendudukGeometri($kab_id);
		$data['dataProyeksiPendudukGeometriStandar'] = $this->ptpa_model->GetDataProyeksiPendudukGeometriStandar($kab_id);
		$data['view_name'] = 'pp/proyeksiPendudukGeometri';
		$data['nama'] = 'Proyeksi Penduduk Geometri';
		$this->load->view('template',$data);
	}

	public function proyeksiPendudukLQ()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksiPendudukLQ'] = $this->ptpa_model->GetDataProyeksiPendudukLQ($kab_id);
		$data['dataProyeksiPendudukLQStandar'] = $this->ptpa_model->GetDataProyeksiPendudukLQStandar($kab_id);
		$data['view_name'] = 'pp/proyeksiPendudukLQ';
		$data['nama'] = 'Proyeksi Penduduk Least Square';
		$this->load->view('template',$data);
	}


// <!-- fungsi Proyeksi PDRB -->

	public function proyeksiPDRB()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahDataPDRB')) {

			$this->ptpa_model->proyeksi_pdrb_delete($kab_id);

			$this->ptpa_model->proyeksiPDRB_tambah($kab_id);
     		redirect($kab_id."/proyeksiPDRB");
 		}

 		if ($this->input->post('tambahDataProyeksi')) {

			$this->ptpa_model->proyeksi_pdrb_delete($kab_id);

 			$a = $this->input->post('tahun_akhir');
 			$b = $this->input->post('proyeksi');

 			for ($i=$a+1; $i<=$b ; $i++) { 
			$this->ptpa_model->proyeksi_pdrb_tambah($i,$kab_id);
		}
	     	redirect($kab_id."/proyeksiPDRB");
 		}

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksi_pdrb'] = $this->ptpa_model->GetDataProyeksi_pdrb($kab_id);
		$data['dataProyeksipdrb'] = $this->ptpa_model->GetDataProyeksipdrb($kab_id);
		$data['view_name'] = 'pdrb/proyeksipdrb';
		$data['nama'] = 'Proyeksi PDRB';
		$this->load->view('template',$data);
	}

	public function proyeksiPDRB_edit() {
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);

		$this->ptpa_model->proyeksiPDRB_edit($id);
		redirect($kab_id."/proyeksiPDRB");
	}

	public function proyeksiPDRB_hapus()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		$this->ptpa_model->proyeksiPDRB_hapus($id);
		redirect($kab_id."/proyeksiPDRB");
	}

	public function proyeksiPDRB_hapus_checked()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);

		$id = $_POST['id'];
        $this->ptpa_model->proyeksiPDRB_hapus_checked($id);
		redirect($kab_id."/proyeksiPDRB");
	}

	public function proyeksipdrbAritmatik()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksipdrbAritmatik'] = $this->ptpa_model->GetDataProyeksipdrbAritmatik($kab_id);
		$data['dataProyeksipdrbAritmatikStandar'] = $this->ptpa_model->GetDataProyeksipdrbAritmatikStandar($kab_id);
		$data['view_name'] = 'pdrb/proyeksipdrbAritmatik';
		$data['nama'] = 'Proyeksi PDRB Aritmatik';
		$this->load->view('template',$data);
	}

	public function proyeksipdrbGeometri()
	{
		$kab_id = $this->uri->segment(1);
		
    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksipdrbGeometri'] = $this->ptpa_model->GetDataProyeksipdrbGeometri($kab_id);
		$data['dataProyeksipdrbGeometriStandar'] = $this->ptpa_model->GetDataProyeksipdrbGeometriStandar($kab_id);
		$data['view_name'] = 'pdrb/proyeksipdrbGeometri';
		$data['nama'] = 'Proyeksi PDRB Geometri';
		$this->load->view('template',$data);
	}

	public function proyeksipdrbLQ()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataProyeksipdrbLQ'] = $this->ptpa_model->GetDataProyeksipdrbLQ($kab_id);
		$data['dataProyeksipdrbLQStandar'] = $this->ptpa_model->GetDataProyeksipdrbLQStandar($kab_id);
		$data['view_name'] = 'pdrb/proyeksipdrbLQ';
		$data['nama'] = 'Proyeksi PDRB Least Square';
		$this->load->view('template',$data);
	} 


// <!-- fungsi Data Drainase -->

	public function drainase()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahDataDrainase')) {
	      	$this->ptpa_model->drainase_tambah($kab_id);
	     	redirect($kab_id."/drainase");
   		}

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
    	$data['dataDrainaseMenit'] = $this->ptpa_model->GetDataDrainaseMenit($kab_id);
    	$data['dataDrainaseJam'] = $this->ptpa_model->GetDataDrainaseJam($kab_id);
    	$data['dataFrekuensiDrainase'] = $this->ptpa_model->GetDataFrekuensiDrainase($kab_id);
    	$data['dataKalaUlangDrainase'] = $this->ptpa_model->GetDataKalaUlangDrainase($kab_id);
    	$data['dataKalaUlangDrainase3'] = $this->ptpa_model->GetDataKalaUlangDrainase3($kab_id);
    	$data['dataTotalKalaUlang'] = $this->ptpa_model->GetdataTotalKalaUlang($kab_id);
    	$data['dataTotalKalaUlang25Tahun'] = $this->ptpa_model->GetdataTotalKalaUlang25Tahun($kab_id);
    	$data['view_name'] = 'drainase/drainase';
    	$data['nama'] = 'Data Drainase';

		$this->load->view('template',$data);
	}

	public function drainase_edit()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		
		$this->ptpa_model->drainase_edit($id,$kab_id);
		redirect($kab_id."/drainase");
	}

	public function drainase_hapus()
	{
		$kab_id = $this->uri->segment(1);

		$id = $this->uri->segment(3);
		$this->ptpa_model->drainase_hapus($id,$kab_id);
		redirect($kab_id."/drainase");
	}

	public function drainase_hapus_checked()
	{
		$id_drainase = $_POST['id_drainase']; // Ambil data NIS yang dikirim oleh view.php melalui form submit
        $this->ptpa_model->drainase_hapus_checked($id_drainase);
		redirect("drainase");
	}


// <!-- fungsi Proyeksi Timbulan Sampah  -->

	public function timbulanDomestik()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('jumlah_timbulan_pdrb')) {
			$a = $this->input->post('tahun_domestik');
			$b = $this->input->post('timbulan_pdrb');
 			$c = $this->input->post('pert_timb');
 			$d= $a+15;
 			$e=$b;

 			$this->ptpa_model->coba_edit($a,$b);

 			for ($i=$a+1; $i <= $d; $i++) {
 				$e = $e + ($c/100);
 				$this->ptpa_model->coba_edit_1($e,$i,$kab_id);
 			}
 			redirect($kab_id."/timbulanDomestik");
 		}

 		if ($this->input->post('jumlah_timbulan_pddk')) {
			$f = $this->input->post('tahun_timb_pddk');
			$g = $this->input->post('timbulan_pddk');
 			$h = $this->input->post('pert_pddk');
 			$j= $f+15;
 			$k=$g;

 			$this->ptpa_model->timbulan_pddk($f,$g,$kab_id);

 			for ($o=$f+1; $o <= $j; $o++) {
 				$k = $k + ($h/100);
 				$this->ptpa_model->timbulan_pddk_1($k,$o);
 			}
 			redirect($kab_id."/timbulanDomestik");
 		}

 		if ($this->input->post('jumlah_tingkat_pelayanan')) {
			$x = $this->input->post('tahun_layanan');
 			$tingkat_pelayanan = $this->input->post('tingkat_pelayanan');

 			for ($u=$x; $u <= ($x+15); $u++) {
 				$this->ptpa_model->tingkat_pelayanan($tingkat_pelayanan,$u,$kab_id);
 			}

 			redirect($kab_id."/timbulanDomestik");
 		}

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataTimbulanDomestik'] = $this->ptpa_model->dataTimbulanDomestik($kab_id);
    	$data['id_timbulan'] = $this->ptpa_model->id_timbulan($kab_id);
    	$data['a_timbulan'] = $this->ptpa_model->a_timbulan($kab_id);
		$data['view_name'] = 'timbulan/timbulanDomestik';
		$data['nama'] = 'Proyeksi Timbulan Sampah Domestik';
		$this->load->view('template',$data);
	}

	/*public function timbulanPDRB_edit() {
		$tahun = $this->uri->segment(2);
		$data = array(
	        'timbulan_pdrb' => $this->input->post('timbulan_pdrb'),       
		);
		$this->ptpa_model->timbulanPDRB_edit($tahun,$data);
		redirect("timbulanDomestik");
	}*/

	public function timbulanNonDomestik()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataTimbulanNonDomestik'] = $this->ptpa_model->dataTimbulanNonDomestik();
		$data['view_name'] = 'timbulan/timbulanNonDomestik';
		$data['nama'] = 'Proyeksi Timbulan Sampah Non-Domestik';
		$this->load->view('template',$data);
	}

	public function timbulanTotal()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataTimbulanTotal'] = $this->ptpa_model->dataTimbulanTotal($kab_id);
		$data['view_name'] = 'timbulan/timbulanTotal';
		$data['nama'] = 'Total Timbulan Sampah';
		$this->load->view('template',$data);
	}


// <!-- fungsi Material Balance  -->

	public function materialBalanceAll()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataMaterialBalanceAll'] = $this->ptpa_model->dataMaterialBalanceAll($kab_id);
		$data['view_name'] = 'materialBalance/materialBalanceAll';
		$data['nama'] = 'Data Material All Balance';
		$this->load->view('template',$data);
	}

	public function materialBalanceTahun()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataMaterialBalanceTahun'] = $this->ptpa_model->dataMaterialBalanceTahun($kab_id);
		$data['dataMaterialBalanceTahun1'] = $this->ptpa_model->dataMaterialBalanceTahun1($kab_id);
		$data['dataMaterialBalanceTahun2'] = $this->ptpa_model->dataMaterialBalanceTahun2($kab_id);
		$data['dataMaterialBalanceTahun3'] = $this->ptpa_model->dataMaterialBalanceTahun3($kab_id);
		$data['view_name'] = 'materialBalance/materialBalanceTahunProyeksi';
		$data['nama'] = 'Data Material Balance Tahun';
		$this->load->view('template',$data);
	}


	public function kebutuhanLuasLahan()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('densSampahAwal')) {
			$a = $this->input->post('dens_sampah_awal');
			$this->ptpa_model->densSampahAwal($a,$kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

		if ($this->input->post('densSampahKompaksi')) {
			$a = $this->input->post('dens_sampah_kompaksi');
			$this->ptpa_model->densSampahKompaksi($a,$kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

		if ($this->input->post('ketebalanLift')) {
			$a = $this->input->post('ketebalan_lift');
			$this->ptpa_model->ketebalanLift($a,$kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

		if ($this->input->post('tebalLapisan')) {
			$a = $this->input->post('tebal_lapisan');
			$this->ptpa_model->tebalLapisan($a,$kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

		if ($this->input->post('jumlahLift')) {
			$a = $this->input->post('jumlah_lift');
			$this->ptpa_model->jumlahLift($a,$kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

		if ($this->input->post('luasTiapsel')) {
			$a = $this->input->post('luas_tiapsel');
			$this->ptpa_model->luasTiapsel($a,$kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

		if ($this->input->post('masaLandfill')) {
			$a = $this->input->post('masa_landfill');
			$this->ptpa_model->masaLandfill($a,$kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['dataKebutuhanLuasLahan'] = $this->ptpa_model->dataKebutuhanLuasLahan($kab_id);
		$data['view_name'] = 'kebutuhanLuasLahan';
		$data['nama'] = 'Kebutuhan Luas Lahan';
		$this->load->view('template',$data);
	}




// <!-- fungsi Data Berdasarkan ID -->


	public function beranda()
	{
		$kab_id = $this->uri->segment(1);
    	$data['dataKabupaten'] = $this->ptpa_model->GetKab();
    	$data['namaKabupaten'] = $this->ptpa_model->Getnamakab($kab_id);
		$data['view_name'] = 'beranda';
		$data['nama'] = 'Beranda';
		$this->load->view('template',$data);
	}

}
