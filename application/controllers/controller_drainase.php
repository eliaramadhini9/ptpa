<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_drainase extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_drainase');
	}	

	public function drainase()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahDataDrainase')) {
	      	$this->model_drainase->drainase_tambah($kab_id);
	     	redirect($kab_id."/drainase");
   		}

    	$data['dataKabupaten'] = $this->model_drainase->GetKab();
    	$data['namaKabupaten'] = $this->model_drainase->Getnamakab($kab_id);
    	$data['dataDrainaseMenit'] = $this->model_drainase->GetDataDrainaseMenit($kab_id);
    	$data['dataDrainaseJam'] = $this->model_drainase->GetDataDrainaseJam($kab_id);
    	$data['dataFrekuensiDrainase'] = $this->model_drainase->GetDataFrekuensiDrainase($kab_id);
    	$data['dataKalaUlangDrainase'] = $this->model_drainase->GetDataKalaUlangDrainase($kab_id);
    	$data['dataKalaUlangDrainase3'] = $this->model_drainase->GetDataKalaUlangDrainase3($kab_id);
    	$data['dataTotalKalaUlang'] = $this->model_drainase->GetdataTotalKalaUlang($kab_id);
    	$data['dataTotalKalaUlang25Tahun'] = $this->model_drainase->GetdataTotalKalaUlang25Tahun($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
    	$data['view_name'] = 'drainase/drainase';
    	$data['nama'] = 'Data Drainase';

		$this->load->view('template',$data);
	}

	public function drainase_edit()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		
		$this->model_drainase->drainase_edit($id,$kab_id);
		redirect($kab_id."/drainase");
	}

	public function drainase_hapus()
	{
		$kab_id = $this->uri->segment(1);

		$id = $this->uri->segment(3);
		$this->model_drainase->drainase_hapus($id,$kab_id);
		redirect($kab_id."/drainase");
	}

	public function drainase_hapus_checked()
	{
		$id_drainase = $_POST['id_drainase']; // Ambil data NIS yang dikirim oleh view.php melalui form submit
        $this->model_drainase->drainase_hapus_checked($id_drainase);
		redirect("drainase");
	}

	public function beranda()
	{
		$kab_id = $this->uri->segment(1);
    	$data['dataKabupaten'] = $this->model_drainase->GetKab();
    	$data['namaKabupaten'] = $this->model_drainase->Getnamakab($kab_id);
		$data['view_name'] = 'beranda';
		$data['nama'] = 'Beranda';
		$this->load->view('template',$data);
	}

}
