<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_beranda extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_beranda');
		$this->load->model('model_notifikasi');
	}	

	public function beranda()
	{
		$kab_id = $this->uri->segment(1);
    	$data['dataKabupaten'] = $this->model_beranda->GetKab();
    	$data['namaKabupaten'] = $this->model_beranda->Getnamakab($kab_id);
		$data['proyeksiPendudukAritmatik'] = $this->model_beranda->proyeksiPendudukAritmatik($kab_id);
		$data['proyeksiPendudukGeometri'] = $this->model_beranda->proyeksiPendudukGeometri($kab_id);
		$data['proyeksiPendudukLS'] = $this->model_beranda->proyeksiPendudukLS($kab_id);
		$data['proyeksiPdrbAritmatik'] = $this->model_beranda->proyeksiPdrbAritmatik($kab_id);
		$data['proyeksiPdrbGeometri'] = $this->model_beranda->proyeksiPdrbGeometri($kab_id);
		$data['proyeksiPdrbLS'] = $this->model_beranda->proyeksiPdrbLS($kab_id);
		$data['jumlahdataproyeksi_pp'] = $this->model_beranda->jumlah_data_proyeksi_pp($kab_id);
		$data['jumlahdataproyeksi_pdrb'] = $this->model_beranda->jumlah_data_proyeksi_pdrb($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'view_beranda';
		$data['nama'] = 'Beranda';
		$this->load->view('template',$data);
	}

	public function kabupaten()
	{

		$kab_id = $this->uri->segment(1);

		if ($this->input->post('buatKabupaten')) {
			$this->model_beranda->tambahKabupaten();
     		redirect($kab_id."/kabupaten");
 		}

    	$data['dataKabupaten'] = $this->model_beranda->GetKab();
    	$data['dataKabupaten1'] = $this->model_beranda->GetKab1($kab_id);
    	$data['dataKabupatenID'] = $this->model_beranda->GetKabID();
    	$data['namaKabupaten'] = $this->model_beranda->Getnamakab($kab_id);
		$data['jumlahdataproyeksi_pp'] = $this->model_beranda->jumlah_data_proyeksi_pp($kab_id);
		$data['jumlahdataproyeksi_pdrb'] = $this->model_beranda->jumlah_data_proyeksi_pdrb($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'view_kabupaten';
		$data['nama'] = 'Kabupaten';
		$this->load->view('template',$data);
	}

	public function editKabupaten()
	{
		$kab_id = $this->uri->segment(1);
		$id_kab = $this->uri->segment(3);

		$this->model_beranda->editKabupaten($id_kab);
     		redirect($kab_id."/kabupaten");
	}

	public function hapusKabupaten()
	{
		$kab_id = $this->uri->segment(1);
		$id_kab = $this->uri->segment(3);

		$this->model_beranda->hapusKabupaten($id_kab);
     		redirect($kab_id."/kabupaten");
	}

	public function kecamatan()
	{

		$kab_id = $this->uri->segment(1);

		if ($this->input->post('buatKecamatan')) {
			$this->model_beranda->tambahKecamatan($kab_id);
     		redirect($kab_id."/kecamatan");
 		}

    	$data['dataKabupaten'] = $this->model_beranda->GetKab();
    	$data['dataKecamatan'] = $this->model_beranda->GetKec($kab_id);
    	$data['dataKabupatenID'] = $this->model_beranda->GetKabID();
    	$data['dataKecamatanID'] = $this->model_beranda->GetKecID();
    	$data['namaKabupaten'] = $this->model_beranda->Getnamakab($kab_id);
		$data['jumlahdataproyeksi_pp'] = $this->model_beranda->jumlah_data_proyeksi_pp($kab_id);
		$data['jumlahdataproyeksi_pdrb'] = $this->model_beranda->jumlah_data_proyeksi_pdrb($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'view_kecamatan';
		$data['nama'] = 'Kecamatan';
		$this->load->view('template',$data);
	}

	public function editKecamatan()
	{
		$kab_id = $this->uri->segment(1);
		$id_kecamatan = $this->uri->segment(3);

		$this->model_beranda->editKecamatan($id_kecamatan);
     		redirect($kab_id."/kecamatan");
	}

	public function hapusKecamatan()
	{
		$kab_id = $this->uri->segment(1);
		$id_kecamatan = $this->uri->segment(3);

		$this->model_beranda->hapusKecamatan($id_kecamatan);
     		redirect($kab_id."/kecamatan");
	}


}
