<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_calonTPA extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_calonTPA');
		$this->load->model('model_notifikasi');
	}	


// <!-- fungsi Kriteria Penyisih -->

	public function kriteriaPenyisih()
	{
		$kab_id = $this->uri->segment(1);
		$bts_admin_jumlah = $this->input->post('bts_admin_jumlah');
		$hak_pemilik_jumlah = $this->input->post('hak_pemilik_jumlah');
		$kapasitas_jumlah = $this->input->post('kapasitas_jumlah');
		$jml_pemilik_jumlah = $this->input->post('jml_pemilik_jumlah');
		$part_mas_jumlah = $this->input->post('part_mas_jumlah');
		$tanah_jumlah = $this->input->post('tanah_jumlah');
		$air_tanah_jumlah = $this->input->post('air_tanah_jumlah');
		$aliran_air_jumlah = $this->input->post('aliran_air_jumlah');
		$kaitan_jumlah = $this->input->post('kaitan_jumlah');
		$bhy_banjir_jumlah = $this->input->post('bhy_banjir_jumlah');
		$penutup_jumlah = $this->input->post('penutup_jumlah');
		$intens_hujan_jumlah = $this->input->post('intens_hujan_jumlah');
		$jln_lokasi_jumlah = $this->input->post('jln_lokasi_jumlah');
		$transport_jumlah = $this->input->post('transport_jumlah');
		$jln_masuk_jumlah = $this->input->post('jln_masuk_jumlah');
		$lalin_jumlah = $this->input->post('lalin_jumlah');
		$tata_lahan_jumlah = $this->input->post('tata_lahan_jumlah');
		$pertanian_jumlah = $this->input->post('pertanian_jumlah');
		$cgr_alam_jumlah = $this->input->post('cgr_alam_jumlah');
		$biologis_jumlah = $this->input->post('biologis_jumlah');
		$bau_jumlah = $this->input->post('bau_jumlah');
		$estetika_jumlah = $this->input->post('estetika_jumlah');
		$jumlah = ($bts_admin_jumlah+$hak_pemilik_jumlah+$kapasitas_jumlah+$jml_pemilik_jumlah+$part_mas_jumlah+$tanah_jumlah+$air_tanah_jumlah+$aliran_air_jumlah+$kaitan_jumlah+$bhy_banjir_jumlah+$penutup_jumlah+$intens_hujan_jumlah+$jln_lokasi_jumlah+$transport_jumlah+$jln_masuk_jumlah+$lalin_jumlah+$tata_lahan_jumlah+$pertanian_jumlah+$cgr_alam_jumlah+$biologis_jumlah+$bau_jumlah+$estetika_jumlah);

		if ($this->input->post('buatCalonTPA')) {
			$this->model_calonTPA->buatCalonTPA($kab_id,$jumlah);
      		redirect($kab_id.'/kriteriaPenyisih');
    	}
		$data['dataKabupaten'] = $this->model_calonTPA->GetKab();
		$data['dataKecamatan'] = $this->model_calonTPA->GetKec($kab_id);
		$data['namaKabupaten'] = $this->model_calonTPA->Getnamakab($kab_id);
		$data['dataKriteria'] = $this->model_calonTPA->GetDataKriteriaPenyisih($kab_id);
		$data['dataCalonTPA'] = $this->model_calonTPA->GetCalonTPA($kab_id);
		$data['jumlahDataCalonTPA'] = $this->model_calonTPA->jumlahDataCalonTPA($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'view_calonTPA';
		$data['nama'] = 'Calon TPA Sampah';
		$this->load->view('template',$data);
	}

	public function kriteriaPenyisih_edit()
	{
		$kab_id = $this->uri->segment(1);
		$id_kriteria = $this->uri->segment(3);
		
		$this->model_calonTPA->kriteriaPenyisih_edit($id_kriteria);
		redirect($kab_id.'/kriteriaPenyisih');
	}

	public function kriteriaPenyisih_hapus()
	{
		$kab_id = $this->uri->segment(1);
		$id_kriteria = $this->uri->segment(3);
		$this->model_calonTPA->kriteriaPenyisih_hapus($id_kriteria);
		redirect($kab_id.'/kriteriaPenyisih');
	}

}
