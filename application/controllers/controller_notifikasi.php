<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_notifikasi extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_notifikasi');
	}

	public function notifikasi(){
		$data['view_name'] = 'view_notifikasi';
		
		$kab_id = $this->uri->segment(1);
		$data['nama'] = 'Notifikasi';

		if ($this->input->post('status')) {

			$this->model_notifikasi->gantiStatus();
	     	redirect($kab_id."/notifikasi");
		}

    	$data['dataKabupaten'] = $this->model_notifikasi->GetKab();
    	$data['namaKabupaten'] = $this->model_notifikasi->Getnamakab($kab_id);
    	$data['dataKomentar'] = $this->model_notifikasi->getDataKomentar();
    	$data['notif'] = $this->model_notifikasi->notifikasi();


		$this->load->view('template', $data);
	}

	public function hapusKomentar()
	{
		$id = $this->uri->segment(3);
		$kab_id = $this->uri->segment(1);
		$this->model_notifikasi->hapusKomentar($id);
		redirect($kab_id."/notifikasi");
	}	

	
}
?>
