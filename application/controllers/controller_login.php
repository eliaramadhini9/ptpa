<?php

class controller_login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('model_login');
	}

	public function login()
	{
    	$data['dataterakhir'] = $this->model_login->Getdataterakhir();
		$id_kab = $this->input->post('id_kab');


		if ($this->input->post('loginValidation')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$login =$this->model_login->loginValidation($username,$password);
		
			if($login->num_rows() > 0){
				$dataSession = array(
					'login' => true
				);
				$this->session->set_userdata($dataSession);			
				redirect(base_url($id_kab.'/beranda'));
			}else{
				$this->load->view('notification/loginFailed');
				$this->load->view('view_login',$data);
			}
		}else{
			$this->load->view('view_login',$data);
		}
	}
	

	public function beranda()
	{
    	$data['dataKabupaten'] = $this->model_login->GetKab();
		$data['view_name'] = 'view_beranda';
		$data['nama'] = 'Beranda';
		$this->load->view('template',$data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('halamanUtama'));
	}

	public function lockscreen()
	{
    	$data['dataterakhir'] = $this->model_login->Getdataterakhir();
		$id_kab = $this->input->post('id_kab');
		$data['nama'] = 'Lock Screen';

		if ($this->input->post('loginUnlock')) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$login =$this->model_login->loginValidation($username,$password);
		
			if($login->num_rows() > 0){
				$dataSession = array(
					'login' => true
				);
				$this->session->set_userdata($dataSession);			
				redirect(base_url($id_kab.'/beranda'));
			}else{
				$this->load->view('notification/loginFailed');
				$this->load->view('view_lockscreen',$data);
			}
		}else{
			$this->load->view('view_lockscreen',$data);
		}
	}

}

?>