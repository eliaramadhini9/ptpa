<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_proyeksiPDRB extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_proyeksiPDRB');
		$this->load->model('model_notifikasi');
	}	

	public function proyeksiPDRB()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahDataPDRB')) {

			$this->model_proyeksiPDRB->proyeksi_pdrb_delete($kab_id);

			$this->model_proyeksiPDRB->proyeksiPDRB_tambah($kab_id);
     		redirect($kab_id."/proyeksiPDRB");
 		}

 		if ($this->input->post('tambahDataProyeksi')) {
			$this->model_proyeksiPDRB->proyeksi_pdrb_delete($kab_id);

 			$a = $this->input->post('tahun_akhir');
 			$b = $this->input->post('proyeksi');

 			for ($i=$a+1; $i<=$b ; $i++) { 
			$this->model_proyeksiPDRB->proyeksi_pdrb_tambah($i,$kab_id);
		}

		/*for ($o=$a-1; $o<=$b ; $o++) { 
			$this->model_proyeksiPenduduk->domestik_tambah($o);
			$this->model_proyeksiPenduduk->nondomestik_tambah($o);
		}
			$this->model_proyeksiPenduduk->domestik_tambah_1($o);
	     	redirect("proyeksiPenduduk");*/
	     	
	     	redirect($kab_id.'/proyeksiPDRB');
 		}

    	$data['dataKabupaten'] = $this->model_proyeksiPDRB->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPDRB->Getnamakab($kab_id);
		$data['dataProyeksi_pdrb'] = $this->model_proyeksiPDRB->GetDataProyeksi_pdrb($kab_id);
		$data['dataProyeksipdrb'] = $this->model_proyeksiPDRB->GetDataProyeksipdrb($kab_id);
		$data['dataProyeksipdrbTotal'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbTotal($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pdrb/view_jumlahPdrb';
		$data['nama'] = 'Jumlah PDRB';
		$this->load->view('template',$data);
	}

	public function proyeksiPDRB_edit() {
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);

		$this->model_proyeksiPDRB->proyeksiPDRB_edit($id);
		redirect($kab_id."/proyeksiPDRB");
	}

	public function proyeksiPDRB_hapus()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);
		$this->model_proyeksiPDRB->proyeksiPDRB_hapus($id);
		redirect($kab_id."/proyeksiPDRB");
	}

	public function proyeksiPDRB_hapus_checked()
	{
		$kab_id = $this->uri->segment(1);
		$id = $this->uri->segment(3);

		$id = $_POST['id'];
        $this->model_proyeksiPDRB->proyeksiPDRB_hapus_checked($id);
		redirect($kab_id."/proyeksiPDRB");
	}

	public function proyeksipdrbAritmatik()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->model_proyeksiPDRB->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPDRB->Getnamakab($kab_id);
		$data['dataProyeksipdrbAritmatik'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbAritmatik($kab_id);
		$data['dataProyeksipdrbAritmatikStandar'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbAritmatikStandar($kab_id);
		$data['dataProyeksipdrbAritmatikStandarDeviasi'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbAritmatikStandarDeviasi($kab_id);
		$data['jumlahdatapp'] = $this->model_proyeksiPDRB->jumlah_data_pdrb($kab_id);
		$data['jumlahdataproyeksi'] = $this->model_proyeksiPDRB->jumlah_data_proyeksi($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pdrb/view_proyeksiPdrbAritmatik';
		$data['nama'] = 'Proyeksi PDRB Metode Aritmatik';
		$this->load->view('template',$data);
	}

	public function proyeksipdrbGeometri()
	{
		$kab_id = $this->uri->segment(1);
		
    	$data['dataKabupaten'] = $this->model_proyeksiPDRB->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPDRB->Getnamakab($kab_id);
		$data['dataProyeksipdrbGeometri'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbGeometri($kab_id);
		$data['dataProyeksipdrbGeometriStandar'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbGeometriStandar($kab_id);
		$data['dataProyeksipdrbGeometriStandarDeviasi'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbGeometriStandarDeviasi($kab_id);
		$data['jumlahdatapp'] = $this->model_proyeksiPDRB->jumlah_data_pdrb($kab_id);
		$data['jumlahdataproyeksi'] = $this->model_proyeksiPDRB->jumlah_data_proyeksi($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pdrb/view_proyeksiPdrbGeometri';
		$data['nama'] = 'Proyeksi PDRB Metode Geometri';
		$this->load->view('template',$data);
	}

	public function proyeksipdrbLQ()
	{
		$kab_id = $this->uri->segment(1);

    	$data['dataKabupaten'] = $this->model_proyeksiPDRB->GetKab();
    	$data['namaKabupaten'] = $this->model_proyeksiPDRB->Getnamakab($kab_id);
		$data['dataProyeksipdrbLQ'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbLQ($kab_id);
		$data['dataProyeksipdrbLQTotal'] = $this->model_proyeksiPDRB->GetDataProyeksiPdrbLQTotal($kab_id);
		$data['dataProyeksipdrbLQStandar'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbLQStandar($kab_id);
		$data['dataProyeksipdrbLQStandarDeviasi'] = $this->model_proyeksiPDRB->GetDataProyeksipdrbLQStandarDeviasi($kab_id);
		$data['jumlahdatapp'] = $this->model_proyeksiPDRB->jumlah_data_pdrb($kab_id);
		$data['jumlahdataproyeksi'] = $this->model_proyeksiPDRB->jumlah_data_proyeksi($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'pdrb/view_proyeksiPdrbLQ';
		$data['nama'] = 'Proyeksi PDRB Metode Least Square';
		$this->load->view('template',$data);
	}

}
