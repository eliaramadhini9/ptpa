<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class controller_kebutuhanLuasLahan extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('model_kebutuhanLuasLahan');
		$this->load->model('model_notifikasi');
	}	

	public function kebutuhanLuasLahan()
	{
		$kab_id = $this->uri->segment(1);

		if ($this->input->post('tambahKebutuhanLuasLahan')) {
			$this->model_kebutuhanLuasLahan->tambahKebutuhanLuasLahan($kab_id);
 			redirect($kab_id."/kebutuhanLuasLahan");
 		}

    	$data['dataKabupaten'] = $this->model_kebutuhanLuasLahan->GetKab();
    	$data['namaKabupaten'] = $this->model_kebutuhanLuasLahan->Getnamakab($kab_id);
		$data['dataKebutuhanLuasLahan'] = $this->model_kebutuhanLuasLahan->dataKebutuhanLuasLahan($kab_id);
		$data['kebutuhanLuasLahan'] = $this->model_kebutuhanLuasLahan->kebutuhanLuasLahan($kab_id);
    	$data['notif'] = $this->model_notifikasi->notifikasi();
		$data['view_name'] = 'view_kebutuhanLuasLahan';
		$data['nama'] = 'Kebutuhan Luas Lahan';
		$this->load->view('template',$data);
	}
}
