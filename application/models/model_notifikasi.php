<?php

class model_notifikasi extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function getDataKomentar() {
    return $this->db->get('tbl_komentar_pengunjung');
  }

  public function notifikasi() {
    return $this->db->query("select count(id) as belumBaca from tbl_komentar_pengunjung where status=0");
  }

  public function hapusKomentar($id) {
    $where = array('id' => $id );
    $this->db->delete('tbl_komentar_pengunjung',$where);
  }

  public function gantiStatus() {
    $where = array('id' => $this->input->post('id'),'kab_id' => $this->input->post('kab_id'));
    $data = array('status' => 1);
    $this->db->where($where);
    $this->db->update('tbl_komentar_pengunjung',$data);
    return true;
  }

}
?>