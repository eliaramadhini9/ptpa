<?php

class model_kebutuhanLuasLahan extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function proyeksikebutuhanLuasLahan_delete($kab_id)
  {
    return $this->db->query("delete from tbl_kebutuhan_luas_lahan where kab_id='$kab_id'");
  }

  public function proyeksiKebutuhanLuasLahan_tambah($i,$kab_id)
  {
    return $this->db->query("insert into tbl_kebutuhan_luas_lahan (kab_id, tahun) values ($kab_id,$i)");
  }

  public function dataKebutuhanLuasLahan($kab_id) {
    return $this->db->query("select * from view_kebutuhan_luas_lahan where kab_id='$kab_id' order by tahun asc LIMIT 1,15 ");
  }

  public function kebutuhanLuasLahan($kab_id)
  {
    return $this->db->query("Select * from view_kebutuhan_luas_lahan where kab_id='$kab_id' ORDER BY tahun ASC LIMIT 1,1");
  }

  public function tambahKebutuhanLuasLahan($kab_id)
  {
    $data = array(
          'dens_sampah_awal' => $this->input->post('dens_sampah_awal'),
          'dens_sampah_kompaksi' => $this->input->post('dens_sampah_kompaksi'), 
          'ketebalan_lift' => $this->input->post('ketebalan_lift'),
          'tebal_lapisan' => $this->input->post('tebal_lapisan'),
          'jumlah_lift' => $this->input->post('jumlah_lift'),
          'luas_tiapsel' => $this->input->post('luas_tiapsel'),
          'masa_landfill' => $this->input->post('masa_landfill'),         
    );

    $this->db->where('kab_id',$kab_id);
    $this->db->update('tbl_kebutuhan_luas_lahan', $data);
    return;   
  }
  
}
?>