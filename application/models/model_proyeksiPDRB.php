<?php

class model_proyeksiPDRB extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function GetDataProyeksipdrb($kab_id) {
    return $this->db->query("select * from view_pdrb where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbTotal($kab_id) {
    return $this->db->query("select * from view_pdrb_total where kab_id='$kab_id'");
  }

  public function GetDataProyeksi_pdrb($kab_id) {
    return $this->db->query("select *, (tahun_akhir+14) as proyeksi15tahun, (tahun_akhir+29) as proyeksi30tahun, (tahun_akhir+49) as proyeksi50tahun from view_tahunakhir_pdrb1 where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbAritmatik($kab_id) {
    return $this->db->query("select * from view_pdrb_aritmatik where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbAritmatikStandar($kab_id) {
    return $this->db->query("select * from view_pdrb_aritmatik_standar where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbAritmatikStandarDeviasi($kab_id) {
    return $this->db->query("select * from view_pdrb_aritmatik_deviasi where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbGeometri($kab_id) {
    return $this->db->query("select * from view_pdrb_geometri where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbGeometriStandar($kab_id) {
    return $this->db->query("select * from view_pdrb_geometri_standar where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbGeometriStandarDeviasi($kab_id) {
    return $this->db->query("select * from view_pdrb_geometri_deviasi where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbLQ($kab_id) {
    return $this->db->query("select a.*, b.* from view_pdrb_lq2 a left join view_pdrb_lq1_total b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function GetDataProyeksiPdrbLQTotal($kab_id) {
    return $this->db->query("select * from view_pdrb_lq1_total b where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbLQStandar($kab_id) {
    return $this->db->query("select * from view_pdrb_lq_standar where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbLQStandarDeviasi($kab_id) {
    return $this->db->query("select * from view_pdrb_lq_deviasi where kab_id='$kab_id'");
  }

  public function proyeksiPDRB_tambah($kab_id)
  {
    $pdrb = $this->input->post('pdrb');
    $data = array(
      'kab_id' => $kab_id,
      'tahun' => $this->input->post('tahun'),
      'tahun_ke' => $this->input->post('tahun_ke'),
      'pdrb' => $this->input->post('pdrb'),
      'pdrb' => (!empty($pdrb)) ? $pdrb : null,
  );

     $this->db->insert('tbl_pdrb',$data);
  }

  public function proyeksi_pdrb_tambah($i,$kab_id)
  {
    return $this->db->query("insert into tbl_pdrb_proyeksi (kab_id,tahun_proyeksi) values('$kab_id','$i')");
  }

  public function proyeksi_pdrb_truncate()
  {
    return $this->db->query("truncate table tbl_pdrb_proyeksi");
  }

  public function proyeksi_pdrb_delete($kab_id)
  {
    return $this->db->query("delete from tbl_pdrb_proyeksi where kab_id='$kab_id'");
  }

  public function proyeksiPDRB_edit($id)
  {
    $data = array(
          'tahun' => $this->input->post('tahun'),
          'tahun_ke' => $this->input->post('tahun_ke'),
          'pdrb' => $this->input->post('pdrb'),        
    );
    $this->db->where('id',$id);
    $this->db->update('tbl_pdrb', $data);
    return TRUE;
  }

  public function proyeksiPDRB_hapus($id) {
    return $this->db->query("delete from tbl_pdrb where id='$id'");
  } 

  public function proyeksiPDRB_hapus_checked($id) {
    $this->db->where_in('id', $id);
    $this->db->delete('tbl_pdrb');
  }

//lain-lain

  public function jumlah_data_pdrb($kab_id) {
    return $this->db->query("select count(tahun) as jumlah_data from tbl_pdrb where kab_id='$kab_id'");
  }

  public function jumlah_data_proyeksi($kab_id) {
    return $this->db->query("select * from tbl_pdrb_proyeksi where kab_id='$kab_id'");
  }

}
?>