<?php

class model_proyeksiPenduduk extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function GetDataProyeksiPenduduk($kab_id) {
    return $this->db->query("select * from view_pp where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukTotal($kab_id) {
    return $this->db->query("select * from view_pp_total where kab_id='$kab_id'");
  } 

  public function GetDataProyeksi_pp($kab_id) {
    return $this->db->query("select *, (tahun_akhir+14) as proyeksi15tahun, (tahun_akhir+29) as proyeksi30tahun, (tahun_akhir+49) as proyeksi50tahun from view_tahunakhir_pp1 where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukAritmatik($kab_id) {
    return $this->db->query("select * from view_pp_aritmatik where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukAritmatikStandar($kab_id) {
    return $this->db->query("select * from view_pp_aritmatik_standar where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukAritmatikStandarDeviasi($kab_id) {
    return $this->db->query("select * from view_pp_aritmatik_deviasi where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukGeometri($kab_id) {
    return $this->db->query("select * from view_pp_geometri where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukGeometriStandar($kab_id) {
    return $this->db->query("select * from view_pp_geometri_standar where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukGeometriStandarDeviasi($kab_id) {
    return $this->db->query("select * from view_pp_geometri_deviasi where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukLQ($kab_id) {
    return $this->db->query("select * from view_pp_lq2 where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukLQTotal($kab_id) {
    return $this->db->query("select * from view_pp_lq1_total b where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukLQStandar($kab_id) {
    return $this->db->query("select * from view_pp_lq_standar where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukLQStandarDeviasi($kab_id) {
    return $this->db->query("select * from view_pp_lq_deviasi where kab_id='$kab_id'");
  }

  public function proyeksiPenduduk_tambah($kab_id)
  {
    $jumlah_penduduk = $this->input->post('jumlah_penduduk');
    $data = array(
      'kab_id' => $kab_id,
      'tahun' => $this->input->post('tahun'),
      'tahun_ke' => $this->input->post('tahun_ke'),
      'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),
      'jumlah_penduduk' => (!empty($jumlah_penduduk)) ? $jumlah_penduduk : null,
    );
     $this->db->insert('tbl_pp',$data);
  }

  public function proyeksi_pp_tambah($i,$kab_id)
  {
    return $this->db->query("insert into tbl_pp_proyeksi (tahun_proyeksi,kab_id) values('$i','$kab_id')");
  }

  public function proyeksi_pp_delete($kab_id)
  {
    return $this->db->query("delete from tbl_pp_proyeksi where kab_id='$kab_id'");
  }

  public function proyeksiPenduduk_edit($id)
  {
    $data = array(
          'tahun' => $this->input->post('tahun'),
          'tahun_ke' => $this->input->post('tahun_ke'),
          'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),        
    );

    $this->db->where('id',$id);
    $this->db->update('tbl_pp', $data);
    return TRUE;
  }

  public function proyeksiPenduduk_hapus($id) {
    return $this->db->query("delete from tbl_pp where id='$id'");
  }

  public function proyeksiPenduduk_hapus_checked($id) {
    $this->db->where_in('id', $id);
    $this->db->delete('tbl_pp');
  }

//lain-lain

  public function jumlah_data_pp($kab_id) {
    return $this->db->query("select count(tahun) as jumlah_data from tbl_pp where kab_id='$kab_id'");
  }

  public function jumlah_data_proyeksi($kab_id) {
    return $this->db->query("select * from tbl_pp_proyeksi where kab_id='$kab_id'");
  }

}
?>