<?php

class model_beranda extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  
  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }
  
  public function GetKab1($kab_id) {
    return $this->db->query("select a.*, count(b.kecamatan) as jml_kec from tbl_kab a left JOIN tbl_kecamatan b on(a.id_kab=b.kab_id) group by a.id_kab asc");
  }

  public function GetKec($kab_id) {
    return $this->db->query("select * from tbl_kecamatan where kab_id=$kab_id order by kecamatan asc");
  }

  public function GetKabID() {
    return $this->db->query("select * from tbl_kab");
  }


  public function GetKecID() {
    return $this->db->query("select * from tbl_kecamatan");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }
   public function Getnamakec($kab_id) {
    return $this->db->query("select * from tbl_kecamatan where kab_id='$kab_id'");
  }


  public function proyeksiPendudukAritmatik($kab_id) {
    $query = $this->db->query("select * from view_pp_aritmatik where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function proyeksiPendudukGeometri($kab_id) {
    $query = $this->db->query("select * from view_pp_geometri where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function proyeksiPendudukLS($kab_id) {
    $query = $this->db->query("select * from view_pp_lq2 where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

   public function proyeksiPdrbAritmatik($kab_id) {
    $query = $this->db->query("select * from view_pdrb_aritmatik where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function proyeksiPdrbGeometri($kab_id) {
    $query = $this->db->query("select * from view_pdrb_geometri where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function proyeksiPdrbLS($kab_id) {
    $query = $this->db->query("select * from view_pdrb_lq2 where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

//lain-lain

  public function jumlah_data_proyeksi_pp($kab_id) {
    return $this->db->query("select count(tahun_proyeksi)+1 as jumlah_data_proyeksi from tbl_pp_proyeksi where kab_id='$kab_id'");
  }

  public function jumlah_data_proyeksi_pdrb($kab_id) {
    return $this->db->query("select count(tahun_proyeksi)+1 as jumlah_data_proyeksi from tbl_pdrb_proyeksi where kab_id='$kab_id'");
  }

  public function tambahKabupaten()
  {
    $data = array(
      'kabupaten' => $this->input->post('kabupaten'),
      'tahun' => $this->input->post('tahun'),
    );
     $this->db->insert('tbl_kab',$data);
  }

  public function editKabupaten($id_kab)
  {
    $data = array(
          'kabupaten' => $this->input->post('kabupaten'),
          'tahun' => $this->input->post('tahun'),        
    );

    $this->db->where('id_kab',$id_kab);
    $this->db->update('tbl_kab', $data);
    return TRUE;
  }

  public function hapusKabupaten($id_kab) {
    return $this->db->query("delete from tbl_kab where id_kab='$id_kab'");
  }


  public function tambahKecamatan($kab_id)
  {
    $data = array(
      'kab_id' => $kab_id,
      'kecamatan' => $this->input->post('kecamatan'),
      'luas' => $this->input->post('luas'),
      'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),
      'kepadatan' => $this->input->post('kepadatan'),
    );
     $this->db->insert('tbl_kecamatan',$data);
  }

  public function editKecamatan($id_kecamatan)
  {
    $data = array(
      'kecamatan' => $this->input->post('kecamatan'),
      'luas' => $this->input->post('luas'),
      'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),
      'kepadatan' => $this->input->post('kepadatan'),    
    );

    $this->db->where('id_kecamatan',$id_kecamatan);
    $this->db->update('tbl_kecamatan', $data);
    return TRUE;
  }

  public function hapusKecamatan($id_kecamatan) {
    return $this->db->query("delete from tbl_kecamatan where id_kecamatan='$id_kecamatan'");
  }


}
?>