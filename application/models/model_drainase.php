<?php

class model_drainase extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function GetDataDrainaseMenit($kab_id) {
    return $this->db->query("select * from tbl_datahujan where kab_id='$kab_id' order by tahun asc");
  }

  public function GetDataDrainaseJam($kab_id) {
    return $this->db->query("select a.*, b.* from view_datahujan a left join view_datahujan_total b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id' ");
  }

  public function GetDataFrekuensiDrainase($kab_id) {
    return $this->db->query("select * from view_analisa_frekuensi_hujan where kab_id='$kab_id' ");
  }

  public function GetDataKalaUlangDrainase($kab_id) {
    return $this->db->query("select * from view_kalaulang3 where kab_id='$kab_id' ");
  }

  public function GetDataKalaUlangDrainase3($kab_id) {
    return $this->db->query("select * from view_kalaulang25tahun where kab_id='$kab_id' ");
  }

  public function GetdataTotalKalaUlang($kab_id) {
    return $this->db->query("select * from view_kalaulang_total where kab_id='$kab_id' ");
  }

  public function GetdataTotalKalaUlang25Tahun($kab_id) {
    return $this->db->query("select * from view_kalaulang25tahun_total where kab_id='$kab_id' ");
  }

  public function drainase_tambah($kab_id)
  {
    $data = array(
      'kab_id' => $kab_id,
      'tahun' => $this->input->post('tahun'),
      'menit5' => $this->input->post('menit5'),
      'menit15' => $this->input->post('menit15'),
      'menit30' => $this->input->post('menit30'),
      'menit45' => $this->input->post('menit45'),
      'menit60' => $this->input->post('menit60'),
      'menit120' => $this->input->post('menit120'),
      'menit180' => $this->input->post('menit180'),
      'menit360' => $this->input->post('menit360'),
      'menit720' => $this->input->post('menit720'),        
      'menit1440' => $this->input->post('menit1440'),        
    );
     $this->db->insert('tbl_datahujan',$data);
  }

  public function drainase_edit($id,$kab_id)
  {

    $data = array(
          'kab_id' => $kab_id,
          'tahun' => $this->input->post('tahun'),
          'menit5' => $this->input->post('menit5'),
          'menit15' => $this->input->post('menit15'),
          'menit30' => $this->input->post('menit30'),
          'menit45' => $this->input->post('menit45'),
          'menit60' => $this->input->post('menit60'),
          'menit120' => $this->input->post('menit120'),
          'menit180' => $this->input->post('menit180'),
          'menit360' => $this->input->post('menit360'),
          'menit720' => $this->input->post('menit720'),        
          'menit1440' => $this->input->post('menit1440'),        
    );

    $this->db->where('id',$id);
    $this->db->update('tbl_datahujan', $data);
    return TRUE;
  }

  public function drainase_hapus_checked($id_drainase) {
    $this->db->where_in('id_drainase', $id_drainase);
    $this->db->delete('tbl_datahujan');
  }

  public function drainase_hapus($id,$kab_id) {
    return $this->db->query("delete from tbl_datahujan where id='$id'");
  } 

}
?>