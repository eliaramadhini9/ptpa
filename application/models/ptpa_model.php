<?php

class ptpa_model extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }


//<!--fungsi proyeksi penduduk--->

  public function GetDataProyeksiPenduduk($kab_id) {
    return $this->db->query("select a.*,b.* from view_pp a join view_pp_total b where a.kab_id='$kab_id' and b.kab_id='$kab_id'");
  }

  public function GetDataProyeksi_pp($kab_id) {
    return $this->db->query("select *, (tahun_akhir+14) as proyeksi15tahun, (tahun_akhir+29) as proyeksi30tahun from view_tahunakhir_pp1 where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukAritmatik($kab_id) {
    return $this->db->query("select * from view_pp_aritmatik where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukAritmatikStandar($kab_id) {
    return $this->db->query("select a.*, b.* from view_pp_aritmatik_standar a left join view_pp_aritmatik_deviasi b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukGeometri($kab_id) {
    return $this->db->query("select * from view_pp_geometri where kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukGeometriStandar($kab_id) {
    return $this->db->query("select a.*, b.* from view_pp_geometri_standar a left join view_pp_geometri_deviasi b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukLQ($kab_id) {
    return $this->db->query("select a.*, b.* from view_pp_lq2 a left join view_pp_lq1_total b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function GetDataProyeksiPendudukLQStandar($kab_id) {
    return $this->db->query("select a.*, b.* from view_pp_lq_standar a left join view_pp_lq_deviasi b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function proyeksiPenduduk_tambah($kab_id)
  {
    $jumlah_penduduk = $this->input->post('jumlah_penduduk');
    $data = array(
      'kab_id' => $kab_id,
      'tahun' => $this->input->post('tahun'),
      'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),
      'jumlah_penduduk' => (!empty($jumlah_penduduk)) ? $jumlah_penduduk : null,
    );
     $this->db->insert('tbl_pp',$data);
  }

  public function proyeksi_tambah($i,$kab_id)
  {
    return $this->db->query("insert into tbl_pp_proyeksi (tahun_proyeksi,kab_id) values('$i','$kab_id')");
  }

  public function timbulan_nondosmetik_tambah($i,$kab_id)
  {
    return $this->db->query("insert into tbl_timbulan_nondomestik (tahun,kab_id) values('$i','$kab_id')");
  }

  public function domestik_tambah($o)
  {
    return $this->db->query("insert into tbl_timbulan_domestik (kab_id, tahun) values('1','$o')");
  }

  public function domestik_tambah_1($o)
  {
    return $this->db->query("insert into tbl_timbulan_domestik (kab_id, tahun) values(NULL,NULL)"); 
  }

  public function nondomestik_tambah($o)
  {
    return $this->db->query("insert into tbl_timbulan_nondomestik (kab_id, tahun) values('1','$o')");
  }

  public function proyeksi_pp_delete($kab_id)
  {
    return $this->db->query("delete from tbl_pp_proyeksi where kab_id='$kab_id'");
  }

  public function domestik_delete($kab_id)
  {
    return $this->db->query("delete from tbl_timbulan_domestik where kab_id='$kab_id' ");
  }

  public function nondomestik_delete($kab_id)
  {
    return $this->db->query("delete from tbl_timbulan_nondomestik where kab_id='$kab_id' ");
  }

  public function proyeksiPenduduk_edit($id)
  {
    $data = array(
          'tahun' => $this->input->post('tahun'),
          'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),        
    );

    $this->db->where('id',$id);
    $this->db->update('tbl_pp', $data);
    return TRUE;
  }

  public function proyeksiPenduduk_hapus($id) {
    return $this->db->query("delete from tbl_pp where id='$id'");
  }

  public function proyeksiPenduduk_hapus_checked($id) {
    $this->db->where_in('id', $id);
    $this->db->delete('tbl_pp');
  }


//<!--fungsi proyeksi pdrb--->

  public function GetDataProyeksipdrb($kab_id) {
    return $this->db->query("select a.*,b.* from view_pdrb a join view_pdrb_total b where a.kab_id='$kab_id' and b.kab_id='$kab_id'");
  }

  public function GetDataProyeksi_pdrb($kab_id) {
    return $this->db->query("select *, (tahun_akhir+14) as proyeksi from view_tahunakhir_pdrb1 where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbAritmatik($kab_id) {
    return $this->db->query("select * from view_pdrb_aritmatik where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbAritmatikStandar($kab_id) {
    return $this->db->query("select a.*, b.* from view_pdrb_aritmatik_standar a left join view_pdrb_aritmatik_deviasi b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbAritmatikStandar_fix() {
    return $this->db->query("select * from view_pdrb_aritmatik_standar_fix join view_pdrb_aritmatik_deviasi_fix");
  }

  public function GetDataProyeksipdrbGeometri($kab_id) {
    return $this->db->query("select * from view_pdrb_geometri where kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbGeometriStandar($kab_id) {
    return $this->db->query("select a.*,b.* from view_pdrb_geometri_standar a left join view_pdrb_geometri_deviasi b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbLQ($kab_id) {
    return $this->db->query("select a.*, b.* from view_pdrb_lq2 a left join view_pdrb_lq1_total b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function GetDataProyeksipdrbLQStandar($kab_id) {
    return $this->db->query("select a.*, b.* from view_pdrb_lq_standar a left join view_pdrb_lq_deviasi b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id'");
  }

  public function proyeksiPDRB_tambah($kab_id)
  {
    $pdrb = $this->input->post('pdrb');
    $data = array(
      'kab_id' => $kab_id,
      'tahun' => $this->input->post('tahun'),
      'pdrb' => $this->input->post('pdrb'),
      'pdrb' => (!empty($pdrb)) ? $pdrb : null,
  );

     $this->db->insert('tbl_pdrb',$data);
  }

  public function proyeksi_pdrb_tambah($i,$kab_id)
  {
    return $this->db->query("insert into tbl_pdrb_proyeksi (kab_id,tahun_proyeksi) values('$kab_id','$i')");
  }

  public function jumlah_timbulan($i)
  {
    return $this->db->query("insert into tbl_timbulan_domestik values('','','$i','','')");
  }

  public function proyeksi_pdrb_truncate()
  {
    return $this->db->query("truncate table tbl_pdrb_proyeksi");
  }

  public function proyeksi_pdrb_delete($kab_id)
  {
    return $this->db->query("delete from tbl_pdrb_proyeksi where kab_id='$kab_id'");
  }

  public function id_timbulan($kab_id)
  {
    return $this->db->query("Select tahun from view_domestik_5 where kab_id='$kab_id' ORDER BY tahun ASC LIMIT 1,1");
  }

  public function a_timbulan($kab_id)
  {
    return $this->db->query("select pert_timb, pert_pddk from view_domestik_5 where kab_id='$kab_id' ORDER BY tahun ASC LIMIT 2,1");
  }

  public function tingkat_pelayanan($tingkat_pelayanan,$u,$kab_id)
  {
    return $this->db->query("UPDATE tbl_timbulan_domestik SET tingkat_pelayanan=$tingkat_pelayanan where kab_id='$kab_id' and tahun=$u");
  }

  public function proyeksiPDRB_edit($id)
  {
    $data = array(
          'tahun' => $this->input->post('tahun'),
          'pdrb' => $this->input->post('pdrb'),        
    );
    $this->db->where('id',$id);
    $this->db->update('tbl_pdrb', $data);
    return TRUE;
  }

  public function timbulanPDRB_edit($tahun,$data)
  {
    $this->db->where('tahun',$tahun);
    $this->db->update('tbl_timbulan_domestik', $data);
    return TRUE;
  }

  public function coba_edit($a,$b, $kab_id)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_pdrb=$b where tahun=$a and kab_id='$kab_id'");    
  }

  public function coba_edit_1($e,$i,$kab_id)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_pdrb=$e where tahun=$i and kab_id='$kab_id'");    
  }

  public function timbulan_pddk($f,$g,$kab_id)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_penduduk=$g where tahun=$f and kab_id='$kab_id' ");    
  }

  public function timbulan_pddk_1($k,$o)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_penduduk=$k where tahun=$o ");    
  }

  public function proyeksiPDRB_hapus($id) {
    return $this->db->query("delete from tbl_pdrb where id='$id'");
  } 

  public function proyeksiPDRB_hapus_checked($id) {
    $this->db->where_in('id', $id);
    $this->db->delete('tbl_pdrb');
  }


//<!--fungsi data drainase--->

  public function GetDataDrainaseMenit($kab_id) {
    return $this->db->query("select * from tbl_datahujan where kab_id='$kab_id' order by tahun asc");
  }

  public function GetDataDrainaseJam($kab_id) {
    return $this->db->query("select a.*, b.* from view_datahujan a left join view_datahujan_total b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id' ");
  }

  public function GetDataFrekuensiDrainase($kab_id) {
    return $this->db->query("select * from view_analisa_frekuensi_hujan where kab_id='$kab_id' ");
  }

  public function GetDataKalaUlangDrainase($kab_id) {
    return $this->db->query("select * from view_kalaulang3 where kab_id='$kab_id' ");
  }

  public function GetDataKalaUlangDrainase3($kab_id) {
    return $this->db->query("select * from view_kalaulang25tahun where kab_id='$kab_id' ");
  }

  public function GetdataTotalKalaUlang($kab_id) {
    return $this->db->query("select * from view_kalaulang_total where kab_id='$kab_id' ");
  }

  public function GetdataTotalKalaUlang25Tahun($kab_id) {
    return $this->db->query("select * from view_kalaulang25tahun_total where kab_id='$kab_id' ");
  }


  public function drainase_tambah($kab_id)
  {
    $data = array(
      'kab_id' => $kab_id,
      'tahun' => $this->input->post('tahun'),
      'menit5' => $this->input->post('menit5'),
      'menit15' => $this->input->post('menit15'),
      'menit30' => $this->input->post('menit30'),
      'menit45' => $this->input->post('menit45'),
      'menit60' => $this->input->post('menit60'),
      'menit120' => $this->input->post('menit120'),
      'menit180' => $this->input->post('menit180'),
      'menit360' => $this->input->post('menit360'),
      'menit720' => $this->input->post('menit720'),        
      'menit1440' => $this->input->post('menit1440'),        
    );
     $this->db->insert('tbl_datahujan',$data);
  }

  public function drainase_edit($id,$kab_id)
  {

    $data = array(
          'kab_id' => $kab_id,
          'tahun' => $this->input->post('tahun'),
          'menit5' => $this->input->post('menit5'),
          'menit15' => $this->input->post('menit15'),
          'menit30' => $this->input->post('menit30'),
          'menit45' => $this->input->post('menit45'),
          'menit60' => $this->input->post('menit60'),
          'menit120' => $this->input->post('menit120'),
          'menit180' => $this->input->post('menit180'),
          'menit360' => $this->input->post('menit360'),
          'menit720' => $this->input->post('menit720'),        
          'menit1440' => $this->input->post('menit1440'),        
    );

    $this->db->where('id',$id);
    $this->db->update('tbl_datahujan', $data);
    return TRUE;
  }

  public function drainase_hapus_checked($id_drainase) {
    $this->db->where_in('id_drainase', $id_drainase);
    $this->db->delete('tbl_datahujan');
  }

  public function drainase_hapus($id,$kab_id) {
    return $this->db->query("delete from tbl_datahujan where id='$id'");
  } 


//<!--fungsi data timbulan sampah--->

  public function dataTimbulanDomestik($kab_id) {
    return $this->db->query("select * from view_domestik_5 where kab_id='$kab_id'");
    return $this->db->query("select * from tbl_timbulan_nondomestik");
  }

  public function dataTimbulanTotal($kab_id) {
    return $this->db->query("select * from view_timbulan_total where kab_id='$kab_id' limit 1,15");
  }

  public function dataMaterialBalanceAll($kab_id) {
    return $this->db->query("select * from view_material_balance_5 where kab_id='$kab_id' LIMIT 1,15");
  }

  public function dataMaterialBalanceTahun($kab_id) {
    return $this->db->query("select a.*, b.* from tbl_komposisi_karakteristik_sampah a left join view_total_material3 b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun1($kab_id) {
    return $this->db->query("select a.*, b.* from tbl_komposisi_karakteristik_sampah a left join view_total_material3 b on (a.kab_id=b.kab_id) where a.komponen!='organik' and a.kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun2($kab_id) {
    return $this->db->query("select a.*, b.* from view_komposisi_sampah a left join view_total_material3 b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun3($kab_id) {
    return $this->db->query("select a.*,b.* from view_komposisi_sampah1 a left join view_total_material3 b on (a.kab_id=b.kab_id) where a.kab_id='$kab_id' ");
  }

  public function kebutuhanLuasLahan_delete($kab_id)
  {
    return $this->db->query("delete from tbl_kebutuhan_luas_lahan where kab_id='$kab_id'");
  }

  public function kebutuhanLuasLahan_tambah($i)
  {
    return $this->db->query("insert into tbl_kebutuhan_luas_lahan (tahun) values ($i)");
  }

  public function dataKebutuhanLuasLahan($kab_id) {
    return $this->db->query("select * from view_kebutuhan_luas_lahan where kab_id='$kab_id' order by tahun asc LIMIT 1,15 ");
  }

  public function densSampahAwal($a,$kab_id)
  {
    return $this->db->query("update tbl_kebutuhan_luas_lahan set dens_sampah_awal=$a where kab_id='$kab_id' ");    
  }

  public function densSampahKompaksi($a,$kab_id)
  {
    return $this->db->query("update tbl_kebutuhan_luas_lahan set dens_sampah_kompaksi=$a where kab_id='$kab_id' ");    
  }

  public function ketebalanLift($a,$kab_id)
  {
    return $this->db->query("update tbl_kebutuhan_luas_lahan set ketebalan_lift=$a where kab_id='$kab_id' ");    
  }

  public function tebalLapisan($a,$kab_id)
  {
    return $this->db->query("update tbl_kebutuhan_luas_lahan set tebal_lapisan=$a where kab_id='$kab_id' ");    
  }

  public function jumlahLift($a,$kab_id)
  {
    return $this->db->query("update tbl_kebutuhan_luas_lahan set jumlah_lift=$a where kab_id='$kab_id' ");    
  }

  public function luasTiapsel($a,$kab_id)
  {
    return $this->db->query("update tbl_kebutuhan_luas_lahan set luas_tiapsel=$a where kab_id='$kab_id' ");    
  }

  public function masaLandfill($a,$kab_id)
  {
    return $this->db->query("update tbl_kebutuhan_luas_lahan set masa_landfill=$a where kab_id='$kab_id' ");    
  }



  

// <!-- LAIN - LAIN -->

  public function coba() {
    return $this->db->query("select * from tbl_pp");
  }
}
?>