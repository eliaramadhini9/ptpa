<?php

class model_calonTPA extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function GetKec($kab_id) {
    return $this->db->query("select * from tbl_kecamatan where kab_id=$kab_id order by kecamatan asc");
  }

  public function GetDataKriteriaPenyisih($kab_id) {
    return $this->db->query("select * from tbl_kriteria_penyisih where kab_id='$kab_id' order by calon_tpa asc");
  }

  public function GetCalonTPA($kab_id) {
    return $this->db->query("select calon_tpa from tbl_kriteria_penyisih where kab_id='$kab_id' order by jumlah desc limit 1");
  }

  public function buatCalonTPA($kab_id,$jumlah)
  {
    $data = array(
      'kab_id' => $kab_id,
      'calon_tpa' => $this->input->post('calon_tpa'),
      'bts_admin_jumlah' => $this->input->post('bts_admin_jumlah'),
      'hak_pemilik_jumlah' => $this->input->post('hak_pemilik_jumlah'),
      'kapasitas_jumlah' => $this->input->post('kapasitas_jumlah'),
      'jml_pemilik_jumlah' => $this->input->post('jml_pemilik_jumlah'),
      'part_mas_jumlah' => $this->input->post('part_mas_jumlah'),
      'tanah_jumlah' => $this->input->post('tanah_jumlah'),
      'air_tanah_jumlah' => $this->input->post('air_tanah_jumlah'),
      'aliran_air_jumlah' => $this->input->post('aliran_air_jumlah'),
      'kaitan_jumlah' => $this->input->post('kaitan_jumlah'),
      'bhy_banjir_jumlah' => $this->input->post('bhy_banjir_jumlah'),
      'penutup_jumlah' => $this->input->post('penutup_jumlah'),
      'intens_hujan_jumlah' => $this->input->post('intens_hujan_jumlah'),
      'jln_lokasi_jumlah' => $this->input->post('jln_lokasi_jumlah'),
      'transport_jumlah' => $this->input->post('transport_jumlah'),
      'jln_masuk_jumlah' => $this->input->post('jln_masuk_jumlah'),
      'lalin_jumlah' => $this->input->post('lalin_jumlah'),
      'tata_lahan_jumlah' => $this->input->post('tata_lahan_jumlah'),
      'pertanian_jumlah' => $this->input->post('pertanian_jumlah'),
      'cgr_alam_jumlah' => $this->input->post('cgr_alam_jumlah'),
      'biologis_jumlah' => $this->input->post('biologis_jumlah'),
      'bau_jumlah' => $this->input->post('bau_jumlah'),
      'estetika_jumlah' => $this->input->post('estetika_jumlah'),
      'jumlah' => $jumlah,
  );

     $this->db->insert('tbl_kriteria_penyisih',$data);
  }

  public function kriteriaPenyisih_edit($id_kriteria)
  {
    $data = array(
          'calon_tpa' => $this->input->post('calon_tpa'),
          'bts_admin_bobot' => $this->input->post('bts_admin_bobot'),
          'bts_admin_jumlah' => $this->input->post('bts_admin_jumlah'),
          'bts_admin1' => $this->input->post('bts_admin1'),
          'bts_admin2' => $this->input->post('bts_admin2'),
          'bts_admin3' => $this->input->post('bts_admin3'),
          'bts_admin4' => $this->input->post('bts_admin4'),
          'hak_pemilik_bobot' => $this->input->post('hak_pemilik_bobot'),
          'hak_pemilik_jumlah' => $this->input->post('hak_pemilik_jumlah'),
          'hak_pemilik1' => $this->input->post('hak_pemilik1'),
          'hak_pemilik2' => $this->input->post('hak_pemilik2'),
          'hak_pemilik3' => $this->input->post('hak_pemilik3'),
          'hak_pemilik4' => $this->input->post('hak_pemilik4'),
          'hak_pemilik5' => $this->input->post('hak_pemilik5'),
          'kapasitas_bobot' => $this->input->post('kapasitas_bobot'),
          'kapasitas_jumlah' => $this->input->post('kapasitas_jumlah'),
          'kapasitas1' => $this->input->post('kapasitas1'),
          'kapasitas2' => $this->input->post('kapasitas2'),
          'kapasitas3' => $this->input->post('kapasitas3'),
          'kapasitas4' => $this->input->post('kapasitas4'),
          'jml_pemilik_bobot' => $this->input->post('jml_pemilik_bobot'),
          'jml_pemilik_jumlah' => $this->input->post('jml_pemilik_jumlah'),
          'jml_pemilik1' => $this->input->post('jml_pemilik1'),
          'jml_pemilik2' => $this->input->post('jml_pemilik2'),
          'jml_pemilik3' => $this->input->post('jml_pemilik3'),
          'jml_pemilik4' => $this->input->post('jml_pemilik4'),
          'jml_pemilik5' => $this->input->post('jml_pemilik5'),
          'part_mas_bobot' => $this->input->post('part_mas_bobot'),
          'part_mas_jumlah' => $this->input->post('part_mas_jumlah'),
          'part_mas1' => $this->input->post('part_mas1'),
          'part_mas2' => $this->input->post('part_mas2'),
          'part_mas3' => $this->input->post('part_mas3'),
          'tanah_bobot' => $this->input->post('tanah_bobot'),
          'tanah_jumlah' => $this->input->post('tanah_jumlah'),
          'tanah1' => $this->input->post('tanah1'),
          'tanah2' => $this->input->post('tanah2'),
          'tanah3' => $this->input->post('tanah3'),
          'air_tanah_bobot' => $this->input->post('air_tanah_bobot'),
          'air_tanah_jumlah' => $this->input->post('air_tanah_jumlah'),
          'air_tanah1' => $this->input->post('air_tanah1'),
          'air_tanah2' => $this->input->post('air_tanah2'),
          'air_tanah3' => $this->input->post('air_tanah3'),
          'air_tanah4' => $this->input->post('air_tanah4'),
          'aliran_air_bobot' => $this->input->post('aliran_air_bobot'),
          'aliran_air_jumlah' => $this->input->post('aliran_air_jumlah'),
          'aliran_air1' => $this->input->post('aliran_air1'),
          'aliran_air2' => $this->input->post('aliran_air2'),
          'aliran_air3' => $this->input->post('aliran_air3'),
          'kaitan_bobot' => $this->input->post('kaitan_bobot'),
          'kaitan_jumlah' => $this->input->post('kaitan_jumlah'),
          'kaitan1' => $this->input->post('kaitan1'),
          'kaitan2' => $this->input->post('kaitan2'),
          'kaitan3' => $this->input->post('kaitan3'),
          'bhy_banjir_bobot' => $this->input->post('bhy_banjir_bobot'),
          'bhy_banjir_jumlah' => $this->input->post('bhy_banjir_jumlah'),
          'bhy_banjir1' => $this->input->post('bhy_banjir1'),
          'bhy_banjir2' => $this->input->post('bhy_banjir2'),
          'bhy_banjir3' => $this->input->post('bhy_banjir3'),
          'penutup_bobot' => $this->input->post('penutup_bobot'),
          'penutup_jumlah' => $this->input->post('penutup_jumlah'),
          'penutup1' => $this->input->post('penutup1'),
          'penutup2' => $this->input->post('penutup2'),
          'penutup3' => $this->input->post('penutup3'),
          'intens_hujan_bobot' => $this->input->post('intens_hujan_bobot'),
          'intens_hujan_jumlah' => $this->input->post('intens_hujan_jumlah'),
          'intens_hujan1' => $this->input->post('intens_hujan1'),
          'intens_hujan2' => $this->input->post('intens_hujan2'),
          'intens_hujan3' => $this->input->post('intens_hujan3'),
          'jln_lokasi_bobot' => $this->input->post('jln_lokasi_bobot'),
          'jln_lokasi_jumlah' => $this->input->post('jln_lokasi_jumlah'),
          'jln_lokasi1' => $this->input->post('jln_lokasi1'),
          'jln_lokasi2' => $this->input->post('jln_lokasi2'),
          'jln_lokasi3' => $this->input->post('jln_lokasi3'),
          'transport_bobot' => $this->input->post('transport_bobot'),
          'transport_jumlah' => $this->input->post('transport_jumlah'),
          'transport1' => $this->input->post('transport1'),
          'transport2' => $this->input->post('transport2'),
          'transport3' => $this->input->post('transport3'),
          'transport4' => $this->input->post('transport4'),
          'jln_masuk_bobot' => $this->input->post('jln_masuk_bobot'),
          'jln_masuk_jumlah' => $this->input->post('jln_masuk_jumlah'),
          'jln_masuk1' => $this->input->post('jln_masuk1'),
          'jln_masuk2' => $this->input->post('jln_masuk2'),
          'jln_masuk3' => $this->input->post('jln_masuk3'),
          'lalin_bobot' => $this->input->post('lalin_bobot'),
          'lalin_jumlah' => $this->input->post('lalin_jumlah'),
          'lalin1' => $this->input->post('lalin1'),
          'lalin2' => $this->input->post('lalin2'),
          'lalin3' => $this->input->post('lalin3'),
          'lalin4' => $this->input->post('lalin4'),
          'tata_lahan_bobot' => $this->input->post('tata_lahan_bobot'),
          'tata_lahan_jumlah' => $this->input->post('tata_lahan_jumlah'),
          'tata_lahan1' => $this->input->post('tata_lahan1'),
          'tata_lahan2' => $this->input->post('tata_lahan2'),
          'tata_lahan3' => $this->input->post('tata_lahan3'),
          'pertanian_bobot' => $this->input->post('pertanian_bobot'),
          'pertanian_jumlah' => $this->input->post('pertanian_jumlah'),
          'pertanian1' => $this->input->post('pertanian1'),
          'pertanian2' => $this->input->post('pertanian2'),
          'pertanian3' => $this->input->post('pertanian3'),
          'pertanian4' => $this->input->post('pertanian4'),
          'cgr_alam_bobot' => $this->input->post('cgr_alam_bobot'),
          'cgr_alam_jumlah' => $this->input->post('cgr_alam_jumlah'),
          'cgr_alam1' => $this->input->post('cgr_alam1'),
          'cgr_alam2' => $this->input->post('cgr_alam2'),
          'cgr_alam3' => $this->input->post('cgr_alam3'),
          'biologis_bobot' => $this->input->post('biologis_bobot'),
          'biologis_jumlah' => $this->input->post('biologis_jumlah'),
          'biologis1' => $this->input->post('biologis1'),
          'biologis2' => $this->input->post('biologis2'),
          'biologis3' => $this->input->post('biologis3'),
          'bau_bobot' => $this->input->post('bau_bobot'),
          'bau_jumlah' => $this->input->post('bau_jumlah'),
          'bau1' => $this->input->post('bau1'),
          'bau2' => $this->input->post('bau2'),
          'bau3' => $this->input->post('bau3'),
          'estetika_bobot' => $this->input->post('estetika_bobot'),
          'estetika_jumlah' => $this->input->post('estetika_jumlah'),
          'estetika1' => $this->input->post('estetika1'),
          'estetika2' => $this->input->post('estetika2'),
          'estetika3' => $this->input->post('estetika3'),
          'jumlah' => $this->input->post('jumlah'),
       );

    $this->db->where('id_kriteria',$id_kriteria);
    $this->db->update('tbl_kriteria_penyisih', $data);
    return TRUE;
  }

  public function kriteriaPenyisih_hapus($id_kriteria) {
  return $this->db->query("delete from tbl_kriteria_penyisih where id_kriteria='$id_kriteria'");
  }


//<!--fungsi proyeksi penduduk--->

 //lain-lain

  public function jumlahDataCalonTPA($kab_id) {
    return $this->db->query("select count(id_kriteria) as jumlahData from tbl_kriteria_penyisih where kab_id='$kab_id'");
  }
}
?>