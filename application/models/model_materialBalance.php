<?php

class model_materialBalance extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  

  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function dataMaterialBalanceAll($kab_id) {
    return $this->db->query("select * from view_material_balance_5 where kab_id='$kab_id' LIMIT 1,15");
  }

  public function dataMaterialBalanceTahun($kab_id) {
    return $this->db->query("select * from tbl_komposisi_karakteristik_sampah where kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahunTotal($kab_id) {
    return $this->db->query("select * from view_total_material3 where kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun1($kab_id) {
    return $this->db->query("select * from tbl_komposisi_karakteristik_sampah where komponen!='organik' and kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun1Total($kab_id) {
    return $this->db->query("select * from view_total_material3 where kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun2($kab_id) {
    return $this->db->query("select * from view_komposisi_sampah where kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun3($kab_id) {
    return $this->db->query("select * from view_komposisi_sampah1 where kab_id='$kab_id' ");
  }

  public function dataMaterialBalanceTahun3Total($kab_id) {
    return $this->db->query("select * from view_total_material3 where kab_id='$kab_id' ");
  }

  public function tambahKomponenSampah($kab_id)
  {
    $data = array(
      'kab_id' => $kab_id,
      'komponen' => $this->input->post('komponen'),
      'persen' => $this->input->post('persen'), 
      'kadar_air' => $this->input->post('kadar_air'),
      'kalor' => $this->input->post('kalor'), 
    );
     $this->db->insert('tbl_komposisi_karakteristik_sampah',$data);
  }


  public function editKomponenSampah($id)
  {
    $data = array(
          'komponen' => $this->input->post('komponen'),
          'persen' => $this->input->post('persen'), 
          'kadar_air' => $this->input->post('kadar_air'),
          'kalor' => $this->input->post('kalor'),
          'asumsi_rencana' => $this->input->post('asumsi_rencana'),          
    );

    $this->db->where('id',$id);
    $this->db->update('tbl_komposisi_karakteristik_sampah', $data);
    return;
  }

  public function hapusKomponenSampah($id) {
    return $this->db->query("delete from tbl_komposisi_karakteristik_sampah where id='$id'");
  }

}
?>