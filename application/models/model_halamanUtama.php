<?php

class model_halamanUtama extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }


   public function coba() {
    $query = $this->db->query("SELECT id_kab FROM `tbl_kab` ORDER BY id_kab DESC LIMIT 1");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function GetDataProyeksiPenduduk($kab_id) {
    $query = $this->db->query("select * from view_pp_aritmatik where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function GetDataProyeksiPdrb($kab_id) {
    $query = $this->db->query("select * from view_pdrb_aritmatik where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }



  public function getProyeksiPendudukGeometri($kab_id) {
    $query = $this->db->query("select * from view_pp_geometri where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function pertumbuhanproyeksiPendudukGeometri($kab_id) {
     return $this->db->query("select a.tahun, a.pn, (sum((a.pn - b.pn))/(count(a.tahun)+1)) as pertumbuhan from view_pp_geometri a left join view_pp_geometri b on (a.tahun=b.tahun+1) where a.kab_id=$kab_id and b.kab_id=$kab_id");
  }


  public function getProyeksiPdrbGeometri($kab_id) {
    $query = $this->db->query("select * from view_pdrb_geometri where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
  }

  public function dataTimbulan($kab_id) {
    $query = $this->db->query("select * from view_timbulan_total where kab_id='$kab_id'");

    if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
    }
  }

  public function pertumbuhanproyeksiPdrbGeometri($kab_id) {
     return $this->db->query("select a.tahun, a.pn, (sum((a.pn - b.pn))/(count(a.tahun)+1)) as pertumbuhan from view_pdrb_geometri a left join view_pdrb_geometri b on (a.tahun=b.tahun+1) where a.kab_id=$kab_id and b.kab_id=$kab_id");
  }

  public function getDataKomentar($kab_id) {
     return $this->db->query("SELECT * FROM tbl_komentar_pengunjung where kab_id=$kab_id");
  }

  public function tambahKomentar($kab_id) {
    $data = array(
      'kab_id' => $kab_id,
      'username' => $this->input->post('username'),
      'komentar' => $this->input->post('komentar'),        
    );

    $this->db->insert('tbl_komentar_pengunjung', $data);
    return TRUE;
  }

}
?>