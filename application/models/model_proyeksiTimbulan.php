<?php

class model_proyeksiTimbulan extends CI_model{
  public function __construct()
  {
    $this->load->database();
  }
  
  public function GetKab() {
    return $this->db->query("select * from tbl_kab order by kabupaten asc");
  }

   public function Getnamakab($kab_id) {
    return $this->db->query("select kabupaten from tbl_kab where id_kab='$kab_id'");
  }

  public function jumlah_timbulan($i)
  {
    return $this->db->query("insert into tbl_timbulan_domestik values('','','$i','','')");
  }

  public function id_timbulan($kab_id)
  {
    return $this->db->query("Select tahun from view_domestik_5 where kab_id='$kab_id' ORDER BY tahun ASC LIMIT 1,1");
  }

  public function tahunTimbulanNonDo($kab_id)
  {
    return $this->db->query("Select * from view_nondomestik where kab_id='$kab_id' ORDER BY tahun ASC LIMIT 0,1");
  }

  public function a_timbulan($kab_id)
  {
    return $this->db->query("select pert_timb, pert_pddk from view_domestik_5 where kab_id='$kab_id' ORDER BY tahun ASC LIMIT 2,1");
  }

  public function tingkat_pelayanan($tingkat_pelayanan,$u,$kab_id)
  {
    return $this->db->query("UPDATE tbl_timbulan_domestik SET tingkat_pelayanan=$tingkat_pelayanan where kab_id='$kab_id' and tahun=$u");
  }

  public function timbulan_pddk($a,$g,$kab_id)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_penduduk=$g where tahun=$a and kab_id='$kab_id'");    
  }

  public function timbulan_pddk_1($k,$o,$kab_id)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_penduduk=$k where tahun=$o and kab_id='$kab_id'");    
  }

  public function dataTimbulanDomestik($kab_id) {
    return $this->db->query("select * from view_domestik_5 where kab_id='$kab_id'");
  }

  public function dataTimbulanNonDomestik($kab_id) {
    return $this->db->query("select * from view_nondomestik3 where kab_id=$kab_id order by tahun asc");
  }

  public function tambahTimbulanNonDomestik($kab_id,$luas_wilayah,$jml_penduduk,$tk_jumlah,$tk_orang,$tk_unit_sampah,$sd_jumlah,$sd_orang,$sd_unit_sampah,$smp_jumlah,$smp_orang,$smp_unit_sampah,$sma_jumlah,$sma_orang,$sma_unit_sampah,$mush_jumlah,$mush_orang,$mush_unit_sampah,$mas_jumlah,$mas_orang,$mas_unit_sampah,$grj_jumlah,$grj_orang,$grj_unit_sampah,$pura_jumlah,$pura_orang,$pura_unit_sampah,$wrg_jumlah,$wrg_unit_sampah,$mini_jumlah,$mini_luas,$mini_unit_sampah,$toko_jumlah,$toko_unit_sampah,$rs_jumlah,$rs_bed,$rs_unit_sampah,$rsb_jumlah,$rsb_bed,$rsb_unit_sampah,$pusk_jumlah,$pusk_bed,$pusk_unit_sampah,$puskp_jumlah,$puskp_bed,$puskp_unit_sampah){
    return $this->db->query("update tbl_timbulan_nondomestik set luas_wilayah=$luas_wilayah,jml_penduduk=$jml_penduduk,tk_jumlah=$tk_jumlah,tk_orang=$tk_orang,tk_unit_sampah=$tk_unit_sampah,sd_jumlah=$sd_jumlah,sd_orang=$sd_orang,sd_unit_sampah=$sd_unit_sampah,smp_jumlah=$smp_jumlah,smp_orang=$smp_orang,smp_unit_sampah=$smp_unit_sampah,sma_jumlah=$sma_jumlah,sma_orang=$sma_orang,sma_unit_sampah=$sma_unit_sampah,mush_jumlah=$mush_jumlah,mush_orang=$mush_orang,mush_unit_sampah=$mush_unit_sampah,mas_jumlah=$mas_jumlah,mas_orang=$mas_orang,mas_unit_sampah=$mas_unit_sampah,grj_jumlah=$grj_jumlah,grj_orang=$grj_orang,grj_unit_sampah=$grj_unit_sampah,pura_jumlah=$pura_jumlah,pura_orang=$pura_orang,pura_unit_sampah=$pura_unit_sampah,wrg_jumlah=$wrg_jumlah,wrg_unit_sampah=$wrg_unit_sampah,mini_jumlah=$mini_jumlah,mini_luas=$mini_luas,mini_unit_sampah=$mini_unit_sampah,toko_jumlah=$toko_jumlah,toko_unit_sampah=$toko_unit_sampah,rs_jumlah=$rs_jumlah,rs_bed=$rs_bed,rs_unit_sampah=$rs_unit_sampah,rsb_jumlah=$rsb_jumlah,rsb_bed=$rsb_bed,rsb_unit_sampah=$rsb_unit_sampah,pusk_jumlah=$pusk_jumlah,pusk_bed=$pusk_bed,pusk_unit_sampah=$pusk_unit_sampah,puskp_jumlah=$puskp_jumlah,puskp_bed=$puskp_bed,puskp_unit_sampah=$puskp_unit_sampah where kab_id=$kab_id"); 
  }

  public function tambahTimbulanNonDomestik1($kab_id,$tahun){
     $data = array(
      'luas_wilayah' => $this->input->post('luas_wilayah'),
      'tk_jumlah' => $this->input->post('tk_jumlah'),
      'tk_orang' => $this->input->post('tk_orang'),
      'tk_unit_sampah' => $this->input->post('tk_unit_sampah'),
      'sd_jumlah' => $this->input->post('sd_jumlah'),
      'sd_orang' => $this->input->post('sd_orang'),
      'sd_unit_sampah' => $this->input->post('sd_unit_sampah'),
      'smp_jumlah' => $this->input->post('smp_jumlah'),
      'smp_orang' => $this->input->post('smp_orang'),
      'smp_unit_sampah' => $this->input->post('smp_unit_sampah'),
      'sma_jumlah' => $this->input->post('sma_jumlah'),
      'sma_orang' => $this->input->post('sma_orang'),
      'sma_unit_sampah' => $this->input->post('sma_unit_sampah'),
      'mush_jumlah' => $this->input->post('mush_jumlah'),
      'mush_orang' => $this->input->post('mush_orang'),
      'mush_unit_sampah' => $this->input->post('mush_unit_sampah'),
      'mas_jumlah' => $this->input->post('mas_jumlah'),
      'mas_orang' => $this->input->post('mas_orang'),
      'mas_unit_sampah' => $this->input->post('mas_unit_sampah'),
      'grj_jumlah' => $this->input->post('grj_jumlah'),
      'grj_orang' => $this->input->post('grj_orang'),
      'grj_unit_sampah' => $this->input->post('grj_unit_sampah'),
      'pura_jumlah' => $this->input->post('pura_jumlah'),
      'pura_orang' => $this->input->post('pura_orang'),
      'pura_unit_sampah' => $this->input->post('pura_unit_sampah'),
      'wrg_jumlah' => $this->input->post('wrg_jumlah'),
      'wrg_unit_sampah' => $this->input->post('wrg_unit_sampah'),
      'mini_jumlah' => $this->input->post('mini_jumlah'),
      'mini_luas' => $this->input->post('mini_luas'),
      'mini_unit_sampah' => $this->input->post('mini_unit_sampah'),
      'toko_jumlah' => $this->input->post('toko_jumlah'),
      'toko_unit_sampah' => $this->input->post('toko_unit_sampah'),
      'rs_jumlah' => $this->input->post('rs_jumlah'),
      'rs_bed' => $this->input->post('rs_bed'),
      'rs_unit_sampah' => $this->input->post('rs_unit_sampah'),
      'rsb_jumlah' => $this->input->post('rsb_jumlah'),
      'rsb_bed' => $this->input->post('rsb_bed'),
      'rsb_unit_sampah' => $this->input->post('rsb_unit_sampah'),
      'pusk_jumlah' => $this->input->post('pusk_jumlah'),
      'pusk_bed' => $this->input->post('pusk_bed'),
      'pusk_unit_sampah' => $this->input->post('pusk_unit_sampah'),
      'puskp_jumlah' => $this->input->post('puskp_jumlah'),
      'puskp_bed' => $this->input->post('puskp_bed'),
      'puskp_unit_sampah' => $this->input->post('puskp_unit_sampah'),
      'density' => $this->input->post('density'),
      'tingkat_pelayanan' => $this->input->post('tingkat_pelayanan'),
    );

     $where = array(
      'kab_id' => $kab_id,
     );

    $this->db->where($where);
    $this->db->update('tbl_timbulan_nondomestik', $data);
    return TRUE; 
  }

  public function tambahTimbulanNonDomestik2($kab_id,$tahun,$a){
  
     $where = array(
      'kab_id' => $kab_id,
      'tahun' => $tahun,
     );

    $this->db->where($where);
    $this->db->update('tbl_timbulan_nondomestik', $a);
    return TRUE; 
  }

    public function inicoba($tahun,$kab_id,$tk_jumlah,$tk_orang,$sd_jumlah,$sd_orang,$smp_jumlah,$smp_orang,$sma_jumlah,$sma_orang,$mush_jumlah,$mush_orang,$mas_jumlah,$mas_orang,$grj_jumlah,$grj_orang,$pura_jumlah,$pura_orang,$wrg_jumlah,$mini_jumlah,$mini_luas,$toko_jumlah,$rs_jumlah,$rs_bed,$rsb_jumlah,$rsb_bed,$pusk_jumlah,$pusk_bed,$puskp_jumlah,$puskp_bed){

    $data = array(
      'tk_jumlah' => $tk_jumlah,
      'tk_orang' => $tk_orang,
      'sd_jumlah' => $sd_jumlah,
      'sd_orang' => $sd_orang,
      'smp_jumlah' => $smp_jumlah,
      'smp_orang' => $smp_orang,
      'sma_jumlah' => $sma_jumlah,
      'sma_orang' => $sma_orang,
      'mush_jumlah' => $mush_jumlah,
      'mush_orang' => $mush_orang,
      'mas_jumlah' => $mas_jumlah,
      'mas_orang' => $mas_orang,
      'grj_jumlah' => $grj_jumlah,
      'grj_orang' => $grj_orang,
      'pura_jumlah' => $pura_jumlah,
      'pura_orang' => $pura_orang,
      'wrg_jumlah' => $wrg_jumlah,
      'mini_jumlah' => $mini_jumlah,
      'mini_luas' => $mini_luas,
      'toko_jumlah' => $toko_jumlah,
      'rs_jumlah' => $rs_jumlah,
      'rs_bed' => $rs_bed,
      'rsb_jumlah' => $rsb_jumlah,
      'rsb_bed' => $rsb_bed,
      'pusk_jumlah' => $pusk_jumlah,
      'pusk_bed' => $pusk_bed,
      'puskp_jumlah' => $puskp_jumlah,
      'puskp_bed' => $puskp_bed,
    );

     $where = array(
      'kab_id' => $kab_id,
      'tahun' => $tahun,
     );

    $this->db->where($where);
    $this->db->update('tbl_timbulan_nondomestik', $data);
    return TRUE; 
  }

    public function inicoba1($tk_jumlah,$b,$kab_id,$tk_orang,$sd_jumlah,$sd_orang,$smp_jumlah,$smp_orang,$sma_jumlah,$sma_orang,$mush_jumlah,$mush_orang,$mas_jumlah,$mas_orang,$grj_jumlah,$grj_orang,$pura_jumlah,$pura_orang,$wrg_jumlah,$mini_jumlah,$mini_luas,$toko_jumlah,$rs_jumlah,$rs_bed,$rsb_jumlah,$rsb_bed,$pusk_jumlah,$pusk_bed,$puskp_jumlah,$puskp_bed){
      $data = array(
      'tk_jumlah' => $tk_jumlah,
      'tk_orang' => $tk_orang,
      'sd_jumlah' => $sd_jumlah,
      'sd_orang' => $sd_orang,
      'smp_jumlah' => $smp_jumlah,
      'smp_orang' => $smp_orang,
      'sma_jumlah' => $sma_jumlah,
      'sma_orang' => $sma_orang,
      'mush_jumlah' => $mush_jumlah,
      'mush_orang' => $mush_orang,
      'mas_jumlah' => $mas_jumlah,
      'mas_orang' => $mas_orang,
      'grj_jumlah' => $grj_jumlah,
      'grj_orang' => $grj_orang,
      'pura_jumlah' => $pura_jumlah,
      'pura_orang' => $pura_orang,
      'wrg_jumlah' => $wrg_jumlah,
      'mini_jumlah' => $mini_jumlah,
      'mini_luas' => $mini_luas,
      'toko_jumlah' => $toko_jumlah,
      'rs_jumlah' => $rs_jumlah,
      'rs_bed' => $rs_bed,
      'rsb_jumlah' => $rsb_jumlah,
      'rsb_bed' => $rsb_bed,
      'pusk_jumlah' => $pusk_jumlah,
      'pusk_bed' => $pusk_bed,
      'puskp_jumlah' => $puskp_jumlah,
      'puskp_bed' => $puskp_bed,
    );

     $where = array(
      'kab_id' => $kab_id,
      'tahun' => $b,
     );

    $this->db->where($where);
    $this->db->update('tbl_timbulan_nondomestik', $data);
    return TRUE; 
  }

    public function inicoba3($kab_id,$luas_wilayah,$tingkat_pelayanan,$tk_unit_sampah,$sd_unit_sampah,$smp_unit_sampah,$sma_unit_sampah,$mush_unit_sampah,$mas_unit_sampah,$grj_unit_sampah,$pura_unit_sampah,$wrg_unit_sampah,$mini_unit_sampah,$toko_unit_sampah,$rs_unit_sampah,$rsb_unit_sampah,$pusk_unit_sampah,$puskp_unit_sampah,$density){

      $data = array(
      'luas_wilayah' => $luas_wilayah,
      'tingkat_pelayanan' => $tingkat_pelayanan,
      'tk_unit_sampah' => $tk_unit_sampah,
      'sd_unit_sampah' => $sd_unit_sampah,
      'smp_unit_sampah' => $smp_unit_sampah,
      'sma_unit_sampah' => $sma_unit_sampah,
      'mush_unit_sampah' => $mush_unit_sampah,
      'mas_unit_sampah' => $mas_unit_sampah,
      'grj_unit_sampah' => $grj_unit_sampah,
      'pura_unit_sampah' => $pura_unit_sampah,
      'wrg_unit_sampah' => $wrg_unit_sampah,
      'mini_unit_sampah' => $mini_unit_sampah,
      'toko_unit_sampah' => $toko_unit_sampah,
      'rs_unit_sampah' => $rs_unit_sampah,
      'rsb_unit_sampah' => $rsb_unit_sampah,
      'pusk_unit_sampah' => $pusk_unit_sampah,
      'puskp_unit_sampah' => $puskp_unit_sampah,
      'density' => $density,
      );
    $this->db->where('kab_id', $kab_id);
    $this->db->update('tbl_timbulan_nondomestik', $data);
    return TRUE; 
  }

  public function dataTimbulanTotal($kab_id) {
    return $this->db->query("select * from view_timbulan_total where kab_id='$kab_id' limit 1,15");
  }

  public function proyeksi_domestik_tambah($o,$kab_id)
  {
    return $this->db->query("insert into tbl_timbulan_domestik (tahun,kab_id) values('$o','$kab_id')");
  }

  public function proyeksi_nondomestik_tambah1($kab_id)
  {
    return $this->db->query("UPDATE `tbl_timbulan_nondomestik` c JOIN (SELECT b.pn, a.tahun FROM tbl_timbulan_nondomestik a
LEFT OUTER JOIN view_pp_aritmatik b ON (a.tahun = b.tahun AND a.kab_id=b.kab_id)) q SET c.jml_penduduk=q.pn WHERE c.kab_id=$kab_id AND c.tahun=q.tahun");
  }

  public function proyeksi_nondomestik_tambah($o,$kab_id)
  {
    return $this->db->query("insert into tbl_timbulan_nondomestik (tahun,kab_id) values('$o','$kab_id')");
  }


  public function proyeksi_nondomestik_tambah_($kab_id,$smph_terlayani)
  {
    return $this->db->query("update tbl_coba set smph_terlayani=$smph_terlayani where kab_id='$kab_id'");    
  }

  public function proyeksi_domestik_delete($kab_id)
  {
    return $this->db->query("delete from tbl_timbulan_domestik where kab_id='$kab_id' ");
  }

  public function proyeksi_nondomestik_delete($kab_id)
  {
    return $this->db->query("delete from tbl_timbulan_nondomestik where kab_id='$kab_id' ");
  }


  public function coba_edit($a,$b,$kab_id)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_pdrb=$b where tahun=$a and kab_id='$kab_id'");    
  }

  public function coba_edit_1($e,$i,$kab_id)
  {
    return $this->db->query("update tbl_timbulan_domestik set timbulan_pdrb=$e where tahun=$i and kab_id='$kab_id'");    
  }

  public function dataTkJumlah($a,$i,$kab_id)
  {
    return $this->db->query("update tbl_timbulan_nondomestik set tk_jumlah=$a where tahun=$i and kab_id='$kab_id'");    
  }
  
  public function a_timbulannon($kab_id)
  {
    return $this->db->query("select * from tbl_timbulan_nondomestik where kab_id='$kab_id' ORDER BY tahun ASC LIMIT 1,1");
  }

}
?>