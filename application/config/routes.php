<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['default_controller'] = 'controller_halamanUtama';
$route['halamanUtama'] = 'controller_halamanUtama/halamanUtama';
$route['perencanaanTPA'] = 'controller_halamanUtama/perencanaanTPA';
$route['tentang'] = 'controller_halamanUtama/tentang';
$route['dataKabupaten/(:any)'] = 'controller_halamanUtama/dataProyeksiKabupaten';

$route['(:any)/notifikasi'] = 'controller_notifikasi/notifikasi';
$route['(:any)/hapusKomentar/(:any)'] = 'controller_notifikasi/hapusKomentar/$1';

$route['(:any)/loginValidation'] = 'controller_login/loginValidation';
$route['login'] = 'controller_login/login';
$route['logout'] = 'controller_login/logout';
$route['tetapaja'] = 'controller_login/tetapaja';

$route['lockscreen'] = 'controller_login/lockscreen';

$route['(:any)/beranda'] = 'controller_beranda/beranda';
$route['(:any)/kecamatan'] = 'controller_beranda/kecamatan';
$route['(:any)/kabupaten'] = 'controller_beranda/kabupaten';
$route['(:any)/editKabupaten/(:any)'] = 'controller_beranda/editKabupaten/$1';
$route['(:any)/hapusKabupaten/(:any)'] = 'controller_beranda/hapusKabupaten/$1';
$route['(:any)/kecamatan'] = 'controller_beranda/kecamatan';
$route['(:any)/editKecamatan/(:any)'] = 'controller_beranda/editKecamatan/$1';
$route['(:any)/hapusKecamatan/(:any)'] = 'controller_beranda/hapusKecamatan/$1';


$route['(:any)/kriteriaPenyisih'] = 'controller_calonTPA/kriteriaPenyisih';
$route['(:any)/kriteriaPenyisih_hapus/(:any)'] = 'controller_calonTPA/kriteriaPenyisih_hapus/$1';
$route['(:any)/kriteriaPenyisih_edit/(:any)'] = 'controller_calonTPA/kriteriaPenyisih_edit/$1';


$route['(:any)/proyeksiPenduduk'] = 'controller_proyeksiPenduduk/proyeksiPenduduk';
$route['(:any)/proyeksiPendudukAritmatik'] = 'controller_proyeksiPenduduk/proyeksiPendudukAritmatik';
$route['(:any)/proyeksiPendudukGeometri'] = 'controller_proyeksiPenduduk/proyeksiPendudukGeometri';
$route['(:any)/proyeksiPendudukLQ'] = 'controller_proyeksiPenduduk/proyeksiPendudukLQ';
$route['(:any)/proyeksiPenduduk_hapus/(:any)'] = 'controller_proyeksiPenduduk/proyeksiPenduduk_hapus/$1';
$route['(:any)/proyeksiPenduduk_hapus_checked'] = 'controller_proyeksiPenduduk/proyeksiPenduduk_hapus_checked';
$route['(:any)/proyeksiPenduduk_edit/(:any)'] = 'controller_proyeksiPenduduk/proyeksiPenduduk_edit/$1';

$route['(:any)/proyeksiPDRB'] = 'controller_proyeksiPDRB/proyeksiPDRB';
$route['(:any)/proyeksiPDRBAritmatik'] = 'controller_proyeksiPDRB/proyeksipdrbAritmatik';
$route['(:any)/proyeksiPDRBGeometri'] = 'controller_proyeksiPDRB/proyeksipdrbGeometri';
$route['(:any)/proyeksiPDRBLQ'] = 'controller_proyeksiPDRB/proyeksipdrbLQ';
$route['(:any)/proyeksiPDRB_hapus_checked'] = 'controller_proyeksiPDRB/proyeksiPDRB_hapus_checked';
$route['(:any)/proyeksiPDRB_edit/(:any)'] = 'controller_proyeksiPDRB/proyeksiPDRB_edit/$1';
$route['(:any)/proyeksiPDRB_hapus/(:any)'] = 'controller_proyeksiPDRB/proyeksiPDRB_hapus/$1';


$route['(:any)/drainase'] = 'controller_drainase/drainase';
$route['(:any)/drainase_hapus_checked'] = 'controller_drainase/drainase_hapus_checked';
$route['(:any)/drainase_edit/(:any)'] = 'controller_drainase/drainase_edit/$1';
$route['(:any)/drainase_hapus/(:any)'] = 'controller_drainase/drainase_hapus/$1';

$route['(:any)/timbulanDomestik'] = 'controller_proyeksiTimbulan/timbulanDomestik';
$route['(:any)/timbulanPDRB_edit/(:any)'] = 'controller_proyeksiTimbulan/timbulanPDRB_edit/$1';
$route['(:any)/tingkatPelayanan_edit/(:any)'] = 'controller_proyeksiTimbulan/timbulanPDRB_edit/$1';
$route['(:any)/timbulanNonDomestik'] = 'controller_proyeksiTimbulan/timbulanNonDomestik';
$route['(:any)/timbulanTotal'] = 'controller_proyeksiTimbulan/timbulanTotal';

$route['(:any)/materialAllBalance'] = 'controller_materialBalance/materialAllBalance';
$route['(:any)/materialBalanceTahun'] = 'controller_materialBalance/materialBalanceTahun';
$route['(:any)/editKomponenSampah/(:any)'] = 'controller_materialBalance/editKomponenSampah/$1';
$route['(:any)/hapusKomponenSampah/(:any)'] = 'controller_materialBalance/hapusKomponenSampah/$1';

$route['(:any)/kebutuhanLuasLahan'] = 'controller_kebutuhanLuasLahan/kebutuhanLuasLahan';


