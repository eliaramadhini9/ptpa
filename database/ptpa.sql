-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2019 at 06:42 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ptpa`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `coba`
-- (See below for the actual view)
--
CREATE TABLE `coba` (
`tahun` int(5)
,`penduduk` decimal(64,2)
,`pdrb` double(19,2)
,`pert_pddk` decimal(64,4)
,`pert_pdrb` double(21,4)
,`pert_timb` double(21,4)
,`timbulan_pdrb` decimal(5,4)
);

-- --------------------------------------------------------

--
-- Table structure for table `cobaa`
--

CREATE TABLE `cobaa` (
  `coba1` int(5) NOT NULL,
  `coba2` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cobaa`
--

INSERT INTO `cobaa` (`coba1`, `coba2`) VALUES
(-8, 0),
(2000, 2008),
(2009, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(100) UNSIGNED NOT NULL,
  `username` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `email`, `password`, `nama`) VALUES
(1, 'elia', 'eliaramadhini9@gmail.com', '12345', 'elia ramadhini');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_analisafrekuensi`
--

CREATE TABLE `tbl_analisafrekuensi` (
  `id_analisafrekuensi` int(255) NOT NULL,
  `periode_ulang` int(255) DEFAULT NULL,
  `yt` decimal(56,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coba`
--

CREATE TABLE `tbl_coba` (
  `id` int(100) NOT NULL,
  `kab_id` int(100) NOT NULL,
  `tahun` int(5) NOT NULL,
  `smph_terlayani` decimal(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_coba`
--

INSERT INTO `tbl_coba` (`id`, `kab_id`, `tahun`, `smph_terlayani`) VALUES
(1, 1, 2016, '70.18'),
(2, 1, 2017, '70.86'),
(3, 1, 2018, '71.54'),
(4, 1, 2019, '72.22'),
(5, 1, 2020, '72.90'),
(6, 1, 2021, '73.58'),
(7, 1, 2022, '74.27'),
(8, 1, 2023, '74.95'),
(9, 1, 2024, '75.64'),
(10, 1, 2025, '76.33'),
(11, 1, 2026, '77.01'),
(12, 1, 2027, '77.70'),
(13, 1, 2028, '78.40'),
(14, 1, 2029, '79.09'),
(15, 1, 2030, '79.78'),
(16, 1, 2031, '80.48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datahujan`
--

CREATE TABLE `tbl_datahujan` (
  `id_drainase` int(255) NOT NULL,
  `kab_id` int(10) DEFAULT NULL,
  `tahun_drainase` year(4) DEFAULT NULL,
  `menit5` decimal(10,2) DEFAULT NULL,
  `menit15` decimal(10,2) DEFAULT NULL,
  `menit30` decimal(10,2) DEFAULT NULL,
  `menit45` decimal(10,2) DEFAULT NULL,
  `menit60` decimal(10,2) DEFAULT NULL,
  `menit120` decimal(10,2) DEFAULT NULL,
  `menit180` decimal(10,2) DEFAULT NULL,
  `menit360` decimal(10,2) DEFAULT NULL,
  `menit720` decimal(10,2) DEFAULT NULL,
  `menit1440` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datahujan`
--

INSERT INTO `tbl_datahujan` (`id_drainase`, `kab_id`, `tahun_drainase`, `menit5`, `menit15`, `menit30`, `menit45`, `menit60`, `menit120`, `menit180`, `menit360`, `menit720`, `menit1440`) VALUES
(1, 1, 2019, '5.10', '6.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(2, 1, 2018, '5.10', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(3, 1, 2017, '5.10', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datatahunterakhir`
--

CREATE TABLE `tbl_datatahunterakhir` (
  `tahun_proyeksi` int(5) NOT NULL,
  `jumlah_penduduk` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datatahunterakhir`
--

INSERT INTO `tbl_datatahunterakhir` (`tahun_proyeksi`, `jumlah_penduduk`) VALUES
(2017, 1199132);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_drainase`
--

CREATE TABLE `tbl_drainase` (
  `id_drainase` int(5) NOT NULL,
  `yt2` double(5,3) NOT NULL,
  `yt5` double(5,3) NOT NULL,
  `yt10` double(5,3) NOT NULL,
  `yt20` double(5,3) NOT NULL,
  `yt25` double(5,3) NOT NULL,
  `n` int(5) NOT NULL,
  `yn` double(5,4) NOT NULL,
  `sn` double(5,4) NOT NULL,
  `i5` decimal(5,2) NOT NULL,
  `i15` decimal(5,2) NOT NULL,
  `i30` decimal(5,2) NOT NULL,
  `i45` decimal(5,2) NOT NULL,
  `i60` decimal(5,2) NOT NULL,
  `i120` decimal(5,2) NOT NULL,
  `i180` decimal(5,2) NOT NULL,
  `i360` decimal(5,2) NOT NULL,
  `i720` decimal(5,2) NOT NULL,
  `i1440` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_drainase`
--

INSERT INTO `tbl_drainase` (`id_drainase`, `yt2`, `yt5`, `yt10`, `yt20`, `yt25`, `n`, `yn`, `sn`, `i5`, `i15`, `i30`, `i45`, `i60`, `i120`, `i180`, `i360`, `i720`, `i1440`) VALUES
(1, 0.367, 1.500, 2.251, 2.971, 3.199, 25, 0.5236, 1.0628, '298.85', '212.52', '132.82', '92.98', '117.88', '65.58', '48.70', '27.67', '13.84', '13.84');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_drainase2`
--

CREATE TABLE `tbl_drainase2` (
  `id_kalaulang` int(5) NOT NULL,
  `i` decimal(5,2) NOT NULL,
  `t` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_drainase2`
--

INSERT INTO `tbl_drainase2` (`id_kalaulang`, `i`, `t`) VALUES
(1, '298.85', 5),
(2, '212.52', 15),
(3, '132.82', 30),
(4, '92.98', 45),
(5, '117.88', 60),
(6, '65.58', 120),
(7, '48.70', 180),
(8, '27.67', 360),
(9, '13.84', 720);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kab`
--

CREATE TABLE `tbl_kab` (
  `id_kab` int(10) NOT NULL,
  `kabupaten` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kab`
--

INSERT INTO `tbl_kab` (`id_kab`, `kabupaten`) VALUES
(1, 'jepara'),
(2, 'indramayu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kriteriapenyisih`
--

CREATE TABLE `tbl_kriteriapenyisih` (
  `id_kriteria` int(255) NOT NULL,
  `kab_id` int(10) NOT NULL,
  `calon_tpa` varchar(255) NOT NULL,
  `jumlah` int(255) DEFAULT NULL,
  `bts_admin_bobot` int(255) DEFAULT NULL,
  `bts_admin_jumlah` int(100) NOT NULL,
  `bts_admin1` int(255) DEFAULT NULL,
  `bts_admin2` int(255) DEFAULT NULL,
  `bts_admin3` int(255) DEFAULT NULL,
  `bts_admin4` int(255) DEFAULT NULL,
  `hak_pemilik_bobot` int(255) DEFAULT NULL,
  `hak_pemilik_jumlah` int(100) NOT NULL,
  `hak_pemilik1` int(255) DEFAULT NULL,
  `hak_pemilik2` int(255) DEFAULT NULL,
  `hak_pemilik3` int(255) DEFAULT NULL,
  `hak_pemilik4` int(255) DEFAULT NULL,
  `hak_pemilik5` int(255) DEFAULT NULL,
  `kapasitas_bobot` int(255) DEFAULT NULL,
  `kapasitas1` int(255) DEFAULT NULL,
  `kapasitas2` int(255) DEFAULT NULL,
  `kapasitas3` int(255) DEFAULT NULL,
  `kapasitas4` int(255) DEFAULT NULL,
  `jml_pemilik_bobot` int(255) DEFAULT NULL,
  `jml_pemilik1` int(255) DEFAULT NULL,
  `jml_pemilik2` int(255) DEFAULT NULL,
  `jml_pemilik3` int(255) DEFAULT NULL,
  `jml_pemilik4` int(255) DEFAULT NULL,
  `jml_pemilik5` int(255) DEFAULT NULL,
  `part_mas_bobot` int(255) DEFAULT NULL,
  `part_mas1` int(255) DEFAULT NULL,
  `part_mas2` int(255) DEFAULT NULL,
  `part_mas3` int(255) DEFAULT NULL,
  `tanah_bobot` int(255) DEFAULT NULL,
  `tanah1` int(255) DEFAULT NULL,
  `tanah2` int(255) DEFAULT NULL,
  `tanah3` int(255) DEFAULT NULL,
  `air_tanah_bobot` int(255) DEFAULT NULL,
  `air_tanah1` int(255) DEFAULT NULL,
  `air_tanah2` int(255) DEFAULT NULL,
  `air_tanah3` int(255) DEFAULT NULL,
  `air_tanah4` int(255) DEFAULT NULL,
  `aliran_air_bobot` int(255) DEFAULT NULL,
  `aliran_air1` int(255) DEFAULT NULL,
  `aliran_air2` int(255) DEFAULT NULL,
  `aliran_air3` int(255) DEFAULT NULL,
  `kaitan_bobot` int(255) DEFAULT NULL,
  `kaitan1` int(255) DEFAULT NULL,
  `kaitan2` int(255) DEFAULT NULL,
  `kaitan3` int(255) DEFAULT NULL,
  `bhy_banjir_bobot` int(255) DEFAULT NULL,
  `bhy_banjir1` int(255) DEFAULT NULL,
  `bhy_banjir2` int(255) DEFAULT NULL,
  `bhy_banjir3` int(255) DEFAULT NULL,
  `penutup_bobot` int(255) DEFAULT NULL,
  `penutup1` int(255) DEFAULT NULL,
  `penutup2` int(255) DEFAULT NULL,
  `penutup3` int(255) DEFAULT NULL,
  `intens_hujan_bobot` int(255) DEFAULT NULL,
  `intens_hujan1` int(255) DEFAULT NULL,
  `intens_hujan2` int(255) DEFAULT NULL,
  `intens_hujan3` int(255) DEFAULT NULL,
  `jln_lokasi_bobot` int(255) DEFAULT NULL,
  `jln_lokasi1` int(255) DEFAULT NULL,
  `jln_lokasi2` int(255) DEFAULT NULL,
  `jln_lokasi3` int(255) DEFAULT NULL,
  `transport_bobot` int(255) DEFAULT NULL,
  `transport1` int(255) DEFAULT NULL,
  `transport2` int(255) DEFAULT NULL,
  `transport3` int(255) DEFAULT NULL,
  `transport4` int(255) DEFAULT NULL,
  `jln_masuk_bobot` int(255) DEFAULT NULL,
  `jln_masuk1` int(255) DEFAULT NULL,
  `jln_masuk2` int(255) DEFAULT NULL,
  `jln_masuk3` int(255) DEFAULT NULL,
  `lalin_bobot` int(255) DEFAULT NULL,
  `lalin1` int(255) DEFAULT NULL,
  `lalin2` int(255) DEFAULT NULL,
  `lalin3` int(255) DEFAULT NULL,
  `lalin4` int(255) DEFAULT NULL,
  `tata_lahan_bobot` int(255) DEFAULT NULL,
  `tata_lahan1` int(255) DEFAULT NULL,
  `tata_lahan2` int(255) DEFAULT NULL,
  `tata_lahan3` int(255) DEFAULT NULL,
  `pertanian_bobot` int(255) DEFAULT NULL,
  `pertanian1` int(255) DEFAULT NULL,
  `pertanian2` int(255) DEFAULT NULL,
  `pertanian3` int(255) DEFAULT NULL,
  `pertanian4` int(255) DEFAULT NULL,
  `cgr_alam_bobot` int(255) DEFAULT NULL,
  `cgr_alam1` int(255) DEFAULT NULL,
  `cgr_alam2` int(255) DEFAULT NULL,
  `cgr_alam3` int(255) DEFAULT NULL,
  `biologis_bobot` int(255) DEFAULT NULL,
  `biologis1` int(255) DEFAULT NULL,
  `biologis2` int(255) DEFAULT NULL,
  `biologis3` int(255) DEFAULT NULL,
  `bau_bobot` int(255) DEFAULT NULL,
  `bau1` int(255) DEFAULT NULL,
  `bau2` int(255) DEFAULT NULL,
  `bau3` int(255) DEFAULT NULL,
  `estetika_bobot` int(255) DEFAULT NULL,
  `estetika1` int(255) DEFAULT NULL,
  `estetika2` int(255) DEFAULT NULL,
  `estetika3` int(255) DEFAULT NULL,
  `air_tanah_jumlah` int(100) NOT NULL,
  `aliran_air_jumlah` int(100) NOT NULL,
  `bau_jumlah` int(100) NOT NULL,
  `bhy_banjir_jumlah` int(100) NOT NULL,
  `biologis_jumlah` int(100) NOT NULL,
  `cgr_alam_jumlah` int(100) NOT NULL,
  `estetika_jumlah` int(100) NOT NULL,
  `intens_hujan_jumlah` int(100) NOT NULL,
  `jln_lokasi_jumlah` int(100) NOT NULL,
  `jln_masuk_jumlah` int(100) NOT NULL,
  `jml_pemilik_jumlah` int(100) NOT NULL,
  `kaitan_jumlah` int(100) NOT NULL,
  `kapasitas_jumlah` int(100) NOT NULL,
  `lalin_jumlah` int(100) NOT NULL,
  `part_mas_jumlah` int(100) NOT NULL,
  `penutup_jumlah` int(100) NOT NULL,
  `pertanian_jumlah` int(100) NOT NULL,
  `tanah_jumlah` int(100) NOT NULL,
  `tata_lahan_jumlah` int(100) NOT NULL,
  `transport_jumlah` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kriteriapenyisih`
--

INSERT INTO `tbl_kriteriapenyisih` (`id_kriteria`, `kab_id`, `calon_tpa`, `jumlah`, `bts_admin_bobot`, `bts_admin_jumlah`, `bts_admin1`, `bts_admin2`, `bts_admin3`, `bts_admin4`, `hak_pemilik_bobot`, `hak_pemilik_jumlah`, `hak_pemilik1`, `hak_pemilik2`, `hak_pemilik3`, `hak_pemilik4`, `hak_pemilik5`, `kapasitas_bobot`, `kapasitas1`, `kapasitas2`, `kapasitas3`, `kapasitas4`, `jml_pemilik_bobot`, `jml_pemilik1`, `jml_pemilik2`, `jml_pemilik3`, `jml_pemilik4`, `jml_pemilik5`, `part_mas_bobot`, `part_mas1`, `part_mas2`, `part_mas3`, `tanah_bobot`, `tanah1`, `tanah2`, `tanah3`, `air_tanah_bobot`, `air_tanah1`, `air_tanah2`, `air_tanah3`, `air_tanah4`, `aliran_air_bobot`, `aliran_air1`, `aliran_air2`, `aliran_air3`, `kaitan_bobot`, `kaitan1`, `kaitan2`, `kaitan3`, `bhy_banjir_bobot`, `bhy_banjir1`, `bhy_banjir2`, `bhy_banjir3`, `penutup_bobot`, `penutup1`, `penutup2`, `penutup3`, `intens_hujan_bobot`, `intens_hujan1`, `intens_hujan2`, `intens_hujan3`, `jln_lokasi_bobot`, `jln_lokasi1`, `jln_lokasi2`, `jln_lokasi3`, `transport_bobot`, `transport1`, `transport2`, `transport3`, `transport4`, `jln_masuk_bobot`, `jln_masuk1`, `jln_masuk2`, `jln_masuk3`, `lalin_bobot`, `lalin1`, `lalin2`, `lalin3`, `lalin4`, `tata_lahan_bobot`, `tata_lahan1`, `tata_lahan2`, `tata_lahan3`, `pertanian_bobot`, `pertanian1`, `pertanian2`, `pertanian3`, `pertanian4`, `cgr_alam_bobot`, `cgr_alam1`, `cgr_alam2`, `cgr_alam3`, `biologis_bobot`, `biologis1`, `biologis2`, `biologis3`, `bau_bobot`, `bau1`, `bau2`, `bau3`, `estetika_bobot`, `estetika1`, `estetika2`, `estetika3`, `air_tanah_jumlah`, `aliran_air_jumlah`, `bau_jumlah`, `bhy_banjir_jumlah`, `biologis_jumlah`, `cgr_alam_jumlah`, `estetika_jumlah`, `intens_hujan_jumlah`, `jln_lokasi_jumlah`, `jln_masuk_jumlah`, `jml_pemilik_jumlah`, `kaitan_jumlah`, `kapasitas_jumlah`, `lalin_jumlah`, `part_mas_jumlah`, `penutup_jumlah`, `pertanian_jumlah`, `tanah_jumlah`, `tata_lahan_jumlah`, `transport_jumlah`) VALUES
(1, 2, 'semarang', 440, 0, 20, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nondomestik`
--

CREATE TABLE `tbl_nondomestik` (
  `id_proyeksi` int(255) NOT NULL,
  `luas_wilayah` decimal(65,0) DEFAULT NULL,
  `jml_penduduk` decimal(65,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pdrb`
--

CREATE TABLE `tbl_pdrb` (
  `id` int(100) NOT NULL,
  `tahun` int(5) NOT NULL,
  `kab_id` int(10) NOT NULL,
  `pdrb` int(255) DEFAULT NULL,
  `tahun_ke` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pdrb`
--

INSERT INTO `tbl_pdrb` (`id`, `tahun`, `kab_id`, `pdrb`, `tahun_ke`) VALUES
(1, 2013, 1, 13358073, 1),
(2, 2014, 1, 13517048, 2),
(3, 2015, 1, 13945165, 3),
(4, 2016, 1, 14293794, 4),
(5, 2017, 1, 14375268, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pdrb_proyeksi`
--

CREATE TABLE `tbl_pdrb_proyeksi` (
  `id` int(255) NOT NULL,
  `tahun_proyeksi` int(5) NOT NULL,
  `kab_id` int(100) DEFAULT NULL,
  `pdrb` int(255) DEFAULT NULL,
  `tahun_ke` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pdrb_proyeksi`
--

INSERT INTO `tbl_pdrb_proyeksi` (`id`, `tahun_proyeksi`, `kab_id`, `pdrb`, `tahun_ke`) VALUES
(1, 2018, NULL, NULL, NULL),
(2, 2019, NULL, NULL, NULL),
(3, 2020, NULL, NULL, NULL),
(4, 2021, NULL, NULL, NULL),
(5, 2022, NULL, NULL, NULL),
(6, 2023, NULL, NULL, NULL),
(7, 2024, NULL, NULL, NULL),
(8, 2025, NULL, NULL, NULL),
(9, 2026, NULL, NULL, NULL),
(10, 2027, NULL, NULL, NULL),
(11, 2028, NULL, NULL, NULL),
(12, 2029, NULL, NULL, NULL),
(13, 2030, NULL, NULL, NULL),
(14, 2031, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pp`
--

CREATE TABLE `tbl_pp` (
  `id` int(100) NOT NULL,
  `tahun` int(5) NOT NULL,
  `kab_id` int(10) NOT NULL,
  `jumlah_penduduk` int(255) DEFAULT NULL,
  `tahun_ke` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pp`
--

INSERT INTO `tbl_pp` (`id`, `tahun`, `kab_id`, `jumlah_penduduk`, `tahun_ke`) VALUES
(1, 2013, 1, 1153213, 1),
(2, 2014, 1, 1170185, 2),
(3, 2015, 1, 1170797, 3),
(4, 2016, 1, 1188289, 4),
(5, 2017, 1, 1199132, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pp_proyeksi`
--

CREATE TABLE `tbl_pp_proyeksi` (
  `id` int(10) NOT NULL,
  `tahun_proyeksi` int(5) NOT NULL,
  `jumlah_penduduk` int(100) DEFAULT NULL,
  `kab_id` int(5) DEFAULT NULL,
  `tahun_ke` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pp_proyeksi`
--

INSERT INTO `tbl_pp_proyeksi` (`id`, `tahun_proyeksi`, `jumlah_penduduk`, `kab_id`, `tahun_ke`) VALUES
(1, 2018, NULL, NULL, NULL),
(2, 2019, NULL, NULL, NULL),
(3, 2020, NULL, NULL, NULL),
(4, 2021, NULL, NULL, NULL),
(5, 2022, NULL, NULL, NULL),
(6, 2023, NULL, NULL, NULL),
(7, 2024, NULL, NULL, NULL),
(8, 2025, NULL, NULL, NULL),
(9, 2026, NULL, NULL, NULL),
(10, 2027, NULL, NULL, NULL),
(11, 2028, NULL, NULL, NULL),
(12, 2029, NULL, NULL, NULL),
(13, 2030, NULL, NULL, NULL),
(14, 2031, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timbulan_domestik`
--

CREATE TABLE `tbl_timbulan_domestik` (
  `id` int(100) NOT NULL,
  `kab_id` int(100) DEFAULT NULL,
  `tahun` int(5) NOT NULL,
  `timbulan_pdrb` decimal(5,4) DEFAULT NULL,
  `timbulan_penduduk` decimal(5,4) DEFAULT NULL,
  `tingkat_pelayanan` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_timbulan_domestik`
--

INSERT INTO `tbl_timbulan_domestik` (`id`, `kab_id`, `tahun`, `timbulan_pdrb`, `timbulan_penduduk`, `tingkat_pelayanan`) VALUES
(1, NULL, 2016, NULL, NULL, NULL),
(2, NULL, 2017, NULL, NULL, NULL),
(3, NULL, 2018, NULL, NULL, NULL),
(4, NULL, 2019, NULL, NULL, NULL),
(5, NULL, 2020, NULL, NULL, NULL),
(6, NULL, 2021, NULL, NULL, NULL),
(7, NULL, 2022, NULL, NULL, NULL),
(8, NULL, 2023, NULL, NULL, NULL),
(9, NULL, 2024, NULL, NULL, NULL),
(10, NULL, 2025, NULL, NULL, NULL),
(11, NULL, 2026, NULL, NULL, NULL),
(12, NULL, 2027, NULL, NULL, NULL),
(13, NULL, 2028, NULL, NULL, NULL),
(14, NULL, 2029, NULL, NULL, NULL),
(15, NULL, 2030, NULL, NULL, NULL),
(16, NULL, 2031, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_avgdrainase`
-- (See below for the actual view)
--
CREATE TABLE `view_avgdrainase` (
`5avg` decimal(20,10)
,`15avg` decimal(20,10)
,`30avg` decimal(20,10)
,`45avg` decimal(20,10)
,`60avg` decimal(20,10)
,`120avg` decimal(20,10)
,`180avg` decimal(20,10)
,`360avg` decimal(20,10)
,`720avg` decimal(20,10)
,`1440avg` decimal(20,10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_avghujan`
-- (See below for the actual view)
--
CREATE TABLE `view_avghujan` (
`5` decimal(20,10)
,`15` decimal(20,10)
,`30` decimal(20,10)
,`45` decimal(20,10)
,`60` decimal(20,10)
,`120` decimal(20,10)
,`180` decimal(20,10)
,`360` decimal(20,10)
,`720` decimal(20,10)
,`1440` decimal(20,10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_calontpa`
-- (See below for the actual view)
--
CREATE TABLE `view_calontpa` (
`total` int(255)
,`saran_calon_tpa` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_datadrainase`
-- (See below for the actual view)
--
CREATE TABLE `view_datadrainase` (
`id_drainase` int(255)
,`tahun_drainase` year(4)
,`menit5jam` decimal(16,6)
,`menit15jam` decimal(16,6)
,`menit30jam` decimal(16,6)
,`menit45jam` decimal(16,6)
,`menit60jam` decimal(16,6)
,`menit120jam` decimal(16,6)
,`menit180jam` decimal(16,6)
,`menit360jam` decimal(16,6)
,`menit720jam` decimal(16,6)
,`menit1440jam` decimal(16,6)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_datahujan`
-- (See below for the actual view)
--
CREATE TABLE `view_datahujan` (
`id_drainase` int(255)
,`tahun_drainase` year(4)
,`5` decimal(16,6)
,`15` decimal(16,6)
,`30` decimal(16,6)
,`45` decimal(16,6)
,`60` decimal(16,6)
,`120` decimal(16,6)
,`180` decimal(16,6)
,`360` decimal(16,6)
,`720` decimal(16,6)
,`1440` decimal(16,6)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_domestik`
-- (See below for the actual view)
--
CREATE TABLE `view_domestik` (
`tahun` int(5)
,`penduduk` decimal(64,2)
,`pdrb` double(19,2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_domestik_1`
-- (See below for the actual view)
--
CREATE TABLE `view_domestik_1` (
`tahun` int(5)
,`penduduk` decimal(64,2)
,`pdrb` double(19,2)
,`pert_pddk` decimal(64,4)
,`pert_pdrb` double(21,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_domestik_2`
-- (See below for the actual view)
--
CREATE TABLE `view_domestik_2` (
`tahun` int(5)
,`penduduk` decimal(64,2)
,`pdrb` double(19,2)
,`pert_pddk` decimal(64,4)
,`pert_pdrb` double(21,4)
,`pert_timb` double(21,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_domestik_3`
-- (See below for the actual view)
--
CREATE TABLE `view_domestik_3` (
`tahun` int(5)
,`penduduk` decimal(64,2)
,`pdrb` double(19,2)
,`pert_pddk` decimal(64,4)
,`pert_pdrb` double(21,4)
,`pert_timb` double(21,4)
,`timbulan_pdrb` decimal(5,4)
,`timbulan_penduduk` decimal(5,4)
,`timb_kapita` decimal(5,3)
,`tingkat_pelayanan` int(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_domestik_4`
-- (See below for the actual view)
--
CREATE TABLE `view_domestik_4` (
`tahun` int(5)
,`penduduk` decimal(64,2)
,`pdrb` double(19,2)
,`pert_pddk` decimal(64,4)
,`pert_pdrb` double(21,4)
,`pert_timb` double(21,4)
,`timbulan_pdrb` decimal(5,4)
,`timbulan_penduduk` decimal(5,4)
,`timb_kapita` decimal(5,3)
,`tingkat_pelayanan` int(100)
,`tot_timb_L` decimal(63,2)
,`tot_timb` decimal(59,2)
,`smph_terlayani` decimal(56,3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_domestik_5`
-- (See below for the actual view)
--
CREATE TABLE `view_domestik_5` (
`tahun` int(5)
,`penduduk` decimal(64,2)
,`pdrb` double(19,2)
,`pert_pddk` decimal(64,4)
,`pert_pdrb` double(21,4)
,`pert_timb` double(21,4)
,`timbulan_pdrb` decimal(5,4)
,`timbulan_penduduk` decimal(5,4)
,`timb_kapita` decimal(5,3)
,`tingkat_pelayanan` int(100)
,`tot_timb_L` decimal(63,2)
,`tot_timb` decimal(59,2)
,`smph_terlayani` decimal(56,3)
,`tot_vol` decimal(59,3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_drainase2`
-- (See below for the actual view)
--
CREATE TABLE `view_drainase2` (
`yt2_5` double(27,10)
,`yt2_15` double(27,10)
,`yt2_30` double(27,10)
,`yt2_45` double(27,10)
,`yt2_60` double(27,10)
,`yt2_120` double(27,10)
,`yt2_180` double(27,10)
,`yt2_360` double(27,10)
,`yt2_720` double(27,10)
,`yt2_1440` double(27,10)
,`yt5_5` double(27,10)
,`yt5_15` double(27,10)
,`yt5_30` double(27,10)
,`yt5_45` double(27,10)
,`yt5_60` double(27,10)
,`yt5_120` double(27,10)
,`yt5_180` double(27,10)
,`yt5_360` double(27,10)
,`yt5_720` double(27,10)
,`yt5_1440` double(27,10)
,`yt10_5` double(27,10)
,`yt10_15` double(27,10)
,`yt10_30` double(27,10)
,`yt10_45` double(27,10)
,`yt10_60` double(27,10)
,`yt10_120` double(27,10)
,`yt10_180` double(27,10)
,`yt10_360` double(27,10)
,`yt10_720` double(27,10)
,`yt10_1440` double(27,10)
,`yt20_5` double(27,10)
,`yt20_15` double(27,10)
,`yt20_30` double(27,10)
,`yt20_45` double(27,10)
,`yt20_60` double(27,10)
,`yt20_120` double(27,10)
,`yt20_180` double(27,10)
,`yt20_360` double(27,10)
,`yt20_720` double(27,10)
,`yt20_1440` double(27,10)
,`yt25_5` double(27,10)
,`yt25_15` double(27,10)
,`yt25_30` double(27,10)
,`yt25_45` double(27,10)
,`yt25_60` double(27,10)
,`yt25_120` double(27,10)
,`yt25_180` double(27,10)
,`yt25_360` double(27,10)
,`yt25_720` double(27,10)
,`yt25_1440` double(27,10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_drainase_deviasi`
-- (See below for the actual view)
--
CREATE TABLE `view_drainase_deviasi` (
`deviasi_talbot` double
,`deviasiPT_talbot` double
,`stdev_talbot` double
,`deviasi_sherman` double
,`deviasiPT_sherman` double
,`stdev_sherman` double
,`deviasi_ishiguro` double
,`deviasiPT_ishiguro` double
,`stdev_ishiguro` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_drainase_frekuensi`
-- (See below for the actual view)
--
CREATE TABLE `view_drainase_frekuensi` (
`yt2_5` double(27,10)
,`yt2_15` double(27,10)
,`yt2_30` double(27,10)
,`yt2_45` double(27,10)
,`yt2_60` double(27,10)
,`yt2_120` double(27,10)
,`yt2_180` double(27,10)
,`yt2_360` double(27,10)
,`yt2_720` double(27,10)
,`yt2_1440` double(27,10)
,`yt5_5` double(27,10)
,`yt5_15` double(27,10)
,`yt5_30` double(27,10)
,`yt5_45` double(27,10)
,`yt5_60` double(27,10)
,`yt5_120` double(27,10)
,`yt5_180` double(27,10)
,`yt5_360` double(27,10)
,`yt5_720` double(27,10)
,`yt5_1440` double(27,10)
,`yt10_5` double(27,10)
,`yt10_15` double(27,10)
,`yt10_30` double(27,10)
,`yt10_45` double(27,10)
,`yt10_60` double(27,10)
,`yt10_120` double(27,10)
,`yt10_180` double(27,10)
,`yt10_360` double(27,10)
,`yt10_720` double(27,10)
,`yt10_1440` double(27,10)
,`yt20_5` double(27,10)
,`yt20_15` double(27,10)
,`yt20_30` double(27,10)
,`yt20_45` double(27,10)
,`yt20_60` double(27,10)
,`yt20_120` double(27,10)
,`yt20_180` double(27,10)
,`yt20_360` double(27,10)
,`yt20_720` double(27,10)
,`yt20_1440` double(27,10)
,`yt25_5` double(27,10)
,`yt25_15` double(27,10)
,`yt25_30` double(27,10)
,`yt25_45` double(27,10)
,`yt25_60` double(27,10)
,`yt25_120` double(27,10)
,`yt25_180` double(27,10)
,`yt25_360` double(27,10)
,`yt25_720` double(27,10)
,`yt25_1440` double(27,10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_drainase_kalaulang`
-- (See below for the actual view)
--
CREATE TABLE `view_drainase_kalaulang` (
`ixt_5` decimal(6,2)
,`ixt_15` decimal(7,2)
,`ixt_30` decimal(7,2)
,`ixt_45` decimal(7,2)
,`ixt_60` decimal(7,2)
,`ixt_120` decimal(8,2)
,`ixt_180` decimal(8,2)
,`ixt_360` decimal(8,2)
,`ixt_720` decimal(8,2)
,`ixt_1440` decimal(9,2)
,`i2_5` double
,`i2_15` double
,`i2_30` double
,`i2_45` double
,`i2_60` double
,`i2_120` double
,`i2_180` double
,`i2_360` double
,`i2_720` double
,`i2_1440` double
,`i2xt_5` double
,`i2xt_15` double
,`i2xt_30` double
,`i2xt_45` double
,`i2xt_60` double
,`i2xt_120` double
,`i2xt_180` double
,`i2xt_360` double
,`i2xt_720` double
,`i2xt_1440` double
,`logt_5` double
,`logt_15` double
,`logt_30` double
,`logt_45` double
,`logt_60` double
,`logt_120` double
,`logt_180` double
,`logt_360` double
,`logt_720` double
,`logt_1440` double
,`logi_5` double
,`logi_15` double
,`logi_30` double
,`logi_45` double
,`logi_60` double
,`logi_120` double
,`logi_180` double
,`logi_360` double
,`logi_720` double
,`logi_1440` double
,`logti_5` double
,`logti_15` double
,`logti_30` double
,`logti_45` double
,`logti_60` double
,`logti_120` double
,`logti_180` double
,`logti_360` double
,`logti_720` double
,`logti_1440` double
,`logt2_5` double
,`logt2_15` double
,`logt2_30` double
,`logt2_45` double
,`logt2_60` double
,`logt2_120` double
,`logt2_180` double
,`logt2_360` double
,`logt2_720` double
,`logt2_1440` double
,`tset_5` double
,`tset_15` double
,`tset_30` double
,`tset_45` double
,`tset_60` double
,`tset_120` double
,`tset_180` double
,`tset_360` double
,`tset_720` double
,`tset_1440` double
,`ixtset_5` double
,`ixtset_15` double
,`ixtset_30` double
,`ixtset_45` double
,`ixtset_60` double
,`ixtset_120` double
,`ixtset_180` double
,`ixtset_360` double
,`ixtset_720` double
,`ixtset_1440` double
,`i2xtset_5` double
,`i2xtset_15` double
,`i2xtset_30` double
,`i2xtset_45` double
,`i2xtset_60` double
,`i2xtset_120` double
,`i2xtset_180` double
,`i2xtset_360` double
,`i2xtset_720` double
,`i2xtset_1440` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_drainase_kalaulang2`
-- (See below for the actual view)
--
CREATE TABLE `view_drainase_kalaulang2` (
`id_kalaulang` int(5)
,`t` int(5)
,`i` decimal(5,2)
,`ixt` decimal(15,2)
,`i2` double
,`i2xt` double
,`logt` double
,`logi` double
,`logti` double
,`logt2` double
,`tset` double
,`ixtset` double
,`i2xtset` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_drainase_kalaulang3`
-- (See below for the actual view)
--
CREATE TABLE `view_drainase_kalaulang3` (
`id_kalaulang` int(5)
,`t` int(5)
,`i` decimal(5,2)
,`talbot_i` double
,`talbot_delta` double
,`talbot_abs` double
,`sherman_i` double
,`sherman_delta` double
,`sherman_abs` double
,`ishiguro_i` double
,`ishiguro_delta` double
,`ishiguro_abs` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_induk_pdrb`
-- (See below for the actual view)
--
CREATE TABLE `view_induk_pdrb` (
`ka` decimal(65,4)
,`r` decimal(65,8)
,`r2` decimal(65,12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_induk_pdrb_lq`
-- (See below for the actual view)
--
CREATE TABLE `view_induk_pdrb_lq` (
`a` double
,`b` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_induk_pp`
-- (See below for the actual view)
--
CREATE TABLE `view_induk_pp` (
`ka` decimal(65,4)
,`r` decimal(65,8)
,`r2` decimal(65,12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_induk_pp_lq`
-- (See below for the actual view)
--
CREATE TABLE `view_induk_pp_lq` (
`a` double
,`b` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_limit`
-- (See below for the actual view)
--
CREATE TABLE `view_limit` (
`nilai_limit` bigint(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb` (
`tahun` int(5)
,`pdrb` int(255)
,`besaran` bigint(67)
,`persen` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_aritmatik`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_aritmatik` (
`tahun` int(5)
,`besaran` int(255)
,`pn` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_aritmatik_deviasi`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_aritmatik_deviasi` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_aritmatik_deviasi_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_aritmatik_deviasi_fix` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_aritmatik_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_aritmatik_fix` (
`tahun` int(11)
,`besaran` int(255)
,`pn` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_aritmatik_standar`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_aritmatik_standar` (
`tahun` int(5)
,`besaran` int(255)
,`hasil_hitung` decimal(65,4)
,`yi_ymean` decimal(65,4)
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_aritmatik_standar_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_aritmatik_standar_fix` (
`tahun` int(11)
,`besaran` int(255)
,`hasil_hitung` decimal(65,4)
,`yi_ymean` decimal(65,4)
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_geometri`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_geometri` (
`tahun` int(5)
,`besaran` int(255)
,`pn` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_geometri_deviasi`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_geometri_deviasi` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_geometri_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_geometri_fix` (
`tahun` int(11)
,`besaran` int(255)
,`pn` double(19,2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_geometri_standar`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_geometri_standar` (
`tahun` int(5)
,`besaran` int(255)
,`hasil_hitung` double
,`yi_ymean` double
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_lq1`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_lq1` (
`tahun` int(5)
,`tahun_ke` int(255)
,`besaran` int(255)
,`xy` bigint(66)
,`x2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_lq1_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_lq1_fix` (
`tahun` int(11)
,`tahun_ke` int(255)
,`besaran` int(255)
,`xy` bigint(66)
,`x2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_lq1_total`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_lq1_total` (
`sum_tahun_ke` decimal(65,0)
,`avg_tahun_ke` decimal(65,4)
,`sum_besaran` decimal(65,0)
,`avg_besaran` decimal(65,4)
,`sum_xy` decimal(65,0)
,`sum_x2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_lq2`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_lq2` (
`tahun` int(5)
,`tahun_ke` int(255)
,`besaran` int(255)
,`xy` bigint(66)
,`x2` double
,`pn` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_lq2_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_lq2_fix` (
`tahun` int(11)
,`tahun_ke` int(255)
,`besaran` int(255)
,`xy` bigint(66)
,`x2` double
,`pn` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_lq_deviasi`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_lq_deviasi` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_lq_standar`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_lq_standar` (
`tahun` int(5)
,`besaran` int(255)
,`hasil_hitung` double
,`yi_ymean` double
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_proyeksi`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_proyeksi` (
`tahun` int(11)
,`kab_id` int(11)
,`pdrb` int(255)
,`tahun_ke` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_tahun_proyeksi`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_tahun_proyeksi` (
`proyeksi` bigint(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pdrb_total`
-- (See below for the actual view)
--
CREATE TABLE `view_pdrb_total` (
`pdrb_total` decimal(65,0)
,`besaran_total` decimal(65,0)
,`persen_total` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pert_pdrb`
-- (See below for the actual view)
--
CREATE TABLE `view_pert_pdrb` (
`tahun` int(5)
,`pdrb` double
,`pertumbuhan_pdrb` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pert_penduduk`
-- (See below for the actual view)
--
CREATE TABLE `view_pert_penduduk` (
`tahun` int(5)
,`jumlah_penduduk` decimal(65,4)
,`pertumbuhan_penduduk` decimal(65,8)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pert_timbulan`
-- (See below for the actual view)
--
CREATE TABLE `view_pert_timbulan` (
`tahun` int(5)
,`pertumbuhan_timbulan` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp`
-- (See below for the actual view)
--
CREATE TABLE `view_pp` (
`id` int(100)
,`tahun` int(5)
,`jumlah_penduduk` int(255)
,`jiwa` bigint(67)
,`persen` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_aritmatik`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_aritmatik` (
`tahun` int(5)
,`penduduk` int(255)
,`pn` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_aritmatik_deviasi`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_aritmatik_deviasi` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_aritmatik_deviasi_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_aritmatik_deviasi_fix` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_aritmatik_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_aritmatik_fix` (
`tahun` int(11)
,`penduduk` int(255)
,`pn` decimal(64,2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_aritmatik_standar`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_aritmatik_standar` (
`tahun` int(5)
,`penduduk` int(255)
,`hasil_hitung` decimal(65,4)
,`yi_ymean` decimal(65,4)
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_aritmatik_standar_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_aritmatik_standar_fix` (
`tahun` int(11)
,`penduduk` int(255)
,`hasil_hitung` decimal(64,2)
,`yi_ymean` decimal(65,4)
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_geometri`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_geometri` (
`tahun` int(5)
,`penduduk` int(255)
,`pn` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_geometri_deviasi`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_geometri_deviasi` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_geometri_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_geometri_fix` (
`tahun` int(11)
,`penduduk` int(255)
,`pn` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_geometri_standar`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_geometri_standar` (
`tahun` int(5)
,`penduduk` int(255)
,`hasil_hitung` double
,`yi_ymean` double
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_lq1`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_lq1` (
`tahun` int(5)
,`tahun_ke` int(255)
,`penduduk` int(255)
,`xy` bigint(66)
,`x2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_lq1_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_lq1_fix` (
`tahun` int(11)
,`tahun_ke` int(255)
,`penduduk` int(255)
,`xy` bigint(66)
,`x2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_lq1_total`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_lq1_total` (
`sum_tahun_ke` decimal(65,0)
,`avg_tahun_ke` decimal(65,4)
,`sum_penduduk` decimal(65,0)
,`avg_penduduk` decimal(65,4)
,`sum_xy` decimal(65,0)
,`sum_x2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_lq2`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_lq2` (
`tahun` int(5)
,`tahun_ke` int(255)
,`penduduk` int(255)
,`xy` bigint(66)
,`x2` double
,`pn` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_lq2_fix`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_lq2_fix` (
`tahun` int(11)
,`tahun_ke` int(255)
,`penduduk` int(255)
,`xy` bigint(66)
,`x2` double
,`pn` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_lq_deviasi`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_lq_deviasi` (
`sum_yi_ymean2` double
,`standar_deviasi` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_lq_standar`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_lq_standar` (
`tahun` int(5)
,`penduduk` int(255)
,`hasil_hitung` double
,`yi_ymean` double
,`yi_ymean2` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_proyeksi`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_proyeksi` (
`tahun` int(11)
,`kab_id` int(11)
,`jumlah_penduduk` int(255)
,`tahun_ke` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_tahun_proyeksi`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_tahun_proyeksi` (
`proyeksi` bigint(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pp_total`
-- (See below for the actual view)
--
CREATE TABLE `view_pp_total` (
`jumlah_penduduk_total` decimal(65,0)
,`jiwa_total` decimal(65,0)
,`persen_total` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_stdevhhujan`
-- (See below for the actual view)
--
CREATE TABLE `view_stdevhhujan` (
`5stdev` double(26,6)
,`15stdev` double(26,6)
,`30stdev` double(26,6)
,`45stdev` double(26,6)
,`60stdev` double(26,6)
,`120stdev` double(26,6)
,`180stdev` double(26,6)
,`360stdev` double(26,6)
,`720stdev` double(26,6)
,`1440stdev` double(26,6)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_tahunakhir_pdrb`
-- (See below for the actual view)
--
CREATE TABLE `view_tahunakhir_pdrb` (
`tahun_akhir` int(5)
,`pdrb_akhir` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_tahunakhir_pp`
-- (See below for the actual view)
--
CREATE TABLE `view_tahunakhir_pp` (
`tahun_akhir` int(5)
,`jumlah_penduduk_akhir` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_tahunawal_pdrb`
-- (See below for the actual view)
--
CREATE TABLE `view_tahunawal_pdrb` (
`tahun_awal` int(5)
,`pdrb_awal` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_tahunawal_pp`
-- (See below for the actual view)
--
CREATE TABLE `view_tahunawal_pp` (
`tahun_awal` int(5)
,`jumlah_penduduk_awal` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_talbot_sherman_ishiguro`
-- (See below for the actual view)
--
CREATE TABLE `view_talbot_sherman_ishiguro` (
`talbot_a` double
,`talbot_b` double
,`sherman_loga` double
,`sherman_a` double
,`ishiguro_a` double
,`ishiguro_b` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_timbulan_total`
-- (See below for the actual view)
--
CREATE TABLE `view_timbulan_total` (
`tahun` int(5)
,`timb_sampah_dom` decimal(56,3)
,`timb_sampah_nondom` decimal(4,2)
,`total` decimal(57,3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_totaldrainase`
-- (See below for the actual view)
--
CREATE TABLE `view_totaldrainase` (
`avg5` decimal(20,10)
,`avg15` decimal(20,10)
,`avg30` decimal(20,10)
,`avg45` decimal(20,10)
,`avg60` decimal(20,10)
,`avg120` decimal(20,10)
,`avg180` decimal(20,10)
,`avg360` decimal(20,10)
,`avg720` decimal(20,10)
,`avg1440` decimal(20,10)
,`stdev5` double(38,10)
,`stdev15` double(38,10)
,`stdev30` double(38,10)
,`stdev45` double(38,10)
,`stdev60` double(38,10)
,`stdev120` double(38,10)
,`stdev180` double(38,10)
,`stdev360` double(38,10)
,`stdev720` double(38,10)
,`stdev1440` double(38,10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_total_kalaulang`
-- (See below for the actual view)
--
CREATE TABLE `view_total_kalaulang` (
`sum_i` decimal(27,2)
,`sum_ixt` decimal(37,2)
,`sum_i2` double
,`sum_i2xt` double
,`sum_logt` double
,`sum_logi` double
,`sum_logti` double
,`sum_logt2` double
,`sum_ixtset` double
,`sum_i2xtset` double
);

-- --------------------------------------------------------

--
-- Structure for view `coba`
--
DROP TABLE IF EXISTS `coba`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coba`  AS  select `a`.`tahun` AS `tahun`,`a`.`penduduk` AS `penduduk`,`a`.`pdrb` AS `pdrb`,`a`.`pert_pddk` AS `pert_pddk`,`a`.`pert_pdrb` AS `pert_pdrb`,greatest(`a`.`pert_pddk`,`a`.`pert_pdrb`) AS `pert_timb`,`b`.`timbulan_pdrb` AS `timbulan_pdrb` from (`view_domestik_1` `a` left join `tbl_timbulan_domestik` `b` on((`a`.`tahun` = `b`.`tahun`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_avgdrainase`
--
DROP TABLE IF EXISTS `view_avgdrainase`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_avgdrainase`  AS  select avg(`view_datahujan`.`5`) AS `5avg`,avg(`view_datahujan`.`15`) AS `15avg`,avg(`view_datahujan`.`30`) AS `30avg`,avg(`view_datahujan`.`45`) AS `45avg`,avg(`view_datahujan`.`60`) AS `60avg`,avg(`view_datahujan`.`120`) AS `120avg`,avg(`view_datahujan`.`180`) AS `180avg`,avg(`view_datahujan`.`360`) AS `360avg`,avg(`view_datahujan`.`720`) AS `720avg`,avg(`view_datahujan`.`1440`) AS `1440avg` from `view_datahujan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_avghujan`
--
DROP TABLE IF EXISTS `view_avghujan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_avghujan`  AS  select avg(`view_datahujan`.`5`) AS `5`,avg(`view_datahujan`.`15`) AS `15`,avg(`view_datahujan`.`30`) AS `30`,avg(`view_datahujan`.`45`) AS `45`,avg(`view_datahujan`.`60`) AS `60`,avg(`view_datahujan`.`120`) AS `120`,avg(`view_datahujan`.`180`) AS `180`,avg(`view_datahujan`.`360`) AS `360`,avg(`view_datahujan`.`720`) AS `720`,avg(`view_datahujan`.`1440`) AS `1440` from `view_datahujan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_calontpa`
--
DROP TABLE IF EXISTS `view_calontpa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_calontpa`  AS  select max(`tbl_kriteriapenyisih`.`jumlah`) AS `total`,`tbl_kriteriapenyisih`.`calon_tpa` AS `saran_calon_tpa` from `tbl_kriteriapenyisih` group by `tbl_kriteriapenyisih`.`calon_tpa` limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_datadrainase`
--
DROP TABLE IF EXISTS `view_datadrainase`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_datadrainase`  AS  select `tbl_datahujan`.`id_drainase` AS `id_drainase`,`tbl_datahujan`.`tahun_drainase` AS `tahun_drainase`,(`tbl_datahujan`.`menit5` * (60 / 5)) AS `menit5jam`,(`tbl_datahujan`.`menit15` * (60 / 15)) AS `menit15jam`,(`tbl_datahujan`.`menit30` * (60 / 30)) AS `menit30jam`,(`tbl_datahujan`.`menit45` * (60 / 45)) AS `menit45jam`,(`tbl_datahujan`.`menit60` * (60 / 60)) AS `menit60jam`,(`tbl_datahujan`.`menit120` * (60 / 120)) AS `menit120jam`,(`tbl_datahujan`.`menit180` * (60 / 180)) AS `menit180jam`,(`tbl_datahujan`.`menit360` * (60 / 360)) AS `menit360jam`,(`tbl_datahujan`.`menit720` * (60 / 720)) AS `menit720jam`,(`tbl_datahujan`.`menit1440` * (60 / 1440)) AS `menit1440jam` from `tbl_datahujan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_datahujan`
--
DROP TABLE IF EXISTS `view_datahujan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_datahujan`  AS  select `tbl_datahujan`.`id_drainase` AS `id_drainase`,`tbl_datahujan`.`tahun_drainase` AS `tahun_drainase`,(`tbl_datahujan`.`menit5` * (60 / 5)) AS `5`,(`tbl_datahujan`.`menit15` * (60 / 15)) AS `15`,(`tbl_datahujan`.`menit30` * (60 / 30)) AS `30`,(`tbl_datahujan`.`menit45` * (60 / 45)) AS `45`,(`tbl_datahujan`.`menit60` * (60 / 60)) AS `60`,(`tbl_datahujan`.`menit120` * (60 / 120)) AS `120`,(`tbl_datahujan`.`menit180` * (60 / 180)) AS `180`,(`tbl_datahujan`.`menit360` * (60 / 360)) AS `360`,(`tbl_datahujan`.`menit720` * (60 / 720)) AS `720`,(`tbl_datahujan`.`menit1440` * (60 / 1440)) AS `1440` from `tbl_datahujan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_domestik`
--
DROP TABLE IF EXISTS `view_domestik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_domestik`  AS  select `a`.`tahun` AS `tahun`,`b`.`pn` AS `penduduk`,`c`.`pn` AS `pdrb` from ((`tbl_timbulan_domestik` `a` left join `view_pp_aritmatik_fix` `b` on((`a`.`tahun` = `b`.`tahun`))) left join `view_pdrb_geometri_fix` `c` on((`a`.`tahun` = `c`.`tahun`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_domestik_1`
--
DROP TABLE IF EXISTS `view_domestik_1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_domestik_1`  AS  select `a`.`tahun` AS `tahun`,`a`.`penduduk` AS `penduduk`,`a`.`pdrb` AS `pdrb`,round((((`a`.`penduduk` - `b`.`penduduk`) / `b`.`penduduk`) * 100),4) AS `pert_pddk`,round((((`a`.`pdrb` - `b`.`pdrb`) / `b`.`pdrb`) * 100),4) AS `pert_pdrb` from (`view_domestik` `a` left join `view_domestik` `b` on((`a`.`tahun` = (`b`.`tahun` + 1)))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_domestik_2`
--
DROP TABLE IF EXISTS `view_domestik_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_domestik_2`  AS  select `a`.`tahun` AS `tahun`,`a`.`penduduk` AS `penduduk`,`a`.`pdrb` AS `pdrb`,`a`.`pert_pddk` AS `pert_pddk`,`a`.`pert_pdrb` AS `pert_pdrb`,greatest(`a`.`pert_pddk`,`a`.`pert_pdrb`) AS `pert_timb` from `view_domestik_1` `a` ;

-- --------------------------------------------------------

--
-- Structure for view `view_domestik_3`
--
DROP TABLE IF EXISTS `view_domestik_3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_domestik_3`  AS  select `a`.`tahun` AS `tahun`,`a`.`penduduk` AS `penduduk`,`a`.`pdrb` AS `pdrb`,`a`.`pert_pddk` AS `pert_pddk`,`a`.`pert_pdrb` AS `pert_pdrb`,`a`.`pert_timb` AS `pert_timb`,`b`.`timbulan_pdrb` AS `timbulan_pdrb`,`b`.`timbulan_penduduk` AS `timbulan_penduduk`,round(greatest(`b`.`timbulan_pdrb`,`b`.`timbulan_penduduk`),3) AS `timb_kapita`,`b`.`tingkat_pelayanan` AS `tingkat_pelayanan` from (`view_domestik_2` `a` left join `tbl_timbulan_domestik` `b` on((`a`.`tahun` = `b`.`tahun`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_domestik_4`
--
DROP TABLE IF EXISTS `view_domestik_4`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_domestik_4`  AS  select `view_domestik_3`.`tahun` AS `tahun`,`view_domestik_3`.`penduduk` AS `penduduk`,`view_domestik_3`.`pdrb` AS `pdrb`,`view_domestik_3`.`pert_pddk` AS `pert_pddk`,`view_domestik_3`.`pert_pdrb` AS `pert_pdrb`,`view_domestik_3`.`pert_timb` AS `pert_timb`,`view_domestik_3`.`timbulan_pdrb` AS `timbulan_pdrb`,`view_domestik_3`.`timbulan_penduduk` AS `timbulan_penduduk`,`view_domestik_3`.`timb_kapita` AS `timb_kapita`,`view_domestik_3`.`tingkat_pelayanan` AS `tingkat_pelayanan`,round((`view_domestik_3`.`timb_kapita` * `view_domestik_3`.`penduduk`),2) AS `tot_timb_L`,round(((`view_domestik_3`.`timb_kapita` * `view_domestik_3`.`penduduk`) / 1000),2) AS `tot_timb`,round((((`view_domestik_3`.`timb_kapita` * `view_domestik_3`.`penduduk`) / 1000) * (`view_domestik_3`.`tingkat_pelayanan` / 100)),3) AS `smph_terlayani` from `view_domestik_3` ;

-- --------------------------------------------------------

--
-- Structure for view `view_domestik_5`
--
DROP TABLE IF EXISTS `view_domestik_5`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_domestik_5`  AS  select `view_domestik_4`.`tahun` AS `tahun`,`view_domestik_4`.`penduduk` AS `penduduk`,`view_domestik_4`.`pdrb` AS `pdrb`,`view_domestik_4`.`pert_pddk` AS `pert_pddk`,`view_domestik_4`.`pert_pdrb` AS `pert_pdrb`,`view_domestik_4`.`pert_timb` AS `pert_timb`,`view_domestik_4`.`timbulan_pdrb` AS `timbulan_pdrb`,`view_domestik_4`.`timbulan_penduduk` AS `timbulan_penduduk`,`view_domestik_4`.`timb_kapita` AS `timb_kapita`,`view_domestik_4`.`tingkat_pelayanan` AS `tingkat_pelayanan`,`view_domestik_4`.`tot_timb_L` AS `tot_timb_L`,`view_domestik_4`.`tot_timb` AS `tot_timb`,`view_domestik_4`.`smph_terlayani` AS `smph_terlayani`,(`view_domestik_4`.`smph_terlayani` * 365) AS `tot_vol` from `view_domestik_4` ;

-- --------------------------------------------------------

--
-- Structure for view `view_drainase2`
--
DROP TABLE IF EXISTS `view_drainase2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_drainase2`  AS  select (`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt2_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt2_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt2_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt2_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt2_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt2_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt2_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt2_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt2_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt2_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt5_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt5_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt5_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt5_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt5_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt5_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt5_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt5_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt5_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt5_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt10_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt10_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt10_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt10_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt10_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt10_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt10_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt10_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt10_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt10_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt20_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt20_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt20_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt20_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt20_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt20_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt20_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt20_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt20_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt20_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt25_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt25_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt25_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt25_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt25_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt25_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt25_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt25_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt25_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt25_1440` from (`tbl_drainase` join `view_totaldrainase`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_drainase_deviasi`
--
DROP TABLE IF EXISTS `view_drainase_deviasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_drainase_deviasi`  AS  select sum(`view_drainase_kalaulang3`.`talbot_abs`) AS `deviasi_talbot`,(sum(`view_drainase_kalaulang3`.`talbot_abs`) / 9) AS `deviasiPT_talbot`,std(`view_drainase_kalaulang3`.`talbot_abs`) AS `stdev_talbot`,sum(`view_drainase_kalaulang3`.`sherman_abs`) AS `deviasi_sherman`,(sum(`view_drainase_kalaulang3`.`sherman_abs`) / 9) AS `deviasiPT_sherman`,std(`view_drainase_kalaulang3`.`sherman_abs`) AS `stdev_sherman`,sum(`view_drainase_kalaulang3`.`ishiguro_abs`) AS `deviasi_ishiguro`,(sum(`view_drainase_kalaulang3`.`ishiguro_abs`) / 9) AS `deviasiPT_ishiguro`,std(`view_drainase_kalaulang3`.`ishiguro_abs`) AS `stdev_ishiguro` from `view_drainase_kalaulang3` ;

-- --------------------------------------------------------

--
-- Structure for view `view_drainase_frekuensi`
--
DROP TABLE IF EXISTS `view_drainase_frekuensi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_drainase_frekuensi`  AS  select (`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt2_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt2_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt2_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt2_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt2_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt2_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt2_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt2_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt2_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt2` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt2_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt5_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt5_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt5_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt5_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt5_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt5_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt5_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt5_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt5_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt5` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt5_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt10_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt10_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt10_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt10_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt10_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt10_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt10_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt10_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt10_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt10` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt10_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt20_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt20_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt20_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt20_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt20_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt20_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt20_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt20_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt20_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt20` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt20_1440`,(`view_totaldrainase`.`avg5` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev5`)) AS `yt25_5`,(`view_totaldrainase`.`avg15` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev15`)) AS `yt25_15`,(`view_totaldrainase`.`avg30` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev30`)) AS `yt25_30`,(`view_totaldrainase`.`avg45` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev45`)) AS `yt25_45`,(`view_totaldrainase`.`avg60` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev60`)) AS `yt25_60`,(`view_totaldrainase`.`avg120` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev120`)) AS `yt25_120`,(`view_totaldrainase`.`avg180` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev180`)) AS `yt25_180`,(`view_totaldrainase`.`avg360` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev360`)) AS `yt25_360`,(`view_totaldrainase`.`avg720` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev720`)) AS `yt25_720`,(`view_totaldrainase`.`avg1440` + (((`tbl_drainase`.`yt25` - `tbl_drainase`.`yn`) / `tbl_drainase`.`sn`) * `view_totaldrainase`.`stdev1440`)) AS `yt25_1440` from (`tbl_drainase` join `view_totaldrainase`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_drainase_kalaulang`
--
DROP TABLE IF EXISTS `view_drainase_kalaulang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_drainase_kalaulang`  AS  select (5 * `tbl_drainase`.`i5`) AS `ixt_5`,(15 * `tbl_drainase`.`i15`) AS `ixt_15`,(30 * `tbl_drainase`.`i30`) AS `ixt_30`,(45 * `tbl_drainase`.`i45`) AS `ixt_45`,(60 * `tbl_drainase`.`i60`) AS `ixt_60`,(120 * `tbl_drainase`.`i120`) AS `ixt_120`,(180 * `tbl_drainase`.`i180`) AS `ixt_180`,(360 * `tbl_drainase`.`i360`) AS `ixt_360`,(720 * `tbl_drainase`.`i720`) AS `ixt_720`,(1440 * `tbl_drainase`.`i1440`) AS `ixt_1440`,pow(`tbl_drainase`.`i5`,2) AS `i2_5`,pow(`tbl_drainase`.`i15`,2) AS `i2_15`,pow(`tbl_drainase`.`i30`,2) AS `i2_30`,pow(`tbl_drainase`.`i45`,2) AS `i2_45`,pow(`tbl_drainase`.`i60`,2) AS `i2_60`,pow(`tbl_drainase`.`i120`,2) AS `i2_120`,pow(`tbl_drainase`.`i180`,2) AS `i2_180`,pow(`tbl_drainase`.`i360`,2) AS `i2_360`,pow(`tbl_drainase`.`i720`,2) AS `i2_720`,pow(`tbl_drainase`.`i1440`,2) AS `i2_1440`,(pow(`tbl_drainase`.`i5`,2) * 5) AS `i2xt_5`,(pow(`tbl_drainase`.`i15`,2) * 15) AS `i2xt_15`,(pow(`tbl_drainase`.`i30`,2) * 30) AS `i2xt_30`,(pow(`tbl_drainase`.`i45`,2) * 45) AS `i2xt_45`,(pow(`tbl_drainase`.`i60`,2) * 60) AS `i2xt_60`,(pow(`tbl_drainase`.`i120`,2) * 120) AS `i2xt_120`,(pow(`tbl_drainase`.`i180`,2) * 180) AS `i2xt_180`,(pow(`tbl_drainase`.`i360`,2) * 360) AS `i2xt_360`,(pow(`tbl_drainase`.`i720`,2) * 720) AS `i2xt_720`,(pow(`tbl_drainase`.`i1440`,2) * 1440) AS `i2xt_1440`,log10(5) AS `logt_5`,log10(15) AS `logt_15`,log10(30) AS `logt_30`,log10(45) AS `logt_45`,log10(60) AS `logt_60`,log10(120) AS `logt_120`,log10(180) AS `logt_180`,log10(360) AS `logt_360`,log10(720) AS `logt_720`,log10(1440) AS `logt_1440`,log10(`tbl_drainase`.`i5`) AS `logi_5`,log10(`tbl_drainase`.`i15`) AS `logi_15`,log10(`tbl_drainase`.`i30`) AS `logi_30`,log10(`tbl_drainase`.`i45`) AS `logi_45`,log10(`tbl_drainase`.`i60`) AS `logi_60`,log10(`tbl_drainase`.`i120`) AS `logi_120`,log10(`tbl_drainase`.`i180`) AS `logi_180`,log10(`tbl_drainase`.`i360`) AS `logi_360`,log10(`tbl_drainase`.`i720`) AS `logi_720`,log10(`tbl_drainase`.`i1440`) AS `logi_1440`,(log10(5) * log10(`tbl_drainase`.`i5`)) AS `logti_5`,(log10(15) * log10(`tbl_drainase`.`i15`)) AS `logti_15`,(log10(30) * log10(`tbl_drainase`.`i30`)) AS `logti_30`,(log10(45) * log10(`tbl_drainase`.`i45`)) AS `logti_45`,(log10(60) * log10(`tbl_drainase`.`i60`)) AS `logti_60`,(log10(120) * log10(`tbl_drainase`.`i120`)) AS `logti_120`,(log10(180) * log10(`tbl_drainase`.`i180`)) AS `logti_180`,(log10(360) * log10(`tbl_drainase`.`i360`)) AS `logti_360`,(log10(720) * log10(`tbl_drainase`.`i720`)) AS `logti_720`,(log10(1440) * log10(`tbl_drainase`.`i1440`)) AS `logti_1440`,pow(log10(5),2) AS `logt2_5`,pow(log10(15),2) AS `logt2_15`,pow(log10(30),2) AS `logt2_30`,pow(log10(45),2) AS `logt2_45`,pow(log10(60),2) AS `logt2_60`,pow(log10(120),2) AS `logt2_120`,pow(log10(180),2) AS `logt2_180`,pow(log10(360),2) AS `logt2_360`,pow(log10(720),2) AS `logt2_720`,pow(log10(1440),2) AS `logt2_1440`,pow(5,0.5) AS `tset_5`,pow(15,0.5) AS `tset_15`,pow(30,0.5) AS `tset_30`,pow(45,0.5) AS `tset_45`,pow(60,0.5) AS `tset_60`,pow(120,0.5) AS `tset_120`,pow(180,0.5) AS `tset_180`,pow(360,0.5) AS `tset_360`,pow(720,0.5) AS `tset_720`,pow(1440,0.5) AS `tset_1440`,(`tbl_drainase`.`i5` * pow(5,0.5)) AS `ixtset_5`,(`tbl_drainase`.`i15` * pow(15,0.5)) AS `ixtset_15`,(`tbl_drainase`.`i30` * pow(30,0.5)) AS `ixtset_30`,(`tbl_drainase`.`i45` * pow(45,0.5)) AS `ixtset_45`,(`tbl_drainase`.`i60` * pow(60,0.5)) AS `ixtset_60`,(`tbl_drainase`.`i120` * pow(120,0.5)) AS `ixtset_120`,(`tbl_drainase`.`i180` * pow(180,0.5)) AS `ixtset_180`,(`tbl_drainase`.`i360` * pow(360,0.5)) AS `ixtset_360`,(`tbl_drainase`.`i720` * pow(720,0.5)) AS `ixtset_720`,(`tbl_drainase`.`i1440` * pow(1440,0.5)) AS `ixtset_1440`,(pow(`tbl_drainase`.`i5`,2) * pow(5,0.5)) AS `i2xtset_5`,(pow(`tbl_drainase`.`i15`,2) * pow(15,0.5)) AS `i2xtset_15`,(pow(`tbl_drainase`.`i30`,2) * pow(30,0.5)) AS `i2xtset_30`,(pow(`tbl_drainase`.`i45`,2) * pow(45,0.5)) AS `i2xtset_45`,(pow(`tbl_drainase`.`i60`,2) * pow(60,0.5)) AS `i2xtset_60`,(pow(`tbl_drainase`.`i120`,2) * pow(120,0.5)) AS `i2xtset_120`,(pow(`tbl_drainase`.`i180`,2) * pow(180,0.5)) AS `i2xtset_180`,(pow(`tbl_drainase`.`i360`,2) * pow(360,0.5)) AS `i2xtset_360`,(pow(`tbl_drainase`.`i720`,2) * pow(720,0.5)) AS `i2xtset_720`,(pow(`tbl_drainase`.`i1440`,2) * pow(1440,0.5)) AS `i2xtset_1440` from `tbl_drainase` ;

-- --------------------------------------------------------

--
-- Structure for view `view_drainase_kalaulang2`
--
DROP TABLE IF EXISTS `view_drainase_kalaulang2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_drainase_kalaulang2`  AS  select `tbl_drainase2`.`id_kalaulang` AS `id_kalaulang`,`tbl_drainase2`.`t` AS `t`,`tbl_drainase2`.`i` AS `i`,(`tbl_drainase2`.`i` * `tbl_drainase2`.`t`) AS `ixt`,pow(`tbl_drainase2`.`i`,2) AS `i2`,(pow(`tbl_drainase2`.`i`,2) * `tbl_drainase2`.`t`) AS `i2xt`,log10(`tbl_drainase2`.`t`) AS `logt`,log10(`tbl_drainase2`.`i`) AS `logi`,(log10(`tbl_drainase2`.`t`) * log10(`tbl_drainase2`.`i`)) AS `logti`,pow(log10(`tbl_drainase2`.`t`),2) AS `logt2`,pow(`tbl_drainase2`.`t`,0.5) AS `tset`,(`tbl_drainase2`.`i` * pow(`tbl_drainase2`.`t`,0.5)) AS `ixtset`,(pow(`tbl_drainase2`.`i`,2) * pow(`tbl_drainase2`.`t`,0.5)) AS `i2xtset` from `tbl_drainase2` order by `tbl_drainase2`.`id_kalaulang` ;

-- --------------------------------------------------------

--
-- Structure for view `view_drainase_kalaulang3`
--
DROP TABLE IF EXISTS `view_drainase_kalaulang3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_drainase_kalaulang3`  AS  select `view_drainase_kalaulang2`.`id_kalaulang` AS `id_kalaulang`,`view_drainase_kalaulang2`.`t` AS `t`,`view_drainase_kalaulang2`.`i` AS `i`,(`view_talbot_sherman_ishiguro`.`talbot_a` / (`view_drainase_kalaulang2`.`t` + `view_talbot_sherman_ishiguro`.`talbot_b`)) AS `talbot_i`,((`view_talbot_sherman_ishiguro`.`talbot_a` / (`view_drainase_kalaulang2`.`t` + `view_talbot_sherman_ishiguro`.`talbot_b`)) - `view_drainase_kalaulang2`.`i`) AS `talbot_delta`,abs(((`view_talbot_sherman_ishiguro`.`talbot_a` / (`view_drainase_kalaulang2`.`t` + `view_talbot_sherman_ishiguro`.`talbot_b`)) - `view_drainase_kalaulang2`.`i`)) AS `talbot_abs`,(pow(10,`view_talbot_sherman_ishiguro`.`sherman_loga`) / pow(`view_drainase_kalaulang2`.`t`,`view_talbot_sherman_ishiguro`.`sherman_a`)) AS `sherman_i`,((pow(10,`view_talbot_sherman_ishiguro`.`sherman_loga`) / pow(`view_drainase_kalaulang2`.`t`,`view_talbot_sherman_ishiguro`.`sherman_a`)) - `view_drainase_kalaulang2`.`i`) AS `sherman_delta`,abs(((pow(10,`view_talbot_sherman_ishiguro`.`sherman_loga`) / pow(`view_drainase_kalaulang2`.`t`,`view_talbot_sherman_ishiguro`.`sherman_a`)) - `view_drainase_kalaulang2`.`i`)) AS `sherman_abs`,(`view_talbot_sherman_ishiguro`.`ishiguro_a` / (pow(`view_drainase_kalaulang2`.`t`,0.5) + `view_talbot_sherman_ishiguro`.`ishiguro_b`)) AS `ishiguro_i`,((`view_talbot_sherman_ishiguro`.`ishiguro_a` / (pow(`view_drainase_kalaulang2`.`t`,0.5) + `view_talbot_sherman_ishiguro`.`ishiguro_b`)) - `view_drainase_kalaulang2`.`i`) AS `ishiguro_delta`,abs(((`view_talbot_sherman_ishiguro`.`ishiguro_a` / (pow(`view_drainase_kalaulang2`.`t`,0.5) + `view_talbot_sherman_ishiguro`.`ishiguro_b`)) - `view_drainase_kalaulang2`.`i`)) AS `ishiguro_abs` from (`view_drainase_kalaulang2` join `view_talbot_sherman_ishiguro`) order by `view_drainase_kalaulang2`.`id_kalaulang` ;

-- --------------------------------------------------------

--
-- Structure for view `view_induk_pdrb`
--
DROP TABLE IF EXISTS `view_induk_pdrb`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_induk_pdrb`  AS  select ((`view_tahunakhir_pdrb`.`pdrb_akhir` - `view_tahunawal_pdrb`.`pdrb_awal`) / (`view_tahunakhir_pdrb`.`tahun_akhir` - `view_tahunawal_pdrb`.`tahun_awal`)) AS `ka`,(`view_pdrb_total`.`persen_total` / 4) AS `r`,((`view_pdrb_total`.`persen_total` / 4) / 100) AS `r2` from ((`view_tahunakhir_pdrb` join `view_tahunawal_pdrb`) join `view_pdrb_total`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_induk_pdrb_lq`
--
DROP TABLE IF EXISTS `view_induk_pdrb_lq`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_induk_pdrb_lq`  AS  select (`view_pdrb_lq1_total`.`avg_besaran` - ((((5 * `view_pdrb_lq1_total`.`sum_xy`) - (`view_pdrb_lq1_total`.`sum_tahun_ke` * `view_pdrb_lq1_total`.`sum_besaran`)) / ((5 * `view_pdrb_lq1_total`.`sum_x2`) - pow(`view_pdrb_lq1_total`.`sum_tahun_ke`,2))) * `view_pdrb_lq1_total`.`avg_tahun_ke`)) AS `a`,(((5 * `view_pdrb_lq1_total`.`sum_xy`) - (`view_pdrb_lq1_total`.`sum_tahun_ke` * `view_pdrb_lq1_total`.`sum_besaran`)) / ((5 * `view_pdrb_lq1_total`.`sum_x2`) - pow(`view_pdrb_lq1_total`.`sum_tahun_ke`,2))) AS `b` from `view_pdrb_lq1_total` ;

-- --------------------------------------------------------

--
-- Structure for view `view_induk_pp`
--
DROP TABLE IF EXISTS `view_induk_pp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_induk_pp`  AS  select ((`view_tahunakhir_pp`.`jumlah_penduduk_akhir` - `view_tahunawal_pp`.`jumlah_penduduk_awal`) / (`view_tahunakhir_pp`.`tahun_akhir` - `view_tahunawal_pp`.`tahun_awal`)) AS `ka`,(`view_pp_total`.`persen_total` / 4) AS `r`,((`view_pp_total`.`persen_total` / 4) / 100) AS `r2` from ((`view_tahunakhir_pp` join `view_tahunawal_pp`) join `view_pp_total`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_induk_pp_lq`
--
DROP TABLE IF EXISTS `view_induk_pp_lq`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_induk_pp_lq`  AS  select (`view_pp_lq1_total`.`avg_penduduk` - ((((5 * `view_pp_lq1_total`.`sum_xy`) - (`view_pp_lq1_total`.`sum_tahun_ke` * `view_pp_lq1_total`.`sum_penduduk`)) / ((5 * `view_pp_lq1_total`.`sum_x2`) - pow(`view_pp_lq1_total`.`sum_tahun_ke`,2))) * `view_pp_lq1_total`.`avg_tahun_ke`)) AS `a`,(((5 * `view_pp_lq1_total`.`sum_xy`) - (`view_pp_lq1_total`.`sum_tahun_ke` * `view_pp_lq1_total`.`sum_penduduk`)) / ((5 * `view_pp_lq1_total`.`sum_x2`) - pow(`view_pp_lq1_total`.`sum_tahun_ke`,2))) AS `b` from `view_pp_lq1_total` ;

-- --------------------------------------------------------

--
-- Structure for view `view_limit`
--
DROP TABLE IF EXISTS `view_limit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_limit`  AS  select (`view_tahunakhir_pp`.`tahun_akhir` - `view_tahunawal_pp`.`tahun_awal`) AS `nilai_limit` from (`view_tahunawal_pp` join `view_tahunakhir_pp`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb`
--
DROP TABLE IF EXISTS `view_pdrb`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb`  AS  select `a`.`tahun` AS `tahun`,`a`.`pdrb` AS `pdrb`,(`a`.`pdrb` - `b`.`pdrb`) AS `besaran`,(((`a`.`pdrb` - `b`.`pdrb`) / `b`.`pdrb`) * 100) AS `persen` from (`tbl_pdrb` `a` left join `tbl_pdrb` `b` on((`a`.`tahun` = (`b`.`tahun` + 1)))) where (`a`.`pdrb` is not null) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_aritmatik`
--
DROP TABLE IF EXISTS `view_pdrb_aritmatik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_aritmatik`  AS  select `tbl_pdrb`.`tahun` AS `tahun`,`tbl_pdrb`.`pdrb` AS `besaran`,(`view_tahunakhir_pdrb`.`pdrb_akhir` + (`view_induk_pdrb`.`ka` * (`view_tahunakhir_pdrb`.`tahun_akhir` - `tbl_pdrb`.`tahun`))) AS `pn` from ((`tbl_pdrb` join `view_tahunakhir_pdrb`) join `view_induk_pdrb`) order by `tbl_pdrb`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_aritmatik_deviasi`
--
DROP TABLE IF EXISTS `view_pdrb_aritmatik_deviasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_aritmatik_deviasi`  AS  select sum(`view_pdrb_aritmatik_standar`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pdrb_aritmatik_standar`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pdrb_aritmatik_standar` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_aritmatik_deviasi_fix`
--
DROP TABLE IF EXISTS `view_pdrb_aritmatik_deviasi_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_aritmatik_deviasi_fix`  AS  select sum(`view_pdrb_aritmatik_standar_fix`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pdrb_aritmatik_standar_fix`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pdrb_aritmatik_standar_fix` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_aritmatik_fix`
--
DROP TABLE IF EXISTS `view_pdrb_aritmatik_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_aritmatik_fix`  AS  select `a`.`tahun` AS `tahun`,`a`.`pdrb` AS `besaran`,(`b`.`pdrb_akhir` + (`c`.`ka` * (`b`.`tahun_akhir` - `a`.`tahun`))) AS `pn` from ((`view_pdrb_proyeksi` `a` join `view_tahunakhir_pdrb` `b`) join `view_induk_pdrb` `c`) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_aritmatik_standar`
--
DROP TABLE IF EXISTS `view_pdrb_aritmatik_standar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_aritmatik_standar`  AS  select `view_pdrb_aritmatik`.`tahun` AS `tahun`,`view_pdrb_aritmatik`.`besaran` AS `besaran`,`view_pdrb_aritmatik`.`pn` AS `hasil_hitung`,(`view_pdrb_aritmatik`.`pn` - `view_pdrb_lq1_total`.`avg_besaran`) AS `yi_ymean`,pow((`view_pdrb_aritmatik`.`pn` - `view_pdrb_lq1_total`.`avg_besaran`),2) AS `yi_ymean2` from (`view_pdrb_aritmatik` join `view_pdrb_lq1_total`) where (`view_pdrb_aritmatik`.`besaran` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_aritmatik_standar_fix`
--
DROP TABLE IF EXISTS `view_pdrb_aritmatik_standar_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_aritmatik_standar_fix`  AS  select `a`.`tahun` AS `tahun`,`a`.`besaran` AS `besaran`,`a`.`pn` AS `hasil_hitung`,(`a`.`pn` - `b`.`avg_besaran`) AS `yi_ymean`,pow((`a`.`pn` - `b`.`avg_besaran`),2) AS `yi_ymean2` from (`view_pdrb_aritmatik_fix` `a` join `view_pdrb_lq1_total` `b`) where (`a`.`besaran` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_geometri`
--
DROP TABLE IF EXISTS `view_pdrb_geometri`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_geometri`  AS  select `tbl_pdrb`.`tahun` AS `tahun`,`tbl_pdrb`.`pdrb` AS `besaran`,(`view_tahunakhir_pdrb`.`pdrb_akhir` / pow((1 + `view_induk_pdrb`.`r2`),(`view_tahunakhir_pdrb`.`tahun_akhir` - `tbl_pdrb`.`tahun`))) AS `pn` from ((`tbl_pdrb` join `view_tahunakhir_pdrb`) join `view_induk_pdrb`) order by `tbl_pdrb`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_geometri_deviasi`
--
DROP TABLE IF EXISTS `view_pdrb_geometri_deviasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_geometri_deviasi`  AS  select sum(`view_pdrb_geometri_standar`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pdrb_geometri_standar`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pdrb_geometri_standar` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_geometri_fix`
--
DROP TABLE IF EXISTS `view_pdrb_geometri_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_geometri_fix`  AS  select `a`.`tahun` AS `tahun`,`a`.`pdrb` AS `besaran`,round((`b`.`pdrb_akhir` / pow((1 + `c`.`r2`),(`b`.`tahun_akhir` - `a`.`tahun`))),2) AS `pn` from ((`view_pdrb_proyeksi` `a` join `view_tahunakhir_pdrb` `b`) join `view_induk_pdrb` `c`) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_geometri_standar`
--
DROP TABLE IF EXISTS `view_pdrb_geometri_standar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_geometri_standar`  AS  select `view_pdrb_geometri`.`tahun` AS `tahun`,`view_pdrb_geometri`.`besaran` AS `besaran`,`view_pdrb_geometri`.`pn` AS `hasil_hitung`,(`view_pdrb_geometri`.`pn` - `view_pdrb_lq1_total`.`avg_besaran`) AS `yi_ymean`,pow((`view_pdrb_geometri`.`pn` - `view_pdrb_lq1_total`.`avg_besaran`),2) AS `yi_ymean2` from (`view_pdrb_geometri` join `view_pdrb_lq1_total`) where (`view_pdrb_geometri`.`besaran` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_lq1`
--
DROP TABLE IF EXISTS `view_pdrb_lq1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_lq1`  AS  select `tbl_pdrb`.`tahun` AS `tahun`,`tbl_pdrb`.`tahun_ke` AS `tahun_ke`,`tbl_pdrb`.`pdrb` AS `besaran`,(`tbl_pdrb`.`tahun_ke` * `tbl_pdrb`.`pdrb`) AS `xy`,pow(`tbl_pdrb`.`tahun_ke`,2) AS `x2` from `tbl_pdrb` where (`tbl_pdrb`.`tahun` <> 0) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_lq1_fix`
--
DROP TABLE IF EXISTS `view_pdrb_lq1_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_lq1_fix`  AS  select `view_pdrb_proyeksi`.`tahun` AS `tahun`,`view_pdrb_proyeksi`.`tahun_ke` AS `tahun_ke`,`view_pdrb_proyeksi`.`pdrb` AS `besaran`,(`view_pdrb_proyeksi`.`tahun_ke` * `view_pdrb_proyeksi`.`pdrb`) AS `xy`,pow(`view_pdrb_proyeksi`.`tahun_ke`,2) AS `x2` from `view_pdrb_proyeksi` order by `view_pdrb_proyeksi`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_lq1_total`
--
DROP TABLE IF EXISTS `view_pdrb_lq1_total`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_lq1_total`  AS  select sum(`view_pdrb_lq1`.`tahun_ke`) AS `sum_tahun_ke`,avg(`view_pdrb_lq1`.`tahun_ke`) AS `avg_tahun_ke`,sum(`view_pdrb_lq1`.`besaran`) AS `sum_besaran`,avg(`view_pdrb_lq1`.`besaran`) AS `avg_besaran`,sum(`view_pdrb_lq1`.`xy`) AS `sum_xy`,sum(`view_pdrb_lq1`.`x2`) AS `sum_x2` from `view_pdrb_lq1` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_lq2`
--
DROP TABLE IF EXISTS `view_pdrb_lq2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_lq2`  AS  select `view_pdrb_lq1`.`tahun` AS `tahun`,`view_pdrb_lq1`.`tahun_ke` AS `tahun_ke`,`view_pdrb_lq1`.`besaran` AS `besaran`,`view_pdrb_lq1`.`xy` AS `xy`,`view_pdrb_lq1`.`x2` AS `x2`,(`view_induk_pdrb_lq`.`a` + (`view_induk_pdrb_lq`.`b` * (`view_pdrb_lq1`.`tahun` - `view_tahunawal_pdrb`.`tahun_awal`))) AS `pn` from ((`view_pdrb_lq1` join `view_induk_pdrb_lq`) join `view_tahunawal_pdrb`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_lq2_fix`
--
DROP TABLE IF EXISTS `view_pdrb_lq2_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_lq2_fix`  AS  select `a`.`tahun` AS `tahun`,`a`.`tahun_ke` AS `tahun_ke`,`a`.`besaran` AS `besaran`,`a`.`xy` AS `xy`,`a`.`x2` AS `x2`,(`b`.`a` + (`b`.`b` * (`a`.`tahun` - `c`.`tahun_awal`))) AS `pn` from ((`view_pdrb_lq1_fix` `a` join `view_induk_pdrb_lq` `b`) join `view_tahunawal_pdrb` `c`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_lq_deviasi`
--
DROP TABLE IF EXISTS `view_pdrb_lq_deviasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_lq_deviasi`  AS  select sum(`view_pdrb_lq_standar`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pdrb_lq_standar`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pdrb_lq_standar` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_lq_standar`
--
DROP TABLE IF EXISTS `view_pdrb_lq_standar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_lq_standar`  AS  select `view_pdrb_lq2`.`tahun` AS `tahun`,`view_pdrb_lq2`.`besaran` AS `besaran`,`view_pdrb_lq2`.`pn` AS `hasil_hitung`,(`view_pdrb_lq2`.`pn` - `view_pdrb_lq1_total`.`avg_besaran`) AS `yi_ymean`,pow((`view_pdrb_lq2`.`pn` - `view_pdrb_lq1_total`.`avg_besaran`),2) AS `yi_ymean2` from (`view_pdrb_lq2` join `view_pdrb_lq1_total`) where (`view_pdrb_lq2`.`besaran` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_proyeksi`
--
DROP TABLE IF EXISTS `view_pdrb_proyeksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_proyeksi`  AS  select `a`.`tahun` AS `tahun`,`a`.`kab_id` AS `kab_id`,`a`.`pdrb` AS `pdrb`,`a`.`tahun_ke` AS `tahun_ke` from `tbl_pdrb` `a` union select `b`.`tahun_proyeksi` AS `tahun_proyeksi`,`b`.`kab_id` AS `kab_id`,`b`.`pdrb` AS `pdrb`,`b`.`tahun_ke` AS `tahun_ke` from `tbl_pdrb_proyeksi` `b` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_tahun_proyeksi`
--
DROP TABLE IF EXISTS `view_pdrb_tahun_proyeksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_tahun_proyeksi`  AS  select (`view_tahunakhir_pdrb`.`tahun_akhir` + 14) AS `proyeksi` from `view_tahunakhir_pdrb` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pdrb_total`
--
DROP TABLE IF EXISTS `view_pdrb_total`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pdrb_total`  AS  select sum(`view_pdrb`.`pdrb`) AS `pdrb_total`,sum(`view_pdrb`.`besaran`) AS `besaran_total`,sum(`view_pdrb`.`persen`) AS `persen_total` from `view_pdrb` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pert_pdrb`
--
DROP TABLE IF EXISTS `view_pert_pdrb`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pert_pdrb`  AS  select `a`.`tahun` AS `tahun`,`a`.`pn` AS `pdrb`,(((`a`.`pn` - `b`.`pn`) / `b`.`pn`) * 100) AS `pertumbuhan_pdrb` from (`view_pdrb_geometri` `a` left join `view_pdrb_geometri` `b` on((`a`.`tahun` = (`b`.`tahun` + 1)))) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pert_penduduk`
--
DROP TABLE IF EXISTS `view_pert_penduduk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pert_penduduk`  AS  select `a`.`tahun` AS `tahun`,`a`.`pn` AS `jumlah_penduduk`,(((`a`.`pn` - `b`.`pn`) / `b`.`pn`) * 100) AS `pertumbuhan_penduduk` from (`view_pp_aritmatik` `a` left join `view_pp_aritmatik` `b` on((`a`.`tahun` = (`b`.`tahun` + 1)))) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pert_timbulan`
--
DROP TABLE IF EXISTS `view_pert_timbulan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pert_timbulan`  AS  select `a`.`tahun` AS `tahun`,greatest(`a`.`pertumbuhan_penduduk`,`b`.`pertumbuhan_pdrb`) AS `pertumbuhan_timbulan` from (`view_pert_penduduk` `a` join `view_pert_pdrb` `b`) group by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp`
--
DROP TABLE IF EXISTS `view_pp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp`  AS  select `a`.`id` AS `id`,`a`.`tahun` AS `tahun`,`a`.`jumlah_penduduk` AS `jumlah_penduduk`,(`a`.`jumlah_penduduk` - `b`.`jumlah_penduduk`) AS `jiwa`,(((`a`.`jumlah_penduduk` - `b`.`jumlah_penduduk`) / `b`.`jumlah_penduduk`) * 100) AS `persen` from (`tbl_pp` `a` left join `tbl_pp` `b` on((`a`.`tahun` = (`b`.`tahun` + 1)))) where (`a`.`jumlah_penduduk` is not null) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_aritmatik`
--
DROP TABLE IF EXISTS `view_pp_aritmatik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_aritmatik`  AS  select `tbl_pp`.`tahun` AS `tahun`,`tbl_pp`.`jumlah_penduduk` AS `penduduk`,(`view_tahunakhir_pp`.`jumlah_penduduk_akhir` + (`view_induk_pp`.`ka` * (`view_tahunakhir_pp`.`tahun_akhir` - `tbl_pp`.`tahun`))) AS `pn` from ((`tbl_pp` join `view_tahunakhir_pp`) join `view_induk_pp`) order by `tbl_pp`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_aritmatik_deviasi`
--
DROP TABLE IF EXISTS `view_pp_aritmatik_deviasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_aritmatik_deviasi`  AS  select sum(`view_pp_aritmatik_standar`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pp_aritmatik_standar`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pp_aritmatik_standar` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_aritmatik_deviasi_fix`
--
DROP TABLE IF EXISTS `view_pp_aritmatik_deviasi_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_aritmatik_deviasi_fix`  AS  select sum(`view_pp_aritmatik_standar_fix`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pp_aritmatik_standar_fix`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pp_aritmatik_standar_fix` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_aritmatik_fix`
--
DROP TABLE IF EXISTS `view_pp_aritmatik_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_aritmatik_fix`  AS  select `a`.`tahun` AS `tahun`,`a`.`jumlah_penduduk` AS `penduduk`,round((`b`.`jumlah_penduduk_akhir` + (`c`.`ka` * (`b`.`tahun_akhir` - `a`.`tahun`))),2) AS `pn` from ((`view_pp_proyeksi` `a` join `view_tahunakhir_pp` `b`) join `view_induk_pp` `c`) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_aritmatik_standar`
--
DROP TABLE IF EXISTS `view_pp_aritmatik_standar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_aritmatik_standar`  AS  select `view_pp_aritmatik`.`tahun` AS `tahun`,`view_pp_aritmatik`.`penduduk` AS `penduduk`,`view_pp_aritmatik`.`pn` AS `hasil_hitung`,(`view_pp_aritmatik`.`pn` - `view_pp_lq1_total`.`avg_penduduk`) AS `yi_ymean`,pow((`view_pp_aritmatik`.`pn` - `view_pp_lq1_total`.`avg_penduduk`),2) AS `yi_ymean2` from (`view_pp_aritmatik` join `view_pp_lq1_total`) where (`view_pp_aritmatik`.`penduduk` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_aritmatik_standar_fix`
--
DROP TABLE IF EXISTS `view_pp_aritmatik_standar_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_aritmatik_standar_fix`  AS  select `a`.`tahun` AS `tahun`,`a`.`penduduk` AS `penduduk`,`a`.`pn` AS `hasil_hitung`,(`a`.`pn` - `b`.`avg_penduduk`) AS `yi_ymean`,pow((`a`.`pn` - `b`.`avg_penduduk`),2) AS `yi_ymean2` from (`view_pp_aritmatik_fix` `a` join `view_pp_lq1_total` `b`) where (`a`.`penduduk` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_geometri`
--
DROP TABLE IF EXISTS `view_pp_geometri`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_geometri`  AS  select `tbl_pp`.`tahun` AS `tahun`,`tbl_pp`.`jumlah_penduduk` AS `penduduk`,(`view_tahunakhir_pp`.`jumlah_penduduk_akhir` / pow((1 + `view_induk_pp`.`r2`),(`view_tahunakhir_pp`.`tahun_akhir` - `tbl_pp`.`tahun`))) AS `pn` from ((`tbl_pp` join `view_tahunakhir_pp`) join `view_induk_pp`) order by `tbl_pp`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_geometri_deviasi`
--
DROP TABLE IF EXISTS `view_pp_geometri_deviasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_geometri_deviasi`  AS  select sum(`view_pp_geometri_standar`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pp_geometri_standar`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pp_geometri_standar` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_geometri_fix`
--
DROP TABLE IF EXISTS `view_pp_geometri_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_geometri_fix`  AS  select `a`.`tahun` AS `tahun`,`a`.`jumlah_penduduk` AS `penduduk`,(`b`.`jumlah_penduduk_akhir` / pow((1 + `c`.`r2`),(`b`.`tahun_akhir` - `a`.`tahun`))) AS `pn` from ((`view_pp_proyeksi` `a` join `view_tahunakhir_pp` `b`) join `view_induk_pp` `c`) order by `a`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_geometri_standar`
--
DROP TABLE IF EXISTS `view_pp_geometri_standar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_geometri_standar`  AS  select `view_pp_geometri`.`tahun` AS `tahun`,`view_pp_geometri`.`penduduk` AS `penduduk`,`view_pp_geometri`.`pn` AS `hasil_hitung`,(`view_pp_geometri`.`pn` - `view_pp_lq1_total`.`avg_penduduk`) AS `yi_ymean`,pow((`view_pp_geometri`.`pn` - `view_pp_lq1_total`.`avg_penduduk`),2) AS `yi_ymean2` from (`view_pp_geometri` join `view_pp_lq1_total`) where (`view_pp_geometri`.`penduduk` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_lq1`
--
DROP TABLE IF EXISTS `view_pp_lq1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_lq1`  AS  select `tbl_pp`.`tahun` AS `tahun`,`tbl_pp`.`tahun_ke` AS `tahun_ke`,`tbl_pp`.`jumlah_penduduk` AS `penduduk`,(`tbl_pp`.`tahun_ke` * `tbl_pp`.`jumlah_penduduk`) AS `xy`,pow(`tbl_pp`.`tahun_ke`,2) AS `x2` from `tbl_pp` order by `tbl_pp`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_lq1_fix`
--
DROP TABLE IF EXISTS `view_pp_lq1_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_lq1_fix`  AS  select `view_pp_proyeksi`.`tahun` AS `tahun`,`view_pp_proyeksi`.`tahun_ke` AS `tahun_ke`,`view_pp_proyeksi`.`jumlah_penduduk` AS `penduduk`,(`view_pp_proyeksi`.`tahun_ke` * `view_pp_proyeksi`.`jumlah_penduduk`) AS `xy`,pow(`view_pp_proyeksi`.`tahun_ke`,2) AS `x2` from `view_pp_proyeksi` order by `view_pp_proyeksi`.`tahun` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_lq1_total`
--
DROP TABLE IF EXISTS `view_pp_lq1_total`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_lq1_total`  AS  select sum(`view_pp_lq1`.`tahun_ke`) AS `sum_tahun_ke`,avg(`view_pp_lq1`.`tahun_ke`) AS `avg_tahun_ke`,sum(`view_pp_lq1`.`penduduk`) AS `sum_penduduk`,avg(`view_pp_lq1`.`penduduk`) AS `avg_penduduk`,sum(`view_pp_lq1`.`xy`) AS `sum_xy`,sum(`view_pp_lq1`.`x2`) AS `sum_x2` from `view_pp_lq1` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_lq2`
--
DROP TABLE IF EXISTS `view_pp_lq2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_lq2`  AS  select `view_pp_lq1`.`tahun` AS `tahun`,`view_pp_lq1`.`tahun_ke` AS `tahun_ke`,`view_pp_lq1`.`penduduk` AS `penduduk`,`view_pp_lq1`.`xy` AS `xy`,`view_pp_lq1`.`x2` AS `x2`,(`view_induk_pp_lq`.`a` + (`view_induk_pp_lq`.`b` * (`view_pp_lq1`.`tahun` - `view_tahunawal_pp`.`tahun_awal`))) AS `pn` from ((`view_pp_lq1` join `view_induk_pp_lq`) join `view_tahunawal_pp`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_lq2_fix`
--
DROP TABLE IF EXISTS `view_pp_lq2_fix`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_lq2_fix`  AS  select `view_pp_lq1_fix`.`tahun` AS `tahun`,`view_pp_lq1_fix`.`tahun_ke` AS `tahun_ke`,`view_pp_lq1_fix`.`penduduk` AS `penduduk`,`view_pp_lq1_fix`.`xy` AS `xy`,`view_pp_lq1_fix`.`x2` AS `x2`,(`view_induk_pp_lq`.`a` + (`view_induk_pp_lq`.`b` * (`view_pp_lq1_fix`.`tahun` - `view_tahunawal_pp`.`tahun_awal`))) AS `pn` from ((`view_pp_lq1_fix` join `view_induk_pp_lq`) join `view_tahunawal_pp`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_lq_deviasi`
--
DROP TABLE IF EXISTS `view_pp_lq_deviasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_lq_deviasi`  AS  select sum(`view_pp_lq_standar`.`yi_ymean2`) AS `sum_yi_ymean2`,pow((sum(`view_pp_lq_standar`.`yi_ymean2`) / 5),0.5) AS `standar_deviasi` from `view_pp_lq_standar` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_lq_standar`
--
DROP TABLE IF EXISTS `view_pp_lq_standar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_lq_standar`  AS  select `view_pp_lq2`.`tahun` AS `tahun`,`view_pp_lq2`.`penduduk` AS `penduduk`,`view_pp_lq2`.`pn` AS `hasil_hitung`,(`view_pp_lq2`.`pn` - `view_pp_lq1_total`.`avg_penduduk`) AS `yi_ymean`,pow((`view_pp_lq2`.`pn` - `view_pp_lq1_total`.`avg_penduduk`),2) AS `yi_ymean2` from (`view_pp_lq2` join `view_pp_lq1_total`) where (`view_pp_lq2`.`penduduk` is not null) ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_proyeksi`
--
DROP TABLE IF EXISTS `view_pp_proyeksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_proyeksi`  AS  select `a`.`tahun` AS `tahun`,`a`.`kab_id` AS `kab_id`,`a`.`jumlah_penduduk` AS `jumlah_penduduk`,`a`.`tahun_ke` AS `tahun_ke` from `tbl_pp` `a` union select `b`.`tahun_proyeksi` AS `tahun_proyeksi`,`b`.`kab_id` AS `kab_id`,`b`.`jumlah_penduduk` AS `jumlah_penduduk`,`b`.`tahun_ke` AS `tahun_ke` from `tbl_pp_proyeksi` `b` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_tahun_proyeksi`
--
DROP TABLE IF EXISTS `view_pp_tahun_proyeksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_tahun_proyeksi`  AS  select (`view_tahunakhir_pp`.`tahun_akhir` + 14) AS `proyeksi` from `view_tahunakhir_pp` ;

-- --------------------------------------------------------

--
-- Structure for view `view_pp_total`
--
DROP TABLE IF EXISTS `view_pp_total`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pp_total`  AS  select sum(`view_pp`.`jumlah_penduduk`) AS `jumlah_penduduk_total`,sum(`view_pp`.`jiwa`) AS `jiwa_total`,sum(`view_pp`.`persen`) AS `persen_total` from `view_pp` ;

-- --------------------------------------------------------

--
-- Structure for view `view_stdevhhujan`
--
DROP TABLE IF EXISTS `view_stdevhhujan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_stdevhhujan`  AS  select std(`tbl_datahujan`.`menit5`) AS `5stdev`,std(`tbl_datahujan`.`menit15`) AS `15stdev`,std(`tbl_datahujan`.`menit30`) AS `30stdev`,std(`tbl_datahujan`.`menit45`) AS `45stdev`,std(`tbl_datahujan`.`menit60`) AS `60stdev`,std(`tbl_datahujan`.`menit120`) AS `120stdev`,std(`tbl_datahujan`.`menit180`) AS `180stdev`,std(`tbl_datahujan`.`menit360`) AS `360stdev`,std(`tbl_datahujan`.`menit720`) AS `720stdev`,std(`tbl_datahujan`.`menit1440`) AS `1440stdev` from `tbl_datahujan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_tahunakhir_pdrb`
--
DROP TABLE IF EXISTS `view_tahunakhir_pdrb`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_tahunakhir_pdrb`  AS  select `tbl_pdrb`.`tahun` AS `tahun_akhir`,`tbl_pdrb`.`pdrb` AS `pdrb_akhir` from `tbl_pdrb` where (`tbl_pdrb`.`pdrb` is not null) order by `tbl_pdrb`.`tahun` desc limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_tahunakhir_pp`
--
DROP TABLE IF EXISTS `view_tahunakhir_pp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_tahunakhir_pp`  AS  select `tbl_pp`.`tahun` AS `tahun_akhir`,`tbl_pp`.`jumlah_penduduk` AS `jumlah_penduduk_akhir` from `tbl_pp` where (`tbl_pp`.`jumlah_penduduk` is not null) order by `tbl_pp`.`tahun` desc limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_tahunawal_pdrb`
--
DROP TABLE IF EXISTS `view_tahunawal_pdrb`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_tahunawal_pdrb`  AS  select `tbl_pdrb`.`tahun` AS `tahun_awal`,`tbl_pdrb`.`pdrb` AS `pdrb_awal` from `tbl_pdrb` where (`tbl_pdrb`.`pdrb` is not null) order by `tbl_pdrb`.`tahun` limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_tahunawal_pp`
--
DROP TABLE IF EXISTS `view_tahunawal_pp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_tahunawal_pp`  AS  select `tbl_pp`.`tahun` AS `tahun_awal`,`tbl_pp`.`jumlah_penduduk` AS `jumlah_penduduk_awal` from `tbl_pp` where (`tbl_pp`.`jumlah_penduduk` is not null) order by `tbl_pp`.`tahun` limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_talbot_sherman_ishiguro`
--
DROP TABLE IF EXISTS `view_talbot_sherman_ishiguro`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_talbot_sherman_ishiguro`  AS  select (((`view_total_kalaulang`.`sum_ixt` * `view_total_kalaulang`.`sum_i2`) - (`view_total_kalaulang`.`sum_i2xt` * `view_total_kalaulang`.`sum_i`)) / ((9 * `view_total_kalaulang`.`sum_i2`) - pow(`view_total_kalaulang`.`sum_i`,2))) AS `talbot_a`,(((`view_total_kalaulang`.`sum_i` * `view_total_kalaulang`.`sum_ixt`) - (9 * `view_total_kalaulang`.`sum_i2xt`)) / ((9 * `view_total_kalaulang`.`sum_i2`) - pow(`view_total_kalaulang`.`sum_i`,2))) AS `talbot_b`,(((`view_total_kalaulang`.`sum_logi` * `view_total_kalaulang`.`sum_logt2`) - (`view_total_kalaulang`.`sum_logt` * `view_total_kalaulang`.`sum_logti`)) / ((9 * `view_total_kalaulang`.`sum_logt2`) - pow(`view_total_kalaulang`.`sum_logt`,2))) AS `sherman_loga`,(((`view_total_kalaulang`.`sum_logi` * `view_total_kalaulang`.`sum_logt`) - (9 * `view_total_kalaulang`.`sum_logti`)) / ((9 * `view_total_kalaulang`.`sum_logt2`) - pow(`view_total_kalaulang`.`sum_logt`,2))) AS `sherman_a`,(((`view_total_kalaulang`.`sum_ixtset` * `view_total_kalaulang`.`sum_i2`) - (`view_total_kalaulang`.`sum_i2xtset` * `view_total_kalaulang`.`sum_i`)) / ((9 * `view_total_kalaulang`.`sum_i2`) - pow(`view_total_kalaulang`.`sum_i`,2))) AS `ishiguro_a`,(((`view_total_kalaulang`.`sum_i` * `view_total_kalaulang`.`sum_ixtset`) - (9 * `view_total_kalaulang`.`sum_i2xtset`)) / ((9 * `view_total_kalaulang`.`sum_i2`) - pow(`view_total_kalaulang`.`sum_i`,2))) AS `ishiguro_b` from `view_total_kalaulang` ;

-- --------------------------------------------------------

--
-- Structure for view `view_timbulan_total`
--
DROP TABLE IF EXISTS `view_timbulan_total`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_timbulan_total`  AS  select `a`.`tahun` AS `tahun`,`a`.`smph_terlayani` AS `timb_sampah_dom`,`b`.`smph_terlayani` AS `timb_sampah_nondom`,(`a`.`smph_terlayani` + `b`.`smph_terlayani`) AS `total` from (`view_domestik_5` `a` left join `tbl_coba` `b` on((`a`.`tahun` = `b`.`tahun`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_totaldrainase`
--
DROP TABLE IF EXISTS `view_totaldrainase`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_totaldrainase`  AS  select avg(`view_datadrainase`.`menit5jam`) AS `avg5`,avg(`view_datadrainase`.`menit15jam`) AS `avg15`,avg(`view_datadrainase`.`menit30jam`) AS `avg30`,avg(`view_datadrainase`.`menit45jam`) AS `avg45`,avg(`view_datadrainase`.`menit60jam`) AS `avg60`,avg(`view_datadrainase`.`menit120jam`) AS `avg120`,avg(`view_datadrainase`.`menit180jam`) AS `avg180`,avg(`view_datadrainase`.`menit360jam`) AS `avg360`,avg(`view_datadrainase`.`menit720jam`) AS `avg720`,avg(`view_datadrainase`.`menit1440jam`) AS `avg1440`,std(`view_datadrainase`.`menit5jam`) AS `stdev5`,std(`view_datadrainase`.`menit15jam`) AS `stdev15`,std(`view_datadrainase`.`menit30jam`) AS `stdev30`,std(`view_datadrainase`.`menit45jam`) AS `stdev45`,std(`view_datadrainase`.`menit60jam`) AS `stdev60`,std(`view_datadrainase`.`menit120jam`) AS `stdev120`,std(`view_datadrainase`.`menit180jam`) AS `stdev180`,std(`view_datadrainase`.`menit360jam`) AS `stdev360`,std(`view_datadrainase`.`menit720jam`) AS `stdev720`,std(`view_datadrainase`.`menit1440jam`) AS `stdev1440` from `view_datadrainase` ;

-- --------------------------------------------------------

--
-- Structure for view `view_total_kalaulang`
--
DROP TABLE IF EXISTS `view_total_kalaulang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_total_kalaulang`  AS  select sum(`view_drainase_kalaulang2`.`i`) AS `sum_i`,sum(`view_drainase_kalaulang2`.`ixt`) AS `sum_ixt`,sum(`view_drainase_kalaulang2`.`i2`) AS `sum_i2`,sum(`view_drainase_kalaulang2`.`i2xt`) AS `sum_i2xt`,sum(`view_drainase_kalaulang2`.`logt`) AS `sum_logt`,sum(`view_drainase_kalaulang2`.`logi`) AS `sum_logi`,sum(`view_drainase_kalaulang2`.`logti`) AS `sum_logti`,sum(`view_drainase_kalaulang2`.`logt2`) AS `sum_logt2`,sum(`view_drainase_kalaulang2`.`ixtset`) AS `sum_ixtset`,sum(`view_drainase_kalaulang2`.`i2xtset`) AS `sum_i2xtset` from `view_drainase_kalaulang2` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cobaa`
--
ALTER TABLE `cobaa`
  ADD PRIMARY KEY (`coba1`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- Indexes for table `tbl_analisafrekuensi`
--
ALTER TABLE `tbl_analisafrekuensi`
  ADD PRIMARY KEY (`id_analisafrekuensi`);

--
-- Indexes for table `tbl_coba`
--
ALTER TABLE `tbl_coba`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_datahujan`
--
ALTER TABLE `tbl_datahujan`
  ADD PRIMARY KEY (`id_drainase`);

--
-- Indexes for table `tbl_datatahunterakhir`
--
ALTER TABLE `tbl_datatahunterakhir`
  ADD PRIMARY KEY (`tahun_proyeksi`);

--
-- Indexes for table `tbl_drainase`
--
ALTER TABLE `tbl_drainase`
  ADD PRIMARY KEY (`id_drainase`);

--
-- Indexes for table `tbl_drainase2`
--
ALTER TABLE `tbl_drainase2`
  ADD PRIMARY KEY (`id_kalaulang`);

--
-- Indexes for table `tbl_kab`
--
ALTER TABLE `tbl_kab`
  ADD PRIMARY KEY (`id_kab`);

--
-- Indexes for table `tbl_kriteriapenyisih`
--
ALTER TABLE `tbl_kriteriapenyisih`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `tbl_nondomestik`
--
ALTER TABLE `tbl_nondomestik`
  ADD PRIMARY KEY (`id_proyeksi`);

--
-- Indexes for table `tbl_pdrb`
--
ALTER TABLE `tbl_pdrb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pdrb_proyeksi`
--
ALTER TABLE `tbl_pdrb_proyeksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pp`
--
ALTER TABLE `tbl_pp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pp_proyeksi`
--
ALTER TABLE `tbl_pp_proyeksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_timbulan_domestik`
--
ALTER TABLE `tbl_timbulan_domestik`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_analisafrekuensi`
--
ALTER TABLE `tbl_analisafrekuensi`
  MODIFY `id_analisafrekuensi` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_coba`
--
ALTER TABLE `tbl_coba`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_datahujan`
--
ALTER TABLE `tbl_datahujan`
  MODIFY `id_drainase` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_drainase`
--
ALTER TABLE `tbl_drainase`
  MODIFY `id_drainase` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_kab`
--
ALTER TABLE `tbl_kab`
  MODIFY `id_kab` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_kriteriapenyisih`
--
ALTER TABLE `tbl_kriteriapenyisih`
  MODIFY `id_kriteria` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_nondomestik`
--
ALTER TABLE `tbl_nondomestik`
  MODIFY `id_proyeksi` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pdrb`
--
ALTER TABLE `tbl_pdrb`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_pdrb_proyeksi`
--
ALTER TABLE `tbl_pdrb_proyeksi`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_pp`
--
ALTER TABLE `tbl_pp`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_pp_proyeksi`
--
ALTER TABLE `tbl_pp_proyeksi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_timbulan_domestik`
--
ALTER TABLE `tbl_timbulan_domestik`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
